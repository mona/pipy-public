import numpy as np
import pandas as pd

np.seterr(divide='ignore', invalid='ignore')


def is_ambiguous(token, annotator, tagset, tags):
    """Checks if a token has an ambiguous annotation.

    Args:
        token (`Token`): The token.
        annotator (str): Name of the annotator.
        tagset (str): Name of the tagset.
        tags (list of str): Relevant tags.
    
    Returns:
        boolean: True iff the token has an annotation that is marked as "ambig".

    """
    for annotation in token._.annotations[annotator].get_annotations(tags=tags, tagset=tagset):
        if "Sicherheit" in annotation.property_values:
            for item in annotation.property_values["Sicherheit"]:
                if "ambig" in item:
                    return True
    return False


def last_annotated_index(doc, tagset, annotators=None):
    """Determine the last annotated sentence in a document and return the index of the last token in that sentence.

    Args:
        doc (`Doc`): The document.
        tagset (str): The tagset name.
        annotators (list of str): Names of the annotator to include.
    
    Returns:
        int: The index of the last token in the last sentence annotated with the given tagset.
            If no sentence is annotated, -1 is returned.
    
    """
    index = -1
    if annotators is None:
        annotators = doc._.annotations.keys()
    for annotator in annotators:
        annotations = doc._.annotations[annotator].get_annotations(tagset=tagset)
        if len(annotations) > 0:
            index = max(index, doc[annotations[-1].tokens[-1].i].sent.end-1)
    return index


def get_single_tag(tags):
    """Return a column/row name for the confusion matrix.

    Args:
        tags (set of str): Set of tags (for a token).
    
    Returns:
        str: "untagged" if the set of tags is empty;
            the name of the tag if there is only one tag in the set;
            "multiple" if there are more than one tags in the set.
    
    """
    if len(tags) == 0:
        tag = "untagged"
    elif len(tags) == 1:
        tag = list(tags)[0]
    else:
        tag = "multiple"
    return tag


def initial_gold_pred(tags, supertag):
    """Initialize counts and confusion matrix.

    Args:
        tags (list of str): Relevant tags.
        supertag (str): For a binary evaluation, name of the supertag.
    
    Returns:
        dict of str:(dict of str:int): Countings.
        dict of str:(dict of str:int): Confusion matrix.

    """
    if supertag is None:
        counts = {tag : {"tp" : 0, "tn" : 0, "fp" : 0, "fn" : 0} for tag in tags+["micro"]}
        confusions = {tag : {tag : 0 for tag in tags+["untagged", "multiple"]} for tag in tags+["untagged", "multiple"]}
    else:
        counts = {tag : {"tp" : 0, "tn" : 0, "fp" : 0, "fn" : 0} for tag in [supertag, "micro"]}
        confusions = {tag : {tag : 0 for tag in [supertag, "untagged"]} for tag in [supertag, "untagged"]}
    return counts, confusions


def gold_pred(obj, passages, annotator, tagset, tags, supertag):
    """Return gold tags and predicted tags for an evaluation sample.

    Args:
        obj (`Token` or `Span`): Token or clause.
        passages (list of `Passage`): Tagged passages for of the sample.
        annotator (str): Name of the annotator to compare the predicted tags with.
        tagset (str): Name of the tagset.
        tags (list of str): Relevant tags.
        supertag (str): For a binary evaluation, name of the supertag.
    
    Returns:
        set of str: Gold tags.
        set of str: Predicted tags.
    
    """
    pred = set.union(*[passage.tags for passage in passages]) if len(passages) > 0 else set()
    gold = obj._.annotations[annotator].get_tags(tagset=tagset)
    pred.intersection_update(set(tags))
    gold.intersection_update(set(tags))
    if supertag is not None:
        pred = set([supertag] if len(pred) > 0 else [])
        gold = set([supertag] if len(gold) > 0 else [])
    return gold, pred


def update_gold_pred(counts, confusions, tags, supertag, gold, pred):
    """Update counts and confusion matrix.

    Args:
        counts (dict of str:(dict of str:int)): Countings.
        confusions (dict of str:(dict of str:int)): Confusion matrix.
        tags (list of str): Relevant tags.
        supertag (str): For a binary evaluation, name of the supertag.
        gold (set of str): Gold tags.
        pred (set of str): Predicted tags.
    
    Returns:
        dict of str:(dict of str:int): Countings.
        dict of str:(dict of str:int): Confusion matrix.

    """
    for tag in (tags if supertag is None else [supertag]):
        if tag in gold:
            if tag in pred:
                counts[tag]["tp"] += 1
                counts["micro"]["tp"] += 1
            else:
                counts[tag]["fn"] += 1
                counts["micro"]["fn"] += 1
        else:
            if tag in pred:
                counts[tag]["fp"] += 1
                counts["micro"]["fp"] += 1
            else:
                counts[tag]["tn"] += 1
                counts["micro"]["tn"] += 1
    p = get_single_tag(pred)
    g = get_single_tag(gold)
    confusions[g][p] += 1
    return counts, confusions


def convert_annotators(annotator):
    """Returns annotators as a list.

    Args:
        annotator (obj): Annotator (str) or list of annotators (list of str).
    
    Returns:
        list of str: List of annotators.
    
    """
    if type(annotator) == list:
        return annotator
    return [annotator]


def compare_gold_pred_tokens(docs, attribute, annotator, tagset, tags, supertag=None, end_of_annotations=None, exclude_ambiguous=False):
    """Count token-level true positives, true negatives, false positives, false negatives and confusions for every tag.

    Args:
        docs (list of `Doc`): The annotated and tagged documents.
        attribute (str): Name of the clause-level custom extension which stores the predicted tags in a list of `Passage` objects.
        annotator (obj): Name of the annotator to compare the predicted tags with.
            If a list of names is given, then the first name which does not raise a KeyError is taken.
        tagset (str): Name of the tagset.
        tags (list of str): Relevant tags. Tags not in this list are ignored for the countings.
        supertag (str): For a binary evaluation, name of the supertag.
            All tags in `tags` are replaced by the supertag.
        end_of_annotations (list of obj): For each document, the index of the last token in the last sentence annotated with the given tagset.
            If None, this index is detected automatically.
        exclude_ambiguous (boolean): Iff True, exclude the tokens for which there is at least one annotation
            with the Property "Sicherheit" and a value containing the substring "ambig".
    
    Returns:
        dict of str:(dict of str:int): Dictionary with one entry for every tag plus one entry for the key "micro".
            Each entry is a dictionary of "tp" (true positives), "tn" (true negatives), "fp" (false positives), "fn" (false negatives)
                for the respective tag. The entry "micro" contains the corresponding sums for all tags.
        dict of str:(dict of str:int): Two-dimensional dictionary with confusion counts.
            The first key is the gold tag and the second key is the predicted tag.
            The keys "untagged" and "multiple" are used for tokens with no or more than one tags, respectively.
    
    """
    annotators = convert_annotators(annotator)
    if end_of_annotations is None:
        end_of_annotations = [None] * len(docs)
    counts, confusions = initial_gold_pred(tags, supertag)
    for doc, end_of_annotation in zip(docs, end_of_annotations):
        for annotator in annotators:
            try:
                if end_of_annotation is None:
                    end_of_annotation = last_annotated_index(doc, tagset, [annotator])
                for token in doc[:end_of_annotation+1]:
                    if not (token.is_punct or token.is_space):
                        if not (exclude_ambiguous and is_ambiguous(token, annotator, tagset, tags)):
                            passages = getattr(token._.clause._, attribute)
                            gold, pred = gold_pred(token, passages, annotator, tagset, tags, supertag)
                            counts, confusions = update_gold_pred(counts, confusions, tags, supertag, gold, pred)
                break
            except KeyError:
                continue
    return counts, confusions


def compare_gold_pred_clauses(docs, attribute, annotator, tagset, tags, supertag=None, end_of_annotations=None, exclude_ambiguous=False):
    """Count clause-level true positives, true negatives, false positives, false negatives and confusions for every tag.

    Args:
        doc (list of `Doc`): The annotated and tagged documents.
        attribute (str): Name of the clause-level custom extension which stores the predicted tags in a list of `Passage` objects.
        annotator (obj): Name of the annotator to compare the predicted tags with.
            If a list of names is given, then the first name which does not raise a KeyError is taken.
        tagset (str): Name of the tagset.
        tags (list of str): Relevant tags. Tags not in this list are ignored for the countings.
        supertag (str): For a binary evaluation, name of the supertag.
            All tags in `tags` are replaced by the supertag.
        end_of_annotations (list of obj): For each document, the index of the last token in the last sentence annotated with the given tagset.
            If None, this index is detected automatically.
        exclude_ambiguous (boolean): Iff True, exclude the clauses for which there is at least one annotation
            with the Property "Sicherheit" and a value containing the substring "ambig".
    
    Returns:
        dict of str:(dict of str:int): Dictionary with one entry for every tag plus one entry for the key "micro".
            Each entry is a dictionary of "tp" (true positives), "tn" (true negatives), "fp" (false positives), "fn" (false negatives)
                for the respective tag. The entry "micro" contains the corresponding sums for all tags.
        dict of str:(dict of str:int): Two-dimensional dictionary with confusion counts.
            The first key is the gold tag and the second key is the predicted tag.
            The keys "untagged" and "multiple" are used for tokens with no or more than one tags, respectively.
    
    """
    annotators = convert_annotators(annotator)
    if end_of_annotations is None:
        end_of_annotations = [None] * len(docs)
    counts, confusions = initial_gold_pred(tags, supertag)
    for doc, end_of_annotation in zip(docs, end_of_annotations):
        for annotator in annotators:
            try:
                if end_of_annotation is None:
                    end_of_annotation = last_annotated_index(doc, tagset, [annotator])
                for clause in doc._.clauses:
                    if clause.end-1 <= end_of_annotation:
                        if not (exclude_ambiguous and is_ambiguous(clause, annotator, tagset, tags)):
                            passages = getattr(clause._, attribute)
                            gold, pred = gold_pred(clause, passages, annotator, tagset, tags, supertag)
                            counts, confusions = update_gold_pred(counts, confusions, tags, supertag, gold, pred)
                break
            except KeyError:
                continue
    return counts, confusions


def confusion_matrix(confusions):
    """Convert the confusion matrix.

    Args:
        confusions (dict of str:(dict of str:int)): Second output of `compare_gold_pred`.
    
    Returns:
        `DataFrame`: Confusion matrix as pandas table.
            Gold tags are rows, predicted tags are columns.
    
    """
    return pd.DataFrame(confusions).transpose()


def tagwise_accuracies(counts):
    """Calculate the accuracy for every tag as well as micro-average and macro-average.

    Args:
        counts (dict of str:(dict of str:int)): First output of `compare_gold_pred`.

    Returns:
        dict of str:float: Dictionary with one entry for every tag plus two entries for the keys "micro" and "macro".
            Each entry is the accuracy for the corresponding tag.
            The entries "micro" and "macro" contain the micro-average and macro-average for all tags, respectively.
            Division by 0 is handled as described here: https://github.com/dice-group/gerbil/wiki/Precision,-Recall-and-F1-measure

    """
    accuracies = {}
    for tag in counts:
        if counts[tag]["tp"]+counts[tag]["tn"]+counts[tag]["fp"]+counts[tag]["fn"] == 0:
            accuracies[tag] = np.float64(1.0)
        else:
            accuracies[tag] = np.float64(1.0)*(counts[tag]["tp"]+counts[tag]["tn"])/(counts[tag]["tp"]+counts[tag]["tn"]+counts[tag]["fp"]+counts[tag]["fn"])
    accuracies["macro"] = macro_average(accuracies)
    return accuracies


def tagwise_precisions(counts):
    """Calculate the precision for every tag as well as micro-average and macro-average.

    Args:
        counts (dict of str:(dict of str:int)): First output of `compare_gold_pred`.

    Returns:
        dict of str:float: Dictionary with one entry for every tag plus two entries for the keys "micro" and "macro".
            Each entry is the precision for the corresponding tag.
            The entries "micro" and "macro" contain the micro-average and macro-average for all tags, respectively.
            Division by 0 is handled as described here: https://github.com/dice-group/gerbil/wiki/Precision,-Recall-and-F1-measure

    """
    precisions = {}
    for tag in counts:
        if counts[tag]["tp"] == 0:
            if counts[tag]["fp"]+counts[tag]["fn"] == 0:
                precisions[tag] = np.float64(1.0)
            else:
                precisions[tag] = np.float64(0.0)
        else:
            precisions[tag] = np.float64(1.0)*counts[tag]["tp"]/(counts[tag]["tp"]+counts[tag]["fp"])
    precisions["macro"] = macro_average(precisions)
    return precisions


def tagwise_recalls(counts):
    """Calculate the recall for every tag as well as micro-average and macro-average.

    Args:
        counts (dict of str:(dict of str:int)): First output of `compare_gold_pred`.

    Returns:
        dict of str:float: Dictionary with one entry for every tag plus two entries for the keys "micro" and "macro".
            Each entry is the recall for the corresponding tag.
            The entries "micro" and "macro" contain the micro-average and macro-average for all tags, respectively.
            Division by 0 is handled as described here: https://github.com/dice-group/gerbil/wiki/Precision,-Recall-and-F1-measure

    """
    recalls = {}
    for tag in counts:
        if counts[tag]["tp"] == 0:
            if counts[tag]["fp"]+counts[tag]["fn"] == 0:
                recalls[tag] = np.float64(1.0)
            else:
                recalls[tag] = np.float64(0.0)
        else:
            recalls[tag] = np.float64(1.0)*counts[tag]["tp"]/(counts[tag]["tp"]+counts[tag]["fn"])
    recalls["macro"] = macro_average(recalls)
    return recalls


def tagwise_fscores(counts):
    """Calculate the f-score for every tag as well as micro-average and macro-average.

    Args:
        counts (dict of str:(dict of str:int)): First output of `compare_gold_pred`.

    Returns:
        dict of str:float: Dictionary with one entry for every tag plus two entries for the keys "micro" and "macro".
            Each entry is the f-score for the corresponding tag.
            The entries "micro" and "macro" contain the micro-average and macro-average for all tags, respectively.
            Division by 0 is handled as described here: https://github.com/dice-group/gerbil/wiki/Precision,-Recall-and-F1-measure

    """
    precisions = tagwise_precisions(counts)
    recalls = tagwise_recalls(counts)
    fscores = {}
    for tag in counts:
        if counts[tag]["tp"] == 0:
            if counts[tag]["fp"]+counts[tag]["fn"] == 0:
                fscores[tag] = np.float64(1.0)
            else:
                fscores[tag] = np.float64(0.0)
        else:
            fscores[tag] = np.float64(2.0)*precisions[tag]*recalls[tag]/(precisions[tag]+recalls[tag])
    fscores["macro"] = macro_average(fscores)
    return fscores


def macro_average(tagwise_values):
    """Calculate the macro-average for tagwise values (such as f-score).

    Args:
       tagwise_values (dict of str:(dict of str:int)): Values in the output format of the `tagwise_` functions.
    
    Returns:
        float: The unweighted average of the values for every tag.
    
    """
    return np.mean([tagwise_values[tag] for tag in tagwise_values if tag not in ["micro", "macro"]])


if __name__ == "__main__":
    # TESTS
    
    import spacy
    from spacy.tokens import Doc, Span, Token
    
    import os
    import sys
    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
    from pipeline.annotation import Annotation, AnnotationList
    from pipeline.passage import Passage
    
    # test tokens with predictions and gold tags
    text = ["Dies", "ist", "ein", "Testsatz", ",",   "der", "eine", "Goldannotation", "hat",      "und",      "von",      "einem",    "Tagger",   "getaggt",  "wurde",    "."       ]
    pred = [[],     [],    [],    [],         [],    [],    ["A"],  ["A"],            ["A"],      ["A"],      ["A"],      ["A", "B"], ["A", "B"], ["A", "B"], ["A", "B"], ["A", "B"]]
    gold = [[],     [],    [],    [],         ["C"], ["C"], ["C"],  ["C"],            ["A", "B"], ["A", "B"], ["A", "B"], ["A", "B"], ["B", "C"], ["B", "C"], ["B", "C"], ["B", "C"]]
    
    # manual counts of true positives, true negatives, false positives, false negatives
    counts_m = {
        "A" : ["tn","tn",  "tn",  "tn",       "",    "tn",  "fp",   "fp",             "tp",       "tp",       "tp",       "tp",       "fp",       "fp",       "fp",       ""        ],
        "B" : ["tn","tn",  "tn",  "tn",       "",    "tn",  "tn",   "tn",             "fn",       "fn",       "fn",       "tp",       "tp",       "tp",       "tp",       ""        ],
        "C" : ["tn","tn",  "tn",  "tn",       "",    "fn",  "fn",   "fn",             "tn",       "tn",       "tn",       "tn",       "fn",       "fn",       "fn",       ""        ]
    }
    
    # component that adds the predictions to the tokens
    def add_pred_tags(doc):
        Token.set_extension("clause", default=None)
        Span.set_extension("tokens", default=None)
        Span.set_extension("predictions", default=None)
        for i, token in enumerate(doc):
            clause = doc[i:i+1]
            clause._.tokens = [token]
            token._.clause = clause
            token._.clause._.predictions = []
            if len(pred[i]) > 0:
                passage = Passage([clause], set(pred[i]))
                token._.clause._.predictions.append(passage)
        return doc
    
    # component that adds the gold tags to the tokens
    def add_gold_tags(doc):
        Doc.set_extension("annotations", default=None)
        Token.set_extension("annotations", default=None)
        doc._.annotations = {"GGG" : AnnotationList()}
        for i, token in enumerate(doc):
            token._.annotations = {"GGG" : AnnotationList()}
            for tag in gold[i]:
                annotation = Annotation(tag, "", {}, doc[i:i+1], None, None, None, None)
                token._.annotations["GGG"].append(annotation)
                doc._.annotations["GGG"].append(annotation)
        return doc

    # toy pipeline
    sp = spacy.load("de_core_news_lg")
    sp.add_pipe(add_pred_tags, name="test_tagger")
    sp.add_pipe(add_gold_tags, name="annotation")
    sp.tokenizer = sp.tokenizer.tokens_from_list
    
    # create document
    doc = sp(text)
    
    # count stuff
    counts_s, confusions = compare_gold_pred_tokens([doc], "predictions", "GGG", "", ["A", "B", "C"])

    # test and print counts
    for x in ["tp", "tn", "fp", "fn"]:
        for tag in ["A", "B", "C"]:
            assert counts_s[tag][x] == counts_m[tag].count(x)
            print(tag, x, counts_s[tag][x])
        tag = "micro"
        assert counts_s[tag][x] == sum([counts_m[t].count(x) for t in ["A", "B", "C"]])
        print(tag, x, counts_s[tag][x])
    
    # test accuracies
    accuracies = tagwise_accuracies(counts_s)
    np.testing.assert_equal(accuracies["A"], np.float64(1.0)*9/14)
    np.testing.assert_equal(accuracies["B"], np.float64(1.0)*11/14)
    np.testing.assert_equal(accuracies["C"], np.float64(1.0)*8/14)
    np.testing.assert_equal(accuracies["micro"], np.float64(1.0)*28/(3*14))
    np.testing.assert_equal(accuracies["macro"], np.nanmean([accuracies["A"], accuracies["B"], accuracies["C"]]))

    # test precisions
    precisions = tagwise_precisions(counts_s)
    np.testing.assert_equal(precisions["A"], np.float64(1.0)*4/(4+5))
    np.testing.assert_equal(precisions["B"], np.float64(1.0)*4/(4+0))
    np.testing.assert_equal(precisions["C"], np.float64(1.0)*0)
    np.testing.assert_equal(precisions["micro"], np.float64(1.0)*8/(8+5))
    np.testing.assert_equal(precisions["macro"], np.nanmean([precisions["A"], precisions["B"], precisions["C"]]))

    # test recalls
    recalls = tagwise_recalls(counts_s)
    np.testing.assert_equal(recalls["A"], np.float64(1.0)*4/(4+0))
    np.testing.assert_equal(recalls["B"], np.float64(1.0)*4/(4+3))
    np.testing.assert_equal(recalls["C"], np.float64(1.0)*0/(0+6))
    np.testing.assert_equal(recalls["micro"], np.float64(1.0)*8/(8+9))
    np.testing.assert_equal(recalls["macro"], np.nanmean([recalls["A"], recalls["B"], recalls["C"]]))

    # test f-scores
    fscores = tagwise_fscores(counts_s)
    np.testing.assert_equal(fscores["A"], np.float64(2.0)*precisions["A"]*recalls["A"]/(precisions["A"]+recalls["A"]))
    np.testing.assert_equal(fscores["B"], np.float64(2.0)*precisions["B"]*recalls["B"]/(precisions["B"]+recalls["B"]))
    np.testing.assert_equal(fscores["C"], np.float64(2.0)*0)
    np.testing.assert_equal(fscores["micro"], np.float64(2.0)*precisions["micro"]*recalls["micro"]/(precisions["micro"]+recalls["micro"]))
    np.testing.assert_equal(fscores["macro"], np.nanmean([fscores["A"], fscores["B"], fscores["C"]]))