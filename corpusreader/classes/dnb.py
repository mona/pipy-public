import requests
from lxml import etree
#from bs4 import BeautifulSoup as soup

ns = {  
    "dc": "http://purl.org/dc/elements/1.1/",
    "dcterms": "http://purl.org/dc/terms/",
    "dnb": "http://d-nb.de/standards/dnbterms",
    "marcRole": "http://id.loc.gov/vocabulary/relators/",
    "marcxml": "http://www.loc.gov/MARC21/slim",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "sru": "http://www.loc.gov/zing/srw/",
    "xsi": "http://www.w3.org/2001/XMLSchema-instance"
    }

record_schema_to_etree_attribute = {
    "MARC21-xml": "marcxml_etree",
    "RDFxml": "rdf_etree",
    "oai_dc": "oai_etree"
}

record_schema_to_string_attribute = {
    "MARC21-xml": "marcxml_string",
    "RDFxml": "rdf_string",
    "oai_dc": "oai_string"
}

record_schema__individual_attributes = {
    "MARC21-xml": ["marcxml_etree", "marcxml_string", "marcxml_language_text", "marcxml_language_original_if_translation", "marcxml_title_a"],
    "RDFxml": ["rdf_etree", "rdf_string", "rdf_translators", "rdf_language_text_vocabulary_uri", "rdf_language_text", "rdf_title"],
    "oai_dc": ["oai_etree", "oai_string"]
}



class DnbList(list):
    """A class that stores a list of Dnb objects.
    """  
    def __init__(self, dnb_list=list()):
#    def __init__(self, record_schema, dnb_list=list()):

        """__init__ method of the class `DnbList`.

        Args:
            dnb_list (list of Dnb objects): a list of Dnb objects.
        """
        super(DnbList, self).__init__(dnb_list)

        # self.record_schemas = list()
        # self.record_schemas.append(record_schema)
        #self.type = type


    def dnb_sru_multi_record_schemas(query, database, record_schemas):
        """SRU API method.
            Performs a SRU request to the DNB database for each record_schema in record_schmemas 
            and returns DnbList with Dnb objects that hold metadata from corresponding record schemas.

        Args:
            query (str): query in CQL format (Contextual Query Language), https://www.dnb.de/DE/Service/Hilfe/Katalog/kataloghilfeExpertensuche_node.html;
                        typical searches that are applied here:
                        - "auRef={dnb_id}": search for all bibliographic entries for a certain author; dnb_id must be a GND-id
                        - "idn"={dnb_id}": search for specific identification number of dnb entry; includes all bibliographic entries and GND-ids for persons (normdata for persons)
                        - "nid"={dnb_id}: search for specific identification number of normdata (GND data); only accepts GND-ids (normdata)
            database (str): database from DNB, possible entries: 
                            'authorities', 'dnb', 'dnb.dma'
            record_schemas (list): list of record_schema (str), possible format values: 
                                'MARC21-xml', 'oai_dc', 'RDFxml'
        
        Returns:
            `DnbList`: list of Dnb objects
        """

        list_of_dnb_list = [DnbList.dnb_sru(query=query, database=database, record_schema=record_schema) for record_schema in record_schemas]

        for record_items in zip(*list_of_dnb_list):
            if all(record_item.dnb_id == record_items[0].dnb_id for record_item in record_items[1:]):
                for record_item in record_items[1:]:
                    record_schema_of_current_record_item = record_item.record_schemas[0]

                    record_items[0].record_schemas.append(record_schema_of_current_record_item)

                    for attribute_name in record_schema__individual_attributes[record_schema_of_current_record_item]:
                        setattr(
                            record_items[0], 
                            attribute_name, 
                            getattr(record_item, attribute_name)
                        )
                    
                
            else:
                print("The request returned unequal responses for different record schemas!")
                exit()
                
        return list_of_dnb_list[0]

    def dnb_sru(query, database, record_schema):
        """SRU API method. 
            Based on a function from German National Library (DNB), source: https://github.com/deutsche-nationalbibliothek/dnblab/blob/main/DNB_SRU_Tutorial.ipynb,
            Documented and updated with record_schema selection, database selection, and lxml output.
            
        Args:
            query (str): query in CQL format (Contextual Query Language), https://www.dnb.de/DE/Service/Hilfe/Katalog/kataloghilfeExpertensuche_node.html;
                        typical searches that are applied here:
                        - "auRef={dnb_id}": search for all bibliographic entries for a certain author; dnb_id must be a GND-id
                        - "idn"={dnb_id}": search for specific identification number of dnb entry; includes all bibliographic entries and GND-ids for persons (normdata for persons)
                        - "nid"={dnb_id}: search for specific identification number of normdata (GND data); only accepts GND-ids (normdata)
            database (str): database from DNB, possible entries: 
                            'authorities', 'dnb', 'dnb.dma'
            record_schema (str): schema of the requested record, possible formats: 
                                'MARC21-xml', 'oai_dc', 'RDFxml'

        Returns:
            `DnbList`: list of Dnb objects
        """

        
        if database in ["authorities", "dnb", "dnb.dma"]:
            base_url = "https://services.dnb.de/sru/%s" %database
        else:
            print("Please use 'dnb', 'authorities', or 'dnb.dma' as database!")
            exit()


        params = {'recordSchema' : record_schema,
            'operation': 'searchRetrieve',
            'version': '1.1',
            'maximumRecords': '100',
            'query': query
            }
        r = requests.get(base_url, params=params)
        
        root = etree.fromstring(r.content)
    
        records = DnbList.return_records(database=database, record_schema=record_schema, root=root, ns=ns)
    
        if len(records) < 100:
            
            return records
        
        else:
            
            num_results = 100
            i = 101
            while num_results == 100:
                
                params.update({'startRecord': i})
                r = requests.get(base_url, params=params)

                root = etree.fromstring(r.content)
                new_records = DnbList.return_records(database=database, record_schema=record_schema, root=root, ns=ns)
                    
                records+=new_records
                i+=100
                num_results = len(new_records)
                
            return records

    def return_records(database, record_schema, root, ns):
        """Helper function that extracts single records from request output and returns them
            as Dnb objects in Dnb List.

                Args:
                    database (str): database from DNB, possible entries: 'authorities', 'dnb', 'dnb.dma'
                    record_schema (str): schema of the requested record, possible formats: 
                                'MARC21-xml', 'oai_dc', 'RDFxml'
                    root (lxml.etree._Element): root element from request output as lxml.etree
                    ns (dict): namespaces used in request output
                
                Returns:
                    `DnbList`: list of Dnb objects
                """
        if record_schema == "MARC21-xml":
            xpath = ".//sru:recordData/marcxml:record"
        elif record_schema == "RDFxml":
            xpath = ".//sru:recordData/rdf:RDF"
        elif record_schema == "oai_dc":
            xpath = ".//sru:recordData/*"
        else:
            print("Please use 'MARC21-xml', 'RDFxml', or 'oai_dc' record_schema!")
            exit()

        return_list = [Dnb(database=database, record_schema=record_schema, record_etree=record_etree) for record_etree in root.xpath(xpath, namespaces=ns)]

        #return DnbList(dnb_list=return_list, record_schema=record_schema)
        return DnbList(dnb_list=return_list)



class Dnb():
    """A class that stores dnb records.
    """   
    def __init__(self, database, record_schema, dnb_id=None, record_etree=None):
        """__init__ method of the class `Dnb`.

        Args:
            database (str): database from dnb, possible entries: 'authorities', 'dnb', 'dnb.dma'
            record_schema (str): schema of the requested record, possible formats: 
                                'MARC21-xml', 'oai_dc', 'RDFxml'
            dnb_id (str): dnb id of either a bibliographic record or an authority record (GND id; normdata);
                          optional, see a)
            record_etree (`lxml.etree`): lxml.etree for the dnb record;
                                                optional, see b)

            There two ways to initialise a Dnb object:
            a) Pass database, record_schema and a dnb_id to the class method.
                This performs a request to the Dnb API and fills attributes with this 
                information.
            b) Pass database, record_schema and the record_etree to the class method.
                This option can be used if a request (e.g. for more than one record
                as in DnbList) is performered beforehand, which is why the record_etree
                is already produced.

        General attributes:
            dnb_id (str) dnb id of either a bibliographic record or an authority record (GND id; normdata)
            dnb_id_url (str): 
            database (str): database from DNB, possible entries: 
                            'authorities', 'dnb', 'dnb.dma'
            type (str): type of record either "authority" (from database "authorities") or "bibliographic" (from databases "dnb" or "dnb.dma")

        Specific attributes for MARC21-xml:
            marcxml_etree (`lxml.etree`): lxml element tree of the MARC21-xml record
            marcxml_string (str): string representation of the MARC21-xml record

            marcxml_title_a (str): main title
            marcxml_language_text (str): language of the bibliographic record (ISO-639-2-Code)
            marcxml_language_original_if_translation (str): language of the original (ISO-639-2-Code) if bibliographic record is translation

        Specific attributes for RDFxml:
            rdf_etree (`lxml.etree`): lxml element tree of the RDFxml record
            rdf_string (str): string representation of the RDFxml record

            rdf_language_text (str): language of the bibliographic record (ISO-639-2-Code)
            rdf_language_text_vocabulary_uri (str): URI of the language of the bibliographic record
            rdf_translators (list): list of dnb_id's (GND id) for translators of the bibliographic record

        Specific attributes for oai_dc:
            oai_etree (`lxml.etree`): lxml element tree of the oai_dc record
            oai_string (str): lxml element tree of the oai_dc record

        """
        self.dnb_id = None
        self.dnb_id_url = None
        self.database = database
        if database=="authorities":
            self.type = "authority"
        elif database in ['dnb','dnb.dma']:
            self.type = "bibliographic"
        self.record_schemas = list()

        #set individual attributes for 'MARC21-xml', 'oai_dc', and 'RDFxml':
        for record_schema_ in record_schema__individual_attributes:
            for individual_attribute in record_schema__individual_attributes[record_schema_]:
                setattr(self,
                        individual_attribute,
                        None
                )

        # if no dnb_id is given a already requested record_etree is required:
        if not dnb_id:
            self.fill(record_schema=record_schema, record_etree=record_etree)
        # if dnb_id is given a sru request id performed to aquire the record_etree:
        elif dnb_id:
            record_etree = Dnb.request_record_etree(dnb_id=dnb_id, database=database, record_schema=record_schema)
            self.fill(record_etree=record_etree, record_schema=record_schema)
            
        self.record_schemas.append(record_schema)
    

    def request_record_etree(dnb_id, record_schema, database):
        """Function that performs one sru request with one record_schema and returns the `lxml.etree`-Element.
        
        Args:
            dnb_id (str): dnb id of either a bibliographic record or an authority record (GND id; normdata)
            record_schema (str): schema of the requested record, possible formats: 
                    'MARC21-xml', 'oai_dc', 'RDFxml'
            database (str): database from DNB, possible entries: 
                            'authorities', 'dnb', 'dnb.dma'

        Returns:
            `lxml.etree`: lxml element tree for the requested record schema
        """

        record_etree = getattr(DnbList.dnb_sru(query="idn=%s"%dnb_id, database=database, record_schema=record_schema)[0], record_schema_to_etree_attribute[record_schema])
        return record_etree

    def fill(self, record_etree, record_schema):
        """Function that filles predefined metadata attributes of corresponding class instance.
        
        Args:
            record_etree (lxml.etree): lxml.etree for the corresponding record
            record_schema (str): schema of the requested record, possible formats: 
                                'MARC21-xml', 'oai_dc', 'RDFxml'        
        """

        if record_schema=="MARC21-xml":
            # TODO
            # Feld 337: Medium
            # Feld 926: Sachgruppen
            # - code="o" 93 -> "Themen", in code="x" eg.:
            #   - Belletristik: Themen, Stoffe, Motive: Seelenleben
            #   - Belletristik: Themen, Stoffe, Motive: Politik
            #   - Belletristik: Themen, Stoffe, Motive: Soziales

            self.marcxml_etree = record_etree
            self.marcxml_string = etree.tostring(self.marcxml_etree).decode('UTF-8')
            self.dnb_id = self.marcxml_etree.xpath("./marcxml:controlfield[@tag='001']", namespaces=ns)[0].text

            # tag 245 / code a: holds main title
            self.marcxml_title_a = self.marcxml_etree.xpath("./marcxml:datafield[@tag='245']/marcxml:subfield[@code='a']", namespaces=ns)[0].text
            
            # tag 041 / code a: holds language of the bibliographic entry
            if len(self.marcxml_etree.xpath("./marcxml:datafield[@tag='041']/marcxml:subfield[@code='a']", namespaces=ns)) != 0:
                self.marcxml_language_text = self.marcxml_etree.xpath("./marcxml:datafield[@tag='041']/marcxml:subfield[@code='a']", namespaces=ns)[0].text
            # tag 041 / code h: holds language of the original if bibliographic entry is a translation
            if len(self.marcxml_etree.xpath("./marcxml:datafield[@tag='041']/marcxml:subfield[@code='h']", namespaces=ns)) != 0:
                self.marcxml_language_original_if_translation = self.marcxml_etree.xpath("./marcxml:datafield[@tag='041']/marcxml:subfield[@code='h']", namespaces=ns)[0].text
            
        elif record_schema=="RDFxml":
            # TODO
            # Gattungsbegriffe aus DNB-Katalog 
            # Bsp -> https://portal.dnb.de/opacPresentation?cqlMode=true&reset=true&referrerPosition=0&referrerResultId=idn%3D1230761209%26any&query=idn%3D1010789759
            # Bsp Text -> https://portal.dnb.de/opac.htm?method=simpleSearch&cqlMode=true&query=idn%3D1230761209
            # <schema:genre>
                # skos notation mit Sachgruppe, eg 110: skos:notation rdf:datatype="https://d-nb.info/standards/elementset/dnb#literary-genre-notation">110</skos:notation>
                # skos type, eg Erzählende Literatur: <skos:prefLabel rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Erza&#776;hlende Literatur</skos:prefLabel>

            self.rdf_etree = record_etree
            self.rdf_string = etree.tostring(self.rdf_etree).decode('UTF-8')
            self.dnb_id = self.rdf_etree.xpath('./rdf:Description/@rdf:about', namespaces=ns)[0].split('/')[-1]

            # language of text and correponding dcterms:language vocabulary
            if len(self.rdf_etree.xpath('./rdf:Description/dcterms:language', namespaces=ns)) != 0:
                self.rdf_language_text_vocabulary_uri = self.rdf_etree.xpath('./rdf:Description/dcterms:language/@rdf:resource', namespaces=ns)[0]
                self.rdf_language_text = self.rdf_language_text_vocabulary_uri.split('/')[-1]

            # list of dnb_id's (GND id) for translators
            list_of_translator_dnb_ids = [translator_gnd_url.split('/')[-1] for translator_gnd_url in self.rdf_etree.xpath('./rdf:Description/marcRole:trl/@rdf:resource', namespaces=ns)]
            if len(list_of_translator_dnb_ids) > 0:
                self.rdf_translators = list_of_translator_dnb_ids                
            
            if len(self.rdf_etree.xpath('./rdf:Description/dc:title', namespaces=ns)) != 0:
                self.rdf_title = self.rdf_etree.xpath('./rdf:Description/dc:title', namespaces=ns)[0].text

        elif record_schema=="oai_dc":

            self.oai_etree = record_etree
            self.oai_string = etree.tostring(self.oai_etree).decode('UTF-8')
            self.dnb_id = self.oai_etree.xpath('./dc:identifier[@xsi:type="dnb:IDN"]', namespaces=ns)[0].text

        self.dnb_id_url = Dnb.return_dnb_id_url(self)

    def return_dnb_id_url(self):
        """
        Helper function that returnes dnb_id_url for authority or bibliographic records.
        """
        if self.type == "authority":
            return "https://d-nb.info/gnd/"+self.dnb_id
        elif self.type == "bibliographic":
            return "https://d-nb.info/"+self.dnb_id

    def __repr__(self):
        return str(vars(self))

    def __str__(self):
        return str(vars(self))

