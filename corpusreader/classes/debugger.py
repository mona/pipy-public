class Debugger():
    """A simple class for debugging.
    
    """
    def __init__(self, debug=False):
        self.DEBUG = debug

    def print(self, *values):
        if self.DEBUG:
            print(*values)

    def input(self):
        if self.DEBUG:
            input()