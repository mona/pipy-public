import json
import pandas as pd
import re
import requests

from difflib import SequenceMatcher
from lxml import etree
from xml.dom.expatbuilder import Namespaces

from corpusreader.utils_methods import viaf_uri

ns = {  
    "dc": "http://purl.org/dc/elements/1.1/",
    "dcterms": "http://purl.org/dc/terms/",
    "dnb": "http://d-nb.de/standards/dnbterms",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "schema" : "http://schema.org/",
    }
class Viaf():
    """A class that holds information for a viaf entry.
    """
    def __init__(self, viaf_id, formats = ["rdf.xml", "viaf.json"], record_type = None, viaf_id_reconciled_ratio = None):
        """init method of class `Viaf`.
        
        Args:
            viaf_id (str): viaf id.
            formats (list): formats to capture, so far options are the following.
                            - "viaf.json"
                            - "rdf.xml"
            record_type (str): type of record:
                            - "author"
                            - "work"         
        """
        self.viaf_id = viaf_id
        self.viaf_id_uri = viaf_uri(self.viaf_id)

        self.viaf_id_reconciled_ratio = viaf_id_reconciled_ratio

        self.gnd_id = None
        self.gnd_id_uri = None

        self.wikidata_id = None
        self.wikidata_id_uri = None

        # attributes json/rdf
        self.number_mapped_sources_viaf = None

        # json attributes
        self.json_dict = None

        # rdf attributes
        self.rdf_etree = None
        self.rdf_string = None

        
        if "viaf.json" in formats:
            self.json_dict = Viaf.viaf_api(viaf_id = viaf_id, format = "viaf.json")

            try:
                self.gnd_id_uri = [element["@nsid"] for element in self.json_dict["sources"]["source"] if "DNB|" in element["#text"]][0]
                self.gnd_id = self.gnd_id_uri.split('/')[-1]
            except:
                pass

            try:
                self.wikidata_id = [element["@nsid"] for element in self.json_dict["sources"]["source"] if "WKP|" in element["#text"]][0]
                self.wikidata_id_uri = "http://www.wikidata.org/entity/" + self.wikidata_id
            except:
                pass

            try:
                self.number_mapped_sources_viaf = len(self.json_dict["sources"]["source"])
            except:
                pass

        elif "rdf.xml" in formats:
            self.rdf_etree = Viaf.viaf_api(viaf_id = viaf_id, format = "rdf.xml")
            self.rdf_string = etree.tostring(self.rdf_etree).decode('UTF-8')

            mapped_ressources = self.rdf_etree.xpath(".//rdf:Description[@rdf:about='http://viaf.org/viaf/%s']/schema:sameAs/@rdf:resource" %viaf_id, namespaces = ns)

            try:
                self.number_mapped_sources_viaf = len(mapped_ressources)
            except:
                pass

            try:
                self.gnd_id_uri = [uri for uri in mapped_ressources if uri.startswith("http://d-nb.info/gnd/")][0]
                self.gnd_id = self.gnd_id_uri.split('/')[-1]
            except:
                pass

            try:
                self.wikidata_id_uri = [uri for uri in mapped_ressources if uri.startswith("http://www.wikidata.org/entity")][0]
                self.wikidata_id = self.wikidata_id_uri.split('/')[-1]
            except:
                pass

    @staticmethod
    def viaf_api(viaf_id: str, format: str = "viaf.json"):
        """Api method. Returns records from viaf database. Full api docu here: 
        - https://www.oclc.org/developer/api/oclc-apis/viaf/authority-cluster.en.html
        - https://www.oclc.org/developer/api/oclc-apis/viaf/authority-source.en.html/

        Args:
            viaf_id (str): viaf_id
            output_format (str): output_format values
                                - "viaf.json"
                                - "rdf.xml"
                                - "marc21.xml"
        Returns:
            if "viaf.json":
                dict: json dict.
            elif "rdf.xml"/"marc21.xml":
                `lxml.etree`: etree for xml oputput.

        """
        base_url = "https://viaf.org/viaf/" + viaf_id + "/" + format
        params = {}
        r = requests.get(base_url, params = params)

        if format.endswith(".json"):
            viaf_json_dict = json.loads(r.text)

            return viaf_json_dict

        elif format.endswith(".xml"):
            viaf_etree = etree.fromstring(r.content)

            return viaf_etree

    @staticmethod
    def viaf_autosuggest_api(name: str):
        """
        Api method. Returns records from viaf database. Full api docu here: 
        - https://www.oclc.org/developer/api/oclc-apis/viaf/authority-cluster.en.html
        - https://www.oclc.org/developer/api/oclc-apis/viaf/authority-source.en.html/
        Example of query: https://viaf.org/viaf/AutoSuggest?query=Stefan%20Stoenescu

        Args:
            name (str): name of the title or person for which it should makes suggestions

        Returns:
            dict: json dict.
        """
        base_url = "https://viaf.org/viaf/AutoSuggest?query=" + name
        params = {}
        r = requests.get(base_url, params = params)

        viaf_json_dict = json.loads(r.text)

        return viaf_json_dict


    @staticmethod
    def reconcile_person_with_viaf(name: str):
        """
        Args:
            name (str): name of the person that needs to be reconciled

        Returns:
            `Viaf`: viaf object to the 
        """
        viaf_json_dict = Viaf.viaf_autosuggest_api(name)

        if not viaf_json_dict["result"]:
            return None
        else:
            return(Viaf(viaf_json_dict["result"][0]["recordID"]).viaf_id)
        
        

        
    @staticmethod
    def reconcile_work_with_viaf(works_from_author_viaf: dict, work_title_from_file: str):
        """Method that identifies viaf_id for work based on author dictionary and returnes viaf object for work.
        Args:
            works_from_author_viaf (dict): dictionary with the works of an author; key is the VIAF-ID of the work, the value contains a dictionary with "title"
            work_title_from_file (str): title of work from metadata in TEI file.

        Returns:
            `Viaf`: viaf object to the reconciled work with the ratio of the similarity.
        """
        try:
            title_short = re.findall(r"^(.[\w \-,\?]+)", work_title_from_file)[0]
        except:
            title_short = work_title_from_file

        try:
            #Iterate through the works in VIAF
            for key, values in works_from_author_viaf.items():

                # Compare the given title in the record and the title of the VIAF data
                ratio = SequenceMatcher(None, title_short.lower(), values["title"].lower()).ratio()
                
                # Add new element to the subditionary with the ratio of the similarity
                works_from_author_viaf[key]["ratio"] = ratio

            # Sort the results, with the first record from VIAF being the closest one to the title of the record
            works_from_author_viaf = dict(sorted(works_from_author_viaf.items(), key = lambda x: (x[1]["ratio"], x[1]["title"]), reverse=True))

            # if the list is not empty
            if len([list(works_from_author_viaf.values())]) > 0:
                # Get the first element
                
                reconciled_viaf_id_work = list(works_from_author_viaf.keys())[0]

                reconciled_viaf_ratio_work = list(works_from_author_viaf.values())[0]["ratio"]


            # Return the viaf_id und ratio
            # TODO: we should also return the ratio and title of the reconciliated work in VIAF
            return Viaf(reconciled_viaf_id_work, viaf_id_reconciled_ratio = reconciled_viaf_ratio_work)
        except:
            print("Reconcilation of work failed for: %s"%work_title_from_file)        
        

        
    @staticmethod
    def make_list_of_works_from_author_from_viaf(author_json):
        """Method that makes takes the VIAF ID of an author and makes a dictionary of their works

        Args:
            author_json (dict): dict represantation of json from viaf api.

        Returns:
            works_from_author_viaf (dict): dictionary of works from the given author, with the VIAF-ID as key and the values containing "title"
        """

        works_from_author_viaf = {}
        for work in author_json["titles"]["work"]:
            try:
                works_from_author_viaf[work["@id"].split("VIAF|")[1]] = {"title": work["title"]}
            except:
                pass
    
        return works_from_author_viaf

    

    def __repr__(self):
        """object representation in string format.
        """
        selected_attributes = [attr for attr in dir(self) if not attr.startswith("__") and attr not in ["json_dict", "rdf_etree", "rdf_string"]]
        return  str({k:v for k,v in vars(self).items() if k in selected_attributes})
