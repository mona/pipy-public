from glob import glob
import json
from pickle import GLOBAL
import requests
import time
from qwikidata.entity import WikidataItem, WikidataLexeme, WikidataProperty
from qwikidata.linked_data_interface import get_entity_dict_from_api
from qwikidata.sparql import return_sparql_query_results
from SPARQLWrapper import SPARQLWrapper, JSON
from difflib import SequenceMatcher

from .dbpedia import DBpedia
from .debugger import Debugger

debugger = Debugger(False)


class WikidataSet(set):
    """Class that stores Wikidata-objectes.
    """   
    def __init__(self, entity_set: set = set(), wikidata_ids: bool = None):
        """__init__ method of the class `WikidataSet`.

        Args:
            entity_set (set of `Wikidata`): a set of wikidata objects.
            wikidata_ids (list): optional; a list of wikidata_id, when used wikidata_objects will
                                be created for each wikidata_id.
        """
        super(WikidataSet, self).__init__(entity_set)

        if wikidata_ids:
            for wikidata_id in wikidata_ids:
                self.add(Wikidata(wikidata_id=wikidata_id))

    def find_highest_sitelinks(self):
        """Method that returns wikidataset object with wikidata objects that share maximum sitelinks.
        """
        max = -1
        for wikidata_object in self:
            if wikidata_object.sitelinks > max:
                max = wikidata_object.sitelinks
        
        for wikidata_object in self.copy():
            if wikidata_object.sitelinks != max:
                self.remove(wikidata_object)


class Wikidata():
    """A class that holds information for a wikidata entity (item) or wikidata property.
    """
    def __init__(self, wikidata_id: str, request_qwikidata_class: bool = True, wikidata_id_reconciled_ratio = None ):
        """__init__ method of the class `Wikidata`.

        Args:
            wikidata_id (str): wikidata_id for corresponding entry.
            request_qwikidata_class (bool): requests qwikidata_dict and qwikidataitem/qwikidataproperty if True.
        """
        self.wikidata_id = wikidata_id
        

        # predifined attributes (filled when requested)
        self.qwikidata_dict = None
        self.qwikidataitem = None
        self.qwikidataproperty = None

        self.wikidata_id_reconciled_ratio = wikidata_id_reconciled_ratio

        self.dnb_id = None


        #request Wikidata class

        # add dict from qwikidata
        if request_qwikidata_class == True:
            self.qwikidata_dict = Wikidata.wikidata_request_time_handling(
                get_entity_dict_from_api,
                entity_id = wikidata_id
                )

        # add class from qwikidata for item/propery
        if self.qwikidata_dict["type"] == "item":
            self.qwikidataitem = WikidataItem(self.qwikidata_dict)
        elif self.qwikidata_dict["type"] == "property":
            self.qwikidataproperty = WikidataProperty(self.qwikidata_dict)

        # Try to get the VIAF-ID with the Wikidata-ID from Wikidata
        try:
            self.viaf_id = self.qwikidata_dict["claims"]["P214"][0]["mainsnak"]["datavalue"]["value"]
        except:
            self.viaf_id = None



       # add dict with labels in de/en
        self.label = dict()
        for language in ["de", "en"]:
            try:
                self.label[language] = self.qwikidata_dict["labels"][language]["value"]
            except:
                self.label[language] = None

        # add dict with descriptions in de/en
        self.description = dict()
        for language in ["de", "en"]:
            try:
                self.description[language] = self.qwikidata_dict["descriptions"][language]["value"]
            except:
                self.description[language] = None

        # amount of sitelinks
        self.sitelinks = len(self.qwikidata_dict["sitelinks"])

        # check if gnd_id_property (https://www.wikidata.org/wiki/Property:P227) exists and add dnb_id (gnd_id)
        # TODO testen
        # if self.wikidata_dict["claims"]["P227"]:
        #     self.dnb_id = self.wikidata_dict["claims"]["P227"]["datavalue"]["value"]


    def wikidata_id_for_viaf_id(viaf_id: str):
        """A method that returns the wikidata_id of a given viaf_id (viaf_id).

        Args:
            viaf_id (str): viaf_id to search wikidata_id.

        Returns
            str: wikidata_id.
        """
        # query pattern:
        # ?author ?viaf_id_property(https://www.wikidata.org/wiki/Property:P214) ?viaf_id

        sparql_query = """
        SELECT ?author
        WHERE
        {
        ?author wdt:P214 "%s" .
        }
        """%viaf_id

        results = return_sparql_query_results(sparql_query)
        if len(results["results"]["bindings"]) == 0:
            return None

        uri = results["results"]["bindings"][0]["author"]["value"]
        wikidata_id = uri.split("/")[-1] 

        return wikidata_id
        

    def wikidata_id_for_dnb_id(dnb_id: str):
        """A method that returns the wikidata_id of a given dnb_id (gnd_id).

        Args:
            dnb_id (str): dnb_id to search wikidata_id.

        Returns
            str: wikidata_id.
        """
        # query pattern:
        # ?author ?gnd_id_property(https://www.wikidata.org/wiki/Property:P227) ?dnb_id

        sparql_query = """
        SELECT ?author
        WHERE
        {
        ?author wdt:P227 "%s" .
        }
        """%dnb_id

        results = return_sparql_query_results(sparql_query)
        if len(results["results"]["bindings"]) == 0:
            return None

        uri = results["results"]["bindings"][0]["author"]["value"]
        wikidata_id = uri.split("/")[-1] 

        return wikidata_id

    def get_wikidata_id_of_work(wikidata_id_author: str, title: str):
        """Method that checks literay works (Q7725634) from a given author (wikidata_id_author) 
        that matches a given work title.

        Args:
            ikidata_id_author (str): wikidata_id of author.
            title (str): title of work to search for.
        """

        # Old: sparql_query about relevant works
        #sparql_query = """
        #SELECT ?item ?itemLabel ?itemDescription
        #{
        #wd:%s wdt:P800 ?item .
        #SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        #}
        #"""%(wikidata_id_author)

        sparql_query = """
            SELECT ?item ?itemLabel ?itemDescription

            WHERE 
            {	
            ?item wdt:P31 wd:Q7725634;
                    wdt:P50 wd:%s;
                    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }

            }"""%(wikidata_id_author)
                
        results = return_sparql_query_results(sparql_query)
        
        # TODO: calculate similarity and give the most similar one
        for result in results["results"]["bindings"]:
            #print(bytes(result["itemLabel"]["value"]), encoding="utf-8")
            if title in result["itemLabel"]["value"]:
                wikidata_id = result["item"]["value"].split("/")[-1]
                return wikidata_id
        return None

    def get_wiki_id_from_author_name(author_name: str, language = "en", specific_wiki_category = "Q36180"):
        """Method that finds a wikidata id for an author name (str).
        
        Args:
            author_name (str): name of the author 
            language (str): language
            specific_wiki_category (str): category to which the entities should belong if the search for humans gives more than one result (Q36180 = writer)
        
        Returns:
           `Wikidata`: Wikidata object with the Wikidata ID and the similarity ratio between the original author_name and the name of the author of the entity which was found 
        """
        # TODO: fuzzy search with filter, see sparql query here: https://w.wiki/67PE, however, it takes too long, not possible directly with Wikidata
        
        human_sparql_query = '''
            SELECT distinct ?item ?itemLabel WHERE{  
            ?item ?label "''' + author_name + '''"@'''  + language + '''.  
            ?item wdt:P31 wd:Q5 ;
            wikibase:sitelinks ?sitelinks .
            SERVICE wikibase:label { bd:serviceParam wikibase:language " '''  + language + ''' ". }    
            }
            ORDER BY DESC(?sitelinks)
        '''

        specific_sparql_query = '''
            SELECT distinct ?item ?itemLabel WHERE{  
            ?item ?label "''' + author_name + '''"@'''  + language + '''.  
            ?item wdt:P31 wd:Q5 ;
                  wdt:P106 wd:''' + specific_wiki_category +  ''' ;
            wikibase:sitelinks ?sitelinks .
            SERVICE wikibase:label { bd:serviceParam wikibase:language " '''  + language + ''' ". }    
            }
            ORDER BY DESC(?sitelinks)
        '''

        if author_name in ["Anonymous"]:
            return None

        # Humans with the given name are searched
        general_results = return_sparql_query_results(human_sparql_query)

        # If we find one, we keep this one
        if len(general_results["results"]["bindings"]) == 1:
            results = general_results

        # If no results, the functions does not return anything
        elif len(general_results["results"]["bindings"]) == 0:
            #print("This won't work")
            return None

        # If the query for humans is too generic, we search for entities belonging to a specific category, such as writers
        elif len(general_results["results"]["bindings"]) > 1:
            
            specific_results = return_sparql_query_results(specific_sparql_query)

            # If we don't find any entity from the specific category (writer), we switch back to the more general results
            if len(specific_results["results"]["bindings"]) == 0:
                results = general_results
            else:
                # Otherwise, we keep the more specific results
                results = specific_results
        
        author_wikidata_id = results["results"]["bindings"][0]["item"]["value"].split("/")[-1]
        author_wikidata_name = results["results"]["bindings"][0]["itemLabel"]["value"]
        
        # Calculates a ratio of the longest contiguous matching subsequence (LCS)
        wikidata_id_reconciled_ratio = SequenceMatcher(None, author_name.lower(), author_wikidata_name.lower()).ratio()
        
        return Wikidata(wikidata_id = author_wikidata_id, wikidata_id_reconciled_ratio = wikidata_id_reconciled_ratio)


    def wikidata_request_time_handling(request_method, sleep_seconds: list = None, initial_seconds: int = 0.3, **kwargs):
        """Method that handles limits and timeouts for wikidata requests.
        
        Args:
            request_method: method for which time handling should be applied.
            sleep_seconds (list): dedicated list of values for sleep_seconds. If None predifined
                                    list is applied (default).
            initial_seconds (int): value to wait between every request.
            kwargs: keyword arguments of request_method.

        Returns:
            request_method: return value of request_method. If repeated request fails None is returned.
        """
        if sleep_seconds is None:
            sleep_seconds = [1, 2, 5, 10, 20, 30, 60]
        time.sleep(initial_seconds)
        try:
            return request_method(**kwargs) 
        except Exception as e:
            if len(sleep_seconds) == 0:
                debugger.print(str(e))
                debugger.print("Wikidata: waited multiple times for request without result!")
                return None
            message = str(e)
            sleep_second = sleep_seconds.pop(0)

            if "Wikimedia Error" in message:
                debugger.print("Wikidata: waiting for request: %s seconds (general backend error)" %sleep_second)
                time.sleep(sleep_second)
                return Wikidata.wikidata_request_time_handling(request_method, sleep_seconds, **kwargs)
                
            elif "Error 429 Too Many Requests" in message:
                sleep_second = int(message.split("Please retry in ")[1].split(" ")[0])
                debugger.print("Wikidata: waiting for request: %s seconds (429 error)" %sleep_second)
                return Wikidata.wikidata_request_time_handling(request_method, sleep_seconds, **kwargs)

            elif "java.util.concurrent.TimeoutException" in message:
                debugger.print("Wikidata: waiting for request: %s seconds (java timeout)" %sleep_second)
                time.sleep(sleep_second)
                return Wikidata.wikidata_request_time_handling(request_method, sleep_seconds, **kwargs)
                
            else:
                debugger.print(message)
                debugger.print("Wikidata: unresolved backend error, see message details above. Waiting %s seconds." %sleep_second)
                time.sleep(sleep_second)
                return Wikidata.wikidata_request_time_handling(request_method, sleep_seconds, **kwargs)
                #return None



    ##################################
    # methods based on mediawiki API

    def wikidata_media_wiki_api(query):
        """A function to query Wikidata by Mediawiki API.
        """

        base_url = "http://www.wikidata.org/w/api.php"
        params = {
            "action": "wbgetentities",
            "sites": "dewiki", #["dewiki", "enwiki"],
            "titles": query,
            "props": "descriptions",# "info",
            #"languages": "de",#"languages": ["en", "es", "de"],
            "format": "json"
        }
        r = requests.get(base_url, params=params)
        #json_dict = r.json()
        json_dict = json.loads(r.content)
        
        return json_dict

    def get_wikidata_ids_of_query(query):
        """A function that searches Wikidata by Mediawiki API and returns a list with wikidata_ids 
        for the hits.
        Args:
            query (str): query term.

        Returns:
            list: a list of wikidata_id.
        """
        json_dict = Wikidata.wikidata_media_wiki_api(query)
        return list(json_dict["entities"].keys())

    # def __repr__(self):
    #     for key in vars(self):
    #         if key == "wikidata_id":
    #             return key

    # def __str__(self):
    #     for key in vars(self):
    #         if key == "wikidata_id":
    #             return key
    #     #return str(vars(self))
