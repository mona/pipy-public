import time
from SPARQLWrapper import SPARQLWrapper, JSON

from .debugger import Debugger

debugger = Debugger(False)

class DBpedia():
    """A class that holds information for a dbpedia entry.
    """
    def __init__(self, dbpedia_id=None):
        
        self.uri = None
        self.wikidata_id = None
        if dbpedia_id:
            self.dbpedia_id = dbpedia_id
            

    def wikidata_id_for_dbpedia_id(dbpedia_id: str) -> str:
        """A method that returns the wikidata_id of a given dbpedia_id.

        Args:
            dbpedia_id (str): dbpedia_id to search wikidata_id.

        Returns
            str: wikidata_id.
        """
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        sparql.setQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX dbpedia: <http://dbpedia.org/resource/>
            PREFIX wd: <http://www.wikidata.org/entity/>
            PREFIX wdt: <http://www.wikidata.org/prop/direct/>
            SELECT * WHERE {
            <http://dbpedia.org/resource/%s> owl:sameAs ?item .
            FILTER ( strstarts(str(?item), "http://www.wikidata.org/entity/"))
            } 
        """%dbpedia_id)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        #TODO case: dbpedia_id is linked with multiple wikidata_ids (e.g. "Johannes_Valentinus_Andreae") 
        # -> disambiugation required
        if len(results["results"]["bindings"]) > 1:
            return None
        elif len(results["results"]["bindings"]) == 1:
            wikidata_id = results["results"]["bindings"][0]["item"]["value"].split("/")[-1]
            return wikidata_id
        else:
            return None


    def dbpedia_request_time_handling(request_method, sleep_seconds=None, initial_seconds=0.3, **kwargs):
        """Method that handles limits and timeouts for dbpedia requests.
        
        Args:
            request_method: method for which time handling should be applied.
            sleep_seconds (list): dedicated list of values for sleep_seconds. If None predifined
                                    list is applied (default).
            initial_seconds (int): value to wait between every request.
            kwargs: keyword arguments of request_method.

        Returns:
            request_method: return value of request_method. If repeated request fails None is returned.
        """
        if sleep_seconds is None:
            sleep_seconds = [1, 2, 5, 10, 20, 30, 60]
        time.sleep(initial_seconds)
        try:
            return request_method(**kwargs) 
        except Exception as e:
            if len(sleep_seconds) == 0:
                debugger.print("Wikidata: waited multiple times for request without result!")
                debugger.print(str(e))
                return None
            message = str(e)
            sleep_second = sleep_seconds.pop(0)

            if "urlopen error" in message:
                debugger.print("DBPedia: waiting for request: %s seconds (time out error)" %sleep_second)
                time.sleep(sleep_second)
                return DBpedia.dbpedia_request_time_handling(request_method, sleep_seconds, **kwargs)

            else:
                debugger.print(message)
                debugger.print("DBPedia: unresolved backend error, see message details above. Waiting %s seconds." %sleep_second)
                time.sleep(sleep_second)
                return DBpedia.dbpedia_request_time_handling(request_method, sleep_seconds, **kwargs)