import os

# path to project root
ROOT_PATH = os.path.dirname(__file__)

global KOLIMO_PATH
KOLIMO_PATH = os.path.join(ROOT_PATH, "..", "..", "KOLIMO")