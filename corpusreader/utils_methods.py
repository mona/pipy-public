import codecs
from operator import concat
import os

def read_txt(text_file:str) -> str:
    """Reads text file file with forced utf8 encoding.

    Args:
        file: A text file.
    
    Returns:
        str: text.
    
    """
    with codecs.open(text_file, 'r', encoding='utf8') as f:
        text = f.read()
        return text

def abspath_without_basename(filename):
    """Returns the aboulute path of a file without the filename.
    """
    abspath = os.path.abspath(filename)
    split = abspath.split("/")
    targetpath = "/".join(split[:-1])

    return targetpath

def viaf_uri(viaf_id):
    """helper function that returns viaf url
    """
    if viaf_id:
        return "https://viaf.org/viaf/" + viaf_id
    else:
        return None

def wikidata_uri(wikidata_id):
    """helper function that returns viaf url
    """
    if wikidata_id:
        return "https://www.wikidata.org/wiki/" + wikidata_id
    else:
        return None

def is_notebook() -> bool:
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter