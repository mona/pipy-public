import codecs
import csv
from weakref import ref
import lxml
import os
import pandas as pd
import re
import sys
import time
import warnings

from bs4 import BeautifulSoup as bs
from hashlib import md5
from lxml import etree
from pathlib import Path
from re import template
from typing import Type

from corpusreader.classes.dnb import DnbList
from corpusreader.classes.debugger import Debugger
from corpusreader.classes.viaf import Viaf
from corpusreader.classes.wikidata import Wikidata
from corpusreader.settings import KOLIMO_PATH, ROOT_PATH
from corpusreader.utils_methods import read_txt, viaf_uri, wikidata_uri, is_notebook

debugger = Debugger(False)

pd.set_option('display.max_rows', None, 'display.max_columns', None)

ns = {
    "marcRole": "http://id.loc.gov/vocabulary/relators/",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    'tei': 'http://www.tei-c.org/ns/1.0' 
    }

xpaths = {
    #TODO refactor "xpath" key to "element"
    #TODO xpaths to global variable XPATHS
    "author": {"xpath": "//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"},
    "author_forename": {"xpath": "//tei:author/tei:persName/tei:forename"},
    "author_surname": {"xpath": "//tei:author/tei:persName/tei:surname"},
    "title": {"xpath": '//tei:fileDesc/tei:titleStmt/tei:title'},
    "title_main": {"xpath": '//tei:fileDesc/tei:titleStmt/tei:title[@type="main"]'},
    "title_sub": {"xpath": '//tei:fileDesc/tei:titleStmt/tei:title[@type="sub"]'},
    "dnb_id_author_uri": {"xpath": "//tei:author/tei:persName", "attrib": "ref"},
    "text": {"xpath": "//tei:TEI/tei:text"},
    "first_publication": {"xpath":"//tei:profileDesc/tei:creation/tei:date[@type='firstPublication']"}#"}#    
}

class Corpus(list):
    """A class that stores a list of Record objectes.
    """
    def __init__(self, corpus=list()):
        """__init__ method of the class `Corpus`.

        Args:
            corpus (list of Record-objectes): a list of Record-objectes.
        """
        super(Corpus, self).__init__(corpus)

        # add dictionary that stores records as values with text (complete string) as key
        self.keyed_records = dict()

    def get_record(self, text):
        """Method to find Record by complete text as string.
        Args:
            text (str): complete text as string.
        Returns:
            `Record`: Record object.
        """
        #key = md5(text.encode()).hexdigest()
        key = text

        try:
            return self.keyed_records[key]
        except KeyError:
            print("KeyError in get_record!")
            return None
        
    def add_record(self, record):
        """Method to add a Record to Corpus; adds Record to keyed_records dict of Corpus as well.
        Args:
            record (`Record`): Record object to append.
        Returns:
            `Corpus`: object of class `Corpus` with appended object of class `Record`.
        """

        #key = md5(record.text.encode()).hexdigest()
        key = record.text

        if key in self.keyed_records:
            warnings.warn("This text is already in the corpus!", category=UserWarning)
        self.append(record)
        self.keyed_records[key] = record

    def get_metadata_df(self):
        """Method to generate a Pandas df with metadata for specific metadata.
        Returns:
            `pandas.core.frame.DataFrame`: Pandas df with metadata for all records.
        """
        df = pd.DataFrame(columns= ["author",
                                    "title",
                                    "viaf_id_author",
                                    "wikidata_id_author",
                                    "viaf_id_work",
                                    "wikidata_id_work"
                                    ], dtype=object
                            )
        for record in self:
            df = df.append({"author": str(record.author),
                            "title": str(record.title),
                            "viaf_id_author": record.viaf_id_author,
                            "wikidata_id_author": record.wikidata_id_author,
                            "viaf_id_work": record.viaf_id_work,
                            "wikidata_id_work": record.wikidata_id_work
                            },
                            ignore_index=True)
        return df

    @staticmethod
    def read_eltec(corpus_folder: str,
                    ref_type: str = "uri",
                    request_viaf: bool = False,
                    request_wikidata: bool = False,
                    slice_: slice = slice(0, None)
                    ):
        """Method to read ELTeC corpora.
        Args:
            corpus_folder (str): path of the corpus folder.
            ref_type (str): authority file refs for author/title
                            - "uri": for complete uri
                            - "colon": colon seperated abbrevation
            sub_folders (list): list of subfolders to read (typically ELTeC corpara contain
                                folders named 'level1' and eventually 'level2').
            request_viaf (bool): request information from viaf.
            request_wikidata (bool): request information from Wikidata.
            slice_ (slice): slice of files to read as corpus object.
        Returns:
            `Corpus`: list of class `Corpus` with objects of class `Record` as elements. 
        """
        corpus_object = Corpus()
        corpus_folder = Path(corpus_folder)
        print("reading ELTeC...")
        for filename in os.listdir(corpus_folder)[slice_]:
            debugger.print(filename)
            if filename.endswith(".xml"):
                path_to_file = str(corpus_folder / filename)
                record_object = Record.read_tei(path_to_file = path_to_file, source = "eltec", ref_type = ref_type, request_viaf = request_viaf, request_wikidata = request_wikidata)
                corpus_object.add_record(record_object)
        
        print("ELTeC finished.")
        return corpus_object

    @staticmethod
    def read_kolimo(corpus_folder: str = KOLIMO_PATH, 
                    tg: bool = True, 
                    gb: bool = False, 
                    request_dnb: bool = True, 
                    request_wikidata: bool = True,
                    request_viaf: bool = True,
                    verbose: bool = False,
                    slice_: slice = slice(0, None)
                    ):
        """Method to read Kolimo corpus from specific folder structure.        
        Args:
            corpus_folder (str): path of the corpus folder.
            tg (bool): specifies if TextGrid subcorpus of Kolimo should be included.
            gb (bool): specifies if GutenbergDE subcorpus of Kolimo should be included.
            request_dnb (bool): while reading with requests a sru request is performed to the DNB database.
            request_wikidata (bool): while reading additional data from wikidata is requested.
            request_viaf (bool): request information from viaf.
            verbose (bool): if true each file that is read is printed to the console.
            slice_ (slice): slice of files to read as corpus object.
        Returns:
            `Corpus`: list of class `Corpus` with objects of class `Record` as elements. 
        """

        print("reading KOLIMO...")
        kolimo_list = Corpus()
        if tg:
            if corpus_folder==KOLIMO_PATH:
                tg_path= Path(KOLIMO_PATH) / "KOLIMO_header_text" / "tg"
            else:
                tg_path = corpus_folder
                print(tg_path)
            for i, filename in enumerate(os.listdir(tg_path)[slice_]):#[105:106] 107508-Verne__Jules.xml
                if verbose:
                    print(filename)
                if filename not in [".DS_Store"]:
                    
                    kolimo_object = Record.read_tei(path_to_file = str(tg_path / filename),
                                                    source="kolimo",
                                                    request_dnb=request_dnb,
                                                    request_wikidata=request_wikidata
                                                    )
                    kolimo_list.add_record(kolimo_object)
        
        if gb:
            if corpus_folder==KOLIMO_PATH:
                gb_path= KOLIMO_PATH + "/KOLIMO_header_text/gb"
            else:
                pass
            for root, dirs, files in list(os.walk(gb_path))[slice_]:
                for name in files:
                    if verbose:
                        print(os.path.join(root, name))
                    if name not in [".DS_Store"]:
                        try:
                            kolimo_object = Record.read_tei( path_to_file = os.path.join(root, name),
                                                                    source = "kolimo",
                                                                    request_dnb = request_dnb,
                                                                    request_wikidata = request_wikidata
                                                                    )
                            kolimo_list.add_record(kolimo_object)
                        except lxml.etree.XMLSyntaxError:
                            continue

        print("KOLIMO finished.")
        return kolimo_list

    @staticmethod
    def read_monaco(path_to_korpus_repo: str, request_wikidata: bool = True):
        """Method to read KOLIMO metadata from XML file and text from plain text file of CATMA annotation.
        Both are expected to be at the same level within the MONACO corpus.
        Args:
            path_to_korpus_repo (str): Path to the "korpus" repository in MONA GWDG project.
            request_wikidata (bool): while reading additional data from wikidata is requested.
        Returns:
            `Corpus`: list of class `Corpus` with objects of class `Record` as elements. 
        """

        corpus = Corpus()

        for foldername in os.listdir(path_to_korpus_repo):
            if os.path.isdir(os.path.join(path_to_korpus_repo, foldername)):
                filepath = os.path.join(path_to_korpus_repo, foldername, foldername + ".xml")
                if os.path.exists(filepath):
                    record_object = Record.read_tei(path_to_file = filepath,
                                                    source = "monaco",
                                                    request_wikidata = request_wikidata
                                                    )
                    corpus.add_record(record_object)

        return corpus

    @staticmethod
    def serialise_corpus(self, output_folder: str, mode: str = "TEI"):
        """Method to serialise all Records of Corpus to decated files; so far only TEI-XML.

        Args:
            output_folder (str): folder/path to store output files.
            mode ("str"): mode for the output format, so far only TEI-XML.

        Returns:
            files: Files in the requested format in the output_folder.
        """

        print("Start serialisation...")

        for record in self:
            record.serialise(output_folder=output_folder, mode=mode)

        print("Serialisation completed!")


class Record():
    """A class that stores information from a corpus record.
    """   

    def __init__(self, 
                # ids
                dnb_id_author: str = None, 
                dnb_id_author_uri: str = None, 
                viaf_id_author: str = None,
                viaf_id_work: str = None,
                wikidata_id_author: str = None,
                wikidata_id_work: str = None,

                # metadata from tei
                # TODO potentially define modes for tei type (KOLIMO, eltec, etc) and define variables within
                # record object intead of read_TEI/read_tei_record methods
                # TODO remove and refactor author_forename/author_surname
                author: str = None,
                author_forename: str = None,
                author_surname: str = None,
                title: str = None,
                title_main: str = None,
                title_sub: str = None,
                first_publication: str = None,
                text: str = None,

                # request parameter
                request_dnb: bool = None,
                request_wikidata: bool = None,
                request_viaf: bool = None,

                # metadata enrichment within file
                enriched_metadata: bool = None,
                enriched_metadata_dict: dict = None,

                # filename and XML content
                filename: str = None,
                filepath: str = None,
                tei_etree: etree = None,
                tei_string: str = None
                ):
        """__init__ method of the class `Record`.

        Args:
            dnb_id_author (str): dnb_id (in this case gnd-id or "nid") of author of literary work.
            dnb_id_author_uri (str): uri of author of literary work.
            wikidata_id_author (str): wikidata id for author.
            wikidata_id_work (str): wikidata id for work.
            viaf_id_author (str): viaf id for author.
            viaf_id_work wikidata id for work.

            author (str): complete name of author
            author_forename (str): author forename.
            author_surname (str): author surname.
            title (str): title of work.
            title_main (str): main title of literary work.
            title_sub (str): subtitle of literary work.
            first_publication (str): year of first publication (present in some cases of gb subcorpus).
            text (str): text of the literary work.

            request_dnb (bool): while reading with requests a sru request is performed to the DNB database.
            request_wikidata (bool): while reading additional data from wikidata is requested.
            request_viaf (bool): request information from viaf.

            enriched_metadata (bool): true if encriched metadata is in file.
            enriched_metadata_dict (dict): dict with values for enriched metadata (keys).

            filename (str): filename from which the record is read.
            tei_etree (`lxml.etree._ElementTree`): lxml.etree for the corresponding record TEI.
            tei_string (str): string represantation for the TEI.
        """
        ## basic attributes
        ## ids ({}_id_uri becomes None if {}_id is None)        
        self.dnb_id_author = dnb_id_author
        self.dnb_id_author_uri = dnb_id_author_uri
        self.viaf_id_author = viaf_id_author
        self.viaf_id_author_uri = viaf_uri(self.viaf_id_author)
        self.viaf_id_work = viaf_id_work
        self.viaf_id_work_uri = viaf_uri(self.viaf_id_work)
        self.wikidata_id_author = wikidata_id_author
        self.wikidata_id_author_uri = wikidata_uri(self.wikidata_id_author)
        self.wikidata_id_work = wikidata_id_work
        self.wikidata_id_work_uri = wikidata_uri(self.wikidata_id_work)

        self.author_source = author
        self.author_forename_source = author_forename
        self.author_surname_source = author_surname

        self.author = self.author_source
        self.author_forename = self.author_forename_source
        self.author_surname = self.author_surname_source

        # In case the first name and surnames were in different variables, now they are joint
        if  self.author:
            pass
        else:
            author_list = list()
            if self.author_forename:
                author_list.append(self.author_forename)
            if self.author_surname:
                author_list.append(self.author_surname)
            self.author = " ".join(author_list)

        self.title_source = title
        self.title_main_source = title_main
        self.title_sub_source = title_sub

        self.title = self.title_source
        self.title_main = self.title_main_source
        self.title_sub = self.title_sub_source
        
        # In case there is no title_main, it is extracted from the variable title
        if  self.title_main:
            pass
        else:
            try:
                self.title_main = Record.extract_main_title(self.title)
            except:
                pass

        self.first_publication = first_publication
        self.text = text

        if filepath:
            self.filepath = filepath
            self.filename = self.filepath.split("/")[-1]
        elif filename:
            self.filename = filename
        

        self.tei_etree = tei_etree
        self.tei_string = tei_string


        # additional attributes
        #######################

        # from DNB requests (filled if request_dnb OR enriched_metadata is present in file)
        # TODO:
        self.enriched_metadata = enriched_metadata
        self.translation = None
        self.author_lifetime = None
        self.text_quality = None
        self.textlength = None
        self.fiction = None # fiction / non-fictional
    
        # from Wikidata requests (filled if request_wikidata OR TODO enriched_metadata is present in file)
        # TODO: add to serialisation; currently viaf also reads from file -> handling for request and file reading needed
        # self.wikidata_id_author = None
        # self.wikidata_id_work = None

        if enriched_metadata:
            for type_key in enriched_metadata_dict:
                type_key
                setattr(
                    self,
                    type_key,
                    enriched_metadata_dict[type_key]
                )

        # In case some typographic characters like [ or ( are in the name, these are deleted
        if re.findall(r"[,\[\(]", self.author):
            # or it should be implementet centrally (not only for this specific case)
            self.author = Record.clean_complete_name(self.author)

        if request_dnb:
            self.enriched_metadata = True
            all_dnb_records_of_author = DnbList.dnb_sru_multi_record_schemas(query="auRef=%s"%self.dnb_id_author, database="dnb", record_schemas=["MARC21-xml", "RDFxml", "oai_dc"])
            self.translation = Record.detect_translation_status(self, all_dnb_records_of_author=all_dnb_records_of_author)
                
        if request_viaf:
            # If there is no VIAF-ID, we try to find it reconciling it with VIAF
            if not self.viaf_id_author:
                self.viaf_id_author = Viaf.reconcile_person_with_viaf(self.author)
            
            # If there is  VIAF-ID, we try to obtain the Wikidata-ID
            if self.viaf_id_author:
                viaf_object_author = Viaf(viaf_id = self.viaf_id_author, record_type="author")
                self.wikidata_id_author = viaf_object_author.wikidata_id
                self.wikidata_id_author_uri = viaf_object_author.wikidata_id_uri

                # If there is no VIAF-ID for work, we try to obtain it 
                if not self.viaf_id_work:
                    works_from_author_viaf_dict = Viaf.make_list_of_works_from_author_from_viaf(author_json = viaf_object_author.json_dict)
                    #print(self.title, self.viaf_id_author)
                    viaf_object_work = Viaf.reconcile_work_with_viaf(works_from_author_viaf = works_from_author_viaf_dict, work_title_from_file = self.title)
                    if viaf_object_work:
                        self.viaf_id_work = viaf_object_work.viaf_id
                        self.viaf_id_work_uri = viaf_object_work.viaf_id_uri

                # If we have a VIAF-ID for work, we try to obtain the Wikidata-ID for work
                if self.viaf_id_work:
                    viaf_object_work = Viaf(viaf_id = self.viaf_id_work)
                    self.wikidata_id_work = viaf_object_work.viaf_id
                    self.wikidata_id_work_uri = viaf_object_work.viaf_id_uri







        if request_wikidata:
            self.enriched_metadata = True

            # if it has a DNB (GND) id, it tries to get the Wikidata ID from Wikidata
            if self.dnb_id_author:
                self.wikidata_id_author = Wikidata.wikidata_request_time_handling(
                    Wikidata.wikidata_id_for_dnb_id, 
                    dnb_id = self.dnb_id_author
                    )

            # if it has a VIAF id, it does nothing
            elif self.viaf_id_author:
                pass
            
            elif not self.wikidata_id_author:
                    
                result = Wikidata.wikidata_request_time_handling(
                    Wikidata.get_wiki_id_from_author_name,
                    author = self.author,
                    )
                if result:
                    self.wikidata_id_author = result.wikidata_id


            if self.wikidata_id_author:

                # If there is Wikidata ID and we don't have VIAF ID, it gets it from Wikidata through the Wiki-ID
                if not self.viaf_id_author:
                    self.viaf_id_author = Wikidata.wikidata_request_time_handling(
                        Wikidata,
                        wikidata_id = self.wikidata_id_author,
                        ).viaf_id

                if self.title:
                    title = self.title
                elif self.title_main:
                    title = self.title_main

            # If there is a VIAF ID
            if self.viaf_id_work:
                # It tries to get the Wikidata ID from Wikidata using the VIAF ID
                self.wikidata_id_work = Wikidata.wikidata_request_time_handling(
                    Wikidata.wikidata_id_for_viaf_id,
                    viaf_id = self.viaf_id_work,
                    )

            # If there is still no Wikidata ID for work, it tries to reconcile it
            if not self.wikidata_id_work:
                self.wikidata_id_work = Wikidata.wikidata_request_time_handling(
                    Wikidata.get_wikidata_id_of_work,
                    wikidata_id_author = self.wikidata_id_author,
                    title = title
                    )
            
    @staticmethod
    def read_tei(
                path_to_file: str,
                source: str,

                # parameter for "eltec"
                request_gnd: bool = False,
                request_viaf: bool = False,
                ref_type: str = "uri",

                # paramteter for "kolimo" (also MONACO)
                request_dnb: bool = False, 
                request_wikidata: bool = False, 
                #monaco_txt_source: str = None
                ):
        """Method to read record object from TEI-XML file specified by a given corpus.

        Args:
            path_to_file (str): Path to the file including filename.
            source (str): select a source out of 
                            - "eltec" 
                            - "kolimo" (Korpus literarischer Moderne)
                            - "monaco" (Modes of Narration and Attribution Corpus: https://gitlab.gwdg.de/mona/korpus-public)
                            - "digitale_bibliothek" (in textgrid)
                            - "conssa"
                            - "ita-textbox"
                            - "it_textbox"
                            - "pt_textbox"
            request_gnd (bool): TODO
            request_dnb (bool): while reading with requests a sru request is performed to the DNB database;
                                        a Corpus object with all bibliographic records of the corresonding author (dnb_id_author) is stored
                                        to aquire additional metadata.
            request_viaf (bool): request information from viaf.
            request_wikidata (bool): while reading additional data from wikidata is requested.
            ref_type (str): authority file refs for author/title
                            - "uri": for complete uri
                            - "colon": colon seperated abbrevation

        Returns:
            `Record`: Record read from file.

        # TODO:
            - Make it possible for the users to pass as parameters new xpaths for other corpora
            - Consider GND as entry identifiers
        """
        parser = etree.XMLParser(huge_tree=True)
        tree = etree.parse(path_to_file, parser)

        if source == "eltec":
            author = " ".join( [string for string in list(tree.xpath(xpaths["author"]["xpath"], namespaces=ns)[0].itertext()) if not re.match("\s+", string)] )
            title = " ".join( [string for string in list(tree.xpath(xpaths["title"]["xpath"], namespaces=ns)[0].itertext()) if not re.match("\s+", string)] ) 
            if ":" in title:
                title = title.split(":")[0].strip()

            viaf_id_author = None
            viaf_id_work = None
            wikidata_id_author = None
            wikidata_id_work = None

            
            try:
                author_ref_list = tree.xpath(xpaths["author"]["xpath"], namespaces=ns)[0].attrib["ref"].split(" ")
                for el in author_ref_list:
                    if ref_type == "colon":
                        if el.startswith("viaf:"):
                            viaf_id_author = el.split(":")[-1]
                            viaf_id_author = Record.remove_missing(viaf_id_author)

                        elif el.startswith("wikidata:"):
                            wikidata_id_author = el.split(":")[-1]
                            wikidata_id_author = Record.remove_missing(wikidata_id_author)
                    elif ref_type == "uri":
                        el = el.strip("/")
                        viaf_id_author = el.split("/")[-1]
            except:
                pass

            try:
                work_ref_list = tree.xpath(xpaths["title"]["xpath"], namespaces=ns)[0].attrib["ref"].split(" ")
                for el in work_ref_list:
                    if ref_type == "colon":
                        if el.startswith("viaf:"):
                            viaf_id_work = el.split(":")[-1]
                            viaf_id_work = Record.remove_missing(viaf_id_work)

                        elif el.startswith("wikidata:"):
                            wikidata_id_work = el.split(":")[-1]
                            wikidata_id_work = Record.remove_missing(wikidata_id_work)
                    elif ref_type == "uri":
                        el = el.strip("/")
                        wikidata_id_work = el.split("/")[-1]
            except:
                pass

            # text
            #print(tree.xpath(xpaths["text"]["xpath"], namespaces=ns))
            #print(''.join(tree.xpath(xpaths["text"]["xpath"], namespaces=ns)[0].itertext()))
            #input()
            text = ''.join(tree.xpath(xpaths["text"]["xpath"], namespaces=ns)[0].itertext())


            return Record(
                author = author,
                title = title,
                viaf_id_author = viaf_id_author,
                wikidata_id_author = wikidata_id_author,
                wikidata_id_work = wikidata_id_work,
                viaf_id_work = viaf_id_work,
                request_viaf = request_viaf,
                request_wikidata = request_wikidata,
                tei_etree = tree,
                tei_string = etree.tostring(tree).decode('UTF-8'),
                text = text
                #filepath = path_to_file
            )

        elif source in ["kolimo", "monaco"]:
            parser = etree.XMLParser(huge_tree = True)
            tree = etree.parse(path_to_file, parser)

            # dnb_id author uri; dnb_id author
            dnb_id_author_uri = tree.xpath(xpaths["dnb_id_author_uri"]["xpath"], namespaces=ns)[0].attrib[xpaths["dnb_id_author_uri"]["attrib"]]
            dnb_id_author = dnb_id_author_uri.split('/')[-1]
            if len(dnb_id_author) == 0:
                dnb_id_author = None

            # author forename
            author_forename = tree.xpath(xpaths["author_forename"]["xpath"], namespaces=ns)[0].text
            
            # author surename
            try:
                author_surname = tree.xpath(xpaths["author_surname"]["xpath"], namespaces=ns)[0].text
            except:
                author_surname = None
            
            # title main
            title_main = tree.xpath(xpaths["title_main"]["xpath"], namespaces=ns)[0].text
            
            # title sub
            try:
                title_sub = tree.xpath(xpaths["title_sub"]["xpath"], namespaces=ns)[0].text
            except:
                title_sub = None

            # first publication
            try:
                first_publication = tree.xpath(xpaths["first_publication"]["xpath"], namespaces=ns)[0].text
                # in tg the element tei:date[@type='firstPublication'] not contains integer
                # but another date element with attributes `notBefore="1777" notAfter="1811"`
                # which results in an empty string of 21 white space chars for var first_publication.
                # This is stripped here:
                first_publication = first_publication.strip()
                if len(first_publication) == 0:
                    first_publication = None
            except:
                first_publication = None
            
            # text
            if source == "kolimo":
                text = tree.xpath(xpaths["text"]["xpath"], namespaces=ns)[0].text
            elif source == "monaco":
                text = read_txt(path_to_file.split(".xml")[0] + ".txt")

            # enriched_metadata
            try:
                enriched_metadata = string_to_bool(tree.xpath("//tei:notesStmt/tei:note[@type='enriched_metadata']", namespaces=ns)[0].text)
            except:
                enriched_metadata = False
            
            enriched_metadata_dict = dict()

            if enriched_metadata:
                try:
                    translation = string_to_bool(tree.xpath("//tei:notesStmt/tei:note[@type='translation']", namespaces=ns)[0].text)
                    if translation == "None":
                        translation = None
                except:
                    translation = None
                enriched_metadata_dict["translation"] = translation

                try:
                    wikidata_id_author = tree.xpath("//tei:notesStmt/tei:note[@type='wikidata_id_author']", namespaces=ns)[0].text
                    if wikidata_id_author == "None":
                        wikidata_id_author = None
                except:
                    wikidata_id_author = None
                enriched_metadata_dict["wikidata_id_author"] = wikidata_id_author

                try:
                    wikidata_id_work = tree.xpath("//tei:notesStmt/tei:note[@type='wikidata_id_work']", namespaces=ns)[0].text
                    if wikidata_id_work == "None":
                        wikidata_id_work = None
                except:
                    wikidata_id_work = None
                enriched_metadata_dict["wikidata_id_work"] = wikidata_id_work

            return  Record(
                            dnb_id_author = dnb_id_author, 
                            dnb_id_author_uri = dnb_id_author_uri, 
                            author_forename = author_forename, 
                            author_surname = author_surname, 
                            title_main = title_main, 
                            title_sub = title_sub,
                            first_publication = first_publication,
                            text = text, 
                            request_dnb = request_dnb,
                            request_wikidata = request_wikidata,
                            filepath = path_to_file,
                            enriched_metadata = enriched_metadata,
                            enriched_metadata_dict = enriched_metadata_dict
                            )

    @staticmethod
    def remove_missing(id):
        """Helper function to remove entries where id for viaf/wikidata is set to missing/inexistent.
        """
        if id in ["missing", "inexistent"]:
            return None

    # def read_csv(path_to_file: str,
    #                     request_viaf: bool = False, 
    #                     request_gnd: bool = False, 
    #                     request_wikidata: bool = False, 
    #                     monaco_txt_source: bool = False
    # ):
    #     #with open(path_to_file, mode="w") as csvfile:
    #     #    annowriter = csv.writer(csvfile, delimiter='\t', quotechar='|')
    #     df = pd.read_csv(path_to_file, sep="\t", index_col=0)
        
    #     df["wikidata_id"] = df["authorid"].str.findall("viaf:[^.*?]")
    #     df["viaf"] = df["authorid"].str.findall("viaf:[^.*?]")

    #     return 
        

    @staticmethod
    def detect_translation_status(self, all_dnb_records_of_author, number_of_records_to_check=None):
        """Method to detect if a Record is a translation from a non-German language into German.

        Args:
            all_dnb_records_of_author (`Corpus`): corpus object with all Dnb records for a certain author (by dnb/gnd id).

        Returns:
            bool: True if text is a translation, False if not.
        """
        if number_of_records_to_check:
            all_dnb_records_of_author = all_dnb_records_of_author[0:number_of_records_to_check]

        for dnb_record in all_dnb_records_of_author:
            # exclude non-german text for marcxml attributes
            if dnb_record.marcxml_language_text != "ger":
                continue
            # exclude non-german text for rdf attributes
            elif dnb_record.rdf_language_text != "ger":
                continue
            elif dnb_record.marcxml_language_original_if_translation and dnb_record.marcxml_language_original_if_translation != "ger":
                return True
            else:
                continue
            
        return False

    def serialise(self, output_folder, mode="TEI", output_method="lxml"):
        """Method to serialise a Record to a decated file; so far only TEI-XML.

        Args:
            output_folder (str): folder/path to store output file.
            mode ("str"): mode for the output format, so far only TEI-XML.

        Returns:
            file: File in the requested format in the output_folder.
        """
        parser = etree.XMLParser()
        tei_template_tree = etree.parse(ROOT_PATH + "/template.xml", parser)

        # author forename
        tei_template_tree.xpath(xpaths["author_forename"]["xpath"], namespaces=ns)[0].text = self.author_forename
        
        # author surename
        tei_template_tree.xpath(xpaths["author_surname"]["xpath"], namespaces=ns)[0].text = self.author_surname
        
        # title main
        tei_template_tree.xpath(xpaths["title_main"]["xpath"], namespaces=ns)[0].text = self.title_main

        # title sub
        tei_template_tree.xpath(xpaths["title_sub"]["xpath"], namespaces=ns)[0].text = self.title_sub

        # dnb_id author uri
        tei_template_tree.xpath(xpaths["dnb_id_author_uri"]["xpath"], namespaces=ns)[0].attrib[xpaths["dnb_id_author_uri"]["attrib"]] = self.dnb_id_author_uri
       
        # text
        tei_template_tree.xpath(xpaths["text"]["xpath"], namespaces=ns)[0].text = self.text

        # enriched_metadata
        note_enriched_metadata = etree.SubElement(tei_template_tree.xpath("//tei:notesStmt", namespaces=ns)[0], "note")
        note_enriched_metadata.text = str(self.enriched_metadata)
        note_enriched_metadata.attrib["type"] = "enriched_metadata"

        if self.enriched_metadata:

            # from DBN
            ##########

            # translation
            note_translation = etree.SubElement(tei_template_tree.xpath("//tei:notesStmt", namespaces=ns)[0], "note")
            #tei_template_tree.xpath("//tei:notesStmt", namespaces=ns)[0].append( etree.Element("note") )
            note_translation.text = str(self.translation)
            note_translation.attrib["type"] = "translation"

            # from Wikidata
            ###############

            # wikidata_id_author
            note_wikidata_id_author = etree.SubElement(tei_template_tree.xpath("//tei:notesStmt", namespaces=ns)[0], "note")
            note_wikidata_id_author.text = str(self.wikidata_id_author)
            note_wikidata_id_author.attrib["type"] = "wikidata_id_author"

            # wikidata_id_work
            note_wikidata_id_work = etree.SubElement(tei_template_tree.xpath("//tei:notesStmt", namespaces=ns)[0], "note")
            note_wikidata_id_work.text = str(self.wikidata_id_work)
            note_wikidata_id_work.attrib["type"] = "wikidata_id_work"

        filename = output_folder + self.filename

        if output_method=="soup":
            #convert the generated TEI-XML to a string
            tei_template_tree_string = etree.tostring(tei_template_tree) 
            #make BeautifulSoup
            tei_template_tree_soup = bs(tei_template_tree_string, "lxml")
            #prettify the TEI-XML          
            tei_template_tree_soup = tei_template_tree_soup.prettify()   


            with open(filename, "w", encoding='utf-8') as file:
                file.write(str(tei_template_tree_soup))

        elif output_method=="lxml":
            tei_template_tree.write(filename, encoding="UTF-8")


    def clean_complete_name(author: str) -> str:
        # Special charaters are deleted
        author = re.sub(r"[\[\(].*?[\]\)]", r"", author)

        # If there is a comma in the name, we assume that the order is "Surname, Name", and we invert this order
        author = re.sub(r"^(.*?)[,;](.*?)$", r"\2 \1", author)
        author = re.sub(r"\s+", r" ", author).strip()

        return author

    def extract_main_title(title: str) -> str:
        if re.findall(r"^(.*?)\s*[:;\.]", title):
            title_main = re.findall(r"^(.*?)\s*[:;\.]", title)[0].strip()
        else:
            title_main = title

        return title_main

    def __repr__(self):
        """object representation in string format.
        """
        selected_attributes = [attr for attr in dir(self) if not attr.startswith("__") and attr not in ["text", "tei_string"]]
        return  str({k:v for k,v in vars(self).items() if k in selected_attributes})



def string_to_bool(string):
    """Helper method to convert True/False strings from TEI to bool.
    """
    if string == "True":
        return True
    elif string == "False":
        return False
    elif string == "None":
        return None
    else:
        print("Undefined bool value!")
        exit()

