import os

# path to project root
ROOT_PATH = os.path.dirname(__file__)


# paths to resources for pipeline components

RESOURCES_PATH = os.path.join(ROOT_PATH, "resources")

global ATTRIBUTION_PATH
ATTRIBUTION_PATH = os.path.join(RESOURCES_PATH, "attribution")

global DIMLEX_PATH
DIMLEX_PATH = os.path.join(RESOURCES_PATH, "dimlex")

global DISCOURSE_SEGMENTATION_PATH
DISCOURSE_SEGMENTATION_PATH = os.path.join(RESOURCES_PATH, "discourse_segmentation")

global EVENT_CLASSIFICATION_PATH
EVENT_CLASSIFICATION_PATH = os.path.join(RESOURCES_PATH, "event-classification")

global FLEXION_PATH
FLEXION_PATH = os.path.join(RESOURCES_PATH, "flexion")

global HEIDELTIME_PATH
HEIDELTIME_PATH = os.path.join(RESOURCES_PATH, "heideltime")

global NORMALISATION_PATH
NORMALISATION_PATH = os.path.join(RESOURCES_PATH, "normalisation")

global NRC_EMOTIONS_PATH
NRC_EMOTIONS_PATH = os.path.join(RESOURCES_PATH, "NRC-Emotion-Lexicon-v0.92")

global PARSING_PATH
PARSING_PATH = os.path.join(RESOURCES_PATH, "parsing")

global REFLECTION_PATH
REFLECTION_PATH = os.path.join(RESOURCES_PATH, "reflective-passages-identification-bert")

global SENTIMENT_TAGGER_PATH
SENTIMENT_TAGGER_PATH = os.path.join(RESOURCES_PATH, "data")

global SPACE_NOUNS_PATH
SPACE_NOUNS_PATH = os.path.join(RESOURCES_PATH, "space_nouns")

global WIKTIONARY_TOOLS_PATH
WIKTIONARY_TOOLS_PATH = os.path.join(RESOURCES_PATH, "wiktionary_tools")


# paths to external resources

global GERMANET_PATH
GERMANET_PATH = os.path.join(ROOT_PATH, "..", "germanet")

global WIKIDATA_JSON_DUMP_PATH
WIKIDATA_JSON_DUMP_PATH = os.path.join(ROOT_PATH, "..", "latest-all.json.bz2")


# paths to external components

global NLTK_PATH
NLTK_PATH = os.path.join(ROOT_PATH, "nltk_data")


# paths to texts

global CORPUS_PUBLIC_PATH
global CORPUS_PUBLIC_VERSION
CORPUS_PUBLIC_VERSION = "-v4.1"
CORPUS_PUBLIC_PATH = os.path.join(ROOT_PATH, "..", "korpus-public" + CORPUS_PUBLIC_VERSION)

global PICKLE_PATH
PICKLE_PATH = os.path.join(ROOT_PATH, "pickles")
