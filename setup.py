from setuptools import setup, find_packages
setup(
    name='pipy-public',
    version='3.1.0',
    packages=find_packages(include=['pipy-public', 'pipy-public.*'])
)