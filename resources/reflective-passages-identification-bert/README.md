This code and models (in `output/saved_models`) were downloaded from [https://github.com/tschomacker/generalizing-passages-identification-bert/releases/tag/v0.3.0](https://github.com/tschomacker/generalizing-passages-identification-bert/releases/tag/v0.3.0).
The code is licensed under MIT.
