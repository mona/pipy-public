This code was developed in the event project and the data was downloaded from [https://github.com/uhh-lt/event-classification/releases/tag/v0.2](https://github.com/uhh-lt/event-classification/releases/tag/v0.2).
The code is licensed under MIT.

For our implementation we commented out the lines 19-21 in `event_classify/datasets.py`.
