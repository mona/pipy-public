from datetime import datetime
from faulthandler import disable
from interruptingcow import timeout
from statistics import mean

import argparse
import ast
import csv
import json
import multiprocessing as mp
import numpy as np
import os
import pandas as pd
import re
import socket
import sys
import spacy
import time
import traceback

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.reflection_tagger import neural_reflection_tagger
from pipeline.global_constants import TORCH_DEVICE, SPACY_MAX_LENGTH
from settings import PARSING_PATH, PICKLE_PATH, ROOT_PATH

from corpusreader import Corpus

COLUMNS = ( "author_forename",
            "author_surname",
            "title_main",
            "title_sub",
            "filepath", 
            "year", 
            "text_length_sents",
            "text_length_clauses",
            "text_length_tokens", 
            "reflective_passages_global",
            "reflective_clauses",
            "reflective_clauses_new",
            "reflective_clauses_scored",
            "reflective_tokens",
            "reflection_score_mean",
            "gi_passages_global",
            "gi_clauses",
            "gi_tokens",
            "nfr_passages_global",
            "nfr_clauses",
            "nfr_tokens",
            "comment_passages_global",
            "comment_clauses",
            "comment_tokens",
            "exception",
            "setting"
            )



def df_line(kolimo_object, timeout_setting, enable_setting, max_units_setting) -> dict:
    print("current text: '" + str(kolimo_object.title_main_source) + "'")
    print("length of text: " + str(len(kolimo_object.text)))
    print("filepath: '" + str(kolimo_object.filepath) + "'")
    t1 = time.time()
    try:
        with timeout(timeout_setting, exception=RuntimeError):

            model = os.path.join(PARSING_PATH, "de_ud_lg")
            nlp = spacy.load(model, disable=["ner"])
            nlp.max_length = SPACY_MAX_LENGTH

            def pw(doc, func, **kwargs):
                return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)

            pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=max_units_setting)
            nlp.add_pipe(pickle_init_, name="pickle_init", before="tagger")

            dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
            nlp.add_pipe(dependency_clausizer_, name="clausizer")

            neural_reflection_tagger_ = lambda doc: pw(doc, neural_reflection_tagger, label_condition="multi")
            nlp.add_pipe(neural_reflection_tagger_, name="reflection_tagger")
            doc = nlp(kolimo_object.text)

            #calculate reflection scores: append `reflective_clauses_scored` and `reflection_score_mean`
            reflection_scores = list()
            reflective_clauses_new = 0.0
            reflective_clauses_scored = 0.0
            for clause in doc._.clauses:
                labels = set()
                if (clause._.rps is not None) and len(clause._.rps) > 0:
                    for rp in clause._.rps:
                        labels.update(rp.tags)
                gi, nfr, comment = 0.0, 0.0, 0.0
                if 'RP' in labels:
                    print("Classifier used in binary mode, use multi-label mode!")
                    exit()
                if 'GI' in labels or "Nichtfiktional" in labels or 'Comment' in labels:
                    reflective_clauses_new += 1
                if 'GI' in labels:
                    gi = 1.0
                if "Nichtfiktional" in labels:
                    nfr = 1.0
                if 'Comment' in labels:
                    comment = 1.0
                reflection_score = calculate_reflection_score(gi=gi, nfr=nfr, comment=comment)
                if reflection_score >= 0.5:
                    reflective_clauses_scored += 1
                reflection_scores.append(reflection_score)
            reflection_score_mean = mean(reflection_scores)

            t2 = time.time()
            print(f"time for '{kolimo_object.author_source}', '{kolimo_object.title_main_source}':", t2-t1)
            print("\n\n")
            return {
                            "author_forename": str(kolimo_object.author_forename_source),
                            "author_surname": str(kolimo_object.author_surname_source),
                            "title_main": str(kolimo_object.title_main_source),
                            "title_sub": str(kolimo_object.title_sub_source), 
                            "year": str(kolimo_object.first_publication),
                            "filepath": str(kolimo_object.filepath),
                            "text_length_sents": len(list(doc.sents)),
                            "text_length_clauses": len(doc._.clauses),
                            "text_length_tokens": len(doc),
                            "reflective_passages_global": len(doc._.rps),
                            "reflective_clauses": len(set([clause for passage in doc._.rps for clause in passage.clauses])),
                            "reflective_tokens": len(set([token for passage in doc._.rps for token in passage.tokens])),
                            "gi_passages_global": len(set([passage for passage in doc._.rps if "GI" in passage.tags])),
                            "gi_clauses": len(set([clause for passage in doc._.rps if "GI" in passage.tags for clause in passage.clauses])),
                            "gi_tokens": len(set([token for passage in doc._.rps if "GI" in passage.tags for token in passage.tokens])),
                            "nfr_passages_global": len(set([passage for passage in doc._.rps if "Nichtfiktional" in passage.tags])),
                            "nfr_clauses": len(set([clause for passage in doc._.rps if "Nichtfiktional" in passage.tags for clause in passage.clauses])),
                            "nfr_tokens": len(set([token for passage in doc._.rps if "Nichtfiktional" in passage.tags for token in passage.tokens])),
                            "comment_passages_global": len(set([passage for passage in doc._.rps if "Comment" in passage.tags])),
                            "comment_clauses": len(set([clause for passage in doc._.rps if "Comment" in passage.tags for clause in passage.clauses])),
                            "comment_tokens": len(set([token for passage in doc._.rps if "Comment" in passage.tags for token in passage.tokens])),
                            "reflection_score_mean": reflection_score_mean,
                            "reflective_clauses_scored": reflective_clauses_scored,
                            "reflective_clauses_new": reflective_clauses_new,
                            "exception": "",
                            "setting": ""
                            }

    except Exception as e:
        #traceback.print_exc()
        # input()
        t2 = time.time()
        print("time for '%s'"%kolimo_object.title_main_source, t2-t1)
        print("Exception:" + str(e))
        print("Exception for: %s. See above."%kolimo_object.title_main_source)
        print("\n\n")
        return {
                        "author_forename": str(kolimo_object.author_forename_source),
                        "author_surname": str(kolimo_object.author_surname_source),
                        "title_main": str(kolimo_object.title_main_source),
                        "title_sub": str(kolimo_object.title_sub_source), 
                        "year": str(kolimo_object.first_publication),
                        "filepath": str(kolimo_object.filepath),
                        "text_length_sents": "",
                        "text_length_clauses": "",
                        "text_length_tokens": "",
                        "reflective_passages_global": "",
                        "reflective_clauses": "",
                        "reflective_tokens": "",
                        "gi_passages_global": "",
                        "gi_clauses": "",
                        "gi_tokens": "",
                        "nfr_passages_global": "",
                        "nfr_clauses": "",
                        "nfr_tokens": "",
                        "comment_passages_global": "",
                        "comment_clauses": "",
                        "comment_tokens": "",
                        "reflection_score_mean": "",
                        "exception": str(e),
                        "setting": ""
                        }

def arg_to_bool(arg):
    if not isinstance(arg, str):
        return arg
    elif arg.lower() in ["true", "1", "yes"]:
        return True
    elif arg.lower() in ["false", "0", "no"]:
        return False
    else:
        return False

def sigmoid(x):
    ex = np.exp(x)
    return ex / (1 + ex)

def calculate_reflection_score(gi: float, nfr: float, comment: float) -> float:
    return sigmoid(0.7155*gi + 0.3398*nfr + 1.2862*comment - 0.6147*(gi*comment) - 0.7238)


def read_database_of_german_literature(file):
    print("reading author database...")
    with open(file, mode="r") as f:
        author_database = list()
        reader = csv.DictReader(f, delimiter=";")
        for row in reader:
            author_item = dict()
            try:
                author_item["author_forename"] = re.search(r".*,(.*)", row["author"]).group(1).strip().strip("[]")
                author_item["author_surname"] = re.search(r"(.*),.*", row["author"]).group(1).strip().strip("[]")
            except:
                author_item["author_forename"] = None
                author_item["author_surname"] = None

            author_item["title"] = row["title"]
            author_item["subtitle"] = row["subtitle"]
            author_item["date"] = row["date"]

            author_database.append(author_item)
        print("finished author database.")

    return author_database

