from datetime import datetime
from faulthandler import disable
from interruptingcow import timeout
from joblib import Parallel, delayed
from logging import exception
from statistics import mean

import argparse
import ast
import csv
import json
import multiprocessing as mp
import numpy as np
import os
import pandas as pd
import re
import socket
import sys
import time
import traceback

sys.path.append(os.path.join(os.path.dirname(__file__), "../../.."))
from pipeline import pipeline as pipelineX
from pipeline.global_constants import TORCH_DEVICE
from settings import ROOT_PATH

sys.path.append(ROOT_PATH)
from corpusreader import Corpus

from functions import *

SETTING = {
    "enable": ['tagger', 'slicer', 'sentencizer', 'parser', 'clausizer', 'reflection_tagger'],
    "kolimo_slice": slice(0, None),
    "max_units": 999999999999,
    "time": None,
    "timeout": 60*60*72, # 72 hours timeout per text, edit if necessary
}

if __name__ == '__main__':
    t_start = time.time()

    kolimo_list = Corpus.read_kolimo(tg=True,
                                    gb=True,
                                    request_dnb=False, 
                                    request_wikidata=False,
                                    verbose = False,
                                    slice_ = SETTING["kolimo_slice"]
                                    )
    
    # read text selection
    selection_file = open(os.path.join(ROOT_PATH, "experiments", "jcls_2024", "reflective_passages_kolimo", "selection_reduced_columns.csv"), mode="r")
    selection_dicts = [dic for dic in csv.DictReader(f=selection_file, delimiter = ';')]
    selection_file.close()

    # set path for output csv file
    file_path = os.path.join(ROOT_PATH, "experiments", "jcls_2024", "reflective_passages_kolimo", "output", "reflections_selection.csv")

    # create output csv file with header if not existing
    if not os.path.exists(file_path):
        with open(file_path, mode="w") as file:
            writer = csv.DictWriter(file, fieldnames = COLUMNS, delimiter = ';')
            writer.writeheader()

    # open output csv file (`reflections_selection.csv`) if already existing
    # and read contents to list of dict (`existing_dicts`)
    existing_file = open(file_path, mode="r")
    existing_dicts = [dic for dic in csv.DictReader(f=existing_file, delimiter = ';')]
    existing_file.close()

    str_or_none = lambda x: x if x != "None" else None
    for selection_dict in selection_dicts:
        # skip already processed texts (identifier by author forename+surname and title main+sub)
        already_processed = False
        for existing_dict in existing_dicts:
            if str_or_none(selection_dict["author_forename"]) == str_or_none(existing_dict["author_forename"]) and \
                str_or_none(selection_dict["author_surname"]) == str_or_none(existing_dict["author_surname"]) and \
                str_or_none(selection_dict["title_main"]) == str_or_none(existing_dict["title_main"]) and \
                str_or_none(selection_dict["title_sub"]) == str_or_none(existing_dict["title_sub"]):
                already_processed = True
        if already_processed:
            continue

        line_dic = None
        for kolimo_object in kolimo_list:
            if kolimo_object.author_forename_source == str_or_none(selection_dict["author_forename"]) and \
                kolimo_object.author_surname_source == str_or_none(selection_dict["author_surname"]) and \
                kolimo_object.title_main_source == str_or_none(selection_dict["title_main"]) and \
                kolimo_object.title_sub_source == str_or_none(selection_dict["title_sub"]):
                line_dic =  df_line(kolimo_object=kolimo_object,
                                    timeout_setting=SETTING["timeout"],
                                    max_units_setting=SETTING["max_units"],
                                    enable_setting=SETTING["enable"])
        if line_dic is None:
            line_dic = {key: val for key, val in selection_dict.items() if key in COLUMNS}
        existing_file = open(file_path, mode="a")
        writer = csv.DictWriter(existing_file, fieldnames = COLUMNS, delimiter = ';')
        writer.writerow(line_dic)
        existing_file.close()

    existing_file = open(file_path, mode="a")
    writer = csv.DictWriter(existing_file, fieldnames = COLUMNS, delimiter = ';')
    t_final = time.time() - t_start
    SETTING["time"] = t_final
    writer.writerow({"setting": SETTING})
    existing_file.close()
    exit()