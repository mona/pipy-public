import csv
import numpy as np
import os
import pandas as pd
import statsmodels.api as sm

from scipy.stats import ttest_ind, spearmanr
from sklearn import linear_model, metrics
from sklearn.feature_selection import SelectKBest, chi2, f_regression
from statistics import mean


reflection_values = {
    "trifft zu": 5,
    "trifft eher zu": 4,
    "teils-teils": 3,
    "trifft eher nicht zu": 2,
    "trifft nicht zu": 1
}

question_string = "Trifft folgende Aussage Ihrer Meinung nach zu? “In der markierten Textpassage wird über etwas reflektiert.”  "
question_string_short = "Trifft folgende Aussage Ihrer Meinung nach zu?"
question_string_exception = "Trifft folgende Aussage Ihrer Meinung nach zu? “In der markierten Textpassage wird über etwas reflektiert.”     "


def read_passages(limesurvey_csv, gtable_csv, mode="rounded"):
    with open(limesurvey_csv, mode="r") as l_csv:
        df_limesurvey = pd.read_csv(l_csv)

    y = list()
    x = list()

    for passage in [colname for colname in df_limesurvey if colname.startswith(question_string_short)]:        
        ## convert pandas series to list with reflection_values instead of strings
        passage_list = [ reflection_values[i] for i in df_limesurvey[passage].tolist() if isinstance(i, str) ]
        try:
            passage_mean = mean(passage_list)
        except:
            print(passage)
            print("error")
            exit()
        if mode == "rounded":
            if passage_mean < 3:
                y.append(0)
            elif passage_mean > 3:
                y.append(1)
            else:
                print("Error")
                exit()
        elif mode == "exact":
            passage_mean_to_one = (passage_mean - 1) / (5 - 1)
            y.append(passage_mean_to_one)
        else:
            "Error: Wrong Mode"
            exit()

        passage_without_question = passage.replace(question_string, "")
        if passage_without_question.startswith(question_string_short):
            passage_without_question = passage.replace(question_string_exception, "")

        #Exceptions:
        if passage_without_question.startswith("Besser wäre es, gleich jetzt vollkommene Arbeit zu tun,"):
            # Negativbsp - Report
            x.append([0,0,0])
        elif passage_without_question.startswith("Freilich Spuren von Menschen, welche dagewesen waren"):
            # Negativbsp - Description
            x.append([0,0,0])
        elif passage_without_question.startswith("Wir kamen nach einem wahren Parforceritte an die Mündung des Rio Boxo"):
            # Negativbsp - Report
            x.append([0,0,0])
        elif passage_without_question.startswith("Auch hier siegreich, ziehen endlich zwei Ströme, die alte Donau"):
            # gi / nfr / comment
            x.append([1,1,1])
        elif passage_without_question.startswith("Er sah Franz scharf an. Auf seinem regelmäßigen, beinahe schönen Gesicht"):
            # gi / nfr / comment
            x.append([0,0,1])
        elif passage_without_question.startswith("Wenn ich zurückkomme, der Friede wieder verschafft ist"):
            # gi / nfr / comment
            x.append([0,0,1])
        elif passage_without_question.startswith("Hunderte Töne waren zu einem drahtigen Geräusch ineinander verwunden,") and len(passage_without_question) == 498:
            # gi / nfr / comment
            x.append([0,1,1])
        elif passage_without_question.startswith("Hunderte Töne waren zu einem drahtigen Geräusch ineinander verwunden,") and len(passage_without_question) == 683:
            # gi / nfr / comment
            x.append([1,1,0])
        elif passage_without_question.startswith("Hier gibt es viele Feinde und noch mehr Helfershelfer der Feinde"):
            # gi / nfr / comment
            x.append([1,0,0])
        elif passage_without_question.startswith("Niemanden habe ich in der ganzen Zeit geradezu am Eingang forschen sehen"):
            # Negativbsp - Report
            x.append([0,0,0])
        elif passage_without_question.startswith("Hat sich die Zunge verbrannt, sagte jemand zu Franz,"):
            # Negativbsp - Report
            x.append([0,0,0])
        elif passage_without_question.startswith("Der alte Stechlin schüttelte jedem die Hand und sprach ihnen aus,"):
            # core sentence not mateched, set manually
            # gi / nfr / comment
            x.append([0,1,0])
        elif passage_without_question.startswith("Tiefer Ernst und eine schwärmerische Melancholie herrschten in seiner Gemütsart"):
            # core sentence not mateched, set manually
            # gi / nfr / comment
            x.append([1,0,1])
        elif passage_without_question.startswith("Aber es geht alles auf und unter in der Welt, und es hält der Mensch"):
            # gi / nfr / comment
            x.append([0,1,0])
        else:
            with open(gtable_csv, mode="r") as g_csv:
                reader_gtable = csv.reader(g_csv, delimiter=",")
                matching_passages = [row for row in reader_gtable if passage_without_question.find(row[3]) != -1]
                if len(matching_passages) == 1:
                    if matching_passages[0][0].find("Negativbsp") != -1:
                        x.append([0,0,0])
                    else:
                        x_current = [0,0,0]
                        rps_tags = [i.strip() for i in list(matching_passages[0][0].split(","))]
                        if "GI" in rps_tags:
                            x_current[0] = 1
                        if "NfR" in rps_tags or "NfR*" in rps_tags:
                            x_current[1] = 1
                        if "Comment" in rps_tags:
                            x_current[2] = 1
                        x.append(x_current)
                else:
                    print("Error")
                    exit()

    return (x, y)


def remove_negative_cases(x, y):
    remove_indices = [i for i, item in enumerate(x) if item == [0,0,0]]
    x = [item for i, item in enumerate(x) if i not in remove_indices]
    y = [item for i, item in enumerate(y) if i not in remove_indices]

    return (x, y)


def regression(x, y):
    x, y = np.array(x), np.array(y)
    reg_variants = {
        "LinearRegression": linear_model.LinearRegression()
        # #"Ridge": linear_model.Ridge(alpha=.5),
        # "RidgeCV": linear_model.RidgeCV(alphas=np.logspace(-6, 6, 13)),
        # #"Lasso": linear_model.Lasso(alpha=0.1),
        # "LassoCV": linear_model.LassoCV(cv=5, random_state=0),
        # #"MultiTaskLasso": linear_model.MultiTaskLasso(alpha=0.1),
        # #"LassoLars": linear_model.LassoLars(alpha=.1, normalize=False),
        # "BayesianRidge": linear_model.BayesianRidge(),
        # "LogisticRegression": linear_model.LogisticRegression()
    }
    for reg in reg_variants:
        model = reg_variants[reg].fit(x, y)
        y_pred = model.predict(x)

        print(reg)
        print("~scores~")
        #print(f"default_score: {model.score(x, y)}")
        print(f"mean_squared_error: {metrics.mean_squared_error(y_true = y, y_pred = y_pred)}")
        print(f"r2_score: {metrics.r2_score(y_true = y, y_pred = y_pred)}")
        #print(metrics.accuracy_score(y_true = y, y_pred = y_pred))

        print("~parameters~")
        print(f"coefficients: {model.coef_}")
        print(f"intercept: {model.intercept_}")
        # print("~prediction~")
        # print(y_pred)
        print("\n")

def sigmoid(x):
    ex = np.exp(x)
    return ex / (1 + ex)

def logistic_regression_soft_labels(X, y, mode="exact", interaction=None):
    if interaction == "all":
        X = [[x[0], x[1], x[2], x[0]*x[1], x[0]*x[2], x[1]*x[2], x[0]*x[1]*x[2]] for x in X]
    elif interaction == "gi*comment":
        X = [[x[0], x[1], x[2], x[0]*x[2]] for x in X]
    X, y = np.array(X), np.array(y)
    X = sm.tools.add_constant(X, prepend=False)

    y = np.clip(y, 1e-8, 1 - 1e-8)   # numerical stability
    inv_sig_y = np.log(y / (1 - y))  # transform to log-odds-ratio space

    model = sm.OLS(endog=inv_sig_y,exog=X)
    model = model.fit()
    if mode == "exact":
        print(model.summary())
        #print(sigmoid(model.predict(X)))
    elif mode == "rounded":
        print(model.summary())
        #print(round(model.predict(X)))

def forward_selection(X, y):
    y = np.array(y)
    y = np.clip(y, 1e-8, 1 - 1e-8)   # numerical stability
    inv_sig_y = np.log(y / (1 - y))  # transform to log-odds-ratio space
    X = [[x[0], x[1], x[2], x[0]*x[1], x[0]*x[2], x[1]*x[2], x[0]*x[1]*x[2]] for x in X]
    remaining_coefs = set(range(len(X[0])))
    used_coefs = set()
    best_aic = None
    while len(remaining_coefs) > 0:
        best_aic_ = None
        best_new_coef = None
        for new_coef in remaining_coefs:
            used_coefs_ = set(used_coefs)
            used_coefs_.add(new_coef)
            X_ = [[x[i] for i in used_coefs_] for x in X]
            X_ = np.array(X_)
            X_ = sm.tools.add_constant(X_, prepend=False)
            model = sm.OLS(endog=inv_sig_y, exog=X_)
            model = model.fit()
            if best_aic_ is None or model.aic < best_aic_:
                best_aic_ = model.aic
                best_new_coef = new_coef
        if best_aic is None or best_aic_ < best_aic:
            best_aic = best_aic_
            used_coefs.add(best_new_coef)
            remaining_coefs.remove(best_new_coef)
            print(used_coefs, model.aic)
        else:
            break
    X = [[x[i] for i in used_coefs] for x in X]
    X = np.array(X)
    X = sm.tools.add_constant(X, prepend=False)
    model = sm.OLS(endog=inv_sig_y, exog=X)
    model = model.fit()
    print(model.summary())

def backward_elimination(X, y):
    y = np.array(y)
    y = np.clip(y, 1e-8, 1 - 1e-8)   # numerical stability
    inv_sig_y = np.log(y / (1 - y))  # transform to log-odds-ratio space
    X = [[x[0], x[1], x[2], x[0]*x[1], x[0]*x[2], x[1]*x[2], x[0]*x[1]*x[2]] for x in X]
    remaining_coefs = set()
    used_coefs = set(range(len(X[0])))
    best_aic = None
    while len(used_coefs) > 0:
        best_aic_ = None
        best_new_coef = None
        for new_coef in used_coefs:
            used_coefs_ = set(used_coefs)
            used_coefs_.remove(new_coef)
            X_ = [[x[i] for i in used_coefs_] for x in X]
            X_ = np.array(X_)
            X_ = sm.tools.add_constant(X_, prepend=False)
            model = sm.OLS(endog=inv_sig_y, exog=X_)
            model = model.fit()
            if best_aic_ is None or model.aic < best_aic_:
                best_aic_ = model.aic
                best_new_coef = new_coef
        if best_aic is None or best_aic_ < best_aic:
            best_aic = best_aic_
            used_coefs.remove(best_new_coef)
            remaining_coefs.add(best_new_coef)
            print(used_coefs, model.aic)
        else:
            break
    X = [[x[i] for i in used_coefs] for x in X]
    X = np.array(X)
    X = sm.tools.add_constant(X, prepend=False)
    model = sm.OLS(endog=inv_sig_y, exog=X)
    model = model.fit()
    print(model.summary())

def spearman_corr(reflection: list, phenomena: list):
    """
    relection: list of reflection values
    phenomena: list of with phenaoma values (gi, nfr, comment) as list
    """
    phenomena_vectors = dict()
    phenomena_vectors["gi"] = [x[0] for x in phenomena]
    phenomena_vectors["nfr"] = [x[1] for x in phenomena]
    phenomena_vectors["comment"] = [x[2] for x in phenomena]
    for phenomenon in phenomena_vectors:
        print(f"{phenomenon}: ", spearmanr(reflection, phenomena_vectors[phenomenon]))


if __name__ == '__main__':
    mode = "exact" #"rounded"
    X, y = read_passages(
                limesurvey_csv = os.path.join(os.path.dirname(__file__), "data/results-survey878751_22-12-01_vollst_Fragentext_nur_komplette_Antworten.csv"),
                gtable_csv = os.path.join(os.path.dirname(__file__), "data/Survey_Passages_Ausgewaehlte_Beispiele.csv"),
                mode=mode
            )
    forward_selection(X, y)
    backward_elimination(X, y)
    spearman_corr(reflection=y, phenomena=X)

    for gi in [0, 1]:
        for comment in [0, 1]:
            for nfr in [0, 1]:
                print(gi, comment, nfr, sigmoid(0.72*gi+0.34*nfr+1.29*comment-0.61*gi*comment-0.72))

