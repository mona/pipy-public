import math
import os
import spacy
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline import pipeline
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.coref import rb_coref
from pipeline.components.entity_linker import wikidata_entity_linker
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.slicer import max_sent_slicer
from pipeline.components.speaker_extractor import rb_speaker_extractor
from pipeline.components.speech_tagger import quotation_marks_speech_tagger
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.global_constants import CORPUS_RECORDS, SPACY_MAX_LENGTH
from pipeline.utils_classes import Entity
from settings import ROOT_PATH, CORPUS_PUBLIC_PATH, PARSING_PATH, PICKLE_PATH

sys.path.append(ROOT_PATH)
from corpusreader.corpus import Corpus, Record


def get_samples(tokens_list):
    """Convert lists to sets.
        Remove passages that are entirely contained in longer passages.

    Args:
        tokens_list (list of (list of `Token`)): List of spans (span = list of token).
    
    Returns:
        set of (frozenset of `Token`): Set of spans (span = set of tokens).
    
    """
    tokens_list = sorted(tokens_list, key=lambda tokens: len(tokens), reverse=True)
    samples = set()
    samples_tokens = set()
    for tokens in tokens_list:
        tokens = [token for token in tokens if not (token.is_punct or token.is_space)]
        tokens = frozenset(tokens)
        if len(tokens.difference(samples_tokens)) > 0:
            samples.add(tokens)
            samples_tokens.update(tokens)
    print(len(tokens_list), len(samples))
    return samples    


def overlap(samples1, samples2):
    """Returns the overlap (`fuzzy` intersection) of two sets of spans (span = set of tokens).
        Not commutative!

    Args:
        samples1 (set of (frozenset of `Token`)): Set 1.
        samples2 (set of (frozenset of `Token`)): Set 2.
    
    Returns:
        set of (frozenset of `Token`): All spans from set 1 that overlap with a span from set 2.
    
    """
    samples = set()
    for sample1 in samples1:
        for sample2 in samples2:
            if len(sample1.intersection(sample2)) > 0:
                samples.add(sample1)
                break
    return samples


if __name__ == "__main__":

    # read-in texts and metadata
    
    texts = [
        ("Dahn__Kampf_um_Rom_ab_Kapitel_2", 206),
        ("Fontane__Der_Stechlin", 375),
        ("Grimmelshausen__Der_abenteuerliche_Simplicissimus", 221),
        ("Hoffmann__Der_Sandmann", 203),
        ("Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland", 228),
        ("LaRoche__Geschichte_des_Fräuleins_von_Sternheim", 217),
        ("May__Winnetou_II", 250),
        ("Musil__Der_Mann_ohne_Eigenschaften", 207),
        ("Novalis__Die_Lehrlinge_zu_Sais", 230),
        ("Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge", 430),
    ]

    corpus = Corpus()
    for text, _ in texts:
        filepath = os.path.join(CORPUS_PUBLIC_PATH, text, text + ".xml")
        record_object = Record.read_tei_record(path_to_file=filepath, request_wikidata=True)
        corpus.add_record(record_object)

    # save the metadata as global constant
    CORPUS_RECORDS.append(corpus)
    
    # collect samples for real entities, generalising passages and non-fictional passages from all texts
    samples = {}
    samples["ent"] = set()
    samples["gen"] = set()
    samples["nfr"] = set()
    texts_with_errors = []
    for text, n in texts:
        print(text, "...")
        path = os.path.join(CORPUS_PUBLIC_PATH, text, text + ".txt")
        with open(path) as f:
            try:
                # build the pipeline

                model = os.path.join(PARSING_PATH, "de_ud_lg")
                nlp = spacy.load(model)
                nlp.max_length = SPACY_MAX_LENGTH

                def pw(doc, func, **kwargs):
                    return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH,  **kwargs)

                pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=n)
                nlp.add_pipe(pickle_init_, name="pickle_init", before="tagger")

                dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
                nlp.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

                lemma_fixer_ = lambda doc: pw(doc, lemma_fixer)
                nlp.add_pipe(lemma_fixer_, name="lemma_fixer", after="tagger")

                spacy_sentencizer_ = lambda doc: pw(doc, spacy_sentencizer)
                nlp.add_pipe(spacy_sentencizer_, name="sentencizer", before="parser")

                max_sent_slicer_ = lambda doc: pw(doc, max_sent_slicer)
                nlp.add_pipe(max_sent_slicer_, name="slicer", after="parser")

                demorphy_analyzer_ = lambda doc: pw(doc, demorphy_analyzer)
                nlp.add_pipe(demorphy_analyzer_, name="analyzer")

                dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
                nlp.add_pipe(dependency_clausizer_, name="clausizer")

                rb_tense_tagger_ = lambda doc: pw(doc, rb_tense_tagger)
                nlp.add_pipe(rb_tense_tagger_, name="tense_tagger")

                quotation_marks_speech_tagger_ = lambda doc: pw(doc, quotation_marks_speech_tagger)
                nlp.add_pipe(quotation_marks_speech_tagger_, name="speech_tagger")

                rb_speaker_extractor_ = lambda doc: pw(doc, rb_speaker_extractor)
                nlp.add_pipe(rb_speaker_extractor_, name="speaker_extractor")

                rb_coref_ = lambda doc: pw(doc, rb_coref)
                nlp.add_pipe(rb_coref_, name="coref")

                wikidata_entity_linker_ = lambda doc: pw(doc, wikidata_entity_linker)
                nlp.add_pipe(wikidata_entity_linker_, name="entity_linker")

                annotation_reader_catma_ = lambda doc: annotation_reader_catma(doc, corpus_path=CORPUS_PUBLIC_PATH)
                nlp.add_pipe(annotation_reader_catma_, name="annotation_reader")

                doc = nlp(f.read())
                print(text)
                sents = list(doc.sents)
                ggg = "GGG"
                samples["ent"].update(get_samples([ent for ent in doc.ents if ent._.entity is not None and ent._.entity.real]))
                samples["gen"].update(get_samples([annotation.tokens for annotation in doc._.annotations[ggg].get_annotations(tagset="Reflexion IV", tags=["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"])]))
                samples["nfr"].update(get_samples([annotation.tokens for annotation in doc._.annotations[ggg].get_annotations(tagset="Reflexion IV", tags=["Nichtfiktional", "Nichtfiktional+mK"])]))
            except Exception as e: 
                print(e)
                print("Error for:", text)
                texts_with_errors.append(text)
    
    # compute the conditional probabilities
    print("Errors for:", texts_with_errors)
    samples["nfr&ent"] = overlap(samples["nfr"], samples["ent"])
    samples["nfr&gen"] = overlap(samples["nfr"], samples["gen"])
    samples["nfr&(ent|gen)"] = samples["nfr&ent"].union(samples["nfr&gen"])
    samples["nfr&(ent&gen)"] = samples["nfr&ent"].intersection(samples["nfr&gen"])
    print("P(x|NfR)")
    print("x=RE", round(100.0*len(samples["nfr&ent"])/len(samples["nfr"])))
    print("x=GI", round(100.0*len(samples["nfr&gen"])/len(samples["nfr"])))
    print("x=RE|GI", round(100.0*len(samples["nfr&(ent|gen)"])/len(samples["nfr"])))
    print("x=RE&GI", round(100.0*len(samples["nfr&(ent&gen)"])/len(samples["nfr"])))
    samples["pfr&ent"] = samples["ent"].difference(overlap(samples["ent"], samples["nfr"]))
    samples["pfr&gen"] = samples["gen"].difference(overlap(samples["gen"], samples["nfr"]))
    samples["pfr&(ent|gen)"] = samples["pfr&ent"].union(samples["pfr&gen"].difference(overlap(samples["pfr&gen"], samples["pfr&ent"])))
    samples["pfr&(ent&gen)"] = overlap(samples["pfr&ent"], samples["pfr&gen"])
    print("P(NfR|x)")
    print("x=RE", round(100.0*len(samples["nfr&ent"])/len(samples["nfr&ent"].union(samples["pfr&ent"]))))
    print("x=GI", round(100.0*len(samples["nfr&gen"])/len(samples["nfr&gen"].union(samples["pfr&gen"]))))
    print("x=RE|GI", round(100.0*len(samples["nfr&(ent|gen)"])/len(samples["nfr&(ent|gen)"].union(samples["pfr&(ent|gen)"]))))
    print("x=RE&GI", round(100.0*len(samples["nfr&(ent&gen)"])/len(samples["nfr&(ent&gen)"].union(samples["pfr&(ent&gen)"]))))
    