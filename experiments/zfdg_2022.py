import numpy as np
import os
import pandas as pd
import spacy
import sys
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from evaluation.measures import *
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.coref import rb_coref
from pipeline.components.gen_tagger import clf_gen_tagger, rb_gen_tagger
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.slicer import max_sent_slicer
from pipeline.components.speaker_extractor import rb_speaker_extractor
from pipeline.components.speech_tagger import quotation_marks_speech_tagger
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.global_constants import SPACY_MAX_LENGTH
from settings import CORPUS_PUBLIC_PATH, PARSING_PATH, PICKLE_PATH

if __name__ == "__main__":
    texts_train = [
        "Dahn__Kampf_um_Rom_ab_Kapitel_2",
        "Fontane__Der_Stechlin",
        "Gellert__Das_Leben_der_schwedischen_Gräfin_von_G",
        "Hoffmann__Der_Sandmann",
        "Kafka__Der_Bau",
        "LaRoche__Geschichte_des_Fräuleins_von_Sternheim",
        "Musil__Der_Mann_ohne_Eigenschaften",
        "Novalis__Die_Lehrlinge_zu_Sais",
    ]

    texts_dev = [
        "Goethe__Die_Wahlverwandtschaften",
    ]

    texts_test = [
        "Wieland__Geschichte_des_Agathon",
    ]

    for tagger in ["rb_gen_tagger", "clf_gen_tagger"]:

        classifiers = [None]
        if tagger == "clf_gen_tagger":
            classifiers = [DecisionTreeClassifier, RandomForestClassifier]
        
        for i, classifier in enumerate(classifiers):

            # build the pipeline

            def pw(doc, func, **kwargs):
                return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)

            model = os.path.join(PARSING_PATH, "de_ud_lg")
            nlp = spacy.load(model)
            nlp.max_length = SPACY_MAX_LENGTH

            pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=700)
            nlp.add_pipe(pickle_init_, name="pickle_init", before="tagger")

            dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
            nlp.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

            lemma_fixer_ = lambda doc: pw(doc, lemma_fixer)
            nlp.add_pipe(lemma_fixer_, name="lemma_fixer", after="tagger")

            spacy_sentencizer_ = lambda doc: pw(doc, spacy_sentencizer)
            nlp.add_pipe(spacy_sentencizer_, name="sentencizer", before="parser")

            max_sent_slicer_ = lambda doc: pw(doc, max_sent_slicer)
            nlp.add_pipe(max_sent_slicer_, name="slicer", after="parser")

            demorphy_analyzer_ = lambda doc: pw(doc, demorphy_analyzer)
            nlp.add_pipe(demorphy_analyzer_, name="analyzer")

            dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
            nlp.add_pipe(dependency_clausizer_, name="clausizer")

            rb_tense_tagger_ = lambda doc: pw(doc, rb_tense_tagger)
            nlp.add_pipe(rb_tense_tagger_, name="tense_tagger")

            quotation_marks_speech_tagger_ = lambda doc: pw(doc, quotation_marks_speech_tagger)
            nlp.add_pipe(quotation_marks_speech_tagger_, name="speech_tagger")

            rb_speaker_extractor_ = lambda doc: pw(doc, rb_speaker_extractor)
            nlp.add_pipe(rb_speaker_extractor_, name="speaker_extractor")

            rb_coref_ = lambda doc: pw(doc, rb_coref)
            nlp.add_pipe(rb_coref_, name="coref")

            if classifier is None:
                # rule-based tagger
                rb_gen_tagger_ = lambda doc: pw(doc, rb_gen_tagger)
                nlp.add_pipe(rb_gen_tagger_, name="gen_tagger")
            else:
                # statistical tagger
                clf_gen_tagger_ = lambda doc: clf_gen_tagger(doc, texts_train=texts_train, classifier=classifier)
                nlp.add_pipe(clf_gen_tagger_, name="gen_tagger")
        
            annotation_reader_catma_ = lambda doc: annotation_reader_catma(doc, corpus_path=CORPUS_PUBLIC_PATH)
            nlp.add_pipe(annotation_reader_catma_, name="annotation_reader")

            print(nlp.pipe_names)

            for s, texts_eval in [
                ("dev", texts_dev), 
                ("test", texts_test)
            ]:
                eval_docs = [nlp(open(os.path.join(CORPUS_PUBLIC_PATH, name, name + ".txt")).read()) for name in texts_eval]
                print(tagger, classifier, s)
                counts, confusions = compare_gold_pred_clauses(eval_docs, "gis", "GGG", "Reflexion IV", ["BARE", "ALL", "MEIST", "EXIST", "DIV", "NEG"], exclude_ambiguous=True)
                print(confusion_matrix(confusions))
                accuracies = tagwise_accuracies(counts)
                precisions = tagwise_precisions(counts)
                recalls = tagwise_recalls(counts)
                fscores = tagwise_fscores(counts)
                print(pd.DataFrame({"A" : accuracies, "P" : precisions, "R" : recalls, "F" : fscores}))
                print()