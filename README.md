# New version available

🔴🔴🔴 **This repository contains a vintage version of MONAPipe.** 🔴🔴🔴

Find the new version with support for spaCy 3.x [here](https://gitlab.gwdg.de/text-plus-collections/mona-pipe).

You can also install it directly from [PyPI](https://pypi.org/project/monapipe).

***
***
***

# MONAPipe

This repository is associated with the project group Modes of Narration and Attribution ([MONA](https://www.uni-goettingen.de/de/mona/626918.html)). We provide natural-language-processing tools for German, implemented in Python/spaCy.

## Table of contents

* [Directory structure](#directory-structure)
* [Set-up](#set-up)
    + [Quick installation](#quick-installation)
    + [Step-by-step installation](#step-by-step-installation)
* [MONACO](#monaco)
* [Pipeline components](#pipeline-components)
    + [Tagger, Sentencizer, DependencyParser, EntityRecogniser](#tagger--sentencizer--dependencyparser--entityrecogniser)
    + [PipelinePickler](#pipelinepickler)
    + [Normalizer](#normalizer)
    + [LemmaFixer](#lemmafixer)
    + [Slicer](#slicer)
    + [Analyzer](#analyzer)
    + [Clausizer](#clausizer)
    + [TenseTagger](#tensetagger)
    + [EventTagger](#eventtagger)
    + [SpeechTagger](#speechtagger)
    + [SpeakerExtractor](#speakerextractor)
    + [Coref](#coref)
    + [EntityLinker](#entitylinker)
    + [TemponymTagger](#temponymtagger)
    + [GermanetTagger](#germanettagger)
    + [EmotionsTagger](#emotionstagger)
    + [FeatureExtractor](#featureextractor)
    + [DiscourseSegmenter](#discoursesegmenter)
    + [GenTagger](#gentagger)
    + [CommentTagger](#commenttagger)
    + [ReflectionTagger](#reflectiontagger)
    + [AttributionTagger](#attributiontagger)
    + [AnnotationReader (CATMA)](#annotationreader--catma-)
* [Example](#example)
* [How to use the PipelinePickler](#how-to-use-the-pipelinepickler)
* [How to train classifiers](#how-to-train-classifiers)
* [Agreement and evaluation measures](#agreement-and-evaluation-measures)
    + [Agreement measures](#agreement-measures)
    + [Evaluation measures](#evaluation-measures)
* [Reproducing published experiments](#reproducing-published-experiments)
    + [JCLS (2024)](#jcls-2024)
    + [ZfdG (2022)](#zfdg-2022)
    + [JCLS (2022)](#jcls-2022)
    + [LREC (2022)](#lrec-2022)
* [Licence](#licence)
* [References](#references)

## Directory structure

```
pipy-public
├── README.md                 # documentation
├── agreement                 # agreement measures
├── corpusreader              # metadata extraction methods
├── evaluation                # evaluation measures
├── experiments               # experiments from publications
├── main                      # example scripts
├── pickles                   # pickled documents (local)
├── pipeline                  # source code of the pipeline
│   ├── annotation.py         # Annotation class
│   ├── classifiers           # source code for training classifiers
│   │   ├── pickled_clfs      # pretrained classifier models
│   │   └── pickled_vecs      # pickled vectors from training (local)
│   ├── components            # source code of pipeline components
│   ├── global_constants.py   # constants for spacy models
│   ├── global_resources.py   # resource handler
│   ├── global_wordlists.py   # handcrafted wordlists
│   ├── passage.py            # Passage class
│   ├── utils_classes.py      # classes used in components
│   └── utils_methods.py      # methods used in components
├── requirements.txt          # requirements file
├── resources                 # resources from external sources
└── settings.py               # path settings
```

## Set-up

MONAPipe was developed under and tested on Python 3.7–3.8 and currently does not run on later Python versions.

You can set-up a virtual environment for your MONAPipe project as follows:

```sh
python3 -m venv env
source env/bin/activate
```

### Quick installation

Clone the repository and install the pipeline with all dependencies:

```sh
git clone https://gitlab.gwdg.de/mona/pipy-public.git
cd pipy-public
./install.sh
```

The installation may take a while, since a few GB of pre-trained models will be downloaded.

### Step-by-step installation

In case the installation script does not work (entirely), you can perform the following steps individually.

Clone the repository and install the requirements:

```sh
git clone https://gitlab.gwdg.de/mona/pipy-public.git
cd pipy-public
pip install -r requirements.txt
```

If `pip install -r requirements.txt` doesn't install any packages in your virtual environment, you can alternatively run the following:

```sh
cat requirements.txt | awk '{print "pip install "$1}' > requirements.sh
sh requirements.sh
```

Download German spaCy model:

```sh
python3 -m spacy download de_core_news_lg
```

#### DEMorphy

**Required for:** Analyzer

```sh
git clone https://github.com/DuyguA/DEMorphy
cd DEMorphy
```

Manually download `words.dg` from [https://github.com/DuyguA/DEMorphy/blob/master/demorphy/data/words.dg](https://github.com/DuyguA/DEMorphy/blob/master/demorphy/data/words.dg) or `resources/demorphy/words.dg` and replace `demorphy/data/words.dg`.

Install DEMorphy and copy the file to your site-packages (you probably have to change the path to your site-packages):

```sh
python3 setup.py install
cp demorphy/data/words.dg ../../env/lib/python3.7/site-packages/demorphy-1.0-py3.7.egg/demorphy/data/
cd ..
```

#### NeuralCoref

**Required for:** Coref

Install NeuralCoref ([v4.0.0](https://github.com/huggingface/neuralcoref/releases/tag/v4.0.0)) from source:

```sh
git clone https://github.com/huggingface/neuralcoref.git --branch v4.0.0
cd neuralcoref
python3 setup.py install
cd ..
```

#### Extended Open Multilingual Wordnet

**Required for:** Coref

Create a directory for your NLTK data in `pipy/nltk_data` (or change the `download_dir` argument to your personal NLTK data path below).

Set-up the NLTK interface in Python:

```python
import nltk
nltk.download('wordnet', download_dir='nltk_data')
nltk.download('omw', download_dir='nltk_data')
```

Download and unzip the EOMW from [http://compling.hss.ntu.edu.sg/omw/summx.html](http://compling.hss.ntu.edu.sg/omw/summx.html) or `resources/open-multilingual-wordnet`. Copy the German WordNet file to the NLTK data (you probably have to change the paths):

```sh
mkdir nltk_data/corpora/omw/deu
cp ./data/wikt/wn-wikt-deu.tab nltk_data/corpora/omw/deu/wn-data-deu.tab
```

(You can copy other WordNets by exchanging all occurrences of `deu` by another language code.)

#### Germanet

**Required for:** GermanetTagger

We use Germanet V150, which consists of the zip-Files `GermaNet_V150.zip` and `GN_V150-FreqLists.zip`. Unzip the files and save them in a into a folder `germanet`, which is stored in the same directory as `pipy-public`.

#### Gitma

**Required for:** EventTagger

Install [gitma](https://github.com/forTEXT/gitma)/[catma_gitlab](https://zenodo.org/record/5669222):

```sh
pip install git+https://github.com/forTEXT/catma_gitlab@1.0.0
```

### CBC solver

**Optional to compute Gamma agreement.**

The python package for pygamma-agreement is already installed with the requirements (otherwise run `pip install pygamma-agreement`). Additionally, install CBC Solver depending on your operating system via:

- Ubuntu/Debian:  ```sudo apt install coinor-libcbc-dev```
- Fedora: ```sudo yum install coin-or-Cbc-devel```
- Arch Linux: ```sudo pacman -S coin-or-cbc```
- Mac OS X:
    - you need Homebrew, install it via: ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
    - ```brew tap coin-or-tools/coinor```
    - ```brew install coin-or-tools/coinor/cbc pkg-config```

Then add CBC solver to Gamma package:

```sh
pip install "pygamma-agreement[CBC]"
```

If running `agreement/agreement.py` causes an import error for `pygamma_agreement`, this could be caused by this [bug](https://github.com/cvxpy/cvxpy/issues/807). The solve this issue, run the following command (you probably have to change the path to your site-packages):

```sh
install_name_tool -change @rpath/libc++.1.dylib /usr/lib/libc++.1.dylib [path-to-folder-with-env]/env/lib/python3.7/site-packages/_cvxcore.cpython-37m-darwin.so
```

## MONACO

If you want to apply the pipeline to texts from MONACO [(Barth et al., 2021)](#references), you can download the corpus [here](https://gitlab.gwdg.de/mona/korpus-public) and save `korpus-public` in the same directory as `pipy-public`.

Alternatively, you can change the variable `CORPUS_PUBLIC_PATH` in `settings.py`. If you use different versions of MONACO, you can also change the version suffix in `CORPUS_PUBLIC_VERSION` accordingly.

## Pipeline components

Below is a list of custom pipeline components for [spaCy](https://spacy.io/) (v2.3.2) models. If you don't know spaCy, we recommend having a look at the [spaCy documentation](https://v2.spacy.io/usage/processing-pipelines) first.

Some components require other components preceding them in the pipeline, which we indicate directly at the beginning of a component's section (be aware of chain dependencies); except for the Tagger, which is required for almost every component and should never be disabled.

### Tagger, Sentencizer, DependencyParser, EntityRecogniser

The default German model can be loaded with:

```python
nlp = spacy.load("de_core_news_lg")
```

The pretrained model already includes [Tokenizer](https://v2.spacy.io/usage/linguistic-features#tokenization), [Tagger](https://v2.spacy.io/usage/linguistic-features#pos-tagging) (`tagger`), [Sentencizer](https://v2.spacy.io/usage/linguistic-features#sbd) (`sentencizer`), [DependencyParser](https://v2.spacy.io/usage/linguistic-features#dependency-parse) (`parser`) and [EntityRecognizer](https://v2.spacy.io/usage/linguistic-features#named-entities) (`ner`).

The DependencyParser constructs trees with [TIGER](https://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/tiger/) dependencies. If you do not depend on TIGER dependencies, we recommend using the custom model from [Dönicke (2020)](#references), which constructs trees with [Universal Dependencies](https://universaldependencies.org/u/dep/):

```python
nlp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
```

The model was trained on the German UD treebanks. However, it has no integrated sentencizer. It is therefore necessary to add a custom sentencizer to the pipeline before the parser:

```python
from pipeline.components.sentencizer import spacy_sentencizer

nlp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
```

After piping a text, you can access e.g. tokens, lemmas, POS tags, dependency labels and named-entity types:

```python
for sent in doc.sents:
    for token in sent:
        print(token.text, token.lemma_, token.pos_, token.dep_, token.ent_type_)
```

### PipelinePickler

**Component Name(s):**
- `pickle_init`

If you want to [pickle intermediate results of pipeline components](#how-to-use-the-pipelinepickler) or if you want to use the Slicer, you have to add the `pickle_init` component:

```python
from pipeline.components.pipeline_pickler import pickle_init

pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=-1)
nlp.add_pipe(pickle_init_, name="pickle_init", before="tagger")
```

If you do not add a Slicer, `slicer` and `max_units` have no effect.

### Normalizer

**Component Name(s):**
- `dictionary_normalizer`

We provide a custom dictionary-based normalizer created from the [German Text Archive](http://www.deutschestextarchiv.de/download) (version from [2020-01-14](https://media.dwds.de/dta/download/dta_komplett_2020-01-14.zip)):

```python
from pipeline.components.normalizer import dictionary_normalizer

nlp.add_pipe(dictionary_normalizer, name="normalizer", before="tagger")
```

After piping a text, the normalised form is accessible via `token.text`, the original form is preserved in `token._.text`; analogously for the character positions (`.idx`):

```python
for token in doc:
    print(token.text, token.idx, token._.text, token._.idx)
```

### LemmaFixer

**Component Name(s):**
- `lemma_fixer`

SpaCy's built-in lemmatizer maps forms to lemmas using [this list](https://raw.githubusercontent.com/explosion/spacy-lookups-data/master/spacy_lookups_data/data/de_lemma_lookup.json). Since the list is incomplete even for frequent words such as articles and modal verbs, this component can be used to do some re-lemmatisation.

```python
from pipeline.components.lemma_fixer import lemma_fixer

nlp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
```

### Slicer

**Component Name(s):**
- `max_char_slicer` (**Requires:** PipelinePickler)
- `max_sent_slicer` (**Requires:** PipelinePickler, Sentencizer)

If you only need to process the beginning of a text, you can use this component. For this, you have to set the `max_units` parameter in the PipelinePickler to a positive integer.

The `max_char_slicer` cuts the text after the first `max_units` characters (as in the original/unnormalised text, even if you use the Normalizer); the `max_sent_slicer` cuts the text after the first `max_units` sentences (as determined by the Sentencizer).

```python
from pipeline.components.slicer import max_sent_slicer

nlp.add_pipe(max_sent_slicer, name="slicer", after="sentencizer")
```

The Slicer adds the custom attribute `doc._.fulltext`, which stores the original text, whereas `doc.text` is set to the sliced text.

### Analyzer

**Component Name(s):**
- `demorphy_analyzer`

You can use the Analyzer from [Dönicke (2020)](#references), which uses [DEMorphy](https://github.com/DuyguA/DEMorphy) [(Altinok, 2018)](#references) to perform context-insensitive morphological analysis, and agreement rules to filter out impossible analyses in context.

```python
from pipeline.components.analyzer import demorphy_analyzer

nlp.add_pipe(demorphy_analyzer, name="analyzer")
```

After piping a text, each token has a custom `morph` attribute that stores morphological analysis information of a token. To access an attribute of a token, e.g. tense, use `token._.morph.tense_` or `token._.morph.tense`. The first expression returns all possible tenses of the token in a set (the set can be empty). The second expression resturns the tense of the token if it is existing and unambiguous; otherwise it returns `None`.

```python
for token in doc:
    print(token.text, token._.morph)
```

The `morph` attribute also stores all the information from spaCy's built-in morphological analysis. The following attributes are available:

| Attribute                | Values                                           |
| ------------------------ | ------------------------------------------------ |
| **spaCy**                |                                                  |
| token._.morph.adp_type   | 'circ', 'post', 'prep'                           |
| token._.morph.aspect     | 'perf'                                           |
| token._.morph.conj_type  | 'comp'                                           |
| token._.morph.foreign    | 'yes'                                            |
| token._.morph.hyph       | 'yes'                                            |
| token._.morph.mood       | 'imp', 'ind'                                     |
| token._.morph.num_type   | 'card'                                           |
| token._.morph.part_type  | 'inf', 'res', 'vbp'                              |
| token._.morph.polarity   | 'neg'                                            |
| token._.morph.poss       | 'yes'                                            |
| token._.morph.pron_type  | 'art', 'dem', 'ind|neg|tot', 'int', 'prs', 'rel' |
| token._.morph.punct_type | 'brck', 'comm', 'peri'                           |
| token._.morph.reflex     | 'yes'                                            |
| token._.morph.verb_form  | 'fin', 'inf', 'part'                             |
| token._.morph.verb_type  | 'mod'                                            |
| **DEMorphy**             |                                                  |
| token._.morph.case       | 'acc', 'dat', 'gen', 'nom'                       |
| token._.morph.degree     | 'comp', 'pos', 'sup'                             |
| token._.morph.gender     | 'fem', 'masc', 'neut', 'noGender'                |
| token._.morph.inflection | 'inf', 'zu'                                      |
| token._.morph.mode       | 'imp', 'ind', 'subj'                             |
| token._.morph.numerus    | 'plu', 'sing'                                    |
| token._.morph.person     | '1per', '2per', '3per'                           |
| token._.morph.starke     | 'strong', 'weak'                                 |
| token._.morph.tense      | 'past', 'ppast', 'ppres', 'pres'                 |

### Clausizer

**Component Name(s):**
- `dependency_clausizer` (**Requires:** DependencyParser)
- `tiger_clausizer` (**Requires:** DependencyParser)
- `ud_clausizer` (**Requires:** DependencyParser)

Clause segmentation can be performed with the Clausizer from [Dönicke (2020)](#references).

```python
from pipeline.components.clausizer import dependency_clausizer

nlp.add_pipe(dependency_clausizer, name="clausizer")
```

The `dependency_clausizer` automatically detects whether `tiger_clausizer` or `ud_clausizer` should be used. Note that the Clausizer yields significantly better results for UD parses.

The clausizer adds clauses (`Span` objects) to the document, which can be accessed by `doc._.clauses`, `sent._.clauses` and `token._.clause`. Each clause has the following custom attributes, which each store a list of `Token` objects: `clause._.prec_punct` for preceding punctuation, `clause._.succ_punct` for succeeding punctuation and `clause._.tokens` for the tokens of a clause. Note that `clause._.tokens` and `list(clause)` may return differing lists of tokens, since a clause can be discontinuous but `Span` objects cannot.

```python
for clause in doc._.clauses:
    print(clause._.prec_punct)
    print(clause._.tokens)
    print(clause._.succ_punct)
```

### TenseTagger

**Component Name(s):**
- `rb_tense_tagger` (**Requires:** Analyzer, Clausizer)

The Tense(-Mood-Voice-Modality)Tagger from [Dönicke (2020)](#references) analyses the (complex) verb form of each clause.

```python
from pipeline.components.tense_tagger import rb_tense_tagger

nlp.add_pipe(rb_tense_tagger, name="tense_tagger")
```

After piping a text, each clause has a custom `form` attribute that stores grammatical analysis information of a clause. To access an attribute of a clause, e.g. tense, use `clause._.form.tense`.

```python
for clause in doc._.clauses:
    print(clause, clause._.form)
```

The `form` attribute has the following attributes:

| Attribute                | Type          | Values/Description                               |
| ------------------------ | ------------- | ------------------------------------------------ |
| clause._.form.aspect     | str           | 'imperf', 'perf'                                 |
| clause._.form.mode       | str           | 'imp', 'ind', 'subj:past', 'subj:pres'           |
| clause._.form.tense      | str           | 'fut', 'past', 'pres'                            |
| clause._.form.voice      | str           | 'active', 'pass', 'pass:dynamic', 'pass:static'  |
| clause._.form.verb_form  | str           | 'fin', 'inf', 'part'                             |
| clause._.form.main       | Token         | The main verb of the complex verb form.          |
| clause._.form.modals     | list of Token | All modal verbs of the complex verb form.        |
| clause._.form.verbs      | list of Token | All verbs of the complex verb form.              |

### EventTagger

**Component Name(s):**
- `event_event_tagger` (**Requires:** Clausizer)

This component is a wrapper for the [event-classification](https://github.com/uhh-lt/event-classification/) model from [Vauth et al. (2021)](#references):

```python
from pipeline.components.event_tagger import event_event_tagger

nlp.add_pipe(event_event_tagger, name="event_tagger")
```

The tagger adds a custom `event` attribute to clauses, which is a dictionary with the following keys: `event_types`, `speech_type`, `thought_representation`, `iterative`, `mental`.

```python
for clause in doc._.clauses:
    print(clause, clause._.event)
```

### SpeechTagger

**Component Name(s):**
- `flair_speech_tagger`
- `quotation_marks_speech_tagger`

We provide two custom components for speech tagging. The first one is a wrapper for the [Redewiedergabe taggers](https://github.com/redewiedergabe/tagger) from [Brunner et al. (2020)](#references), that utilise the [FLAIR NLP framework](https://github.com/flairNLP/flair):

```python
from pipeline.components.speech_tagger import flair_speech_tagger

nlp.add_pipe(flair_speech_tagger, name="speech_tagger")
```

The tagger adds a custom `speech` attribute to tokens and clauses: `token._.speech` and `clause._.speech`, both being of type `dict of str:float`. The keys of the dictionary are the speech types `"direct"`, `"freeIndirect"`, `"indirect"` and `"reported"`. For tokens, the value is the probability of the speech type. For clauses, the value is the number of tokens of that speech type in the clause plus the product of the probabilities from these tokens. (Effectively, this means that the speech type with the highest score is the most probable one for a token/clause.)

```python
for token in doc:
    print(token.text, token._.speech)
```

Although the `flair_speech_tagger` provides scores for four different speech types, it also has two disadvantages: 1) it requires a comparatively long computation time, and 2) the models were trained on historical German texts and do not always yield satisfiable results. As an alternative, we provide the `quotation_marks_speech_tagger`. This component only tags `"direct"` speech, but is comparatively fast and robust in doing so:

```python
from pipeline.components.speech_tagger import quotation_marks_speech_tagger

nlp.add_pipe(quotation_marks_speech_tagger, name="speech_tagger")
```

### SpeakerExtractor

**Component Name(s):**
- `rb_speaker_extractor` (**Requires:** EntityRecognizer, SpeechTagger)

This component extracts speaker and addressee for direct speech segments. The current implementation is very simple and can only extract explicitly mentioned speakers/addressees.

```python
from pipeline.components.speaker_extractor import rb_speaker_extractor

nlp.add_pipe(rb_speaker_extractor, name="speaker_extractor")
```

After piping a text, you can iterate over the speech segments (`Span` objects) in a document and get the speaker and addressee (also `Span` objects).

```python
for speech_segment in doc._.speech_segments:
    print(speech_segment, speech_segment._.speaker_, speech_segment._.addressee_)
```

### Coref

**Component Name(s):**
- `rb_coref` (**Requires:** DependencyParser, Analyzer, SpeakerExtractor)

The custom component for coreference resolution is based on the algorithm from [Krug et al. (2015)](#references), which was extended by [Barth & Dönicke (2021)](#references) to create coreference clusters for all noun phrases in a text and not only character mentions.

```python
from pipeline.components.coref import rb_coref

nlp.add_pipe(rb_coref, name="coref")
```

After piping a text, you can access the [same custom attributes](https://github.com/huggingface/neuralcoref#Using-NeuralCoref) that are assigned by [NeuralCoref](https://github.com/huggingface/neuralcoref)'s custom spaCy component. (So in case they develop a German model in the future, you can replace our `coref` component by theirs without getting errors in follow-up components.)

```python
for coref_cluster in doc._.coref_clusters:
    print(coref_cluster.main, coref_cluster.mentions)
```

Coreference resolution with `rb_coref` is very slow and grows in *O(n^2)*. We recommend enabling it only when needed and you should consider using the PipelinePickler and/or Slicer to reduce computation time.


### EntityLinker

**Component Name(s):**
- `wikidata_entity_linker` (**Requires:** Coref)

The EntityLinker from [Barth et al. (2022)](#references) classifies PER and LOC entities according to their fictionality status (`fictional`/`real`) and assigns a [`Wikidata`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/utils_classes.py) item if possible. 

```python
from pipeline.components.entity_linker import wikidata_entity_linker

nlp.add_pipe(wikidata_entity_linker, name="entity_linker")
```

The enriched information about the entity is stored an `Entity` object that is assigned to the `Span` object of the Spacy entity.

```python
for entity in doc.ents:
    print(entity[0]._.entity.fictional)
    print(entity[0]._.entity.real)
    print(entity[0]._.entity.wikidata.label)
```

The entity linker requires metadata in TEI/xml format for each piped text. (See our [template](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/corpusreader/template.xml) for the structure of the metadata files or MONACO for concrete examples.) Before starting to pipe texts, you should create a `Record` object for each text, bundle them in a `Corpus` object and save it in the global variable `CORPUS_RECORDS` as shown below (the for loop might look different for you; in this example, we assume a directory structure as in MONACO):

```python
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline.global_constants import CORPUS_RECORDS
from settings import ROOT_PATH, CORPUS_PUBLIC_PATH
sys.path.append(ROOT_PATH)
from corpusreader.corpus import Corpus, Record

corpus = Corpus()
for foldername in os.listdir(CORPUS_PUBLIC_PATH):
    if os.path.isdir(os.path.join(CORPUS_PUBLIC_PATH, foldername)):
        filepath = os.path.join(CORPUS_PUBLIC_PATH, foldername, foldername + ".xml")
        if os.path.exists(filepath):
            record_object = Record.read_tei_record(path_to_file=filepath, request_wikidata=True)
            corpus.add_record(record_object)

CORPUS_RECORDS.append(corpus)
```

### TemponymTagger

**Component Name(s):**
- `heideltime_temponym_tagger`

The TemponymTagger extracts and normalises temporal expressions from a document. The original algorithm presented in [Strötgen & Gertz (2010)](#references) was re-implemented for spaCy by [Barth & Dönicke (2021)](#references), using the [German resource files](https://github.com/HeidelTime/heideltime/tree/master/resources/) from [HeidelTime](https://github.com/HeidelTime/heideltime).

```python
from pipeline.components.temponym_tagger import heideltime_temponym_tagger

nlp.add_pipe(heideltime_temponym_tagger, name="temponym_tagger")
```

The temponyms (`Span` objects) in a text can be accessed through the document or the tokens:

```python
for temponym in doc._.temponyms:
    print(temponym.text, temponym._.temponym_norm)

for token in doc:
    if token._.in_temponym:
        print(token._.temponyms)
```

The custom `temponym_norm` attribute is of type `dict of str:str` with the following keys (and possible values): `"NORM_MOD"` (END/MID/START), `"NORM_QUANT"` (EVERY), `"NORM_FREQ"` (1M/1S/1W), `"NORM_VALUE"` (*normalised time expression*), `"TYPE"` (date/duration/interval/set/time).

### GermanetTagger

**Component Name(s):**
- `germanet_tagger_verbs_adjectives` (**Requires:** Clausizer)

The GermanetTagger presented in [Weimer et al. (2022)](#references) adds semantic categories from [GermaNet](http://www.sfs.uni-tuebingen.de/GermaNet/) [(Hamp & Feldweg, 1997)](#references) to main verbs and adjectives, disambiguated in context. GermaNet uses [Levin (1993)](#references)'s categories for verbs and [Hundsnurscher & Splett (1982)](#references)'s categories for adjectives.

```python
from pipeline.components.germanet_tagger import germanet_tagger_verbs_adjectives

nlp.add_pipe(germanet_tagger_verbs_adjectives, name="germanet_tagger")
```

After piping a text, `token._.synset_id` stores the ID of the GermaNet synset (if available). Additionally, `clause._.verb_synset_id` stores the ID of the clause's main verb. (`clause._.verb_synset_id` is equivalent to `clause.root._.synset_id`.) You can use the synset ID to look up the semantic category of the verb/adjective as follows:

```python
from pipeline.global_resources import GERMANET

GERMANET.provide()

for clause in doc._.clauses:
    synset_id = clause._.verb_synset_id
    if synset_id is not None:
        print(token.text, synset_id, GERMANET._.get_synset_by_id(synset_id).word_class)
    for token in clause._.tokens:
        if token.pos_ == "ADJ":
            synset_id = token._.synset_id
            if synset_id is not None:
                print(token.text, synset_id, GERMANET._.get_synset_by_id(synset_id).word_class)
```

### EmotionsTagger

**Component Name(s):**
- `nrc_emotions`

The EmotionsTagger presented in [Weimer et al. (2022)](#references) is based on the [NRC Word-Emotion Associated Lexicon](http://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.htm) [(Mohammad & Turney, 2013)](#references) and assigns scores for positive/negative sentiment and [Ekman (1992)](#references)'s basic emotions to each token.

```python
from pipeline.components.emotions_tagger import nrc_emotions

nlp.add_pipe(nrc_emotions, name="emotions_tagger")
```

The custom attribute is a dictionary that maps an emotion (`"Anger"`, `"Anticipation"`, `"Disgust"`, `"Fear"`, `"Joy"`, `"Sadness"`, `"Surprise"`, `"Trust"`) or sentiment (`"Negative"`, `"Positive"`) to a score.

```python
for token in doc:
    print(token.text, token._.emotions)
```

### FeatureExtractor

**Component Name(s):**
- `dfg_feature_extractor` (**Requires:** TenseTagger, SpeechTagger, GermanetTagger)

The FeatureExtractor assigns features to each clause. The current implementation from [Weimer et al. (2022)](#references) uses the grammatical features from [Dönicke (2021)](#references), and extends it with semantic features. (Some features, e.g. emotions, are only added if the corresponding pipeline component is enabled.)

```python
from pipeline.components.feature_extractor import dfg_feature_extractor

nlp.add_pipe(dfg_feature_extractor, name="feature_extractor")
```

After piping a text, `clause._.feats` stores a set of strings, feature-value pairs concatenated by `=`.

```python
for clause in doc._.clauses:
    print(clause, clause._.feats)
```

The extracted features are:

| Prefix         | Feature |
| -------------- | ------- |
| clause_        | dep, head, pos, punct_inner, punct_prec, punct_succ, sent_conditional, sent_start, speech |
| NP_*dep*_    | adj_category, adj_degree, adj_emotion, adj_pos, adj_sentiment, adp, art_lemma, art_pos, case, comment, deictic, dep, emotion, gender, numerus, person, pos, quant_pos, quant_type, sentiment |
| verb_          | aspect, category, comment, deictic, dep, emotion, form, mode, pos, quant_pos, quant_type, sentiment, tense, voice
| disc_*dep*_ | comment, dep, index, pos |

### DiscourseSegmenter

**Component Name(s):**
- `disrpt_discourse_segmenter` (**Requires:** FeatureExtractor)

The DiscourseSegmenter uses the model from [Dönicke (2021)](#references) to detect minimal discourse segments.

```python
from pipeline.components.discourse_segmenter import disrpt_discourse_segmenter

nlp.add_pipe(disrpt_discourse_segmenter, name="discourse_segmenter")
```

After piping a text, `clause._.seg_start` indicates whether the clause is the start of a new discourse segment:

```python
for clause in doc._.clauses:
    print(clause, clause._.seg_start)
```

### GenTagger

**Component Name(s):**
- `clf_gen_tagger` (**Requires:** TenseTagger, Coref)
- `clf_dfg_gen_tagger` (**Requires:** FeatureExtractor)
- `neural_gen_tagger` (**Requires:** Clausizer)
- `rb_gen_tagger` (**Requires:** TenseTagger, Coref)

The GenTagger labels generalising passages in a document. We provide the rule-based tagger (`rb_gen_tagger`) and the statistical tagger (`clf_gen_tagger`) from [Gödeke et al. (2022)](#references), trained (or [trainable](#how-to-train-classifiers)) on MONACO, as well as the neural tagger from [Schomacker et al. (2022)](#references).

```python
from pipeline.components.gen_tagger import neural_gen_tagger

nlp.add_pipe(neural_gen_tagger, name="gen_tagger")
```

After piping a text, you can access all generalising passages in a document or clause with `doc._.gis` or `clause._.gis`, respectively. The `gis` attribute stores a list of [`Passage`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/passage.py) objects:

```python
for passage in doc._.gis:
    print(passage.tokens, passage.tags)
```

If not instructed to do otherwise, the statistical tagger uses a default pretrained model. If you want to use another pretrained model or if you want to train your own model, see our [How-to section](#how-to-train-classifiers).

### CommentTagger

**Component Name(s):**
- `clf_dfg_comment_tagger` (**Requires:** FeatureExtractor)
- `clf_dfg_majority_comment_tagger` (**Requires:** FeatureExtractor)
- `rb_comment_tagger` (**Requires:** TenseTagger, GermanetTagger)

The CommentTagger from [Weimer et al. (2022)](#references) labels comment passages in a document. We provide a rule-based tagger (`rb_comment_tagger`) and statistical taggers (`clf_dfg_comment_tagger`, `clf_dfg_majority_comment_tagger`) trained (or [trainable](#how-to-train-classifiers)) on MONACO. In addition to GermaNet, the rule-based tagger uses features from [DiMLex](https://github.com/discourse-lab/dimlex) [(Stede, 2002)](#references).

```python
from pipeline.components.comment_tagger import clf_dfg_comment_tagger

nlp.add_pipe(clf_dfg_comment_tagger, name="comment_tagger")
```

After piping a text, you can access all comment passages in a document or clause with `doc._.comments` or `clause._.comments`, respectively. The `comments` attribute stores a list of [`Passage`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/passage.py) objects:

```python
for passage in doc._.comments:
    print(passage.tokens, passage.tags)
```

If not instructed to do otherwise, the statistical CommentTagger uses a default pretrained model. If you want to use another pretrained model or if you want to train your own model, see our [How-to section](#how-to-train-classifiers).

### ReflectionTagger

**Component Name(s):**
- `neural_reflection_tagger` (**Requires:** Clausizer)

Similarly to the GenTagger and the CommentTagger, the ReflectionTagger labels reflective passages (i.e. generalising passages, comment passages, or passages of non-fictional speech) in a document. It is a retrained version of the model from [Schomacker et al. (2022)](#references).

```python
from pipeline.components.gen_tagger import neural_reflection_tagger

nlp.add_pipe(neural_reflection_tagger, name="reflection_tagger")
```

After piping a text, you can access all reflective passages in a document or clause with `doc._.rps` or `clause._.rps`, respectively. The `rps` attribute stores a list of [`Passage`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/passage.py) objects:

```python
for passage in doc._.rps:
    print(passage.tokens, passage.tags)
```

### AttributionTagger

**Component Name(s):**
- `neural_attribution_tagger` (**Requires:** Clausizer)

The AttributionTagger labels clauses with attribution categories: `Figur` (*character*), `Erzählinstanz` (*narrator*), `Verdacht Autor` (*assumed author*). It is a retrained version of the model from [Dönicke et al. (2022)](#references).

```python
from pipeline.components.attribution_tagger import neural_attribution_tagger

nlp.add_pipe(neural_attribution_tagger, name="attribution_tagger")
```

After piping a text, `clause._.attribution` contains the attributed speakers:

```python
for clause in doc._.clauses:
    print(clause, clause._.attribution)
```

### AnnotationReader (CATMA)

**Component Name(s):**
- `annotation_reader_catma` (**Requires:** Clausizer)

If you want to add annotations made in [CATMA](https://catma.de/) to a spaCy document, you can use our AnnotationReader:

```python
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from settings import CORPUS_PUBLIC_PATH

annotation_reader_catma_ = lambda doc: annotation_reader_catma(doc, corpus_path=CORPUS_PUBLIC_PATH)
nlp.add_pipe(annotation_reader_catma_, name="annotation_reader")
```

`annotation_reader_catma` requires the argument `corpus_path`, which is the path to the directory that contains the annotation collections. If you use MONACO, you should set it to `CORPUS_PUBLIC_PATH`.

The component will automatically search the correct annotation collections for the piped text. (Therefore it is necessary that the plain text file is stored next to the annotation file, as in a CATMA export.)

After piping a text, you can access all annotations in a document, or all annotations that overlap with a clause or token, with `doc._.annotations`, `clause._.annotations` or `token._.annotations`, respectively. The `annotations` attribute is of type `dict of str:AnnotationList`. The key is the name of the annotation collection. The [`AnnotationList`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/annotation.py) stores the corresponding annotations ([`Annotation`](https://gitlab.gwdg.de/mona/pipy-public/-/blob/main/pipeline/annotation.py) objects):

```python
for anno in doc._.annotations:
    for annotation in doc._.annotations[anno]:
        print(annotation.tokens, annotation.tag)
```

## Example

Using the pipeline with custom components could look as follows (the code is also saved in `main/example.py`):

```python
import os
import spacy
import sys

# import pipeline components
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.tense_tagger import rb_tense_tagger
from settings import PARSING_PATH

# build the pipeline
nlp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
nlp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
nlp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
nlp.add_pipe(demorphy_analyzer, name="analyzer")
nlp.add_pipe(dependency_clausizer, name="clausizer")
nlp.add_pipe(rb_tense_tagger, name="tense_tagger")
print(nlp.pipe_names)

text = """Johann Wolfgang Goethe
Die Wahlverwandtschaften
Ein Roman
Erster Teil
Erstes Kapitel
Erstes Kapitel
Eduard – so nennen wir einen reichen Baron im besten Mannesalter – Eduard hatte in seiner Baumschule die schönste Stunde eines Aprilnachmittags zugebracht, um frisch erhaltene Pfropfreiser auf junge Stämme zu bringen. Sein Geschäft war eben vollendet; er legte die Gerätschaften in das Futteral zusammen und betrachtete seine Arbeit mit Vergnügen, als der Gärtner hinzutrat und sich an dem teilnehmenden Fleiße des Herrn ergetzte.
"""

# parse the text
doc = nlp(text)

# iterate over sentences, clauses and tokens
for j, sent in enumerate(doc.sents):
    for k, clause in enumerate(sent._.clauses):
        for i, token in enumerate(clause._.tokens):
            print(j, k, i, token.text, token.lemma_, token.pos_, token.dep_, token._.clause._.form.tense)
```

An extended example with more components is saved in `main/example2.py`.

## How to use the PipelinePickler

Since some pipeline components (e.g. DependencyParser, Coref) require a certain amount of computation time, especially for long texts, we provide the possibility to pickle intermediate results of pipeline components to avoid unnecessary re-computation.

First, make sure that you added the pipeline pickler (`pickle_init`) to your pipeline.

Second, you may define a pickle wrapper for your pipeline (usually, the parameters of `pickle_wrapper` are the same for every use, which is why we recommend creating a shortcut function like `pw`):

```python
from pipeline.components.pipeline_pickler import pickle_wrapper
from settings import PICKLE_PATH

def pw(doc, func, **kwargs):
    return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)
```

If `load_output=True`, intermediate results are loaded if they have been calculated before. If `save_output=True`, intermediate results are saved if they are calculated for the first time (`overwrite=False`) or every time (`overwrite=True`). `pickle_path` is the location where the intermediate results should be saved. You can use the `PICKLE_PATH` from `settings.py` for this.

Finally, you can wrap your pipeline components into the pickle wrapper before adding them to the pipeline:

```python
dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
nlp.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

# ...

annotation_reader_catma_ = lambda doc: pw(doc, annotation_reader_catma, corpus_path=CORPUS_PUBLIC_PATH)
nlp.add_pipe(annotation_reader_catma_, name="annotation_reader")
```

When a text is piped, wrapped components are only executed if (`load_output=False` or) there are no *matching* intermediate results found in `PICKLE_PATH`. Otherwise, the current `doc` is replaced by the loaded intermediate results. Intermediate results *match* the current step in the pipeline call if and only if they have been created under identical conditions with respect to the piped text, the spaCy model, the slicer, the `max_units` as well as the source code of each wrapped component preceding and including the current component.

## How to train classifiers

Some pipeline components can be trained on MONACO. Possible parameters vary between components -- therefore you should always have a look in the documentation inside the component's source code --, but the general procedure is always similar. We illustrate the training process by the example of the `clf_dfg_comment_tagger`.

For training, the pipeline has to be constructed and called as usual. For the `clf_dfg_comment_tagger` component, we set the optional training paramters:

```python
from sklearn.tree import DecisionTreeClassifier

texts_train = [
    "Dahn__Kampf_um_Rom_ab_Kapitel_2",
    "Fontane__Der_Stechlin",
    "Gellert__Das_Leben_der_schwedischen_Gräfin_von_G",
    "Hoffmann__Der_Sandmann",
    "Kafka__Der_Bau",
    "LaRoche__Geschichte_des_Fräuleins_von_Sternheim",
    "Musil__Der_Mann_ohne_Eigenschaften",
    "Novalis__Die_Lehrlinge_zu_Sais"
]

texts_dev = [
    "Goethe__Die_Wahlverwandtschaften"
]

clf_dfg_comment_tagger_ = lambda doc: clf_dfg_comment_tagger(doc, texts_train=texts_train, texts_dev=texts_dev, label_condition="multi", classifier=DecisionTreeClassifier, param_grid=[{'max_depth': [5, 10, 15, 20, 25, None], 'min_samples_leaf': [1, 2, 5, 10, 15, 20]}], window=(-1, 1), disable_feats=[], enable_feats=[])
nlp.add_pipe(clf_dfg_comment_tagger_, name="comment_tagger")
```

- `texts_train` and `texts_dev` are the training set and the development set, respectively. The strings in the list refer to texts in MONACO. Set the development set to `None` if you don't want to optimise parameters.
- Comment is annotated with three tags in MONACO; the `label_condition` determines whether you want to train a multi-output (`multi`) classifier that also predicts all of these tags, or a binary (`binary`) classifier that merges the tags into a single tag and only makes any-vs-none predictions.
- `classifier` should be set to a classifier from [scikit-learn](https://scikit-learn.org).
- If you want to perform grid search on the development set for parameter optimisation, you should set the `param_grid` parameter accordingly.
- The classifiers predict labels on clause-level, i.e. they always take the features of the current clause as input. `window=(-n,m)` determines that `n` preceding and `m` succeeding clauses should be considered as well.
- Either `disable_feats` or `enable_feats` can be used for manual feature selection. If you, for example, want to exclude all sentiment features, you can set `disable_feats=["sentiment"]`. In this way, all features that contain the substring `sentiment` are excluded. If both lists are empty, all features are included.

**Caution:** The PipelinePickler should not be used on a component that is currently trained (or you should set `load_output=False` for this component), but you can still use it on the preceding components.

## Agreement and evaluation measures

### Agreement measures

After reading annotations with our AnnotationReader, you can use the methods in `agreement/agreement.py` to calculate inter-annotator agreement. You can calculate Alpha [(Krippendorff, 1980)](#references), Kappa [(Fleiss et al., 1981)](#references) and Gamma [(Mathet et al., 2015)](#references):

```python
from agreement.agreement import return_krippendorff_alpha, return_fleiss_kappa, return_mathet_gamma
```

The methods take the same parameters, exemplarily for Kappa:

```python
print(return_fleiss_kappa(doc, searched_tags, tagset, annotators))
```

- The first argument is the document for which you want to calculate agreement. The agreement is calculated on token-level. If you want to calculate agreement on clause-level, you should pass `doc._.clauses` instead of `doc`. For `gamma`, you must always pass `doc._.clauses`.
- `searched_tags` is the list of tags on which you want to calculate agreement.
- `tagset` is the name of the tagset.
- `annotators` is the list of names of annotation collections that you want to calculate agreement for.


### Evaluation measures

If you want to compare the predictions of a pipeline component with manual annotation, you can use the methods in `evaluation/measures.py`. The pipeline component has to assign `Passage` objects (as e.g. the CommentTagger) and the annotations have to be `Annotation` objects (as read by the AnnotationReader).

```python
from evaluation.measures import compare_gold_pred_tokens, compare_gold_pred_clauses, confusion_matrix, tagwise_accuracies, tagwise_precisions, tagwise_recalls, tagwise_fscores
```

First, count true positives, true negative, false positives and false negatives. Depending on whether you want to do the evaluation on token-level or clause-level, you should use `compare_gold_pred_tokens` or `compare_gold_pred_clauses`:

```python
counts, confusions = compare_gold_pred_clauses([doc], attribute, annotator, tagset, tags)
```

- The first argument is the list of documents for which you want to apply evaluation measures. (The list may contain only one document.)
- `attribute` is the custom attribute that is set by the pipeline component that makes the predictions. For example, the CommentTagger sets the attribute `comments` (which provides a list of `Passage` objects for each clause).
- `annotator` is the name of the annotation collection that you want to compare the predictions with.
- `tagset` is the name of the tagset.
- `tags` is the list of tags that you want to include.

You can plot the confusions in a confusion matrix:

```python
print(confusion_matrix(confusions))
```

Note that confusion matrices only work for single-label problems. For multi-label problems, all samples that are labelled with more than one tag are treated to be tagged as `multiple`, regardless of the tag-combination.

You can calculate precision, recall and fscore for each tag as well as the micro- and the macro-average:

```python
import pandas as pd

precisions = tagwise_precisions(counts)
recalls = tagwise_recalls(counts)
fscores = tagwise_fscores(counts)
print(pd.DataFrame({"P" : precisions, "R" : recalls, "F" : fscores}))
```

## Reproducing published experiments

In this section, we provide scripts to reproduce experiments from published papers.

### JCLS (2024)

To reproduce the detection of reflective passages you can run the following script. Please notice that you need to provide the KOLIMO corpus from our external ressource and place it at the same level as the repo `pipy-public` in order to run the script.
Please keep also in mind that the calculation with the neural classifier can take very long for the selection of the KOLIMO corpus (10k texts) and should be performed on a machine with CUDA support. If you can't make the calculation, we provide our result in `experiments/jcls_2024/reflective_passages_kolimo/output/reflections_selection_2023-06-06.csv`.

```sh
python experiments/jcls_2024/reflective_passages_kolimo/kolimo_reflections_selection.py
```

You can reproduce the regression calculation with the following script to obtain the reflection scores presented in the paper. 

```sh
python experiments/jcls_2024/regression/regression.py
```

### ZfdG (2022)

For [Gödeke et al. (2022)](#references), we used [MONACO v1.1](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v1.1). If you want to reproduce our experiments, you can run the following script:

```sh
python experiments/zfdg_2022.py
```

### JCLS (2022)

For [Weimer et al. (2022)](#references), we used [MONACO v2.0](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v2.0). Unfortunately, the texts `Mann__Der_Zauberberg` and `Seghers__Das_siebte_Kreuz` are not in the public domain, yet. If you want to reproduce our experiments, you can contact us to get information about where to get these texts. After placing them in the corresponding directory, you can run the following script:

```sh
python experiments/jcls_2022.py
```

### LREC (2022)

For [Barth et al. (2022)](#references), we used 10 texts from [MONACO v4.0](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v4.0). If you want to reproduce our experiments, you can run the following script:

```sh
python experiments/lrec_2022.py
```

## Licence

This work is licensed under a Creative Commons Attribution 4.0 International License.

If you use this code, you should cite our paper:

> Tillmann Dönicke, Florian Barth, Hanna Varachkina, and Caroline Sporleder (2022). [MONAPipe: Modes of Narration and Attribution Pipeline for German Computational Literary Studies and Language Analysis in spaCy](https://aclanthology.org/2022.konvens-1.2/). In Proceedings of the 18th Conference on Natural Language Processing (KONVENS 2022).

In addition, you should cite each paper that has contributed to the parts of the code that you use. We reference all contributing works in the documentation above.

## References

> Duygu Altinok (2018). [DEMorphy, German Language Morphological Analyzer](https://arxiv.org/ftp/arxiv/papers/1803/1803.00902.pdf). arXiv:1803.00902.

> Florian Barth and Tillmann Dönicke (2021). [Participation in the KONVENS 2021 Shared Task on Scene Segmentation Using Temporal, Spatial and Entity Feature Vectors](http://ceur-ws.org/Vol-3001/paper4.pdf). In Proceedings of the Shared Task on Scene Segmentation.

> Florian Barth, Tillmann Dönicke, Benjamin Gittel, Luisa Gödeke, Anna Mareike Hofmann, Anke Holler, Caroline Sporleder, and Hanna Varachkina (2021). [MONACO: Modes of Narration and Attribution Corpus](https://gitlab.gwdg.de/mona/korpus-public). URL: `https://gitlab.gwdg.de/mona/korpus-public`

> Florian Barth, Hanna Varachkina, Tillmann Dönicke, and Luisa Gödeke (2022). [Levels of Non-Fictionality in Fictional Texts](http://www.lrec-conf.org/proceedings/lrec2022/workshops/ISA-18/pdf/2022.isa18-1.4.pdf). In Proceedings of The Eighteenth Joint ACL - ISO Workshop on Interoperable Semantic Annotation.

> Beth Levin (1993). *English verb classes and alternations: A preliminary Investigation*.

> Annelen Brunner, Ngoc Duyen Tanja Tu, Lukas Weimer, Fotis Jannidis (2020). [To BERT or not to BERT – Comparing contextual embeddings in a deep learning architecture for the automatic recognition of four types of speech, thought and writing representation](http://ceur-ws.org/Vol-2624/paper5.pdf). In Proceedings of the 5th Swiss Text Analytics Conference (SwissText) & 16th Conference on Natural Language Processing (KONVENS).

> Tillmann Dönicke (2020). [Clause-Level Tense, Mood, Voice and Modality Tagging for German](https://aclanthology.org/2020.tlt-1.1.pdf). In Proceedings of the 19th Workshop on Treebanks and Linguistic Theories.

> Tillmann Dönicke (2021). [Delexicalised Multilingual Discourse Segmentation for DISRPT 2021 and Tense, Mood, Voice and Modality Tagging for 11 Languages](https://aclanthology.org/2021.disrpt-1.4.pdf). In Proceedings of the 2nd Shared Task on Discourse Relation Parsing and Treebanking (DISRPT 2021).

> Tillmann Dönicke, Hanna Varachkina, Anna M. Weimer, Luisa Gödeke, Florian Barth, Benjamin Gittel, Anke Holler, and Caroline Sporleder (2022). [Modelling Speaker Attribution in Narrative Texts With Biased and Bias-Adjustable Neural Networks](https://www.frontiersin.org/articles/10.3389/frai.2021.725321/full). Frontiers in Artificial Intelligence.

> Paul Ekman (1992). [An argument for basic emotions](https://www.tandfonline.com/doi/pdf/10.1080/02699939208411068). Cognition & emotion, 6(3-4).

> Joseph L. Fleiss, Bruce Levin, and Myunghee Cho Paik (1981). *The measurement of interrater agreement*. Statistical methods for rates and proportions.

> Luisa Gödeke, Florian Barth, Tillmann Dönicke, Anna Mareike Weimer, Hanna Varachkina, Benjamin Gittel, Anke Holler, and Caroline Sporleder (to appear). [Generalisierungen als literarisches Phänomen. Charakterisierung, Annotation und automatische Erkennung](https://zfdg.de/2022_010). Zeitschrift für digitale Geisteswissenschaften.

> Birgit Hamp and Helmut Feldweg (1997). [GermaNet – a Lexical-Semantic Net for German](https://www.aclweb.org/anthology/W97-0802.pdf). Automatic information extraction and building of lexical semantic resources for NLP applications.

> Franz Hundsnurscher and Jochen Splett (1982). *Semantik der Adjektive des Deutschen. Analyse der semantischen Relationen*.

> Klaus Krippendorff (1980). *Content analysis: An introduction to its methodology*.

> Markus Krug, Frank Puppe, Fotis Jannidis, Luisa Macharowsky, Isabella Reger, and Lukas Weimar (2015). [Rule-based Coreference Resolution in German Historic Novels](https://aclanthology.org/W15-0711.pdf). In Proceedings of the Fourth Workshop on Computational Linguistics for Literature.

> Yann Mathet, Antoine Widlöcher, and Jean-Philippe Métivier (2015). [The Unified and Holistic Method Gamma (γ) for Inter-Annotator Agreement Measure and Alignment](https://doi.org/10.1162/COLI\_a\_00227). Computational Linguistics, 41(3).

> Saif Mohammad and Peter Turney (2013). [Crowdsourcing a Word-Emotion Association Lexicon](https://arxiv.org/pdf/1308.6297.pdf). Computational Intelligence, 29(3).

> Thorben Schomacker, Tillmann Dönicke, and Marina Tropmann-Frick (2022). [Automatic Identification of Generalizing Passages in German Fictional Texts using BERT with Monolingual and Multilingual Training Data](https://zenodo.org/record/6979859). Extended abstract submitted and accepted for the KONVENS 2022 Student Poster Session.

> Manfred Stede (2002). [DiMLex: A Lexical Approach to Discourse Markers](https://www.ling.uni-potsdam.de/~stede/Papers/lenci02.pdf). In: A. Lenci, V. Di Tomaso (eds.): Exploring the Lexicon - Theory and Computation.

> Jannik Strötgen and Michael Gertz (2010). [Heideltime: High quality rule-based extraction and normalization of temporal expressions](https://www.aclweb.org/anthology/S10-1071.pdf). In Proceedings of the 5th International Workshop on Semantic Evaluation.

> Michael Vauth, Hans Ole Hatzel, Evelyn Gius, and Chris Biemann (2021). [Automated Event Annotation in Literary Texts](http://ceur-ws.org/Vol-2989/short_paper18.pdf). Computational Humanities Research Conference (CHR 2021).

> Anna Mareike Weimer, Florian Barth, Tillmann Dönicke, Luisa Gödeke, Hanna Varachkina, Anke Holler, Caroline Sporleder, and Benjamin Gittel (to appear). [The (In-)Consistency of Literary Concepts. Operationalising, Annotating and Detecting Literary Comment](https://jcls.io/article/id/90/). Journal of Computational Literary Studies.
