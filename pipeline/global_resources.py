"""This file contains global resources used in the SpaCy pipeline.
"""

import csv
import json
import os
import pickle
import re
import spacy
import sys
import xml.etree.cElementTree as ET
from flair.models import SequenceTagger
from germanetpy.germanet import Germanet
from tensorflow.keras import models
from torch import cuda
from transformers import AutoTokenizer

from .global_constants import *

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from settings import *

sys.path.append(EVENT_CLASSIFICATION_PATH)
from event_classify.util import get_model

sys.path.append(os.path.join(REFLECTION_PATH, "src"))
from ml.model_util import load_model
from xai.prediction_pipeline import PredictionPipeline


class ResourceHandler():
    """Wrapper class for resources.
        If a resource is needed, it should be loaded with `provide()`.

    """
    def __init__(self, load_func):
        """__init__ method of the class `ResourceHandler`.

        Args:
            load_func (`func`): The function which loads and returns the resource.
        
        """
        self._ = None
        self.load = load_func


    def provide(self):
        """Load the resource, if not already loaded, and save it in `self._`.
        
        """
        if self._ is None:
            self._ = self.load()


def load_attribution_model():
    """Load the pre-trained attribution model.

    Returns:
        `tensorflow.keras.model`: The model.
    
    """
    model = models.load_model(os.path.join(ATTRIBUTION_PATH, "no_encoding"))
    return model


def load_aux_verbs(only_one_form=False):
    """Returns all possible auxiliary verbs for German verbs.

    Args:
        only_one_form (boolean): True if only the more common auxiliary should be returned; False otherwise.
    
    Returns:
        dict of str:(list of str): Each key is a lemma of a verbs; each value is a list with the possible auxiliaries ("haben" and/or "sein").
    
    """
    with open(os.path.join(WIKTIONARY_TOOLS_PATH, "verb_tense.json"), "r") as f:
        aux_verbs = json.load(f)
        if only_one_form:
            aux_verbs = {verb : aux_verbs[verb][0] for verb in aux_verbs if len(aux_verbs[verb]) > 0}
        return aux_verbs


def load_dimlex_lexicon():
    """Extracts word, possible part of speech tag and the marker types from the lexicon.

    Returns:
        lexicon (list of list of list): for each discourse marker a list of three lists: 1. words contained in this discourse marker, 2. list of its possible  part of speech tags, 3. list of possible discouse marker types
    
    """
    file = os.path.join(DIMLEX_PATH, "DimLex.xml")
    lexicon = []
    with open(file, 'rt') as f:
        tree = ET.parse(file)
    root = tree.getroot()
    for el in root.findall("./entry"):
        list_of_dim = []
        n_p_s = []
        c_p_s = []
        sences = []
        w = el.get('word')
        word = re.split("\s|,\s|\s\.\.\.\s", w)
        word = [string for string in word if string != ""]
        list_of_dim.append(word)
        for inst in el.findall('./non_conn_reading/example'):
            n_p_s.append(inst.get('type'))
        for inst in el.findall('./stts/example'):
            c_p_s.append(inst.get('type'))
        pos = []
        for i in n_p_s:
            for j in c_p_s:
                if i != j:
                    pos.append(j)
        try:
            list_of_dim.append(pos)
        except:
            list_of_dim.append([])
            
        for inst in el.findall('./syn/sem/pdtb3_relation'):
            sences.append(inst.get('sense'))
        try:
            list_of_dim.append(sences)
        except:
            list_of_dim.append([])
        lexicon.append(list_of_dim)
    return lexicon


def load_disrpt_model():
    """Load DISRPT model for discourse unit segmentation.

    Returns:
        (`DecisionTreeClassifier`, `DictVectorizer`, `VarianceThreshold`): Classifier, vectorizer and feature selector of an sklearn model.
    
    """
    path_to_model = os.path.join(DISCOURSE_SEGMENTATION_PATH, "True_deu.rst.pcc_train.conllu.pickle")
    with open(path_to_model, 'rb') as f:
        clf, dv, fs, _, _ = pickle.load(f)
        return (clf, dv, fs)


def load_event_model():
    """Load the demo model for event classification.

    Returns:
        obj: Model.
        obj: Tokenizer.
    
    """
    model_path = os.path.join(EVENT_CLASSIFICATION_PATH, "demo_model")
    model, tokenizer = get_model(model_path)
    return model, tokenizer


def load_germanet():
    """Load Germanet API.

    Returns:
        `Germanet`: Germanet API.
    
    """
    data_path = os.path.join(GERMANET_PATH, "GN_V150", "GN_V150_XML")
    frequencylist_nouns = GERMANET_PATH + "/GN_V150/FreqLists/noun_freqs_decow14_16.txt"
    germanet = Germanet(data_path)
    return germanet


def load_heideltime(language="german"):
    """Load Heideltime resources.

    Returns:
        dict of str:(dict of str:str): Normalisation rules.
            The first key corresponds to the filename; the second key is the search expression; the value is the replacement.
        dict of str:(list of str): Regex patterns.
            The key corresponds to the filename; the values are search expressions.
        dict of str:(list of (dict of str:str)): Search and normalisation rules.
            The first key corresponds to the filename; the list corresponds to rules in that file; the second key corresponds to a rule property; the value is the value of that property.
    
    """
    normalization = {}
    for filename in os.listdir(os.path.join(HEIDELTIME_PATH, language, "normalization")):
        normalization_ = {}
        with open(os.path.join(HEIDELTIME_PATH, language, "normalization", filename), "r") as f:
            for line in f.read().splitlines():
                if line != "" and not line.startswith("//"):
                    line = line.split(",")
                    normalization_[line[0][1:-1]] = line[1][1:-1]
        normalization[filename[len("resources_normalization_"):-4]] = normalization_
    
    repattern = {}
    for filename in os.listdir(os.path.join(HEIDELTIME_PATH, language, "repattern")):
        repattern_ = []
        with open(os.path.join(HEIDELTIME_PATH, language, "repattern", filename), "r") as f:
            for line in f.read().splitlines():
                if line != "" and not line.startswith("//"):
                    repattern_.append(line)
        repattern[filename[len("resources_repattern_"):-4]] = repattern_
    
    rules = {}
    for filename in os.listdir(os.path.join(HEIDELTIME_PATH, language, "rules")):
        rules_ = []
        with open(os.path.join(HEIDELTIME_PATH, language, "rules", filename), "r") as f:
            for line in f.read().splitlines():
                if line.startswith("RULENAME="):
                    parts = {}
                    if line.endswith(","):
                        line = line[:-1]
                    line = line[:-1]
                    line = line.split('",')
                    for part in line:
                        part = part.split('="')
                        parts[part[0]] = part[1]
                    rules_.append(parts)
        rules[filename[len("resources_rules_"):-4]] = rules_

    return (normalization, repattern, rules)
    


def load_norm_exceptions():
    """Load a dictionary with norms of historical spellings.

    Returns:
        dict of str:(list of (dict of int:str)): A dictionary.
            The format is described here: https://spacy.io/usage/adding-languages#tokenizer-exceptions
            Example:
                {
                    "sey": [{ORTH: "sey", NORM : "sei"}],
                    "seyn": [{ORTH: "seyn", NORM : "sein"}],
                    ...
                }
    """
    with open(os.path.join(NORMALISATION_PATH, "spacy_orth.json"), "r") as f:
        spacy_orth = json.load(f)
        # Convert inner keys back to integers (since JSON converts all keys to strings):
        spacy_orth = {spell : [{int(symb) : spacy_orth[spell][i][symb] for symb in spacy_orth[spell][i]} for i in range(len(spacy_orth[spell]))] for spell in spacy_orth}
        return spacy_orth


def load_nrc_dic_german(file=os.path.join(NRC_EMOTIONS_PATH, "NRC-Emotion-Lexicon-v0.92-In105Languages-Nov2017Translations.csv"), language="German (de)", emotions= ['Positive', 'Negative', 'Anger', 'Anticipation', 'Disgust', 'Fear', 'Joy', 'Sadness', 'Surprise', 'Trust']):
    """Builds german dictionary for each German word (key) in nrc emotion lexicon with a dict
        as return value that contains emotion (key) and emotion value (value) pairs.

    Args:
        file: csv file
        language (str): searched language in nrc file
        emotions (list): emotions to consider from nrc file (default are all emotions form nrc)

    Returns:
        dict: words as key; dict as value with emotion (key) and emotion value (value) pairs
    
    """
    with open(file, newline='') as csvfile:
        nrc_reader = csv.DictReader(csvfile, delimiter=';', quotechar='|')
        nrc_dic_german = dict()
        # iterate over rows (words/emotion values) where cols == languages/emotions
        for row in nrc_reader:
            if row[language] != "NO TRANSLATION":
                #add for each german word (row) the specified emotions (cols) as key value pair to a new dic (word_dic)
                word_dic = dict()
                for emotion in emotions:
                    word_dic[emotion] = row[emotion]
                # add word_dic to nrc dictionary
                nrc_dic_german[row[language]] = word_dic

        return nrc_dic_german


def load_reflection_models():
    """Load the pre-trained reflection models.

    Returns:
        dict of str:`PredictionPipeline`: The models.
    
    """
    saved_models = {
        "gi_binary" : "binary_gbert-large_monaco_epochs_20_lamb_0.0001_None_dh_0.3_da_0.0.pt",
        "gi_multi" : "multi_gbert-large_monaco_epochs_20_lamb_0.0001_None_dh_0.3_da_0.0.pt",
        "reflexive_binary" : "reflexive_ex_mk_binary_gbert-large_monaco-ex-kleist_epochs_20_lamb_0.0001_None_dh_0.3_da_0.0.pt",
        "reflexive_multi" : "reflexive_ex_mk_multi_gbert-large_monaco-ex-kleist_epochs_20_lamb_0.0001_None_dh_0.3_da_0.0.pt"
    }
    model_labels = {
        "gi_binary" : ['generalization'],
        "gi_multi" : ['none', 'ALL', 'BARE', 'DIV', 'EXIST', 'MEIST', 'NEG'],
        "reflexive_binary" : ['reflexive'],
        "reflexive_multi" : ['gi', 'comment', 'nfr_ex_mk']
    }
    
    pretrained_model_str = "deepset/gbert-large"
    evaluation_tokenizer = AutoTokenizer.from_pretrained(pretrained_model_str)
    device = TORCH_DEVICE
    
    prediction_pipelines = {}
    for name in saved_models:
        saved_model_path = os.path.join(REFLECTION_PATH, "output", "saved_models", saved_models[name])
        labels = model_labels[name]
        fine_tuned_model = load_model(model_path = saved_model_path, device = device, petrained_model_str = pretrained_model_str, no_labels=len(labels))
        prediction_pipelines[name] = PredictionPipeline(fine_tuned_model, evaluation_tokenizer, 206, device, labels)

    return prediction_pipelines

def load_space_nouns():
    """Load a list of German nouns that denote spaces.

    Returns:
        `set of str`: Set of space nouns.
    
    """
    space_nouns = set()
    with open(os.path.join(SPACE_NOUNS_PATH, "space_nouns.txt"), "r") as f:
        for line in f:
            space_nouns.add(line.strip().lower())
    return space_nouns


def load_spacy_model():
    """Load German spacy model `de_core_news_lg`.

    Returns:
        `spacy`: Spacy model.
    
    """
    spacy_model = spacy.load("de_core_news_lg", disable=["ner"])
    spacy_model.max_length = SPACY_MAX_LENGTH
    return spacy_model


def load_spacy_model_tokenized():
    """Load German spacy model `de_core_news_lg`.
        Sets the tokenizer to `tokens_from_list`.

    Returns:
        `spacy`: Spacy model.
    
    """
    spacy_model = spacy.load("de_core_news_lg", disable=["ner"])
    spacy_model.tokenizer = spacy_model.tokenizer.tokens_from_list
    return spacy_model


def load_speech_taggers():
    """Load pre-trained "Redewiedergabe" taggers.

    Returns:
        dict of str:`SequenceTagger`: Dictionary mapping a speech type to the corresponding tagger.
            The speech types are: "indirect", "freeIndirect", "direct", "reported"
    
    """
    speech_taggers = {}
    speech_taggers["indirect"] = SequenceTagger.load('de-historic-indirect')
    speech_taggers["freeIndirect"] = SequenceTagger.load('de-historic-free-indirect')
    speech_taggers["direct"] = SequenceTagger.load('de-historic-direct')
    speech_taggers["reported"] = SequenceTagger.load('de-historic-reported')
    return speech_taggers
    

def load_verb_forms(only_one_form=False):
    """Returns all possible German verb forms (tense + aspect) and analyses.

    Args:
        only_one_form (boolean): True if only the most plausible analysis should be returned; False otherwise.
    
    Returns:
        dict of (str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str):(list of (str,str,str,str)): A dictionary mapping a form to its analyses.
            The form is expressed as a str vector:
                0: contains an infinite form of the verb? "inf": yes, "" : no
                1: contains an inflected present form of the verb? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                2: contains an inflected past form of the verb? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                3: contains a participle form of the verb? "pres" : present participle, "past" : past participle, "" : none
                4: contains an infinite form of 'haben'? "inf": yes, "" : no
                5: contains an inflected present form of 'haben'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                6: contains an inflected past form of 'haben'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                7: contains a participle form of 'haben'? "pres" : present participle, "past" : past participle, "" : none
                8: contains an infinite form of 'sein'? "inf": yes, "" : no
                9: contains an inflected present form of 'sein'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                10: contains an inflected past form of 'sein'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                11: contains a participle form of 'sein'? "pres" : present participle, "past" : past participle, "" : none
                12: contains an infinite form of 'werden'? "inf": yes, "" : no
                13: contains an inflected present form of 'werden'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                14: contains an inflected past form of 'werden'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                15: contains a participle form of 'werden'? "pres" : present participle, "past" : past participle, "" : none
                16: the auxiliary verb: "haben" or "sein"
            An analysis is a tuple:
                0: tense : "pres", "past" or "fut"
                1: aspect : "imperf" or "perf"
                2: voice : "active", "pass", "pass:dynamic" or "pass:static"
                3: mode : "imp", "ind", "subj:pres", "subj:past", "inf" or "part"
                
    """
    forms = {}
    with open(os.path.join(FLEXION_PATH, "flexion.csv")) as f:
        lines = [line.strip().split(",") for line in f.readlines()]
        for i, row in enumerate(lines):
            if i == 1:
                keys = row[1:5]
            if i > 1:
                aux = row[0]
                analysis = tuple(row[1:5])
                form = tuple(row[5:-1]) + (aux,)
                example = row[-1]
                try:
                    forms[form].add(analysis)
                except KeyError:
                    forms[form] = set([analysis])
        for form in sorted(forms):
            if len(forms[form]) == 1 or not only_one_form:
                forms[form] = list(forms[form])
            else:
                # sometimes the form is ambiguous between an active and a passive form;
                # in this case we select the active form because the competing passive forms are hardly used:
                forms[form] = [sorted(filter(lambda x: x[2] == "active", list(forms[form])))[0]]
    return forms


# model for speaker attribution
ATTRIBUTION_MODEL = ResourceHandler(load_attribution_model)

# verb table for perfect auxiliaries
AUX_VERBS = ResourceHandler(load_aux_verbs)

# DimLex lexicon
DIMLEX_LEXICON = ResourceHandler(load_dimlex_lexicon)

# DISRPT model for discourse unit segmentation
DISRPT_MODEL = ResourceHandler(load_disrpt_model)

# EVENT model for event classification
EVENT_MODEL = ResourceHandler(load_event_model)

# Germanet api
GERMANET = ResourceHandler(load_germanet)

# Heideltime resources
HEIDELTIME = ResourceHandler(load_heideltime)

# norm exceptions
NORM_EXCEPTIONS = ResourceHandler(load_norm_exceptions)

# nrc emotion lexicon
NRC_DIC_GERMAN = ResourceHandler(load_nrc_dic_german)

# models for reflective passages
REFLECTION_MODELS = ResourceHandler(load_reflection_models)

# space nouns
SPACE_NOUNS = ResourceHandler(load_space_nouns)

# spacy model
SPACY_MODEL = ResourceHandler(load_spacy_model)

# spacy model for tokens
SPACY_MODEL_TOKENIZED = ResourceHandler(load_spacy_model_tokenized)

# speech taggers
SPEECH_TAGGERS = ResourceHandler(load_speech_taggers)

# verb table for inflection
VERB_FORMS = ResourceHandler(load_verb_forms)
