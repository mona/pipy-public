"""This file contains global constants used in the SpaCy pipeline.
"""

from torch import cuda


# Metadata for texts to pipe, must be a list that is empty
# or has a single `Corpus` (corpusreader.corpus.Corpus) element
CORPUS_RECORDS = []


# Maximum number of characters per document (default: 1000000)
SPACY_MAX_LENGTH = 12000000


# The device on which a torch.Tensor is or will be allocated,
# e.g. "cuda" or "cpu" (must be in a list to be changeable).
# Per default, "cuda" is used if available, else "cpu".
TORCH_DEVICE = ("cuda" if cuda.is_available() else "cpu")