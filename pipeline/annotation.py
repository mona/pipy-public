class AnnotationList(list):
    """A class that stores a list of Annotation-objectes.
    """   
    def __init__(self, annotation_list=list()):
        """__init__ method of the class `AnnotationList`.

        Args:
            annotation_list (list of Annotation-objectes): a list of Annotation-objectes.
        
        """
        super(AnnotationList, self).__init__(annotation_list)


    def get_annotations(self, tags=set(), tagset=None):
        """Find Annotation-objects with specified tags and tagset.

        Args:
            tags (iterable): Tags to search for.
            tagset (str): Parameter to filter for specific tagset.
        
        Returns:
            `AnnotationList`: all annotation-objects with specified tags and tagset.
                If `tags` is empty, all tags of the given tagset are searched.
                If `tagset` is None, no filtering for a specific tagset is applied.
        
        """
        annotations_to_return = AnnotationList()
        for annotation in self:
            if (len(tags) == 0 or annotation.tag in tags) and (tagset is None or annotation.tagset.startswith(tagset)):
                annotations_to_return.append(annotation)
        return annotations_to_return


    def get_tags(self, tags=None, tagset=None):
        """Find all or specified tags in a AnnotationList.

        Args:
            tags (str): Parameter to filter for specific tags.
            tagset (str): Parameter to filter for specific tagset.
        
        Returns:
            set: all or specified tags that occour in the AnnotationList.
        """
        tags_to_return = set()
        for annotation in self:
            if tagset is None or annotation.tagset.startswith(tagset):
                if tags is None or annotation.tag in tags:
                    tags_to_return.add(annotation.tag)
        return tags_to_return


class Annotation():
    """A class that stores information from an annotation.
    """   
    def __init__(self, tag, tagset, property_values_dict, tokens, clauses, id_value, strings, string_positions):
        """__init__ method of the class `Annotation`.

        Args:
            tag (str): name of the tag.
            tagset (str): name of the tagset.
            property_values (dict of dict): dictionary with "properties" as key and list with "property values" as value
            tokens (list of `Token`): tokens of the annotation
            clauses (list of `Span`): closest (predicted) clauses of the annotation
            id_value (str): ID of the annotation.
            string (list of str): The strings of the annotation.
            string_positions (list of (int,int)): start/end string positions of annotation in text (not normalized)
        
        
        """
        self.tag = tag
        self.tagset = tagset
        self.property_values = property_values_dict
        self.tokens = tokens
        self.clauses = clauses
        self.id = id_value
        self.strings = strings
        self.string_positions = string_positions


    def __repr__(self):
        return str(vars(self))


    def __str__(self):
        return str(vars(self))