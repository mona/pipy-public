import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import spacy
from nltk import ngrams
from sklearn import tree
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import f_classif, SelectKBest, VarianceThreshold
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier

from .. import CORPUS_PUBLIC_PATH, PARSING_PATH, PICKLE_PATH
from ..components.analyzer import demorphy_analyzer
from ..components.annotation_reader_catma import annotation_reader_catma
from ..components.clausizer import dependency_clausizer
from ..components.coref import rb_coref
from ..components.lemma_fixer import lemma_fixer
from ..components.normalizer import dictionary_normalizer
from ..components.pipeline_pickler import pickle_init, pickle_wrapper
from ..components.sentencizer import spacy_sentencizer
from ..components.slicer import max_sent_slicer
from ..components.speaker_extractor import rb_speaker_extractor
from ..components.speech_tagger import quotation_marks_speech_tagger
from ..components.tense_tagger import rb_tense_tagger
from ..global_constants import SPACY_MAX_LENGTH
from ..global_wordlists import *
from ..utils_methods import *


# GI labels and encoder for single-label classification
QUANTIFIER_TAGS = ["BARE", "ALL", "MEIST", "EXIST", "DIV", "NEG"]
LABEL_ENCODER = LabelEncoder()
LABEL_ENCODER.fit(QUANTIFIER_TAGS + ["None"])

# Name of a pre-trained classifier
DEFAULT_CLF_NAME = "gen_tagger_RandomForestClassifier_8_Dahn+Fontane+Gellert+Hoffmann+Kafka+LaRoche+Musil+Novalis"


def get_clf_gen_tagger(texts_train=None, tree_classifier=RandomForestClassifier, show_feature_importances=False, plot_tree_to=None, subselect_features_k=None, pickle_clfs=True):
    """Returns a function that maps a sample to a label using a classifier.

    Args:
        texts_train (list of str): List of training texts.
            If None, the default classifier is loaded.
        tree_classifier (class): A tree classifier, e.g. `DecisionTreeClassifier`, `RandomForestClassifier`, `ExtraTreesClassifier`.
        plot_tree_to (str): Plot the (first) decision tree to a file at the given path.
            If None, nothing is plotted.
        subselect_features_k (int): Maximum number of best features to select from all featurs.
            If None, no feature selection is applied.
        pickle_clfs (boolean): Iff True, classifiers are dumped and loaded with pickle.
    
    Returns:
        func: A function that maps a clause (more precisely: a list of clauses and a clause index) to a GI label.
    
    """
    current_dir = os.path.dirname(__file__)
    if texts_train is None:
        # load the default classifier
        clf_path = os.path.join(current_dir, "pickled_clfs", DEFAULT_CLF_NAME + ".pickle")
        clf, dv, fs1, fs2 = pickle.load(open(clf_path, "rb"))
    else:
        names = [text_train.split("__")[0] for text_train in texts_train]
        clf_name = "gen_tagger_" + tree_classifier.__name__ + "_" + str(len(names)) + "_" + "+".join(sorted(names))
        clf_path = os.path.join(current_dir, "pickled_clfs", clf_name + ".pickle")
        if os.path.exists(clf_path) and pickle_clfs:
        # load the classifier trained on the given documents
            clf, dv, fs1, fs2 = pickle.load(open(clf_path, "rb"))
        else:
            # train the classifier on the given documents and save it
            
            def pw(doc, func, **kwargs):
                return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)
            model = os.path.join(PARSING_PATH, "de_ud_lg")
            pipe = spacy.load(model, disable=["ner"])
            pipe.max_length = SPACY_MAX_LENGTH
            pipe.add_pipe(lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=400), name="pickle_init", before="tagger")
            pipe.add_pipe(lambda doc: pw(doc, dictionary_normalizer), name="normalizer", before="tagger")
            pipe.add_pipe(lambda doc: pw(doc, lemma_fixer), name="lemma_fixer", after="tagger")
            pipe.add_pipe(lambda doc: pw(doc, spacy_sentencizer), name="sentencizer", before="parser")
            pipe.add_pipe(lambda doc: pw(doc, max_sent_slicer), name="slicer", after="parser")
            pipe.add_pipe(lambda doc: pw(doc, demorphy_analyzer), name="analyzer")
            pipe.add_pipe(lambda doc: pw(doc, dependency_clausizer), name="clausizer")
            pipe.add_pipe(lambda doc: pw(doc, rb_tense_tagger), name="tense_tagger")
            pipe.add_pipe(lambda doc: pw(doc, quotation_marks_speech_tagger), name="speech_tagger")
            pipe.add_pipe(lambda doc: pw(doc, rb_speaker_extractor), name="speaker_extractor")
            pipe.add_pipe(lambda doc: pw(doc, rb_coref), name="coref")
            pipe.add_pipe(lambda doc: annotation_reader_catma(doc, corpus_path=CORPUS_PUBLIC_PATH), name="annotation_reader")
            
            X_train, Y_train = [], []
            for n, text_train in enumerate(texts_train):
                vec_path = os.path.join(current_dir, "pickled_vecs", "gen_tagger_" + names[n] + ".pickle")
                if os.path.exists(vec_path):
                    # load the vectors for the given document
                    X, Y = pickle.load(open(vec_path, "rb"))
                else:
                    # compute the vectors for the given document and save them
                    doc_train = pipe(open(os.path.join(CORPUS_PUBLIC_PATH, text_train, text_train + ".txt")).read())
                    X, Y = vectorize_doc(doc_train)
                    pickle.dump((X, Y), open(vec_path, "wb"))
                X_train.extend(X)
                Y_train.extend(Y)
            
            # encode features
            dv = DictVectorizer(sparse=False)
            dv.fit(X_train)
            X_train = dv.transform(X_train)
            
            # encode labels (single-label classification)
            Y_train = LABEL_ENCODER.transform(Y_train)
            Y_train = list(Y_train)

            fs1, fs2 = None, None
            if subselect_features_k is not None:
                fs1 = VarianceThreshold(threshold=0)
                fs1.fit(X_train, Y_train)
                X_train = fs1.transform(X_train)
                fs2 = SelectKBest(f_classif, k=min(subselect_features_k, len(list(X_train[0]))))
                fs2.fit(X_train, Y_train)
                X_train = fs2.transform(X_train)
            
            clf = tree_classifier(class_weight="balanced", random_state=0, max_depth=15, min_samples_leaf=2)
            clf.fit(X_train, Y_train)

            if pickle_clfs:
                pickle.dump((clf, dv, fs1, fs2), open(clf_path, "wb"))

    if show_feature_importances:
        fn = [dv.feature_names_]
        if fs1 is not None:
            fn = fs1.transform(fn)
        if fs2 is not None:
            fn = fs2.transform(fn)
        fn = fn[0]
        fi = clf.feature_importances_
        for fn_, fi_ in sorted(zip(fn, fi), key=lambda tup: tup[1], reverse=True):
            print(fn_, fi_)
            input()
        exit()

    if plot_tree_to is not None:
        if type(clf) == DecisionTreeClassifier:
            clf_tree = clf
        else:
            clf_tree = clf.estimators_[0]
        plt.figure(figsize=(120,40))
        if subselect_features_k is None:
            tree.plot_tree(clf_tree, max_depth=10, feature_names=dv.feature_names_, class_names=[str(c) for c in LABEL_ENCODER.classes_], fontsize=10)
        else:
            tree.plot_tree(clf_tree, max_depth=10, feature_names=fs2.transform(fs1.transform([dv.feature_names_]))[0], class_names=[str(c) for c in LABEL_ENCODER.classes_], fontsize=10)
        plt.savefig(plot_tree_to)
    
    def clf_predict(clauses, i):
        x = feature_extractor(clauses, i)
        X = [x]
        X = dv.transform(X)
        if fs1 is not None:
            X = fs1.transform(X)
        if fs2 is not None:
            X = fs2.transform(X)
        Y = clf.predict(X)
        Y = LABEL_ENCODER.inverse_transform(Y)
        y = Y[0]
        tags = set([y])
        tags.discard("None")
        return tags
    
    return clf_predict


def vectorize_doc(doc):
    """Vectorize the clauses of a document.

    Args:
        doc (`Doc`): The document.
    
    Returns:
        list of (dict of str:str): A list of feature dicts (feature-value pairs), one for every clause.
        list of str: A list of corresponding gold classes (single labels); `None` for untagged clauses.
    
    """
    X = []
    Y = []
    for anno in ["GGG", "GGG-A", "GGG-H"]:
        try:
            annotations = doc._.annotations[anno].get_annotations(tags=QUANTIFIER_TAGS, tagset="Reflexion IV")
            if len(annotations) > 0:
                last_sent = annotations[-1].tokens[-1].sent
                last_clause = last_sent._.clauses[-1]
                clauses = doc._.clauses[:doc._.clauses.index(last_clause)+1]
                for i in range(len(clauses)):
                    features = feature_extractor(clauses, i)
                    labels = label_extractor(clauses, i)
                    for label in labels:
                        X.append(features)
                        Y.append(label)
                    if len(labels) == 0:
                        X.append(features)
                        Y.append("None")
        except KeyError:
            pass
    return X, Y


def feature_extractor(clauses, index):
    """Extract features from a clause and its preceding and succeeding clauses.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    for i in [-1, 0, 1]:
        features_ = feature_extractor_(clauses, index-i)
        for k in features_:
            features["clause_"+str(i)+"_"+k] = str(features_[k])
    features_ = extract_multi_clause_features(clauses, index)
    for k in features_:
        features[k] = str(features_[k])
    return features


def feature_extractor_(clauses, index, include_clausal_features=True, include_NP_and_VP_features=True, include_ngram_features=False):
    """Extract features from a clause.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
        include_clausal_features (boolean): True iff the features from `extract_clausal_features` should be extracted.
        include_NP_and_VP_features (boolean): True iff the features from `extract_NP_and_VP_features` should be extracted.
        include_ngram_features (boolean): True iff the features from `extract_ngram_features` should be extracted.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    all_features = []
    if include_clausal_features:
        all_features.append(extract_clausal_features(clauses, index))
    if include_NP_and_VP_features:
        all_features.append(extract_NP_and_VP_features(clauses, index))
    if include_ngram_features:
        all_features.append(extract_ngram_features(clauses, index))
    for features_ in all_features:
        for k in features_:
            features[k] = features_[k]
    return features


def label_extractor(clauses, index, exclude_ambiguous=True):
    """Extract gold labels from a clause.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
        exclude_ambiguous (boolean): Iff True, exclude ambiguous annotations.
    
    Returns:
        list of str: List of gold labels.
    
    """
    try:
        tags = set()
        for anno in ["GGG", "GGG-A", "GGG-H"]:
            try:
                for annotation in clauses[index]._.annotations[anno].get_annotations(tags=QUANTIFIER_TAGS, tagset="Reflexion IV"):
                    ambig = False
                    if "Sicherheit" in annotation.property_values:
                        for item in annotation.property_values["Sicherheit"]:
                            if "ambig" in item:
                                ambig = True
                                break
                    if not (exclude_ambiguous and ambig):
                        tags.add(annotation.tag)
                return list(tags)
            except KeyError:
                pass
    except IndexError:
        return []


def extract_clausal_features(clauses, index):
    """Extract clausal features.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        clause = clauses[index]
        features["tense"] = clause._.form.tense
        features["aspect"] = clause._.form.aspect
        features["mode"] = clause._.form.mode
        features["voice"] = clause._.form.voice
        features["verb_form"] = clause._.form.verb_form
        for verb in clause._.form.modals:
            if verb.lemma_.lower() == "pflegen":
                features["pflegen"] = True
        if is_direct_speech(clause):
            features["direct_speech"] = True
        for token in clause._.tokens:
            if token.lemma_.lower() in NEGATIONS:
                features["NEG"] = True
                break
    except IndexError:
        pass
    return features


def extract_multi_clause_features(clauses, index):
    """Extract features for multi-clause constructions.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        if clauses[index-1].sent == clauses[index].sent:
            features["sent_-1"] = True
        if clauses[index-1].root.head._.clause == clauses[index]:
            features["head_-1"] = True
        if clauses[index].root.head._.clause == clauses[index-1]:
            features["clause_-1"] = True
        features["-1"] = True
    except IndexError:
        pass
    try:
        if clauses[index].sent == clauses[index+1].sent:
            features["sent_1"] = True
        if clauses[index].root.head._.clause == clauses[index+1]:
            features["head_1"] = True
        if clauses[index+1].root.head._.clause == clauses[index]:
            features["clause_1"] = True
        features["1"] = True
    except IndexError:
        pass
    for n, i in enumerate([index-1, index, index+1]):
        try:
            for token in clauses[i]:
                if token.lower_ in ["wenn", "wer", "wem", "wen"]:
                    features["clause_"+str(n)+"_conditional"] = True
                    break
            if clauses[i].root.dep_.split(":")[0] == "acl":
                features["clause_"+str(n)+"_relative"] = True
        except IndexError:
            pass
    return features


def extract_NP_and_VP_features(clauses, index, max_len=3, include_lemma_feats=False):
    """Extract features for all NPs and VPs in a clause.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
        max_len (int): Features are n-tuples; `max_len` specified the maximum length.
        include_lemma_feats (boolean): True iff the lemmas of quantifier and restrictor should be added as features.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        clause = clauses[index]
        for token in clause._.tokens:
            tag = None
            if token.lemma_.lower() in QUANTIFIERS:
                tag = QUANTIFIERS[token.lemma_.lower()]
                restrictors = [get_restrictor(token)]
            elif token.lemma_ == "existieren" or (token.lemma_ == "geben" and "es" in [word.lower_ for word in token.children]):
                tag = "EXIST"
                restrictors = list(token.children)                
            elif no_quantifiers_in_subtree(token):
                tag = "BARE"
                restrictors = [token]
            if tag is not None:
                for restrictor in restrictors:
                    if is_NP(restrictor):
                        children = [restrictor]+list(restrictor.children)
                        names = ["Q_tag", "Q_pos", "N_specific", "N_pos", "N_dep", "N_numerus", "N_det", "N_num", "Q_lemma", "N_lemma"]
                        feats = [
                            tag,
                            (None if tag == "BARE" else token.pos_),
                            is_specific(restrictor, True, 3, 1),
                            restrictor.pos_,
                            restrictor.dep_,
                            token._.morph.numerus,
                            True in [(child.pos_ == "DET" or child.lemma_ in ["beid", "beide", "einzig"]) for child in children if child != token],
                            True in [(child.pos_ == "NUM" or child.dep_.split(":")[0] == "nummod" or child.lemma_ in ["beid", "beide", "einzig"]) for child in children if child != token],
                            (None if tag == "BARE" else token.lemma_.lower()),
                            restrictor.lemma_.lower()
                        ]
                        if not include_lemma_feats:
                            names = names[:-2]
                            feats = feats[:-2]
                        feats = [names[i]+"="+str(x) for i, x in enumerate(feats)]
                        feats = ["NP_"+str(x) for x in list(powerset(feats)) if "Q_tag="+tag in x and len(x) <= max_len]
                        for feat in feats:
                            features[feat] = True
                    elif is_VP(restrictor):
                        names = ["Q_tag", "Q_pos", "V_specific", "V_pos", "V_generic_attr", "V_has_vague_attr", "Q_lemma", "V_lemma"]
                        feats = [
                            tag,
                            (None if tag == "BARE" else token.pos_),
                            is_specific(restrictor, True, 3, 1),
                            restrictor.pos_,
                            has_generic_attr(restrictor),
                            has_vague_attr(restrictor),
                            (None if tag == "BARE" else token.lemma_.lower()),
                            restrictor.lemma_.lower()
                        ]
                        if not include_lemma_feats:
                            names = names[:-2]
                            feats = feats[:-2]
                        feats = [names[i]+"="+str(x) for i, x in enumerate(feats)]
                        feats = ["VP_"+str(x) for x in list(powerset(feats)) if "Q_tag="+tag in x and len(x) <= max_len]
                        for feat in feats:
                            features[feat] = True
    except IndexError:
        pass
    return features


def extract_ngram_features(clauses, index, max_len=3):
    """Extract token n-grams in a clause.

    Args:
        clauses (list of `Span`): List of clause in the document.
        index (int): Index of the current clause.
        max_len (int): Features are n-tuples; `max_len` specified the maximum length.
    
    Returns:
        dict of str:obj: Feature dict.
    
    """
    features = {}
    try:
        clause = clauses[index]
        tokens = [token.text.lower() for token in clause._.tokens]
        feats = []
        for n in range(1, max_len+1):
            feats.extend([str(n)+"_"+str(x) for x in ngrams(tokens, n)])
        for feat in feats:
            features[feat] = True
    except IndexError:
        pass
    return features