import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import spacy
from hashlib import md5
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import make_scorer, precision_recall_fscore_support
from sklearn.model_selection import GridSearchCV, PredefinedSplit
from sklearn.multioutput import MultiOutputClassifier
from sklearn.preprocessing import LabelEncoder, MultiLabelBinarizer
from sklearn.tree import DecisionTreeClassifier
from spacy_sentiws import spaCySentiWS

from .. import CORPUS_PUBLIC_PATH, PARSING_PATH, PICKLE_PATH, SENTIMENT_TAGGER_PATH
from ..components.analyzer import demorphy_analyzer
from ..components.annotation_reader_catma import annotation_reader_catma
from ..components.clausizer import dependency_clausizer
from ..components.emotions_tagger import nrc_emotions
from ..components.feature_extractor import dfg_feature_extractor
from ..components.germanet_tagger import germanet_tagger_verbs_adjectives
from ..components.lemma_fixer import lemma_fixer
from ..components.normalizer import dictionary_normalizer
from ..components.pipeline_pickler import pickle_init, pickle_wrapper
from ..components.sentencizer import spacy_sentencizer
from ..components.slicer import max_sent_slicer
from ..components.speech_tagger import quotation_marks_speech_tagger
from ..components.tense_tagger import rb_tense_tagger
from ..global_constants import SPACY_MAX_LENGTH


def get_clf_dfg_tagger(
    default_clf_name=None, 
    texts_train=None, 
    texts_dev=None, 
    tags=None, 
    supertag=None, 
    label_condition="multi", 
    classifier=DecisionTreeClassifier, 
    param_grid=[{'max_depth': [5, 10, 15, 20, 25, None], 'min_samples_leaf': [1, 2, 5, 10, 15, 20]}], 
    window=(0, 0), 
    disable_feats=[], 
    enable_feats=[], 
    show_feature_importances=False, 
    plot_tree_to=None, 
    pickle_vecs=True, 
    pickle_clfs=True
):
    """Returns a function that maps a clause to a set of labels using a classifier.

    Args:
        default_clf_name (str): Name of a default classifier.
        texts_train (list of str): List of training texts.
            If None, the default classifier is loaded.
        texts_dev (list of str): List of development texts.
        tags (iterable of str): Labels to conider for the classification.
        supertag (str): For a binary evaluation, name of the supertag.
            Should always be specified, even for a non-binary classification.
        label_condition (str): Label condition in training.
            "multi":  Multiple classifiers are trained, one for each label.
            "single": A single classifier is trained for all labels / combinations of labels.
            "binary": A single classifier is trained; labels are merged into the `supertag`.
        classifier (class): A classifier, e.g. `DecisionTreeClassifier`.
        param_grid (list of (dict of str:(list of obj))): The parameter grid for grid search.
        window ((int,int)): Context window (clauses to look to the left and the right).
        disable_feats (list of str): List of features (or feature substrings) to exclude.
        enable_feats (list of str): List of features (or feature substrings) to include.
        show_feature_importances (boolean): Iff True, features importances are printed after training the classifier.
            After that, the program is terminated.
        plot_tree_to (str): For tree classifiers, plot the (first) decision tree to a file at the given path.
            If None, nothing is plotted.
        pickle_vecs (boolean): Iff True, vectorised training samples are dumped and loaded with pickle.
        pickle_clfs (boolean): Iff True, classifiers are dumped and loaded with pickle.
    
    Returns:
        func: A function that maps a list of clauses to a list of corresponding sets of labels.
    
    """
    # If `texts_dev` is None, its the same as if it was empty.
    if texts_dev is None:
        texts_dev = []
    
    # transform `tags` to set (could be list)
    if tags is not None:
        tags = set(tags)
    
    # Make sure that the supertag is specified.
    if supertag is None:
        raise ValueError("Supertag must be specified, even if not used.")
    
    # Make sure that the window is correctly specified.
    if window[0] > 0 or window[1] < 0:
        raise ValueError("Context clause offsets must be <=0 and >=0, respectively.")
    
    # If `param_grid` is None, use the default values for decision trees.
    if param_grid is None:
        param_grid = [{'max_depth': [5, 10, 15, 20, 25, None], 'min_samples_leaf': [1, 2, 5, 10, 15, 20]}]

    current_dir = os.path.dirname(__file__)
    
    if texts_train is None:
        # load the default classifier
        clf_path = os.path.join(current_dir, "pickled_clfs", default_clf_name + ".pickle")
        clf, dv, le = pickle.load(open(clf_path, "rb"))
    
    else:
        # load the classifier trained on the given documents
        names_train = [text_train.split("__")[0] for text_train in texts_train]
        names_dev = [text_dev.split("__")[0] for text_dev in texts_dev]
        #clf_name = "dfg_tagger_" + ("" if supertag is None else supertag) + "_" + label_condition + "_" + classifier.__name__ + "_" + str(window) + "_" + str(len(names_train)) + "_" + str(len(names_dev)) + "_" + "+".join(sorted(names_train)) + "_" + "+".join(sorted(names_dev))
        clf_name = "dfg_tagger_" + unique_clf_name(names_train, names_dev, tags, supertag, label_condition, classifier, param_grid, window, disable_feats, enable_feats)
        clf_path = os.path.join(current_dir, "pickled_clfs", clf_name + ".pickle")
        
        if os.path.exists(clf_path) and pickle_clfs:
            clf, dv, le = pickle.load(open(clf_path, "rb"))
        
        else:
            # train the classifier on the given documents and save it

            def pw(doc, func, **kwargs):
                return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)
            model = os.path.join(PARSING_PATH, "de_ud_lg")
            pipe = spacy.load(model, disable=["ner"])
            pipe.max_length = SPACY_MAX_LENGTH
            pipe.add_pipe(lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=700), name="pickle_init", before="tagger")
            pipe.add_pipe(spaCySentiWS(os.path.join(SENTIMENT_TAGGER_PATH, "sentiws")), name="sentiment_tagger", before="pickle_init")
            pipe.add_pipe(lambda doc: pw(doc, dictionary_normalizer), name="normalizer", before="tagger")
            pipe.add_pipe(lambda doc: pw(doc, lemma_fixer), name="lemma_fixer", after="tagger")
            pipe.add_pipe(lambda doc: pw(doc, spacy_sentencizer), name="sentencizer", before="parser")
            pipe.add_pipe(lambda doc: pw(doc, max_sent_slicer), name="slicer", after="parser")
            pipe.add_pipe(lambda doc: pw(doc, demorphy_analyzer), name="analyzer")
            pipe.add_pipe(lambda doc: pw(doc, dependency_clausizer), name="clausizer")
            pipe.add_pipe(lambda doc: pw(doc, rb_tense_tagger), name="tense_tagger")
            pipe.add_pipe(lambda doc: pw(doc, quotation_marks_speech_tagger), name="speech_tagger")
            pipe.add_pipe(lambda doc: pw(doc, germanet_tagger_verbs_adjectives), name="germanet_tagger")
            pipe.add_pipe(lambda doc: pw(doc, nrc_emotions), name="emotions_tagger")
            pipe.add_pipe(lambda doc: pw(doc, dfg_feature_extractor), name="feature_extractor")
            pipe.add_pipe(lambda doc: annotation_reader_catma(doc, corpus_path=CORPUS_PUBLIC_PATH), name="annotation_reader")

            X_train, Y_train, Y_train_ambig = get_X_Y(pipe, texts_train, names_train, window, current_dir, pickle_vecs)
            X_dev, Y_dev, Y_dev_ambig = get_X_Y(pipe, texts_dev, names_dev, window, current_dir, pickle_vecs)
            
            # remove ambiguous samples and convert labels to desired format
            X_train, Y_train = select_samples_and_labels(X_train, Y_train, Y_train_ambig, tags, supertag, label_condition)
            X_dev, Y_dev = select_samples_and_labels(X_dev, Y_dev, Y_dev_ambig, tags, supertag, label_condition)

            # subselect features (only needed for train set)
            X_train = subselect_features(X_train, disable_feats, enable_feats)

            # convert string features to Boolean features
            X_train = [dict.fromkeys(x, True) for x in X_train]
            X_dev = [dict.fromkeys(x, True) for x in X_dev]

            # encode features
            dv = DictVectorizer(sparse=False)
            X_train = dv.fit_transform(X_train)
            X_dev = dv.transform(X_dev)

            # encode labels
            if label_condition == "multi":
                le = MultiLabelBinarizer()
            else:
                le = LabelEncoder()
            le.fit(Y_train + Y_dev)
            Y_train = le.transform(Y_train)
            Y_dev = le.transform(Y_dev)

            if len(texts_dev) == 0:
                # create classifier with fixed parameters
                try:
                    clf = classifier(random_state=0)
                except TypeError: # no random state argument
                    clf = classifier()
            
            else:
                # define a scoring function (multi-label macro-averaged F1)
                f1_scorer = make_scorer(lambda Y1, Y2: multi_label_scores(le, Y1, Y2, "macro")[2])

                # select best classifier by grid search on dev set
                X_train_dev = np.concatenate([X_train, X_dev], axis=0)
                Y_train_dev = np.concatenate([Y_train, Y_dev], axis=0)
                ps = PredefinedSplit([-1] * X_train.shape[0] + [0] * X_dev.shape[0])
                if label_condition == "multi":
                    param_grid = [{"estimator__" + str(p) : pg[p] for p in pg} for pg in param_grid]
                    try:
                        gs = GridSearchCV(MultiOutputClassifier(classifier(random_state=0, class_weight="balanced")), param_grid=param_grid, scoring=f1_scorer, n_jobs=-1, refit=True, cv=ps, verbose=False)
                    except TypeError: # no random state argument / no class weight
                        gs = GridSearchCV(MultiOutputClassifier(classifier()), param_grid=param_grid, scoring=f1_scorer, n_jobs=-1, refit=True, cv=ps, verbose=False)
                else:
                    try:
                        gs = GridSearchCV(classifier(random_state=0, class_weight="balanced"), param_grid=param_grid, scoring=f1_scorer, n_jobs=-1, refit=True, cv=ps, verbose=False)
                    except TypeError: # no random state argument / no class weight
                        gs = GridSearchCV(classifier(), param_grid=param_grid, scoring=f1_scorer, n_jobs=-1, refit=True, cv=ps, verbose=False)
                gs.fit(X_train_dev, Y_train_dev)
                print(gs.best_params_)
                clf = gs.best_estimator_
            
            # train classifier on train set
            clf.fit(X_train, Y_train)

            if pickle_clfs:
                pickle.dump((clf, dv, le), open(clf_path, "wb"))

    # select base classifier(s)
    if type(clf) == MultiOutputClassifier:
        clf_per_label = list(zip(clf.estimators_, le.classes_))
    else:
        clf_per_label = [(clf, None)]

    if plot_tree_to is not None:
        for label_clf, clf_label in clf_per_label:
            clf_trees = get_trees_from_clf(label_clf)
            clf_tree = clf_trees[0]
            print(clf_label)
            print(tree.export_text(clf_tree, max_depth=10, feature_names=dv.feature_names_))
            plt.figure(figsize=(100,50))
            tree.plot_tree(clf_tree, max_depth=10, feature_names=dv.feature_names_, class_names=[str(c) for c in (le.classes_ if clf_label is None else [None, clf_label])])
            if clf_label is None:
                plot_tree_to_path = plot_tree_to
            else:
                plot_tree_to_path = clf_label + "-" + plot_tree_to
            plt.savefig(plot_tree_to_path)
    
    if show_feature_importances:
        for label_clf, clf_label in clf_per_label:
            if hasattr(label_clf, "estimators_"):
                clf_estimators = label_clf.estimators_
            else:
                clf_estimators = [label_clf]
            fn = dv.feature_names_
            fi = np.zeros(len(fn))
            for clf_estimator in clf_estimators:
                try:
                    fi += clf_estimator.feature_importances_
                except AttributeError: # for LogisticRegression
                    fi += clf_estimator.coef_[0]
            fi /= len(clf_estimators)
            print(clf_label)
            for fn_, fi_ in sorted(zip(fn, fi), key=lambda tup: abs(tup[1]), reverse=True)[:50]:
                print(fn_, fi_)
        exit()
    
    def clf_predict(clauses):
        X = []
        for index, clause in enumerate(clauses):
            x = set()
            for i in range(window[0], window[1]+1):
                try:
                    for f in clauses[index+i]._.feats:
                        x.add(str(i) + "_" + f)
                except IndexError:
                    pass
            X.append(x)
        X = [dict.fromkeys(x, True) for x in X]
        X = dv.transform(X)
        Y = clf.predict(X)
        Y = le.inverse_transform(Y)
        return [multi_label(y) for y in Y]
    
    return clf_predict


def unique_clf_name(names_train, names_dev, tags, supertag, label_condition, classifier, param_grid, window, disable_feats, enable_feats):
    """Creates a unique name for a classifier.
    
    Args:
        names_train (list of str): List of training texts.
            If None, the default classifier is loaded.
        names_dev (list of str): List of development texts.
        tags (iterable of str): Labels to conider for the classification.
        supertag (str): Name of the supertag.
        label_condition (str): Label condition.
        classifier (class): Classifier.
        param_grid (list of (dict of str:(list of obj))): The parameter grid for grid search.
        window ((int,int)): Context window.
        disable_feats (list of str): List of features (or feature substrings) to exclude.
        enable_feats (list of str): List of features (or feature substrings) to include.
    
    Returns:
        str: Name.
    
    """
    clf_name = []

    # phenomenon
    clf_name.append("" if supertag is None else supertag)
    
    # label condition
    clf_name.append(label_condition)
    
    # classifier
    clf_name.append(classifier.__name__)
    
    # window
    clf_name.append(str(window))
    
    # number of training texts
    clf_name.append(str(len(names_train)))
    
    # number of development texts
    clf_name.append(str(len(names_dev)))
    
    # list of training texts
    clf_name.append("+".join(sorted(names_train)))
    
    # list of development texts
    clf_name.append("+".join(sorted(names_dev)))
    
    # number of disabled/enabled features
    clf_name.append(("-" if len(disable_feats) > 0 else ("+" if len(enable_feats) > 0 else "")) + str(len(disable_feats + enable_feats)))

    clf_params = []
    
    # list of subtags
    clf_params.append("" if tags is None else "+".join(sorted(tags)))
    
    # parameter grid
    clf_params.append(json.dumps(param_grid, sort_keys=True))
    
    # list of disabled/enabled features
    clf_params.append("+".join(sorted(disable_feats + enable_feats)))
    
    # hashed parameters
    clf_name.append(md5("_".join(clf_params).encode()).hexdigest())
    
    return "_".join(clf_name)


def get_trees_from_clf(clf):
    """Return a classifier's decision trees.
        Raises an error if the classifier is not tree-based.

    Args:
        clf (obj): A classifier based on decision trees, e.g. `DecisionTreeClassifier` or `RandomForestClassifier`.
    
    Return:
        list of `DecisionTreeClassifier`: The classifier's decision trees.
    
    """
    if type(clf) == DecisionTreeClassifier:
        return [clf]
    if hasattr(clf, "estimators_") and type(clf.estimators_[0]) == DecisionTreeClassifier:
        return clf.estimators_
    else:
        raise ValueError("Classifier must be a tree or a tree ensemble if a tree should be plotted.")


def multi_label(labels):
    """Converts labels in any format to a set of labels.

    Args:
        labels (obj): Labels as joined string or as set of strings.
    
    Returns:
        set of str: Set of labels.
    
    """
    if type(labels) in [str, np.str_]:
        if labels == "":
            return set()
        return set(labels.split(","))
    return set(labels)


def multi_label_scores(le, Y_gold, Y_pred, average):
    """Compute multi-label scores between gold and predicted labels in any format.

    Args:
        le (obj): Label encoder that was used to encode the labels.
        Y_gold (np.array): Gold labels.
        Y_pred (np.array): Predicted labels.
        average (str): Average method, e.g. "micro" or "macro".
    
    Returns:
        float: Precision.
        float: Recall.
        float: F-Score.
        int: Support.
    
    """
    Y_gold = [multi_label(y) for y in le.inverse_transform(Y_gold)]
    Y_pred = [multi_label(y) for y in le.inverse_transform(Y_pred)]
    mlb = MultiLabelBinarizer()
    mlb.fit(Y_gold + Y_pred)
    Y_gold = mlb.transform(Y_gold)
    Y_pred = mlb.transform(Y_pred)
    result = precision_recall_fscore_support(Y_gold, Y_pred, average=average, zero_division=1)
    return result


def get_X_Y(pipe, texts, names, window, current_dir, pickle_vecs):
    """Get samples and labels for the given texts.

    Args:
        pipe (func): The spacy pipeline for feature extraction.
        texts (list of str): The (file)names of the texts without extension.
        names (list of str): The authors of the texts as in the corresponding filename.
        window ((int,int)): Index offset range for contextualised feature extraction,
            e.g. (-1, 1) to include the features of the preceding and succeeding clause.
        current_dir (str): Path to the current directory.
        pickle_vecs (boolean): Iff True, vectorised training samples are dumped and loaded with pickle.
    
    Returns:
        list of (set of str): List of sets of features, one for every clause.
        list of (set of str): List of sets of corresponding gold labels.
        list of (set of str): List of sets of corresponding gold labels that are marked as ambiguous.

    """
    X, Y, Y_ambig = [], [], []
    for n, text in enumerate(texts):
        vec_path = os.path.join(current_dir, "pickled_vecs", "dfg_tagger_" + names[n] + ".pickle")
        if pickle_vecs and os.path.exists(vec_path):
            # load the vectors for the given document
            samples, labels, ambiguous_labels = pickle.load(open(vec_path, "rb"))
        else:
            # compute the vectors for the given document and save them
            doc = pipe(open(os.path.join(CORPUS_PUBLIC_PATH, text, text + ".txt")).read())
            samples, labels, ambiguous_labels = vectorize_doc(doc)
            pickle.dump((samples, labels, ambiguous_labels), open(vec_path, "wb"))

        # construct training samples of several subsequent clauses each
        samples_with_context = []
        for index, sample in enumerate(samples):
            sample_with_context = set()
            for i in range(window[0], window[1]+1):
                try:
                    sample_ = samples[index+i]
                    for feat in sample_:
                        sample_with_context.add(str(i) + "_" + feat)
                except IndexError:
                    pass
            samples_with_context.append(sample_with_context)
        samples = samples_with_context

        X.extend(samples)
        Y.extend(labels)
        Y_ambig.extend(ambiguous_labels)
    
    return X, Y, Y_ambig


def vectorize_doc(doc):
    """Vectorize the clauses of a document.

    Args:
        doc (`Doc`): The document.
    
    Returns:
        list of (set of str): List of sets of features, one for every clause.
        list of (set of str): List of sets of corresponding gold labels.
        list of (set of str): List of sets of corresponding gold labels that are marked as ambiguous.
    
    """
    samples = []
    labels = []
    ambiguous_labels = []
    for anno in ["GGG", "GGG-A", "GGG-H"]:
        try:
            last_index_found = False
            for clause in reversed(doc._.clauses):
                annotations = clause._.annotations[anno].get_annotations(tagset="Reflexion IV")
                if (not last_index_found) and len(annotations) > 0:
                    last_index_found = True
                if last_index_found:
                    samples.append(clause._.feats)
                    labels_ = set()
                    ambiguous_labels_ = set()
                    for annotation in annotations:
                        labels_.add(annotation.tag)
                        if "Sicherheit" in annotation.property_values:
                            for item in annotation.property_values["Sicherheit"]:
                                if "ambig" in item:
                                    ambiguous_labels_.add(annotation.tag)
                                    break
                    labels.append(labels_)
                    ambiguous_labels.append(ambiguous_labels_)
        except KeyError:
            pass
    return samples, labels, ambiguous_labels


def select_samples_and_labels(X, Y, Y_ambig, tags, supertag, label_condition):
    """Convert samples and labels for the specified classification task.

    Args:
        X (list of obj): List of feature vectors.
        Y (list of (set of str)): List of sets of corresponding labels.
        Y_ambig (list of (set of str)): List of sets of corresponding labels that are marked as ambiguous.
        tags (set of str): Labels that are relevant for the current classification.
        supertag (str): Name of the combined label for a binary classification.
        label_condition (str): `multi`, `binary` or `single`.
    
    Returns:
       X (list of obj): List of feature vectors.
       Y (list of obj): List of corresponding labels, either as string or as set of strings.
    
    """
    X_new = []
    Y_new = []
    for i, x in enumerate(X):
        y = Y[i]
        
        # select the relevant labels
        if tags is not None:
            y = y.intersection(tags)
        
        # only include samples that do not have ambiguous
        # annotations among the relevant labels
        if len(y.intersection(Y_ambig[i])) == 0:
            y = sorted(list(y))
            
            # multi-class multi-label
            if label_condition == "multi":
                X_new.append(x)
                Y_new.append(y)
            
            # binary-class single-label
            elif label_condition == "binary":
                X_new.append(x)
                if len(y) > 0:
                    Y_new.append(supertag)
                else:
                    Y_new.append("")
            
            # multi-class single-label
            elif label_condition == "single":

                # 7x GI:       "", "ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"
                if supertag == "GI":
                    if len(y) == 0:
                        X_new.append(x)
                        Y_new.append("")
                    else:
                        for y_ in y:
                            X_new.append(x)
                            Y_new.append(y_)
                
                # 8x Comment:  "", "Einstellung", "Interpretation", "Meta", "Einstellung,Interpretation", "Einstellung,Meta", "Interpretation,Meta", "Einstellung,Interpretation,Meta"
                # 4x NfR:      "", "Nichtfiktional", "Nichtfiktional+mK", "Nichtfiktional,Nichtfiktional+mK"
                else:
                    X_new.append(x)
                    Y_new.append(",".join(y))
            
            else:
                raise ValueError("Unknown label_condition: " + label_condition)
    return X_new, Y_new


def subselect_features(X, disable_feats, enable_feats):
    """Remove unwanted features.

    Args:
        X (list of (set of str)): List of feature sets.
        disable_feats (iterable of str): Unwanted features.
        enable_feats (iterable of str): Wanted features.
    
    Returns:
        list of (set of str): List of feature sets without the unwanted features / with only the wanted features.
    
    """
    if len(disable_feats) > 0 and len(enable_feats) > 0:
        raise ValueError("You can either enable or disable features but not both!")
    if len(disable_feats) == 0 and len(enable_feats) == 0:
        return X
    X_new = []
    disable = (len(disable_feats) > 0)
    _feats = disable_feats + enable_feats
    for x in X:
        x_new = set()
        for f in x:
            include = disable
            for s in _feats:
                if s in f:
                    include = (not disable)
                    break
            if include:
                x_new.add(f)
        X_new.append(x_new)
    return X_new