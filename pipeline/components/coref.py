import signal
import spacy
import sys
from neuralcoref.neuralcoref import Cluster, get_resolved
from spacy.tokens import Doc, Span, Token
from spacy.vocab import Vocab

from ..utils_classes import Debugger
from ..utils_methods import add_extension, agreement, get_ent_subjects, get_ents, get_head_nouns, get_morph_attr, get_synonyms, is_direct_speech, is_NE, is_NN, is_PRON, longest_common_prefix, stringify


# enable/disable debug logs with True/False
debugger = Debugger(False)


def rb_coref(doc):
    """Spacy pipeline component.
        Adds coreference clusters to the document.
        Re-implementation of the algorithm presented in Krug et al. (2015):
        "Rule-based Coreference Resolution in German Historic Novels".
        Works both for UD and TIGER relations.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    ents = get_ents(doc, remove_doublings=True)
    synonyms = [None] * len(ents) # set of synonyms for each entity
    # (initialised with None and only computed if needed in pass 8)
    
    # compute the clusters
    clusters = []
    lookahead = []
    for i, ent in enumerate(ents):
        is_pron = is_PRON(ent)

        # check whether the current entity is an indefinite, interrogative or expletive pronoun;
        # these should be excluded from coreference resolution:
        if is_pron and (ent.root._.morph.pron_type in ["ind|neg|tot", "int"] or (ent.root.lower_ == "es" and ent.root.dep_.split(":")[0] in ["expl", "ep"])):
            debugger.print("-", ent)
            continue

        # check whether current entity covers the end of the last entity 
        # (happens sometimes if entities from doc.ents and doc.noun_chunks overlap);
        # if so, ignore the current (i.e. shorter) entity:
        if len(clusters) > 0 and ent.end == ents[clusters[0][0]].end:
            debugger.print(0, ent)
            continue
        
        # for each entity, go through all passes until a cluster is found
        # (some passes are only for pronouns, others only for nouns):
        if (not is_pron) and pass_1_exact_match(ents, i, clusters):
            debugger.print(1, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_2_nameflexion(ents, i, clusters):
            debugger.print(2, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_3_attributes(ents, i, clusters):
            debugger.print(3, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (is_pron or ent.root.dep_.split(":")[0] in ["appos", "app"]) and pass_4_precise_constructs(ents, i, clusters):
            debugger.print(4, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_5_strict_head_match(ents, i, clusters):
            debugger.print(5, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_6_relaxed_head_match(ents, i, clusters):
            debugger.print(6, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_7_title_match(ents, i, clusters):
            debugger.print(7, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if (not is_pron) and pass_8_semantic_pass(ents, i, clusters, synonyms):
            debugger.print(8, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if is_pron and pass_9_pronoun_resolution(ents, i, clusters):
            debugger.print(9, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if is_pron and pass_10_detection_of_the_addressed_person_in_direct_speech(ents, i, clusters):
            debugger.print(10, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue
        if is_pron and pass_11_pronouns_in_direct_speech(ents, i, clusters):
            debugger.print(11, i, clusters[0][1], i-clusters[0][1], ent, ents[clusters[0][1]])
            continue

        # if the entity appears within direct speech, it might be the case that speaker and addressee are mentioned 
        # after the entity and have not been seen, yet; we push the entity on a lookahead list and try to cluster it 
        # after we went through all entities:
        if is_pron and is_direct_speech(ent) and ("1per" in ent.root._.morph.person_ or "2per" in ent.root._.morph.person_):
            lookahead.append(i)
            continue
        
        # if no cluster is found, the entity gets a new cluster:
        clusters.insert(0, [i])
        debugger.print("+", i, ent)

    # do pass 11 again with the lookahead entities
    for i in lookahead:
        
        if pass_11_pronouns_in_direct_speech(ents, i, clusters):
            debugger.print(11, i, clusters[0][1], i-clusters[0][1], ents[i], ents[clusters[0][1]])
            continue
        
        # if no cluster is found, the entity gets a new cluster:
        clusters.insert(0, [i])
        debugger.print("++", i, ents[i])
            
    # re-sort the order of clusters and entities
    clusters = sorted(clusters, key=lambda cluster: cluster[-1])
    clusters = [Cluster(i, ents[cluster[-1]], [ents[j] for j in reversed(cluster)]) for i, cluster in enumerate(clusters)]
    
    fill_doc_with_coref_clusters(doc, clusters)
    
    return doc


def fill_doc_with_coref_clusters(doc, clusters):
    """Adds coreference clusters to a document.
    
    Args:
        doc (`Doc`): The document.
        clusters (list of `Cluster`): All coref clusters of mentions in the document.
    
    """
    add_extension(Doc, "has_coref")
    add_extension(Doc, "coref_clusters")
    add_extension(Doc, "coref_resolved")
    add_extension(Doc, "coref_scores")
    add_extension(Span, "is_coref")
    add_extension(Span, "coref_cluster")
    add_extension(Span, "coref_scores")
    add_extension(Token, "in_coref")
    add_extension(Token, "coref_clusters")
    
    # initialise Token-level cluster lists
    for token in doc:
        token._.coref_clusters = []

    # fill Doc, Span, Token properties
    doc._.has_coref = True
    doc._.coref_clusters = clusters
    doc._.coref_resolved = get_resolved(doc, clusters)
    doc._.coref_scores = None # not used
    for cluster in clusters:
        for mention in cluster.mentions:
            mention._.is_coref = True
            mention._.coref_cluster = cluster
            mention._.coref_scores = None # not used
            for token in mention:
                token._.in_coref = True
                if cluster not in token._.coref_clusters:
                    token._.coref_clusters.append(cluster)


# Two variants to iterate over recent entities:

# variant 1
def iterate_over_recent_clusters(ents, index, clusters, merge_condition):
    for j, cluster in enumerate(clusters):
        if merge_condition(ents, cluster, index):
            clusters[j].insert(0, index)
            clusters.insert(0, clusters.pop(j))
            return True
    return False

# variant 2
"""
def iterate_over_recent_entities(ents, index, clusters, merge_condition, k=500):
    for i in sorted([c for cluster in clusters for c in cluster], reverse=True)[:k]:
        if merge_condition(ents, [i], index):
            for j, cluster in enumerate(clusters):
                if i in cluster:
                    clusters[j].insert(0, index)
                    clusters.insert(0, clusters.pop(j))
                    return True
    return False
"""
def iterate_over_recent_entities(ents, index, clusters, merge_condition):
    for i, j in sorted([(ci, cj) for cj, cluster in enumerate(clusters) for ci in cluster], key=lambda c: c[0], reverse=True):
        if merge_condition(ents, [i], index):
            clusters[j].insert(0, index)
            clusters.insert(0, clusters.pop(j))
            return True
    return False


# alarm handler for function `pass_container`
def alarm_handler(signum, frame):
    raise TimeoutError()


def pass_container(ents, index, clusters, merge_condition, timeout=2):
    """Tries to find a cluster (of previous entities) for the current entity.

    Args:
        ents (list of `Span`): List of all entities in the document.
        index (int): Index of the current entity.
        clusters (list of (list of int)): List of clusters. A cluster is a list of indices of entities.
            The order is all-reverse:
                The most recently updated cluster is the first cluster in `clusters` etc.
                The most recently added index is the first index in a cluster etc.
        merge_condition (func): A function taking three arguments: `ents`, `cluster`, `index`;
            and returning whether the entity with index `index` can be merged with the cluster.
        timeout (int): Time in seconds after which this function is terminated.
    
    Returns:
        boolean: True iff a cluster could be found.
            Note that `index` is added to the corresponding cluster in `clusters`.
    
    """
    signal.signal(signal.SIGALRM, alarm_handler)
    signal.alarm(timeout)
    try:
        cluster_found = iterate_over_recent_entities(ents, index, clusters, merge_condition)
    except TimeoutError:
        cluster_found = False
    signal.alarm(0)
    return cluster_found


# The following passes 1-11 are re-implementations of Krug et al. (2015):
# "Rule-based Coreference Resolution in German Historic Novels".

def pass_1_exact_match(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        for i in cluster:
            ent2 = ents[i]
            if (is_NE(ent1) or is_NE(ent2)) and stringify(ent1) == stringify(ent2):
                return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_2_nameflexion(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        text1 = stringify(ent1)
        for i in cluster:
            ent2 = ents[i]
            text2 = stringify(ent2)
            p = len(longest_common_prefix(text1, text2))
            suffixes = ["en", "chen", "lein", "i"]
            if p > 1 and (is_NE(ent1) or is_NE(ent2)) and (text1[p:] in suffixes or text2[p:] in suffixes):
                return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_3_attributes(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        for form in ["text", "lemma_"]:
            adjs1, nouns1, propns1 = get_head_nouns(ent1, form=form)
            text1 = " ".join(adjs1 + nouns1 + propns1)
            if text1 != "":
                gender1 = get_morph_attr(ent1, "gender")
                number1 = get_morph_attr(ent1, "numerus")
                for i in cluster:
                    ent2 = ents[i]
                    adjs2, nouns2, propns2 = get_head_nouns(ent2, form=form)
                    text2 = " ".join(adjs2 + nouns2 + propns2)
                    if text2 != "" and len(adjs1+adjs2) > 0:
                        gender2 = get_morph_attr(ent2, "gender")
                        number2 = get_morph_attr(ent2, "numerus")
                        if agreement(gender1, gender2) and agreement(number1, number2) and (text1.startswith(text2 + " ") or text2.startswith(text1 + " ")):
                            return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_4_precise_constructs(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        for i in cluster:
            ent2 = ents[i]
            if ent1.root.dep_.split(":")[0] in ["appos", "app"]:
                return ent1.root.head.i == ent2.root.i
            elif is_PRON(ent1) and ent1.root.sent == ent2.root.sent:
                match = False
                m = ent2.root._.morph
                if ent1.root.head.dep_.split(":")[0] in ["acl", "rc"] and [tok for tok in ent1.root.head.children if not (tok.is_punct or tok.is_space)][0].i == ent1.root.i:
                    if (
                        agreement(ent1.root._.morph.person_, m.person_) and 
                        agreement(ent1.root._.morph.numerus_, m.numerus_)
                    ):
                        match = True
                elif ent1.root.lower_ in ["mich", "dich", "sich", "uns", "euch"]:
                    if (
                        (ent1.root.lower_ == "mich" and "1per" in m.person_ and "sing" in m.numerus_) or
                        (ent1.root.lower_ == "dich" and "2per" in m.person_ and "sing" in m.numerus_) or
                        (ent1.root.lower_ == "uns" and "1per" in m.person_ and "plu" in m.numerus_) or
                        (ent1.root.lower_ == "euch" and "2per" in m.person_ and "plu" in m.numerus_) or
                        (ent1.root.lower_ == "sich" and "1per" not in m.person_ and "2per" not in m.person_)
                    ):
                        match = True
                if match:
                    ent1.root._.morph.gender = m.gender
                    ent1.root._.morph.gender_ = m.gender_
                    ent1.root._.morph.numerus = m.numerus
                    ent1.root._.morph.numerus_ = m.numerus_
                    return True
        return False
    return pass_container(ents, index, clusters, cond)

def head_match_cond(ents, cluster, index, strict, pos_):
    """Checks whether the current entity should be merged with a cluster.
        Used for passes 5, 6 and 7.

    Args:
        ents (list of `Span`): List of all entities in the document.
        cluster (list of int): List of indices of entities.
        index (int): Index of current entity.
        strict (boolean): Exact word match or substring match.
        pos_ (str): POS tag of entitiy's head (should be "PROPN" or "NOUN").
    
    Returns:
        boolean: True iff the entity has a head match with one of the entities in the cluster.
    
    """
    ent1 = ents[index]
    if is_NE(ent1) or pos_ == "NOUN":
        _, nouns1, propns1 = get_head_nouns(ent1, form="lemma_")
        words1 = propns1
        if pos_ == "NOUN":
            words1 = nouns1
        gender1 = get_morph_attr(ent1, "gender")
        number1 = get_morph_attr(ent1, "numerus")
        for i in cluster:
            ent2 = ents[i]
            if is_NE(ent2):
                _, nouns2, propns2 = get_head_nouns(ent2, form="lemma_")
                words2 = propns2
                if pos_ == "NOUN":
                    words2 = nouns2
                gender2 = get_morph_attr(ent2, "gender")
                number2 = get_morph_attr(ent2, "numerus")
                if agreement(gender1, gender2) and agreement(number1, number2):
                    if strict:
                        if len(set(words1).intersection(set(words2))) > 0:
                            return True
                    else:
                        for w1 in words1:
                            for w2 in words2:
                                if w1 in w2 or w2 in w1:
                                    return True
    return False

def pass_5_strict_head_match(ents, index, clusters):
    def cond(ents, cluster, index):
        return head_match_cond(ents, cluster, index, True, "PROPN")
    return pass_container(ents, index, clusters, cond)

def pass_6_relaxed_head_match(ents, index, clusters):
    def cond(ents, cluster, index):
        return head_match_cond(ents, cluster, index, False, "PROPN")
    return pass_container(ents, index, clusters, cond)

def pass_7_title_match(ents, index, clusters):
    def cond(ents, cluster, index):
        return head_match_cond(ents, cluster, index, True, "NOUN")
    return pass_container(ents, index, clusters, cond)

def pass_8_semantic_pass(ents, index, clusters, synonyms):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        if not is_PRON(ent1):
            gender1 = get_morph_attr(ent1, "gender")
            number1 = get_morph_attr(ent1, "numerus")
            if synonyms[index] is None:
                _, words1, _ = get_head_nouns(ent1, form="lemma_")
                synonyms[index] = [lemma for w in words1 for lemma in get_synonyms(w)]
            synonyms1 = set(synonyms[index])
            for i in cluster:
                ent2 = ents[i]
                if not is_PRON(ent2):
                    gender2 = get_morph_attr(ent2, "gender")
                    number2 = get_morph_attr(ent2, "numerus")
                    if synonyms[i] is None:
                        _, words2, _ = get_head_nouns(ent2, form="lemma_")
                        synonyms[i] = [lemma for w in words2 for lemma in get_synonyms(w)]
                    synonyms2 = set(synonyms[i])
                    if agreement(gender1, gender2) and agreement(number1, number2):
                        if len(synonyms1.intersection(synonyms2)) > 0:
                            return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_9_pronoun_resolution(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        if is_PRON(ent1):
            gender1 = get_morph_attr(ent1, "gender")
            number1 = get_morph_attr(ent1, "numerus")
            person1 = get_morph_attr(ent1, "person")
            direct1 = is_direct_speech(ent1)
            for i in cluster:
                ent2 = ents[i]
                subjs = get_ent_subjects(ents, ent2.root.sent)
                if ent2 not in subjs:
                    subjs = [ents.index(subj) for subj in reversed(subjs)]
                    if cond(ents, [j for j in subjs if j < index], index):
                        return False
                gender2 = get_morph_attr(ent2, "gender")
                number2 = get_morph_attr(ent2, "numerus")
                person2 = get_morph_attr(ent2, "person")
                direct2 = is_direct_speech(ent2)
                if agreement(gender1, gender2) and agreement(number1, number2, strict=True) and agreement(person1, person2) and (
                    (not direct1 and not direct2) or (direct1 and direct2 and ent1.root._.speech_segment == ent2.root._.speech_segment)
                    # (either both entities are not within direct speech or both are within the same direct speech segment)
                ):
                    return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_10_detection_of_the_addressed_person_in_direct_speech(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        direct1 = is_direct_speech(ent1)
        person1 = get_morph_attr(ent1, "person")
        if direct1 and "2per" in person1:
            number1 = get_morph_attr(ent1, "numerus")
            for i in cluster:
                ent2 = ents[i]
                direct2 = is_direct_speech(ent2)
                if (
                    direct2 and is_NE(ent2) and 
                    ent1.root.sent == ent2.root.sent and # same sentence
                    ent1.root._.speech_segment == ent2.root._.speech_segment # same direct speech segment
                ):
                    number2 = get_morph_attr(ent2, "numerus")
                    if agreement(number1, number2, strict=True):
                        return True
        return False
    return pass_container(ents, index, clusters, cond)

def pass_11_pronouns_in_direct_speech(ents, index, clusters):
    def cond(ents, cluster, index):
        ent1 = ents[index]
        if is_direct_speech(ent1):
            person1 = get_morph_attr(ent1, "person")
            if "1per" in person1 or "2per" in person1:
                number1 = get_morph_attr(ent1, "numerus")
                number2 = get_morph_attr(ents[ent1.root._.speaker], "numerus")
                if ent1.root._.speaker in cluster and "1per" in person1 and agreement(number1, number2, strict=True):
                    return True
                number2 = get_morph_attr(ents[ent1.root._.addressee], "numerus")
                if ent1.root._.addressee in cluster and "2per" in person1 and agreement(number1, number2, strict=True):
                    return True
        for i in cluster:
            if i < index and cond(ents, [index], i):
                return True
        return False
    return pass_container(ents, index, clusters, cond)
