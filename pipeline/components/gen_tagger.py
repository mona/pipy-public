import spacy
from sklearn.tree import DecisionTreeClassifier
from spacy.tokens import Doc, Span

from ..classifiers.clf_dfg_tagger import get_clf_dfg_tagger
from ..classifiers.clf_gen_tagger import get_clf_gen_tagger
from ..passage import Passage
from ..global_wordlists import *
from ..utils_classes import Debugger
from ..utils_methods import *


# enable/disable debug logs with True/False
debugger = Debugger(False)


LABEL_NAMES = {
    "binary" : ["GI"],
    "multi" : ["NONE", "ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"]
}


def rb_gen_tagger(doc, bare_policy="single", neg_policy="single", exclude_tags=["FAST", "ZAHL"], check_deixis=True, check_corefs=(3,1), check_gnomic=True):
    """Spacy pipeline component.
        Add GI tags to clauses.

    Args:
        doc (`Doc`): A spacy document object.
        bare_policy (str): Specifies how the BARE tag should be handled for passages with multiple tags.
            "multiple": Keep all tags.
            "single": Remove the BARE tag.
        neg_policy (str): Specifies how the NEG tag should be handled for passages with multiple tags.
            "multiple": Keep all tags.
            "single": Keep only NEG tag.
        exclude_tags (list of str): Tags that should not be removed from the output.
        check_deixis (boolean): If False, treats every quantified restrictor as being generic.
            Otherwise checks for deictic expressions to determine whether a restrictor is specific/generic.
        check_corefs (int,int): Number of coreferenced mentions before and after each potential restrictor to check.
            (Useful to resolve anaphoric pronouns.)
        check_gnomic (boolean): If True,
            1. a VP is generic as soon as it potentially expresses gnomic present tense.
            2. plural NPs with determiners are generic only if they are in a present tense clause.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "gis")
    add_extension(Doc, "gis")

    before, after = check_corefs

    for clause in doc._.clauses:
        tags = set()

        # search generalisations with an overt quantifier
        for token in clause._.tokens:
            if token.lemma_.lower() in QUANTIFIERS:
                tag = QUANTIFIERS[token.lemma_.lower()]
                if token.pos_ in ["DET", "ADJ", "ADV"]:
                    restrictor = get_restrictor(token)
                    if not is_specific(restrictor, check_deixis, before, after):
                        # almost all
                        if tag == "ALL" and "fast" in [word.lower_ for word in token.children]:
                            tags.add("FAST")
                        # vague quantifier
                        elif tag == "DIV" and (token.head.pos_ == "ADJ" or (is_NP(restrictor) and "plu" not in restrictor._.morph.numerus_)):
                            pass # (modifiers of adjectives or singular NPs)
                        # any other quantifier
                        else:
                            tags.add(tag)
            # counting quantifier
            elif token.lemma_.lower() in NUMBERS:
                restrictor = get_restrictor(token)
                if not is_specific(restrictor, check_deixis, before, after):
                    if token.lemma_ in ["zwei", "drei", "vier"]:
                        pass # (very small numbers do not generalise well)
                    else:
                        tags.add("ZAHL")
        
        # search existentials
        for token in clause._.tokens:
            if token.lemma_ == "existieren" or (token.lemma_ == "geben" and "es" in [word.lower_ for word in token.children]):
                for word in token.children:
                    if is_NP(word) and not is_specific(word, check_deixis, before, after):
                        tags.add("EXIST")

        # search genuine generics
        for token in clause._.tokens:
            if no_quantifiers_in_subtree(token):
                if is_generic_NP(token, before, after, check_gnomic) or is_generic_VP(token, check_gnomic):
                    if not is_specific(token, check_deixis, before, after):
                        if has_vague_attr(token):
                            tags.add("DIV")
                        else:
                            tags.add("BARE")
        
        debugger.print(clause)
        debugger.print(tags)
        debugger.input()
        
        # remove unwanted tags
        tags.difference_update(set(exclude_tags))

        # assign passage to clause
        clause._.gis = []
        if len(tags) > 0:
            clause._.gis.append(Passage([clause], tags))

    # handle multi-clause constructions
    expand_clauses_to_passages(doc)
    
    # assign passages to doc; while doing so, search negations and handle NEG and BARE policies
    doc._.gis = []
    for clause in doc._.clauses:
        for passage in clause._.gis:
            if passage not in doc._.gis:
                handle_negs(passage, neg_policy)
                handle_bares(passage, bare_policy)
                doc._.gis.append(passage)
    
    return doc


def handle_negs(passage, neg_policy):
    """Assures the NEG policy for a passage.

    Args:
        passage (`Passage`): A passage object.
        neg_policy (str): Specifies how the NEG tag should be handled for passages with multiple tags.
            "multiple": Keep all tags.
            "single": Keep only NEG tag.

    """
    for clause in passage.clauses:
        for token in clause._.tokens:
            if token.lemma_.lower() in NEGATIONS:
                passage.tags.add("NEG")
                if neg_policy == "single":
                    passage.tags.intersection_update(set(["NEG"]))
                elif neg_policy != "multiple":
                    raise ValueError("Unknown neg_policy: " + neg_policy)
                break


def handle_bares(passage, bare_policy):
    """Assures the BARE policy for a passage.

    Args:
        passage (`Passage`): A passage object.
        bare_policy (str): Specifies how the BARE tag should be handled for passages with multiple tags.
            "multiple": Keep all tags.
            "single": Remove the BARE tag.

    """
    if len(passage.tags) > 1:
        if bare_policy == "single":
            passage.tags.discard("BARE")
        elif bare_policy != "multiple":
            raise ValueError("Unknown bare_policy: " + bare_policy)


def expand_clauses_to_passages(doc):
    """Expand GI lables from clauses to passages.
        Merges the clauses of a conditional or relative construction into one passage.

    Args:
        doc (`Doc`): A spacy document object.
    
    """
    for sent in doc.sents:
        constructions = []
        for clause in sent._.clauses:
            conditional = False
            relative = False
            for token in clause:
                if token.lower_ in ["wenn", "wer", "wem", "wen"]:
                    conditional = True
                    break
            if clause.root.dep_.split(":")[0] == "acl":
                relative = True
            if conditional or relative:
                clauses = set()
                clauses.add(sent._.clauses.index(clause))
                try:
                    clauses.add(sent._.clauses.index(clause.root.head._.clause))
                except ValueError:
                    pass
                constructions.append(clauses)
                for n, construction in enumerate(constructions[:-1]):
                    if len(construction.intersection(clauses)) > 0:
                        constructions[n].update(clauses)
                        constructions.pop()
                        break
        for construction in constructions:
            clauses = [sent._.clauses[i] for i in construction]
            tags = set()
            for clause in clauses:
                if len(clause._.gis) > 0:
                    tags.update(clause._.gis[0].tags)
            if len(tags) > 0:
                passage = Passage(clauses, tags)
                for clause in clauses:
                    clause._.gis = [passage]


def connect_clauses_to_passages(doc):
    """Expand GI lables from clauses to passages.
        Connects subsequent clauses with identical(!) tags into one passage.
        (Two clauses are not connected if there is at least one tag that is not assigned to both clauses.)

    Args:
        doc (`Doc`): A spacy document object.
    
    """
    current_passage_clauses = []
    current_passage_tags = set()
    for clause in doc._.clauses:
        if len(clause._.gis) > 0 and clause._.gis[0].tags == current_passage_tags:
            current_passage_clauses.append(clause)
        else:
            if len(current_passage_clauses) > 1 and len(current_passage_tags) > 0:
                current_passage = Passage(current_passage_clauses, current_passage_tags)
                for current_passage_clause in current_passage_clauses:
                    current_passage_clause._.gis = [current_passage]
            current_passage_clauses = [clause]
            if len(clause._.gis) > 0:
                current_passage_tags = clause._.gis[0].tags
            else:
                current_passage_tags = set()


def clf_gen_tagger(doc, texts_train=None, classifier=DecisionTreeClassifier):
    """Spacy pipeline component.
        Add GI tags to clauses using a statistical classifier.

    Args:
        doc (`Doc`): A spacy document object.
        texts_train (list of str): List of training texts.
        classifier (class): A classifier type.
    
    Returns:
        `Doc`: A spacy document object.

    """
    clf_predict = get_clf_gen_tagger(texts_train, classifier)

    add_extension(Span, "gis")
    add_extension(Doc, "gis")
    
    for i, clause in enumerate(doc._.clauses):
        tags = clf_predict(doc._.clauses, i)

        debugger.print(clause)
        debugger.print(tags)
        debugger.input()

        # assign passage to clause
        clause._.gis = []
        if len(tags) > 0:
            clause._.gis.append(Passage([clause], tags))
    
    # handle multi-clause constructions
    connect_clauses_to_passages(doc)
    
    # assign passages to doc
    doc._.gis = []
    for clause in doc._.clauses:
        for passage in clause._.gis:
            if passage not in doc._.gis:
                doc._.gis.append(passage)
    
    return doc


def clf_dfg_gen_tagger(doc, texts_train=None, texts_dev=None, label_condition="multi", classifier=DecisionTreeClassifier, param_grid=None, window=(-1, 1), disable_feats=[], enable_feats=[]):
    """Spacy pipeline component.
        Add GI tags to clauses using a statistical classifier.

    Args:
        doc (`Doc`): A spacy document object.
        texts_train (list of str): List of training texts.
        texts_dev (list of str): List of development texts.
        label_condition (str): Label condition ("multi" or "binary").
        classifier (class): A classifier type.
        window ((int,int)): Context window.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "gis")
    add_extension(Doc, "gis")
    
    clf_predict = get_clf_dfg_tagger(None, texts_train, texts_dev, ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"], "GI", label_condition, classifier, param_grid, window, disable_feats, enable_feats)
    labels = clf_predict(doc._.clauses)

    transfer_labels_to_passages(doc, "gis", labels)
    
    return doc


def neural_gen_tagger(doc, label_condition="multi"):
    """Spacy pipeline component.
        Add generalising passages to the document.
        Uses the code and models from here:
        https://github.com/tschomacker/generalizing-passages-identification-bert

    Args:
        doc (`Doc`): A spacy document object.
        label_condition (str): Label condition ("multi" or "binary").
    
    Returns:
        `Doc`: A spacy document object.

    """
    global REFLECTION_MODELS
    REFLECTION_MODELS.provide()
    model = REFLECTION_MODELS._["gi_" + label_condition]
    label_names = LABEL_NAMES[label_condition]

    add_extension(Span, "gis")
    add_extension(Doc, "gis")

    labels = []
    sents = [None] + list(doc.sents) + [None]
    for i in range(1, len(sents)-1):
        context_tokens = []
        try:
            context_tokens.extend(list(sents[i-1]))
        except TypeError:
            pass # first sentence
        context_tokens.extend(list(sents[i]))
        try:
            context_tokens.extend(list(sents[i+1]))
        except TypeError:
            pass # last sentence
        context_tokens = [token for token in context_tokens if not token.is_space]
        
        for clause in sents[i]._.clauses:
            clause_open = False
            clause_text_embed_in_context = ''
            for token in context_tokens:
                if token._.clause is not None:
                    # token is part of the current clause
                    if token._.clause == clause:
                        if not clause_open:
                            # start the clause mark up
                            clause_text_embed_in_context += '<b>'
                            clause_open = True
                    # token is NOT part of the current clause
                    else:
                        if clause_open:
                            # close the clause mark up
                            clause_text_embed_in_context += '</b>'
                            clause_open = False
                    clause_text_embed_in_context += ' ' + token.text
                # punctation
                else:
                    clause_text_embed_in_context += token.text
                # closing the mark up, in case the passage conists of a single clause
            if clause_open:
                clause_text_embed_in_context += '</b>'
            
            prediction = [round(x) for x in model.predict(clause_text_embed_in_context)]
            labels.append(set([label_names[label_i] for label_i, label_name in enumerate(label_names) if bool(prediction[label_i]) and label_name != "NONE"]))
    
    transfer_labels_to_passages(doc, "gis", labels)
    
    return doc