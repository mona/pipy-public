import json
import os
import nltk
import spacy
from nltk.tokenize import sent_tokenize
from settings import NLTK_PATH
nltk.data.path.append(NLTK_PATH)

from ..global_resources import SPACY_MODEL
from ..utils_methods import get_doc_text


def nltk_sentencizer(doc):
    """Spacy pipeline component.
        Splits the document into sentences using the `sent_tokenize` from NLTK.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    sents = sent_tokenize(doc.text, language="german")
    return align_sents(doc, sents)


def spacy_sentencizer(doc):
    """Spacy pipeline component.
        Splits the document into sentences using the `de_core_news_lg` model from spacy.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    global SPACY_MODEL
    SPACY_MODEL.provide()
    
    sents = [sent.text for sent in SPACY_MODEL._(doc.text).sents]
    return align_sents(doc, sents)


def align_sents(doc, sents):
    """Aligns the output of a sentence splitter with a spacy document.
        Iterates over every token and sets the `is_sent_start` property.
    
    Args:
        doc (`Doc`): A spacy document object.
        sents (list of str): The output of an external sentence splitter applied to the document's text.
    
    Returns:
        `Doc`: A spacy document object.
    
    """
    if len(sents) == 0:
        return doc
    current_sent_tool = sents.pop(0)
    current_sent_spacy = ""
    for token in doc:
        if current_sent_tool.strip() == current_sent_spacy.strip():
            token.is_sent_start = True
            if len(sents) == 0:
                return doc
            current_sent_tool = sents.pop(0)
            current_sent_spacy = ""
        else:
            token.is_sent_start = False
        current_sent_spacy += token.text_with_ws
    if len(doc) > 0:
        doc[0].is_sent_start = True
    return doc