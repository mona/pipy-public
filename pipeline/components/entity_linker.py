import copy
import imp
import re
import spacy
import sys
import time

from operator import imod
from spacy.tokens import Doc, Span
from qwikidata.entity import WikidataItem, WikidataLexeme, WikidataProperty
from qwikidata.json_dump import WikidataJsonDump
from qwikidata.sparql import return_sparql_query_results
from SPARQLWrapper import SPARQLWrapper, JSON
from typing import Iterable
from spacy.tokenizer import Tokenizer
from spacy.lang.de import German

from ..global_constants import CORPUS_RECORDS
from ..global_identifiers import FICTIONAL_ITEMS, REAL_ITEMS, REAL_ITEMS_INVERTED, REAL_CONCEPTS
from ..utils_classes import Entity, EntitySet, Debugger
from ..utils_methods import add_extension, get_doc_text
from settings import ROOT_PATH 

sys.path.append(ROOT_PATH)
from corpusreader.corpus import Record
from corpusreader.classes.dbpedia import DBpedia
from corpusreader.classes.wikidata import Wikidata, WikidataSet


# enable/disable debug logs with True/False
debugger = Debugger(True)


def wikidata_entity_linker(doc: Doc) -> Doc:
    """Method to link a spacy ent with an Entity-object that contains information about its fictionality status 
    (attributes `fictional` and `real`). If disambiguation is successful (currently by judging amount of sitelinks) a concrete Wikidata-object is given as attribute
    `wikidata` (otherwise attribute `wikidata` is None).

    Args:
        doc (`Doc`): The spacy document.

    Returns
        doc (`Doc`): The spacy document with enriched ents.
    """
    add_extension(Span, "entity")

    # collect metadata
    text = get_doc_text(doc, normalized=False)
    record = None

    if len(CORPUS_RECORDS) > 0:
        record = CORPUS_RECORDS[-1].get_record(text)

    debugger.print("Entity Linker in progress...")

    # get entities (fictional / real) with instance_of first, select query later in qwikidata dict:
    fictional_items_qwikidata_results = list()
    for key in FICTIONAL_ITEMS:
        fictional_items_qwikidata_results.append(Wikidata.wikidata_request_time_handling(
                                                        find_instances_of,
                                                        instance_object_wikidata_id=FICTIONAL_ITEMS[key]
                                                        )
                                                )
    real_items_qwikidata_results = list()
    for key in REAL_ITEMS:
        real_items_qwikidata_results.append(Wikidata.wikidata_request_time_handling(
                                                    find_instances_of,
                                                    instance_object_wikidata_id=REAL_ITEMS[key]
                                                    )
                                            )

    # link ent with wikidata entry (disambiguate if necessary)
    entities_for_ent_dict = dict()

    for ent in doc.ents:
        if ent[0].ent_type_ in ["PER", "LOC"]:
            queries = set()
            queries.add(ent.text)
 
            if ent[0].ent_type_ == 'PER':
                # add alternative spelling of ent_string
                queries = queries.union(get_entities_for_alternative_spellings(ent))
                # add prefered_coref_string (if different from ent_string) and alternative spelling for prefered_coref_string
                queries = queries.union(get_prefered_coref(ent=ent, alternative_spellings=False))
                queries = queries.union(get_prefered_coref(ent=ent, alternative_spellings=True))

                if frozenset(queries) in entities_for_ent_dict:
                    entities = copy.deepcopy(entities_for_ent_dict[frozenset(queries)])
                else:
                    entities = get_entities(    ent=ent,
                                                    fictional_items_qwikidata_results=fictional_items_qwikidata_results,
                                                    real_items_qwikidata_results=real_items_qwikidata_results,
                                                    record=record,
                                                    queries=queries
                                                    )
                    entities_for_ent_dict[frozenset(queries)] = copy.deepcopy(entities)

            elif ent[0].ent_type_ == 'LOC':
                for e in get_entities_for_alternative_spellings(ent):
                    queries.add(e)
                prefered_coref_ent = get_longest_coref(ent)
                if prefered_coref_ent != None:
                    if ent.text != prefered_coref_ent.text and ent.lemma_ != prefered_coref_ent.text:
                        queries.add(prefered_coref_ent.text)
                        alternatives_for_coref = get_entities_for_alternative_spellings(prefered_coref_ent)
                        for alternative_coref in alternatives_for_coref:
                            queries.add(alternative_coref)

                if frozenset(queries) in entities_for_ent_dict:
                    entities = copy.deepcopy(entities_for_ent_dict[frozenset(queries)])
                else:
                    entities = get_entities(    ent=ent,
                                                    record=record,
                                                    queries=queries
                                                    )
                    entities_for_ent_dict[frozenset(queries)] = copy.deepcopy(entities)

            if len(entities) == 0:
                entity = Entity(queried_strings=queries, record=record, ent_type=ent[0].ent_type_)
                # ents which are not found in wikidata become fictional by default
                entity.fictional = True
                entity.real = False
                ent._.entity = entity
            
            else:
                found_entity = disambiguate_entities(   ent=ent, 
                                                        entities=entities, 
                                                        record=record              
                                                        )
                ent._.entity = found_entity

        else:
            ent._.entity = None

    return doc

def get_entities(ent: Span, record: Record, queries: set, fictional_items_qwikidata_results: dict = None, real_items_qwikidata_results: dict = None) -> EntitySet:
    """Get list of Entity class items for a Spacy ent.

    Args:
        ent (`Span`): An entity span.
        record (`Record`): Record-object with metadata for the current doc.
        queries (set): set of string.
        fictional_items_qwikidata_results (dict): dict of all items (subject) which are "instance_of" a fictional item (object) (see fictional items in `FICTIONAL_ITEMS`)
        real_items_qwikidata_results (dict): dict of all items (subject) which are "instance_of" a real item (object) (see fictional items in `REAL_ITEMS`)
    
    Returns:
        `EntitySet`: set of `Entity`.
    """
    # entityset object for entity candidats
    entities = EntitySet(queried_strings=queries)

    # set of wikidataitem_ids for entity candidats; to be added to "entities" (entityset object)
    entity_candidats_wikidata_ids = set()

    # clean queries
    queries = clean_queries(queries)

    for query in queries:
        if len(query) > 1:
            # PER ents
            if ent[0].ent_type_ == 'PER':
                # check if query_string is in label of qwikidata result with fictional/real items:
                ### query for fictional items
                #############################
                for qwikidata_results in fictional_items_qwikidata_results:
                    entity_candidats_wikidata_ids.update(filter_sparql_results_by_query_string(
                                                                                    query=query, 
                                                                                    qwikidata_sparql_results=qwikidata_results
                                                                                )
                                                        )
                ### query for real items
                ########################
                for qwikidata_results in real_items_qwikidata_results:
                    entity_candidats_wikidata_ids.update(filter_sparql_results_by_query_string(
                                                                            query=query,
                                                                            qwikidata_sparql_results=qwikidata_results
                                                                        )
                                        )
                # find humans in dpedia (by string contains), get wikidata entries
                # TODO at the moment wikidata_set isn't checked if all foaf:humans have instance_of/human relation in Wikidata (since results only come from DBpedia)
                if len(query) > 2:
                    wikidata_set = search_string_in_dbpedia_filter_with_wikidata(string=query)
                    for wikidata_object in wikidata_set:
                        entities.add(Entity(queried_strings=queries, record=record, ent_type=ent[0].ent_type_, wikidata_object=wikidata_object))

            # LOC ents
            if ent[0].ent_type_ == 'LOC':
                results = Wikidata.wikidata_request_time_handling(
                            get_location_candidates,
                            query_string=query
                        )
                #if len(results) == 0:               
                    #results = get_entities_for_alternative_spellings(ent)
                for result in results:
                    entities.add(Entity(queried_strings=queries, wikidata_id=result, record=record, ent_type=ent[0].ent_type_))
    
    for wikidata_id in entity_candidats_wikidata_ids:
        entities.add(Entity(queried_strings=queries, wikidata_id=wikidata_id, record=record, ent_type=ent[0].ent_type_))

    return entities

def disambiguate_entities(ent: Span, entities: EntitySet, record: Record) -> Entity:
    """Selects one Entity-object out of EntitySet-object which most likely corresponds
    to the spacy ent (Span) in the text (Doc). The returned Entity-object holds attributes
    `fictional` and `real` to adress it's fictionality status. If disambiguation is successful
    (currently by judging amount of sitelinks) a concrete Wikidata-object is given as attribute
    `wikidata` (otherwise attribute `wikidata` is None).

    Args:
        ent (`Span`): An entity span.
        entities (list of `Entity`): Possible wikidata entries for the entity.
        record (`Record`): record object of the text.
    
    Returns:
        `Entity`: Prefered entity.
    """    
    # TODO integrate length of coref cluster
    # idea: long coref chain -> fictional
    if len(ent[0]._.coref_clusters) > 0:
        length_of_coref_cluster = len(ent[0]._.coref_clusters[0].mentions)
    else:
        length_of_coref_cluster = 0

    for entity in entities:
        if ent[0].ent_type_ == "PER" and entity.wikidata:
            if entity.query_in_work:
                entity.fictional_score += 50

            if entity.fictional_item_ids and len(entity.fictional_item_ids) > 1:
                entity.fictional_score += 20

            if entity.real_item_ids and len(entity.real_item_ids) > 0:
                entity.real_score += entity.wikidata.sitelinks
                for item_id in entity.real_item_ids:
                    entity.real_score += 20
            
            if entity.instance_of_human:
                entity.real_score += entity.wikidata.sitelinks
            
        elif ent[0].ent_type_ == "LOC":
            # check whether the context (sentence) helps to disambiguate the entity (find the correct Q-id)
            # by comaparing it with the description from wikidata
            if entity.wikidata.description["de"] != None:
                nlp = German()
                tokenizer = Tokenizer(nlp.vocab)
                tokens = tokenizer(entity.wikidata.description["de"])
                tokens_l = [t.lemma_ for t in tokens]
                for token in ent.root._.clause:
                    if token.pos_ in ["NOUN", "PROPN"] and token.lemma_ in tokens_l and token.text not in ent.text:
                        entity.real_score += 200
                        break
            if entity.wikidata_id:
                entity.real_score += 100
                entity.real_score += entity.wikidata.sitelinks
            else:
                entity.fictional_score += 100

    found_entity = entities.find_highest_scoring_entity()

    if ent[0].ent_type_ == "PER":
        # remove linked wikidata entries with less than 35 sitelinks:
        if found_entity.wikidata.sitelinks < 35:
            found_entity.wikidata = None
        # if an entity is classified as fictional and a human/real wikidata entry is linked, remove that entry:
        if found_entity.fictional and found_entity.wikidata and (found_entity.instance_of_human or found_entity.wikidata.wikidata_id in REAL_ITEMS_INVERTED):
            found_entity.wikidata = None

    elif ent[0].ent_type_ == "LOC" and found_entity.wikidata.sitelinks < 2:
        found_entity.wikidata = None
        
    return found_entity
    
def clean_queries(queries: set) -> set:
    """Clean queries from chars that SPARQL cannot handle or empty queries.
    """
    for query in queries.copy():
        cleaned_query = clean_query(query)
        queries.remove(query)
        if len(cleaned_query) > 0:
            queries.add(cleaned_query)
    return queries
    
def clean_query(query: str) -> str:
    """Clean query from chars that SPARQL cannot handle or empty queries.
    """
    query = query.replace("'", "")
    query = query.replace("\n", "")
    query = query.strip(" ")
    return query

def search_string_in_dbpedia_filter_with_wikidata(string: str) -> WikidataSet:
    """Method that searches string in dbpedia and finds corresponding foaf-items (real persons).
    For these dbpedia items, wikidata_ids are requesested (for wikidata items that are usually
    should be `instances_of` `human`). If only one dppedia id is found, it directly gets the
    wikidata_id. Otherwise, the wikidata_ids are disambiguated via the hightest sitelinks.
    Mostly, only one Wikidata-object has highest sitelinks but since there can be more, the method
    returns a WikidataSet-object.

    Args:
        string (str)
    Returns:
        `WikidataSet`: WikidataSet-object with Wikidata-objects refering to Wikidata-items of persons
                       that share (same) highest amount of sitelinks.
    """
    dbpedia_ids = DBpedia.dbpedia_request_time_handling( 
                                                        dbpedia_find_persons, 
                                                        string=string
                                                        )
    wikidata_set = WikidataSet()

    if len(dbpedia_ids) == 1:
        wikidata_id = DBpedia.dbpedia_request_time_handling(
                                                            DBpedia.wikidata_id_for_dbpedia_id, 
                                                            dbpedia_id=dbpedia_ids[0]
                                                            )
        # skip if wikidata_id is None 
        # (wikidata_id becomes None if DBpedia entry is linked to mulitple wikidata_ids -> disambiguation needed)
        if wikidata_id:
            wikidata_object = Wikidata(wikidata_id=wikidata_id)
            wikidata_set.add(wikidata_object)
    elif 1 < len(dbpedia_ids) < 15:
        wikidata_ids = list()
        for dbpedia_id in dbpedia_ids:
            wikidata_id = DBpedia.dbpedia_request_time_handling(
                                                                DBpedia.wikidata_id_for_dbpedia_id,
                                                                dbpedia_id=dbpedia_id
                                                                )
            if wikidata_id:
                wikidata_ids.append(wikidata_id)
        wikidata_set = WikidataSet(wikidata_ids=wikidata_ids)
        wikidata_set.find_highest_sitelinks()
    # else:
        # debugger.print("Found %s foaf item(s) in DBPedia, skipping..."%str(len(dbpedia_ids)))
        
    return wikidata_set


def dbpedia_find_persons(string: str) -> list:
    """Method to find dbpedia_ids for persons (foaf:Person) that contain given string.

    Args:
        string (str): String to query dbpedia.
    Returns:
        list: list of dbpedia_id.
    """

    string_split = string.split(" ")
    string_split = [i for i in string_split if len(i) > 1] 
    string_split = ["'"+i+"'" for i in string_split]
    string_join = " AND ".join(string_split)

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql_query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        SELECT * WHERE {
        ?item a foaf:Person .
        ?item rdfs:label ?label .
        ?label bif:contains "%s" .
        filter(langMatches(lang(?label), "de"))
        } 
    """%string_join
    sparql.setQuery(sparql_query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    dbpedia_ids = list()
    for hit in results["results"]["bindings"]:
        dbpedia_id = hit["item"]["value"].split("/")[-1]
        dbpedia_ids.append(dbpedia_id)

    return dbpedia_ids


def get_entities_for_alternative_spellings(entity_iterable: Iterable) -> set:
    """Function that finds alternative spellings of an entity if it contains a word with the suffix "isch".
    Either pass spacy-span to the function or a list of spacy tokens.

    Args:
        entity_iterable (`Span` or `list`): spacy-span object of ent or list of spacy tokens.
        
    Returns:
        set: set of strings for alternative spellings    
    """
        
    POS_strip_list = ["DET", "PRON", "ADP", "CCONJ", "SPACE", "PUNCT"]
    
    alternatives = set()

    for token in entity_iterable:
        if "isch" in token.text:
            alternatives.add(token.text.split("isch")[0]+ "isch")
            
    new_token = []
    
    for token in entity_iterable:
        if token.pos_ not in POS_strip_list:
            new_token.append(token)
            
    n_t = [t.text for t in new_token]                
    alternatives.add(" ".join(n_t).strip())

    m = False
    n = False
    f = False
    gen = False
    #not_nom = False
    
    for token in new_token:
        if token.pos_ in ["NOUN", "PROPN"]:
            if token._.morph.gender in ["masc"]:
                m = True
            elif token._.morph.gender in ["neut"]:
                n = True
            elif token._.morph.gender in ["fem"]:
                f = True
                
            if token._.morph.case in ["gen"]:
                gen = True
            #elif token._.morph.case not in ["nom"]:
            #    not_nom = True
            
    modified_token1 = []
    modified_token2 = []
    for token in new_token:
        if token.pos_ in ["ADJ"]:
            if m:
                if token.text[-2:] in ["en", "es", "em"]:
                    modified_token1.append(token.text[:-2] + "er")
                    modified_token2.append(token.text[:-2] + "er")
            elif n:
                if token.text[-2:] in ["en", "es"]:
                    modified_token1.append(token.text[:-2] + "es")
                    modified_token2.append(token.text[:-2] + "es")
            elif f:
                if token.text[-2:] in ["en"]:
                    modified_token1.append(token.text[:-2] + "e")
                    modified_token2.append(token.text[:-2] + "e")
            else:
                modified_token1.append(token.text)
                modified_token2.append(token.text)
                
        elif token.pos_ in ["NOUN", "PROPN"]:
            if (m or n) and gen:
                if token.text[-2:] in ["en", "es"]:
                    modified_token1.append(token.text[:-2])
                    # Gebäudes gen
                    modified_token2.append(token.text[:-1])
                elif token.text[-1] in ["s", "n"]:
                    modified_token1.append(token.text[:-1])
                    modified_token2.append(token.text[:-1])
                else:
                    modified_token1.append(token.text)
                    modified_token2.append(token.text)
            else:
                modified_token1.append(token.text)
                modified_token2.append(token.text)
        else:
            modified_token1.append(token.text)
            modified_token2.append(token.text)
            
    alternatives.add(" ".join(modified_token1).strip())
    alternatives.add(" ".join(modified_token2).strip()) 

    token_with_ss = []
    for token in new_token:
        if "ß" in token.text:
            token_with_ss.append(token.text.replace("ß", "ss"))
        else:
            token_with_ss.append(token.text)
    alternatives.add(" ".join(token_with_ss).strip())

    if "" in alternatives:
        alternatives.remove("")

    return alternatives
                        
                        
def get_location_candidates(query_string):
    """Get wikidata entities for a string of a LOC (location) named entity.

    Args:
        query_string (str): a string.
    
    Returns:
        list: list of strings for wikidata entities (e.g. Q42)
    """
    #P625: coordinate location
    #P17: country
    #P2046: area
    #P7153: significant place
    #P8138: located in the statistical territorial entity
    #P131: located in the administrative territorial entity
    #P206: located in or next to body of water 
    #P421: located in time zone
    
    props = ["P625", "P131", "P206", "P421", "P17", "P2046", "P7153", "P8138"] 
    
    entities = []
    results = []
    
    # if the search is not sucessful using the most wide-spread property like "coordinate location" is not successful, 
    # then other properties in the list of props will be used
    for prop in props:
        title_query = """SELECT DISTINCT ?s ?o where
    {
    ?s rdfs:label "%s"@de .
    ?s wdt:%s ?o
    SERVICE wikibase:label
        {bd:serviceParam wikibase:language "en" .
            }
    }
    """ % (query_string, prop)
        r = return_sparql_query_results(title_query)
        if r['results']['bindings'] != []:
            results.append(r)
        else:
            also_known_query = """SELECT DISTINCT ?s ?o where
        {
        ?s ?label "%s"@de .
        ?s wdt:%s ?o
        SERVICE wikibase:label
            {bd:serviceParam wikibase:language "en" .
                }
        }
        """ % (query_string, prop)
            r = return_sparql_query_results(also_known_query)
            if r not in results:
                results.append(r)
        
        # it's enough if an entity is found using the most wide-spread property
        if results != []:
            break
        
    for result in results:
        if len(result['results']['bindings']) != 0:
            for s_o in result['results']['bindings']:
                entities.append(s_o['s']['value'].split("/")[-1])
        #else:
            #entities.append(None)
        
    return entities
    

def get_longest_coref(ent):
    """Function that finds the longest ent in a coref cluster

    Args:
        ent (`Span`): Entity.

    Returns:
        ent (`Span`): Entity.    
    """

    max = 0
    position = 0

    if ent[0]._.coref_clusters != []:
        cluster = ent[0]._.coref_clusters[0]
        for i, mention in enumerate(cluster.mentions):
            if len(mention) > max:
                max = len(mention)
                position = i
        longest_mention = cluster.mentions[position]
        return longest_mention
    return None


def get_prefered_coref(ent: Span, alternative_spellings: bool = False) -> str:
    """Function that finds the longest string in coref cluster and returns tokens
    with selected pos tags as string.

    Args:
        ent (`Span`): Entity.

    Returns:
        str: prefered coref string.    
    """
    POS_keep_list = ["NOUN", "PROPN", "X", "NUM", "ADJ"]
    POS_strip_list = ["DET", "ADP", "PRON", "CCONJ", "SPACE", "PUNCT", "VERB"]

    # if ent has no coref cluster return ent.text
    if len(ent[0]._.coref_clusters) == 0:
        no_alternative_set = set()
        no_alternative_set.add(ent.text)
        return no_alternative_set

    cluster = ent[0]._.coref_clusters[0]
    max = 0
    position = 0
    for i, mention in enumerate(cluster.mentions):
        if len(mention) > max:
            max = len(mention)
            position = i
    longest_mention = cluster.mentions[position]
    target_indices_start = list()
    for i, token in enumerate(longest_mention):
        if token.pos_ in POS_keep_list:
            target_indices_start.append(i)

    if not alternative_spellings:
        target_string = " ".join([longest_mention[index].text for index in target_indices_start])
        target_string_set = set()
        target_string_set.add(target_string)

        return set(target_string_set)

    elif alternative_spellings:
        target_spacy_tokens = [longest_mention[index] for index in target_indices_start]
        alternatives_set = get_entities_for_alternative_spellings(entity_iterable=target_spacy_tokens)

        return alternatives_set


def find_instances_of(instance_object_wikidata_id: str) -> dict:
    """A function that queries Wikidata via SPARQL to find entities that are instances of selected objects and
    returns their wikidata_ids in a list.

    Args:
        query (str): name of literary character as string.

    Returns:
        dict: a dictionary from qwikidata api.

    """
    # query pattern
    # ?entity ?instace_of(https://www.wikidata.org/wiki/Property:P31) ?instance_object

    sparql_query = """
    SELECT ?entity ?entityLabel ?entityDescription
    WHERE {
    ?entity wdt:P31 wd:%s .
    SERVICE wikibase:label { bd:serviceParam wikibase:language "de". }    
    }
    """%(instance_object_wikidata_id)

    results = return_sparql_query_results(sparql_query)

    return results


def filter_sparql_results_by_query_string(query: str, qwikidata_sparql_results: dict) -> set:
    """Function checks results of qwikidata sparql request and returns set of 
    wikidata_ids if query_string occurs in label of entity.

    Args:
        query (str): name of literary character as string.
        qwikidata_sparql_results (dict): qwikidata dict with results of sparql request

    Returns:
        set: set of wikidata_uri.
    """
    result_ids = set()

    list_with_entity_label_description = qwikidata_sparql_results["results"]["bindings"]
    for entity_label_descr_dict in list_with_entity_label_description:
        if re.search(query, entity_label_descr_dict["entityLabel"]["value"]):
            result_ids.add(entity_label_descr_dict["entity"]["value"].split("/")[-1])
    
    return result_ids


#############################
# Currently unused functions

def find_humans(query_string, language):
    """Function that queries for instance_of relations to item human limited to results
    for the exact match of query_string and rdfs:label.

    Args:
        query_string (str): the string to query.

    Returns:
        set: set of wikidata_uri.
    """
    sparql_query = """
    SELECT DISTINCT ?entity
    {
    ?entity rdfs:label "%s"@%s .
    ?entity wdt:P31 wd:Q5 .
    }
    """%(query_string, language)

    results = return_sparql_query_results(sparql_query)

    result_ids = set()
    list_with_entity_dicts = results["results"]["bindings"]
    for entity_dict in list_with_entity_dicts:
        result_ids.add(entity_dict["entity"]["value"].split("/")[-1])

    return result_ids


def find_instances_of_restricted_by_label(query, instance_object_wikidata_id):
    """A function that queries Wikidata via SPARQL to find entities that are instances of selected objects and
    returns their wikidata_ids in a list.

    Args:
        query (str): name of literary character as string.
        instance_object_wikidata_id (str): wikidata_id of the object for instance_of relation.

    Returns:
        list: a list of wikidata_uri.

    """
    # query pattern
    # ?entity ?instace_of(https://www.wikidata.org/wiki/Property:P31) ?instance_object

    sparql_query = """
    SELECT ?entity
    WHERE {
    ?entity wdt:P31 wd:%s .
    ?entity rdfs:label ?label .
    FILTER (lang(?label) = "de")
    FILTER (regex(?label, "%s"))
    }
    """%(instance_object_wikidata_id, query)

    results = return_sparql_query_results(sparql_query)

    results_ids = [i["entity"]["value"].split("/")[-1] for i in results["results"]["bindings"]]
    unique_result_ids = set(results_ids)

    return unique_result_ids
