from spacy.tokens import Doc

from ..utils_methods import add_extension


def max_sent_slicer(doc):
    """Spacy pipeline component.
        Reduces the document to the first `max_units` sentences.
    
    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Doc, "fulltext")
    max_sents = doc._.pickle_id["max_units"]
    if max_sents < 0:
        return doc
    max_sents = min(len(list(doc.sents)), max_sents)
    span = doc[0:list(doc.sents)[max_sents-1].end]
    new_doc = span.as_doc(copy_user_data=True)
    new_doc._.fulltext = doc.text
    return new_doc


def max_char_slicer(doc, complete_sentences=True):
    """Spacy pipeline component.
        Reduces the document to the first `max_units` characters.
    
    Args:
        doc (`Doc`): A spacy document object.
        complete_sentences (boolean): If False, cut before the token that contains the threshold character.
                e.g. "This is a sentence. I lik|e it." -> "This is a sentence. I"
            If True, cut after the sentence that contains the threshold character.
                e.g. "This is a sentence. I lik|e it." -> "This is a sentence. I like it."
    
    Returns:
        `Doc`: A spacy document object.
    
    """
    add_extension(Doc, "fulltext")
    max_chars = doc._.pickle_id["max_units"]
    if max_chars < 0:
        return doc
    i = 0
    for token in doc:
        i = token.i
        try:
            idx = token._.idx
        except AttributeError:
            idx = token.idx
        if idx > max_chars:
            if complete_sentences:
                i = token.sent.end
            break
    span = doc[0:i]
    new_doc = span.as_doc(copy_user_data=True)
    new_doc._.fulltext = doc.text
    return new_doc