import os
import spacy
import sys
#from pathlib import Path
from spacy.tokens import Token, Span

from ..global_resources import GERMANET
from ..passage import Passage
from ..utils_classes import Debugger
from ..utils_methods import add_extension


def germanet_tagger_verbs_adjectives(doc):
    """Spacy pipeline component.
        Add germanet synsets to root verbs (token) and clause.

    Args:
        doc (`Doc`): A spacy document object.

    Returns:
        doc `Doc`: A spacy document object.
    
    """
    
    global GERMANET
    GERMANET.provide()

    add_extension(Span, "verb_synset_id")
    add_extension(Token, "synset_id")

    for clause in doc._.clauses:
        if verb_class_disambiguation_via_subj_obj(clause).startswith("s"):
            ##target_synset = germanet.get_synset_by_id(verb_class_disambiguation_via_subj_obj(clause))
            target_synset = verb_class_disambiguation_via_subj_obj(clause)
            clause._.verb_synset_id = target_synset
            #add id to clause root (token):
            if clause.root.pos_ == "VERB":
                clause.root._.synset_id = target_synset
        for token in clause:
            if token.pos_ == "ADJ":
                token._.synset_id = adjective_classe_disambiguation(token)
                #print("Target", token, token.head, token.head.pos_, token.dep_, target_synset)
    return doc


def get_synsets_for_token(token):
    """Extracts synstets for a token

    Args:
        token (`Token`): The token.
    
    Returns:
        list: list of synsets for the token.
    
    """
    
    synsets = GERMANET._.get_synsets_by_orthform(token.lemma_)
    if len(synsets) > 0:
        return synsets


def sort_synsets_by_distance(synsets_list_1, synsets_list_2):
    """Sorts synsets by distance between them

    Args:
        synsets_list_1, synsets_list_2 (list of Synset, Synset): two synset lists for different words.
    
    Returns:
        path_sorted_synset_list (list of Synset, Synset, int): list with two synsets and path between them sorted by the shortest path.
    
    """
    distances_between_synsets = []        
    for s_1 in synsets_list_1:
        for s_2 in synsets_list_2:
            distances_between_synsets.append([s_1, s_2, len(s_1.shortest_path(s_2)[0])])
            path_sorted_synset_list = sorted(distances_between_synsets, key=lambda element:element[2], reverse=False)
    return path_sorted_synset_list


def add_ranking(path_sorted_synset_list):
    """Ranks synsets by distance between them.

    Args:
        path_sorted_synset_list (list of Synset, Synset, int): list with two synsets and path between them sorted by the shortest path.
    
    Returns:
        ranked_path_sorted_synset_list (list of Synset, Synset, int, int): list with two synsets and path between them sorted by the shortes path.
    
    """
    shortest_dist = []
    for i, syns in enumerate(path_sorted_synset_list):
        try:
            if path_sorted_synset_list[i][2] == path_sorted_synset_list[i+1][2]:
                shortest_dist.append(syns)
        except:
            pass
    return shortest_dist    
    rank = 1
    ranked_path_sorted_synset_list = []
    for i, distance_sorted in enumerate(path_sorted_synset_list):
        synsets_list = distance_sorted
        synsets_list.append(rank)
        try:
            if path_sorted_synset_list[i][2] != path_sorted_synset_list[i+1][2]:
                rank += 1
            else:
                rank = rank
            ranked_path_sorted_synset_list = synsets_list
        except:
            pass
        return ranked_path_sorted_synset_list


def adjective_classe_disambiguation(token):
    """Disambiguated tokens by the synset id

    Args:
        token (`Token`): The token.
    
    Returns:
        synset_id (str): synset id
    
    """
    adjective_synsets = get_synsets_for_token(token)
    if adjective_synsets:
        # if the word is unambiguous and has the same meaning in all contexts
        if len(adjective_synsets) == 1:
            return adjective_synsets[0].id                   
        
        head_synsets = []
        head_of_head_synsets = [] 
        
        if len(adjective_synsets) > 1:
            # assumption that you should not disambiguate an adj with another (conjuncted) adj, but maybe it's wrong           
            # if token.head.pos_ in ["ADJ", "CCONJ"]:
                # while token.head.pos_ not in ["ADJ", "CCONJ"]:
                    # token = token.head        
            head_synsets = get_synsets_for_token(token.head)
            head_of_head = get_synsets_for_token(token.head.head)
            if head_synsets == None:
                while head_synsets == None:
                    token = token.head
                    if token.dep_ == "ROOT":
                        break
                head_synsets = get_synsets_for_token(token.head)
                head_of_head = get_synsets_for_token(token.head.head)
            # if the head is also an adective move up in the structure until    
            if head_of_head == None:               
                while head_of_head == None:
                    token = token.head
                    if token.dep_ == "ROOT":
                        break
                head_of_head = get_synsets_for_token(token.head.head)              
            if head_of_head == head_synsets and head_of_head != None:
                head_of_head_synsets = get_synsets_for_token(token.head.head)
            else:
                head_of_head_synsets = head_of_head
            # if the token is root it can be disambiguated through the subject in the noun phrase            
            if token.dep_ == "ROOT":
                for word in token.subtree:
                    # better to disambiguate with subject np, but if it's not there copular verb will be used
                    # TODO: consider different sentence structures
                    if word.dep_ == "cop":
                        head_synsets = get_synsets_for_token(word)
                    if word.dep_ == "nsubj":
                        head_synsets = get_synsets_for_token(word)                       
            elif token.head.dep_ == "ROOT":
                for word in token.subtree:
                    # disambiguate either with nsubj, if not available -> cop
                    if word.dep_ == "nsubj":
                        head_of_head_synsets = get_synsets_for_token(word)
                    elif word.dep_ == "cop":
                        head_of_head_synsets = get_synsets_for_token(word)                        
            #elif token.head.head.dep_ == "ROOT":
            #    head_of_head_synsets = get_synsets_for_token(token.head.head)
        distances = []
        if type(head_synsets) is list:
            distance = add_ranking(sort_synsets_by_distance(adjective_synsets, head_synsets))
            if len(distance) == 1:
                return distance[0][0].id
            elif len(distance) > 1:
                distances = distance
                
            
        elif type(head_of_head_synsets) is list and len(distances) > 0:
            distance = add_ranking(sort_synsets_by_distance(head_synsets, head_of_head_synsets))
            if len(distance) == 1:
                s_id = distance[0][0].id
                for i in distances:
                    dist = []
                    if s_id == i[1].id:
                        dist.append(i)
                    if len(dist) == 1:
                        return i[0].id
                            

def verb_class_disambiguation_via_subj_obj(clause):
    """ TODO

    Args:
        clause.
    Returns:
        str : lowest_scoring_verb_synset_id
    
    """

    if clause.root.pos_ == "VERB":
        verb_token = clause.root
    else:
        return "ellipsis"

    ##find synsets for verb
    verb_synsets = GERMANET._.get_synsets_by_orthform(verb_token.lemma_)
    if len(verb_synsets) == 0:
        return "verb not in germanet"
    
    ##find synsets for subj and obj
    #define list for sents with multiple objects
    all_obj_synsets = list()
    for token in clause:
        if token.dep_ in ["nsubj", "nsubj:pass"] and len(GERMANET._.get_synsets_by_orthform(token.lemma_)) > 0:
            nsubj_synsets = GERMANET._.get_synsets_by_orthform(token.lemma_)
        if token.dep_ in ["obj", "obl", "obl:agent", "obl:arg", "obl:lmod", "obl:tmod"] and len(GERMANET._.get_synsets_by_orthform(token.lemma_)) > 0:
            all_obj_synsets.append(GERMANET._.get_synsets_by_orthform(token.lemma_))
    
    # find all path distances for each combination of subj synsets to verb synsets
    if "nsubj_synsets" in locals():
        verb_subj_distances = list()
        for verb_synset_i, verb_synset in enumerate(verb_synsets):
            for nsubj_synset_i, nsubj_synset in enumerate(nsubj_synsets):
                verb_subj_distances.append([verb_synset, nsubj_synset, len(verb_synset.shortest_path(nsubj_synset)[0])])
        verb_subj_distances_sorted = sorted(verb_subj_distances, key=lambda element:element[2], reverse=False)

    # find all path distances for each combination of obj synsets for each obj to verb synsets
    if len(all_obj_synsets) > 0:
        all_verb_obj_distances_sorted = list()
        for obj_synsets in all_obj_synsets:
            if len(obj_synsets) > 0:
                verb_obj_distances = list()
                for verb_synset_i, verb_synset in enumerate(verb_synsets):
                    for obj_synset_i, obj_synset in enumerate(obj_synsets):
                        verb_obj_distances.append([verb_synset, obj_synset, len(verb_synset.shortest_path(obj_synset)[0])])          
                verb_obj_distances_sorted = sorted(verb_obj_distances, key=lambda element:element[2], reverse=False)
                all_verb_obj_distances_sorted.append(verb_obj_distances_sorted)

    #all_distances_sorted = all_verb_obj_distances_sorted + [verb_subj_distances_sorted]
    all_distances_sorted = list()
    if "all_verb_obj_distances_sorted" in locals():
        all_distances_sorted += all_verb_obj_distances_sorted
    if "verb_subj_distances_sorted" in locals():
        all_distances_sorted += [verb_subj_distances_sorted]
    if len(all_distances_sorted) == 0:
        return "no subj and obj or no germanet synsets for it."
    #RANKING
    # ranks the smallest distances for each verb-subj or verb-obj from 1 to 3
    # if distances equal each other the rank is give twice/multiple times
    all_distances_sorted_ranks = list()
    for distances_sorted_i, distances_sorted in enumerate(all_distances_sorted):
        rank = 1
        distances_sorted_ranks = list()
        for distance_sorted_i, distance_sorted in enumerate(distances_sorted):
            try:
                synset = distances_sorted[distance_sorted_i]
                synset.append(rank)
                distances_sorted_ranks.append(synset)
                if distances_sorted[distance_sorted_i][2] != distances_sorted[distance_sorted_i+1][2]:
                    rank += 1
            except:
                continue
        all_distances_sorted_ranks.append(distances_sorted_ranks)
    
    # SCORING VERB CATEGORY
    # builds a ordered list for verb synsets that have at least one time an rank between 1 and 3
    verb_synset_dic = dict()
    for distances_sorted_ranks in all_distances_sorted_ranks:
        for distances_sorted_rank in distances_sorted_ranks:
            verb_synset = distances_sorted_rank[0]
            rank = distances_sorted_rank[3]
            if verb_synset.id not in verb_synset_dic:
                verb_synset_dic[verb_synset.id] = rank
            else:
                verb_synset_dic[verb_synset.id] += rank                
    verb_synset_dic_sorted = sorted(verb_synset_dic.items(), key=lambda element:element[1], reverse=False)
    
    # the lowest score is assumed as the assigned synset to the verb
    lowest_scoring_verb_synset_id = verb_synset_dic_sorted[0][0]
    
    # returns the synset id for lowest verb score
    return lowest_scoring_verb_synset_id
