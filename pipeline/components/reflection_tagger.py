import spacy
from sklearn.tree import DecisionTreeClassifier
from spacy.tokens import Doc, Span

from ..classifiers.clf_dfg_tagger import get_clf_dfg_tagger
from ..global_resources import REFLECTION_MODELS
from ..utils_methods import add_extension, transfer_labels_to_passages


LABEL_NAMES = {
    "binary" : ["RP"],
    "multi" : ["GI", "Comment", "Nichtfiktional"]
}


def clf_dfg_reflection_tagger(doc, texts_train=None, texts_dev=None, label_condition="binary", classifier=DecisionTreeClassifier, param_grid=None, window=(-1, 1), disable_feats=[], enable_feats=[]):
    """Spacy pipeline component.
        Add reflective-passages ("RP") tags to clauses using a statistical classifier.

    Args:
        doc (`Doc`): A spacy document object.
        texts_train (list of str): List of training texts.
        texts_dev (list of str): List of development texts.
        label_condition (str): Label condition ("multi" or "binary").
        classifier (class): A classifier type.
        window ((int,int)): Context window.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "rps")
    add_extension(Doc, "rps")
    
    default_clf_name = None
    if label_condition == "binary":
        default_clf_name = "dfg_tagger_RP_binary_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Schnabel+Zesen_Fontane+Gellert_-3_48af4d378193b6ea5fa06e0aa3aef029"
    
    clf_predict = get_clf_dfg_tagger(default_clf_name, texts_train, texts_dev, ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG", "Einstellung", "Interpretation", "Meta", "Nichtfiktional"], "RP", label_condition, classifier, param_grid, window, disable_feats, enable_feats)
    labels = clf_predict(doc._.clauses)

    transfer_labels_to_passages(doc, "rps", labels)
    
    return doc


def neural_reflection_tagger(doc, label_condition="multi"):
    """Spacy pipeline component.
        Add reflective passages to the document.
        Uses the code and models from here:
        https://github.com/tschomacker/generalizing-passages-identification-bert

    Args:
        doc (`Doc`): A spacy document object.
        label_condition (str): Label condition ("multi" or "binary").
    
    Returns:
        `Doc`: A spacy document object.

    """
    global REFLECTION_MODELS
    REFLECTION_MODELS.provide()
    model = REFLECTION_MODELS._["reflexive_" + label_condition]
    label_names = LABEL_NAMES[label_condition]

    add_extension(Span, "rps")
    add_extension(Doc, "rps")

    labels = []
    sents = [None] + list(doc.sents) + [None]
    for i in range(1, len(sents)-1):
        context_tokens = []
        try:
            context_tokens.extend(list(sents[i-1]))
        except TypeError:
            pass # first sentence
        context_tokens.extend(list(sents[i]))
        try:
            context_tokens.extend(list(sents[i+1]))
        except TypeError:
            pass # last sentence
        context_tokens = [token for token in context_tokens if not token.is_space]
        
        for clause in sents[i]._.clauses:
            clause_open = False
            clause_text_embed_in_context = ''
            for token in context_tokens:
                if token._.clause is not None:
                    # token is part of the current clause
                    if token._.clause == clause:
                        if not clause_open:
                            # start the clause mark up
                            clause_text_embed_in_context += '<b>'
                            clause_open = True
                    # token is NOT part of the current clause
                    else:
                        if clause_open:
                            # close the clause mark up
                            clause_text_embed_in_context += '</b>'
                            clause_open = False
                    clause_text_embed_in_context += ' ' + token.text
                # punctation
                else:
                    clause_text_embed_in_context += token.text
                # closing the mark up, in case the passage conists of a single clause
            if clause_open:
                clause_text_embed_in_context += '</b>'
            
            prediction = [round(x) for x in model.predict(clause_text_embed_in_context)]
            labels.append(set([label_names[label_i] for label_i, label_name in enumerate(label_names) if bool(prediction[label_i])]))
    
    transfer_labels_to_passages(doc, "rps", labels)
    
    return doc