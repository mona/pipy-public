import spacy
from spacy.tokens import Doc, Span, Token

from ..global_wordlists import Q_MARKS_C, Q_MARKS_O, SPEECH_VERBS
from ..utils_methods import add_extension, get_ents, is_NE, is_NN, is_PRON


def rb_speaker_extractor(doc):
    """Spacy pipeline component.
        Extract speaker and addressee for direct speech segments.
        The current implementation is very simple and can only extract explicitly mentioned speakers etc.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Token, "speaker")
    add_extension(Token, "speaker_")
    add_extension(Token, "addressee")
    add_extension(Token, "addressee_")
    add_extension(Token, "speech_segment")
    add_extension(Token, "speech_segment_")
    add_extension(Span, "speaker")
    add_extension(Span, "speaker_")
    add_extension(Span, "addressee")
    add_extension(Span, "addressee_")
    add_extension(Doc, "speech_segments")
    ents = get_ents(doc, remove_doublings=True) # `remove_doublings` value must be the same as in "coref"
    speakers_and_addressees = extract_speakers_and_addressees(doc, ents)
    speech_segments = []
    for segment, (speaker, addressee, start, end) in enumerate(speakers_and_addressees):
        for i in range(start, end+1):
            token = doc[i]
            span = doc[start:end+1]
            token._.speaker = speaker
            span._.speaker = speaker
            if speaker > -1:
                token._.speaker_ = ents[speaker]
                span._.speaker_ = ents[speaker]
            token._.addressee = addressee
            span._.addressee = addressee
            if addressee > -1:
                token._.addressee_ = ents[addressee]
                span._.addressee_ = ents[addressee]
            doc[i]._.speech_segment = segment
            doc[i]._.speech_segment_ = span
        speech_segments.append(span)
    doc._.speech_segments = speech_segments
    return doc


def extract_speakers_and_addressees(doc, ents):
    """Detects the speaker and addressee for every direct speech segment in a document.

    Args:
        doc (`Doc`): The document.
        ents (list of `Span`): The namend entities in the document.
    
    Returns:
        list of (int,int,int,int): A list of a tuple for every direct speech segment.
            Such a tuple contains the index of the speaker and the addressee in `ents` as well as the start and end index for the speech segment.
            If speaker and/or addressee cannot be extracted, their indices are set to -1.
    
    """
    speakers_addressees = []
    indices = {ent.root.i : k for k, ent in enumerate(ents)} # maps a token index to an entity index
    tokens = list(doc)
    k = 0
    while k < len(tokens):
        if "direct" in tokens[k]._.speech:
            
            # Determine start and end of the current segment
            start = k
            while k+1 < len(tokens) and "direct" in tokens[k+1]._.speech and not (tokens[k].text in Q_MARKS_C and tokens[k+1].text in Q_MARKS_O):
                # (the second part of the while condition assures that two adjacent segments are not merged)
                k += 1
            end = k
            
            # The next step is to determine the context of the current segment, 
            # i.e. the text directly before or after the segment, 
            # which potentially contains references to speaker and/or addressee.
            context = []
            
            # offsets to jump over / "ignore" whitespace tokens between the segment and its context
            x = 1 # offset for context before segment
            while start-x > -1 and tokens[start-x].is_space:
                x += 1
            y = 1 # offset for context after segment
            while end+y < len(tokens) and tokens[end+y].is_space:
                y += 1
            
            # If the token before the segment is a colon or a comma, we choose the preceding tokens as context;
            # otherwise, if the token after the segment is a comma or a verb (but not in a new sentence), 
            # we choose the succeeding tokens as context; otherwise, we choose no context.
            # The context always starts before/after the segment and then runs backwards/forwards 
            # until a sentence boundary or a new direct speech segment is found.
            if start-x > -2 and tokens[start-x].text in [":", ","]:
                n = start-x
                while n > -1 and (not tokens[n].is_sent_start) and ("direct" not in tokens[n]._.speech):
                    context.insert(0, n)
                    n -= 1
                if n > -1 and "direct" not in tokens[n]._.speech:
                    context.insert(0, n)
            elif end+y < len(tokens) and (tokens[end+y].text == "," or (tokens[end+y].pos_ in ["VERB", "AUX"] and not tokens[end+y].is_sent_start)):
                n = end+y
                while n < len(tokens) and (not tokens[n].is_sent_start) and ("direct" not in tokens[n]._.speech):
                    context.append(n)
                    n += 1
            
            # Search for potential speakers. These are all subjects within the context:
            subjects = [n for n in context if tokens[n].dep_ == "nsubj" and n in indices]
            
            # Search for potential speech verbs and addressees. Potential addressees are all objects of a verb within the context
            # (currently, we only select NEs and PRONs as potential addressees because NNs are often just adjuncts):
            speech_verbs = []
            other_verbs = []
            for n in context:
                if tokens[n].pos_ == "VERB":
                    objects = [tok.i for tok in tokens[n].children if tok.i in context and tok.dep_ in ["obj", "iobj", "obl"] and tok.i in indices and (is_NE(ents[indices[tok.i]]) or is_PRON(ents[indices[tok.i]]))]
                    if tokens[n].lemma_ in SPEECH_VERBS:
                        speech_verbs.append((n, objects))
                    else:
                        other_verbs.append((n, objects))
            
            # Only if no speech verb was found, we consider other verbs:
            verbs = speech_verbs
            if len(speech_verbs) == 0:
                verbs = other_verbs
            
            # Choose a speaker from the list of potential speakers.
            # We choose the entitiy which is closest to the speech segment.
            speaker = -1
            if len(subjects) > 0:
                if subjects[-1] < start:
                    speaker = indices[subjects[-1]]
                else:
                    speaker = indices[subjects[0]]
            
            # Choose a verb from the list of potential verbs.
            # We choose the verb which is closest to the speech segment.
            verb = -1
            addressee = -1
            if len(verbs) > 0:
                if verbs[-1][0] < start:
                    verb, objects = verbs[-1]
                else:
                    verb, objects = verbs[0]
                
                # Choose an addressee from the list of potential addressees.
                # We choose the addressee which is closest to the speech segment.
                if len(objects) > 0:
                    if objects[-1] < start:
                        addressee = indices[objects[-1]]
                    else:
                        addressee = indices[objects[0]]
            
            # Add the index of the speaker and the addressee in `ents` as well as 
            # the segment's start and end index:
            speakers_addressees.append((speaker, addressee, start, end))
            
        k += 1
    return speakers_addressees