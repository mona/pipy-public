import math
import os
import spacy
import sys
from sklearn.tree import DecisionTreeClassifier
from spacy.tokens import Doc, Span

from ..classifiers.clf_dfg_tagger import get_clf_dfg_tagger
from ..global_resources import DIMLEX_LEXICON, GERMANET
from ..global_wordlists import COMMENT_MARKERS, COMMENT_MODIFIERS, COMMENT_NOUNS, COMMENT_VERBS
from ..passage import Passage
from ..utils_methods import *


def rb_comment_tagger(doc):
    """Spacy pipeline component.
        Add comments tags to clauses.
    Args:
        doc (`Doc`): A spacy document object.
    Returns:
        `Doc`: A spacy document object.
    
    """
    global DIMLEX_LEXICON
    DIMLEX_LEXICON.provide()
    
    global GERMANET
    GERMANET.provide()
    
    add_extension(Span, "comments")
    add_extension(Doc, "comments")

    
    for clause in doc._.clauses:
        tags = set()
        if has_parenthesis(clause):
            # at least one of the dashes/hypens remains after clausizing, the clause that it is in gets the comment tag 
            for token in clause._.tokens:
                if token.text in ["-", "–", "(", ")"]:
                    #print("PAR", clause)
                    tags.add("Comment")
                    break

        if is_vocative(clause) or is_question(clause):
            if not is_direct_speech(clause):
                #print("VOC", clause)
                tags.add("Comment")

        if self_reflexion_character(clause):
            tags.add("Einstellung")

        if narrative_break(clause, outside_direct=True) and has_modifier(clause):
            for token in clause:
                if has_no_person_coreference(token, 3, 3):
                    #print("MOD")
                    tags.add("Comment")
                    break 

        if narrative_break(clause, outside_direct=True):
            for token in clause:
                if type(token._.synset_id) == str and token.pos_ == "ADJ" and str(GERMANET._.get_synset_by_id(token._.synset_id).word_class) == "WordClass.Allgemein":
                    #print("Allgemein")
                    tags.add("Comment")
                if has_argument(token) and has_no_person_coreference(token, 3, 3):
                    #print("ARGUMENT")
                    tags.add("Comment")
                if check_modifier(token, lexicon = DIMLEX_LEXICON._):
                    #print("DIMLEX")
                    tags.add("Comment")
        if narrative_break(clause, outside_direct=True) and has_sentiment(clause):
            tags.add("Comment") 
        
        # TODO needs to be modeled in conjunction with diegesis:
        # this feature only works if narrator in not part of the story
        # if self_reflexion_narrator(clause):
        #     tags.add("Meta")

        if narrative_break(clause, outside_direct=True) and has_emotion_or_sentiment_score(clause):
            tags.add("Einstellung")

        #assign passage to clause
        clause._.comments = []
        if len(tags) > 0:
            clause._.comments.append(Passage([clause], tags))

    # expand clauses to passages
    expand_clauses_to_passages_for_comment_types(doc)

    # assign passages to doc
    doc._.comments = []
    for clause in doc._.clauses:
        for passage in clause._.comments:
            if passage not in doc._.comments:
                doc._.comments.append(passage)
    return doc


def expand_clauses_to_passages_for_comment_types(doc):
    """Expand comment lables from clauses to passages.

    Args:
        doc (`Doc`): A spacy document object.
    
    """
    indices_comment = list() 
    indices_einstellung = list()
    indices_interpretation = list()
    indices_meta = list()
    for clause_i, clause in enumerate(doc._.clauses):
        if len(clause._.comments) > 0:
            for passage in clause._.comments:
                if "Comment" in passage.tags:
                    indices_comment.append(clause_i)
                if "Einstellung" in passage.tags:
                    indices_einstellung.append(clause_i)
                if "Interpretation" in passage.tags:
                    indices_interpretation.append(clause_i)
                if "Meta" in passage.tags:
                    indices_meta.append(clause_i)
    consecutively_annotated_clauses_comment = find_consecutively_annotated_clauses(indices_comment)
    consecutively_annotated_clauses_einstellung = find_consecutively_annotated_clauses(indices_einstellung)
    consecutively_annotated_clauses_interpretation = find_consecutively_annotated_clauses(indices_interpretation)
    consecutively_annotated_clauses_meta = find_consecutively_annotated_clauses(indices_meta)

    # comment tag
    for indices_list in consecutively_annotated_clauses_comment:
        clauses = [doc._.clauses[i] for i in indices_list]
        passage = Passage(clauses, {"Comment"})
        for index in indices_list:
            for p in doc._.clauses[index]._.comments:
                if "Comment" in p.tags:
                    doc._.clauses[index]._.comments = [passage]
            else:
                doc._.clauses[index]._.comments.append(passage)
    # einstellung tag
    for indices_list in consecutively_annotated_clauses_einstellung:
        clauses = [doc._.clauses[i] for i in indices_list]
        passage = Passage(clauses, {"Einstellung"})
        for index in indices_list:
            for p in doc._.clauses[index]._.comments:
                if "Einstellung" in p.tags:
                    doc._.clauses[index]._.comments = [passage]
            else:
                doc._.clauses[index]._.comments.append(passage)

    # interpretation tag
    for indices_list in consecutively_annotated_clauses_interpretation:
        clauses = [doc._.clauses[i] for i in indices_list]
        passage = Passage(clauses, {"Interpretation"})
        for index in indices_list:
            for p in doc._.clauses[index]._.comments:
                if "Interpretation" in p.tags:
                    doc._.clauses[index]._.comments = [passage]
            else:
                doc._.clauses[index]._.comments.append(passage)
                
    # einstellung tag
    for indices_list in consecutively_annotated_clauses_meta:
        clauses = [doc._.clauses[i] for i in indices_list]
        passage = Passage(clauses, {"Meta"})
        for index in indices_list:
            for p in doc._.clauses[index]._.comments:
                if "Meta" in p.tags:
                    doc._.clauses[index]._.comments = [passage]
            else:
                doc._.clauses[index]._.comments.append(passage)


def find_consecutively_annotated_clauses(indices_of_clauses):
    """TODO
    
    Args:
        indices_of_claises (list of int): List of indices for clauses in doc which are annoteted with searched cateogory

    Returns:
        list of (list of int): List with lists of clauses that form a passage, eg. [[1, 2, 3], [5, 6], [49, 50, 51, 52]]

    """
    blocks = list()
    for index_i, index in enumerate(indices_of_clauses):
        block = list()
        try:
            if index-1 == indices_of_clauses[index_i-1]:
                #print("auslassen: ", index)
                continue
        except:
            continue
        
        try: 
            if index-1 != indices_of_clauses[index_i-1] and index+1 != indices_of_clauses[index_i+1]:
                #print("auslassen: ", index)
                continue
        except:
            continue
        
        i = index_i
        try:
            while indices_of_clauses[i]+1 == indices_of_clauses[i+1]:
                block.append(indices_of_clauses[i])
                i+=1
        except:
            pass
        block.append(indices_of_clauses[i])
        #print("block: ",block)
        blocks.append(block)

    return blocks


def clf_dfg_comment_tagger(doc, texts_train=None, texts_dev=None, label_condition="multi", classifier=DecisionTreeClassifier, param_grid=None, window=(-1, 1), disable_feats=[], enable_feats=[]):
    """Spacy pipeline component.
        Add Comment tags to clauses using a statistical classifier.

    Args:
        doc (`Doc`): A spacy document object.
        texts_train (list of str): List of training texts.
        texts_dev (list of str): List of development texts.
        label_condition (str): Label condition in training.
        classifier (class): A classifier type.
        window ((int,int)): Context window.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "comments")
    add_extension(Doc, "comments")
    
    if label_condition == "multi":
        default_clf_name = "dfg_tagger_Comment_multi_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Schnabel+Zesen_Fontane+Gellert_-3_2bd158d64a3ae2681ab627616626a6b8"
    else:
        default_clf_name = "dfg_tagger_Comment_binary_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Schnabel+Zesen_Fontane+Gellert_-3_2bd158d64a3ae2681ab627616626a6b8"
    
    clf_predict = get_clf_dfg_tagger(default_clf_name, texts_train, texts_dev, ["Einstellung", "Interpretation", "Meta"], "Comment", label_condition, classifier, param_grid, window, disable_feats, enable_feats)
    labels = clf_predict(doc._.clauses)

    transfer_labels_to_passages(doc, "comments", labels)
    
    return doc


def clf_dfg_majority_comment_tagger(doc, texts_trains=None, texts_devs=None, label_condition="multi", classifier=DecisionTreeClassifier, param_grid=None, window=(-1, 1), disable_feats=[], enable_feats=[]):
    """Spacy pipeline component.
        Add Comment tags to clauses using the majority vote of several statistical classifiers.

    Args:
        doc (`Doc`): A spacy document object.
        texts_trains (list of (list of str)): List of lists of training texts.
        texts_devs (list of (list of str)): List of list of development texts.
        label_condition (str): Label condition in training.
        classifier (class): A classifier type.
        window ((int,int)): Context window.
    
    Returns:
        `Doc`: A spacy document object.

    """
    if texts_trains is None:
        texts_trains = [None for n in range(3)]
    if texts_devs is None:
        texts_devs = [None for texts_train in texts_trains]
    if len(texts_trains) != len(texts_devs):
        raise ValueError("Number of training sets must be equal to number of dev sets.")

    add_extension(Span, "comments")
    add_extension(Doc, "comments")

    if label_condition == "multi":
        default_clf_names = [
            "dfg_tagger_Comment_multi_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Fontane+Gellert+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kleist+LaRoche+May+Musil+Novalis+Schnabel+Zesen_Kafka+Mann_-3_2bd158d64a3ae2681ab627616626a6b8",
            "dfg_tagger_Comment_multi_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Fontane+Gellert+Goethe+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Zesen_Grimmelshausen+Schnabel_-3_2bd158d64a3ae2681ab627616626a6b8",
            "dfg_tagger_Comment_multi_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Schnabel+Zesen_Fontane+Gellert_-3_2bd158d64a3ae2681ab627616626a6b8"
        ]
    else:
        default_clf_names = [
            "dfg_tagger_Comment_binary_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Fontane+Gellert+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kleist+LaRoche+May+Musil+Novalis+Schnabel+Zesen_Kafka+Mann_-3_2bd158d64a3ae2681ab627616626a6b8",
            "dfg_tagger_Comment_binary_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Fontane+Gellert+Goethe+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Zesen_Grimmelshausen+Schnabel_-3_2bd158d64a3ae2681ab627616626a6b8",
            "dfg_tagger_Comment_binary_LogisticRegression_(-1, 1)_15_2_Andreae+Dahn+Goethe+Grimmelshausen+Hoffmann+Hölderlin+Kafka+Kleist+LaRoche+Mann+May+Musil+Novalis+Schnabel+Zesen_Fontane+Gellert_-3_2bd158d64a3ae2681ab627616626a6b8"
        ]

    label_counts = [{} for clause in doc._.clauses]
    for n, texts_train in enumerate(texts_trains):
        texts_dev = texts_devs[n]
        clf_predict = get_clf_dfg_tagger(default_clf_names[n] if len(default_clf_names) == len(texts_trains) else None, texts_train, texts_dev, ["Einstellung", "Interpretation", "Meta"], "Comment", label_condition, classifier, param_grid, window, disable_feats, enable_feats)
        labels = clf_predict(doc._.clauses)
        for k, label_set in enumerate(labels):
            for label in label_set:
                try:
                    label_counts[k][label] += 1
                except KeyError:
                    label_counts[k][label] = 1
    
    m = math.floor(len(texts_trains)/2.0)
    labels = []
    for label_count in label_counts:
        label_set = set()
        for label in label_count:
            if label_count[label] > m:
                label_set.add(str(label))
        labels.append(label_set)

    transfer_labels_to_passages(doc, "comments", labels)
    
    return doc