import os
import sys
from spacy.tokens import Doc, Span, Token

from ..annotation import Annotation, AnnotationList
from ..global_resources import NRC_DIC_GERMAN
from ..utils_classes import Debugger
from ..utils_methods import add_extension


def nrc_emotions(doc):
    """TODO
    """
    global NRC_DIC_GERMAN
    NRC_DIC_GERMAN.provide()

    add_extension(Token, "emotions")

    for token in doc:
        token._.emotions = dict()
        if token.lemma_ in NRC_DIC_GERMAN._:
            token._.emotions = NRC_DIC_GERMAN._[token.lemma_]

    return doc
