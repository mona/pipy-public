import os
import sys
import torch
from spacy.tokens import Doc, Span
from torch.utils.data import DataLoader

from ..global_constants import TORCH_DEVICE
from ..global_resources import EVENT_MODEL
from ..utils_methods import add_extension

from settings import EVENT_CLASSIFICATION_PATH
sys.path.append(EVENT_CLASSIFICATION_PATH)
from event_classify.eval import evaluate
from event_classify.datasets import JSONDataset, SpanAnnotation


def event_event_tagger(doc: Doc) -> Doc:
    """Integration of event classification from event project https://github.com/uhh-lt/event-classification

    Args:
        doc(`Doc`): spacy doc.
        
    Returns:
        doc(`Doc`): spacy doc with events on clause level.
    
    """
    add_extension(Span, "event")

    global EVENT_MODEL
    EVENT_MODEL.provide()

    model, tokenizer = EVENT_MODEL._

    special_tokens = True
    batch_size = 8

    annotations = []
    annotation_start_to_clause = {}
    for clause in doc._.clauses:
        annotation = {
            "start" : clause[0].idx,
            "end" : clause[-1].idx,
            "spans": [(clause[0].idx, clause[-1].idx)],
            "predicted": None,
        }
        annotations.append(annotation)
        annotation_start_to_clause[clause[0].idx] = clause
    data = {"text" : doc.text, "annotations" : annotations, "title" : None}
    data["annotations"].extend(annotations)

    dataset = JSONDataset(
        dataset_file=None, data=[data], include_special_tokens=special_tokens
    )
    loader = DataLoader(
        dataset,
        batch_size=batch_size,
        collate_fn=lambda list_: SpanAnnotation.to_batch(list_, tokenizer),
    )
    
    device = TORCH_DEVICE
    model.to(device)
    result = evaluate(loader, model, device=device)
    data = dataset.get_annotation_json(result)[0]
    for annotation in data["annotations"]:
        annotation_start_to_clause[annotation["start"]]._.event = annotation["additional_predictions"]

    return doc