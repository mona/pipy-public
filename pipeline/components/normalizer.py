import os
import sys
from spacy.symbols import ORTH, NORM
from spacy.tokens import Doc, Token

from ..global_resources import NORM_EXCEPTIONS
from ..utils_methods import add_extension


# should SPACE tokens be removed?
REMOVE_SPACES = False


def _normalizer(doc, norm_func):
    """Container method for the normalizer components.
        Normalizes each token and returns an updated document.

    Args:
        doc (`Doc`): A spacy document object.
        norm_func (func): A function which maps a token to its norm.
    
    Returns:
        `Doc`: A spacy document object.

    """
    # token.text cannot be overwritten, so we create a new document:
    tokens = []
    spaces = []
    for token in doc:
        if not (REMOVE_SPACES and token.is_space):
            if token.is_oov:
                # (only replace unknown, i.e. out-of-vocabulary, words)
                tokens.append(norm_func(token))
            else:
                tokens.append(token.text)
            if token.whitespace_ == "":
                spaces.append(False)
            else:
                spaces.append(True)
    new_doc = Doc(doc.vocab, tokens, spaces)

    # copy the pickle ID
    attr = "pickle_id"
    if hasattr(doc._, attr):
        add_extension(Doc, attr)
        setattr(new_doc._, attr, getattr(doc._, attr))

    # copy some document-level information from the original document:
    for attr in ["text", "text_with_ws"]:
        add_extension(Doc, attr)
        setattr(new_doc._, attr, getattr(doc, attr))
    
    # copy some token-level information from the original tokens:
    for attr in ["text", "text_with_ws", "whitespace_", "idx"]:
        add_extension(Token, attr)
        offset = 0
        for i, token in enumerate(doc):
            if not (REMOVE_SPACES and token.is_space):
                setattr(new_doc[i-offset]._, attr, getattr(doc[i], attr))
            else:
                offset += 1
    
    return new_doc


def dictionary_normalizer(doc):
    """Spacy pipeline component.
        Uses a dictionary with historical spelling variants to normalise a document.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    global NORM_EXCEPTIONS
    NORM_EXCEPTIONS.provide()

    def norm_func(token):
        if token.text in NORM_EXCEPTIONS._:
            return NORM_EXCEPTIONS._[token.text][0][NORM]
        return token.text

    return _normalizer(doc, norm_func)
