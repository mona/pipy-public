import codecs
import csv
import os
import re
import sys
import time
import xml.etree.cElementTree as ET
from spacy.tokens import Doc, Span, Token

from ..annotation import Annotation, AnnotationList
from ..utils_classes import Debugger
from ..utils_methods import add_extension, get_doc_text, get_sub_clauses, map_tokens_to_closest_clauses


# name space for CATMA TEI/XML
NAMESPACE = {"tei": "http://www.tei-c.org/ns/1.0", "xml": "http://www.w3.org/XML/1998/namespace"}


def find_collection_path(doc, corpus_path):
    """Find the annotation collections for a document.

    Args:
        doc (`Doc`): The document.
        corpus_path (str): The path to the corpus.
            The corpus contains one directory for every text.
            One directory contains the plain text and all annotation files.
    
    Returns:
        str: The path to the directory with the annotations.
            Returns None if no directory is found.
    
    """
    text = get_doc_text(doc, normalized=False)
    for folder in os.listdir(corpus_path):
        folder = os.path.join(corpus_path, folder)
        if os.path.isdir(folder):
            for file in os.listdir(folder):
                file = os.path.join(folder, file)
                if file.endswith(".txt"):
                    if open(file).read() == text:
                        # piped text was read with `open(...).read()`
                        return folder
                    elif read_annotated_file(file) == text:
                        # piped text was read with `read_annotated_file(...)`
                        return folder
    return None


def annotation_reader_catma(doc, corpus_path):
    """Spacy pipeline component.
        Reads path to a CATMA collection (a CATMA collection is the 
            unzipped tar.gz folder, which contains the annotated text as
            txt and a folder "annotationcollections" with the annotations
            of each annotator as TEI).
        Adds annotation to spacy document object (`Doc`) and spacy token object (`Token`)
            as _.annotations (name to call on spacy object).
    
        Doc._.annotations returns list of all annotions. Each annotation (list element) contains a dic with:
            - "tagset": tagset name (str)
            - "tag": tag name (str)
            - "property_values": dict with key for each property value (dict)
            - "id": unique id for annotated span (int)
            - "strings": list of str
            - "string_positions": list of tuple with start/end string position for anno in original text
            - "span": List of (Spacy-)Token 

    Args:
        doc (`Doc`): A spacy document object.
        corpus_path (str): Path to CATMA collection with a txt-file and TEI-files.
            The TEI files can be stored in an "annotationcollections"-folder or directly in `corpus_path`.
    
    Returns:
        doc (`Doc`): A spacy document object.
    
    """
    collectionPath = find_collection_path(doc, corpus_path)
    if collectionPath is None:
        return doc

    annotation_collection = read_annotation_(collectionPath)

    add_extension(Token, "annotations")
    add_extension(Span, "annotations")
    add_extension(Doc, "annotations")

    # 1. assign tokens to character positions for faster look-up
    # 2. assign empty AnnotationLists to every token for every annotator
    charpos_to_token_i = {}
    for token_i, token in enumerate(doc):
        try:
            charpos = token._.idx
        except AttributeError:
            charpos = token.idx
        charpos_to_token_i[charpos] = token_i

        token._.annotations = dict()
        for annotator_key in annotation_collection:
            token._.annotations[annotator_key] = AnnotationList()
    
    sub_clauses, sub_clause_starts, sub_clause_ends = get_sub_clauses(doc)
    
    if hasattr(doc._, "clauses"):
        for clause in doc._.clauses:
            clause._.annotations = dict()

    doc._.annotations = dict()
    for annotator_key in annotation_collection:
        ##print(annotator_key)
        
        # define each value for doc._.annotations[annotator_key] as AnnotationList
        doc._.annotations[annotator_key] = AnnotationList()

        # do the same for clauses, if the document is clausized
        if hasattr(doc._, "clauses"):
            for clause in doc._.clauses:
                clause._.annotations[annotator_key] = AnnotationList()

        for annotation_i, annotation in enumerate(annotation_collection[annotator_key]):
            token_indices = list()
            for string_position_tuple in annotation["string_positions"]:
                anno_range = range(string_position_tuple[0], string_position_tuple[1])
                for charpos in anno_range:
                    try:
                        token_indices.append(charpos_to_token_i[charpos])
                    except KeyError:
                        pass

            if len(token_indices) > 0:
                # define span as list of tokens
                anno_span = [doc[token_index] for token_index in token_indices]
                # add Annotation-object to anno_span._.annotation
                annotation_object = Annotation(tag=annotation['tag'], 
                                tagset=annotation['tagset_name'], 
                                property_values_dict=annotation['property_values'],
                                tokens=anno_span,
                                clauses=[],
                                id_value=annotation['id'], 
                                strings=annotation['strings'], 
                                string_positions=annotation['string_positions'])

                if len(sub_clauses) > 0:
                    annotation_object.clauses = map_tokens_to_closest_clauses(anno_span, sub_clauses, sub_clause_starts, sub_clause_ends)

                # append Annotation to AnnotationList for annotator
                doc._.annotations[annotator_key].append(annotation_object)

                # append Annotation to corresponding tokens
                for token_of_span in anno_span:
                    token_of_span._.annotations[annotator_key].append(annotation_object)
                
                # append Annotation to corresponding clauses
                for clause in annotation_object.clauses:
                    clause._.annotations[annotator_key].append(annotation_object)

        doc._.annotations[annotator_key] = AnnotationList(sorted(doc._.annotations[annotator_key], key=lambda anno: anno.string_positions[0][0]))

    return doc


def read_annotated_file(text_file:str) -> str:
    """Reads text file file with forced utf8 encoding to avoid offset issus.

    Args:
        file: A text file.
    
    Returns:
        str: text.
    
    """
    with codecs.open(text_file, 'r', encoding='utf8') as f:
        text = f.read()
        return text


def read_annotation(tei_file:str, text:str) -> list:
    """Stores all annotations of one annotator in list.

    Args:
        tei_file (str): annotation in CATMA-TEI-format
        text (str): annotated text
    
    Returns:
        List of annotation dictionaries.
        Each list element contains a dic with:
        - "tagset": tagset name (str)
        - "tag": tag name (str)
        - "property_values": dict with key for each property value
        - "id": unique id for annotated span
        - "strings": list of str
        - "string_positions": list of tuple with start/end string position for anno in original text    
    """
    with open(tei_file) as f:
        tree = ET.ElementTree(file=f)
        root = tree.getroot()
        
        annotation_list = list()
        
        # Every fs-Element contains an annotation.
        # Each fs-Element contains information about:
        # - the string position: fs[@xml:id = CATMA-ID] -> points to -> seg-Element[@ana = same CATMA-ID] 
        # - the tagset: fs[@xml:type = CATMA-ID] -> points to -> fsDecl[@xml:id = same CATMA-ID]
        # - Property-Values for each anno-span as fs[@attribute_for_prop_value]
        
        # select fs-element in tei:text (other fs-element in sourceDesc should not be considered)
        fs_list = root.findall("tei:text/tei:fs", NAMESPACE)
        for index_fs_list, fs in enumerate(fs_list):
            fs_dic = dict()
            
            #################################
            # Find String Positions via @xml:id
            xml_id = fs.attrib['{http://www.w3.org/XML/1998/namespace}id']

            # if seg[@ana] contains multiple catma-ids: <seg ana="#CATMA_D7AF9100-5264-49BC-BB25-16DE3F5F842B #CATMA_6057DA8D-9438-4DAC-A0C9-9A5F12EC83A4">
            # -> find all segs and check if xml_id is in attrib:
            seg_list = [elm for elm in root.findall(".//tei:seg[@ana]", NAMESPACE) if xml_id in elm.attrib["ana"]]
            
            # build list with all character positions as tuples with start/end-char
            chars = list()
            for seg in seg_list:
                ptr_target = seg.find("tei:ptr", NAMESPACE).attrib["target"]
                char = re.findall(r"\d+,\d+", ptr_target)
                char = char[0].split(",")
                char = (int(char[0]), int(char[1]))
                chars.append(char)
            
            string_positions = chars
            fs_dic["string_positions"] = string_positions
            
            #Find string via string_position
            string_list = list()
            for string_position in string_positions:
                string_list.append(text[string_position[0]:string_position[1]])
            fs_dic["strings"] = string_list

            #################################
            # Find tagset name, tag via @type
            type_ = fs.attrib['type']
            #tag
            fsDecl = root.findall(".//tei:fsDecl[@xml:id='%s']"%type_, NAMESPACE)
            #tagset name
            tag = fsDecl[0].find("tei:fsDescr", NAMESPACE).text
            tagset_name = root.findall(".//tei:fsDecl[@xml:id='%s']/.."%type_, NAMESPACE)[0].attrib["n"]
            fs_dic["tag"] = tag
            fs_dic["tagset_name"] = tagset_name

            #################################
            # Read Property Values in list:
            property_values = dict()
            for index_fs, f in enumerate(fs):
                #select property values (all f.attrib except the in the list below)
                if f.attrib["name"] not in ["catma_markupauthor", "catma_markuptimestamp", "catma_displaycolor"]:
                    #if one property value is selected, choose subelement string
                    if len(f.findall("tei:string", NAMESPACE)) != 0:
                        property_values[f.attrib["name"]] = [f.findall("tei:string", NAMESPACE)[0].text]
                    #if more property values are selected, choose subelements ("string") of vRange-Element
                    if len(f.findall("tei:vRange", NAMESPACE)) != 0:
                        property_values[f.attrib["name"]] = [i.text for i in f.findall(".//tei:string", NAMESPACE)]
            fs_dic["property_values"] = property_values
            
            #################################
            # Add index as identifier for each anno
            fs_dic["id"] = index_fs_list
            
            #################################
            # Append fs_dic for anno to list with all annos
            annotation_list.append(fs_dic)
            
            
        return annotation_list


def annotation_list2csv(annotation_list:list, csv_filename:str="anno_table.csv"):
    """Writes all annotations to a csv table.

    Args:
        annotation_list: list of dict, output of read_annotation().
        csv_filename (str): the designated file for the csv output.
    
    Returns:
        file: A csv file.
    """
    with open(csv_filename, mode="w") as csvfile:
        annowriter = csv.writer(csvfile, delimiter='^', quotechar='|')
        annowriter.writerow(["id", "strings", "string_positions", "tagset_name", "tag"])
        for anno_dic in annotation_list:
            annowriter.writerow([anno_dic["id"], [i.replace("\n","") for i in anno_dic["strings"]], anno_dic["string_positions"], anno_dic["tagset_name"], anno_dic["tag"], anno_dic["property_values"]])

            
def read_annotation_collection(collection_folder, write_csv_table=True):
    """Reads collection of CATMA annotations from the folder "annotationcollections".

    Args:
        collection_folder (str): collection_folder is the unzipped content of the CATMA-Corpus_Export-XXX.tar.gz
                                  which is named after the annotated text and which contains a folder "annotationcollections".
        write_csv_table (bool): If true a csv file with all annotations is written for each annotator.
    
    Returns:
        file: dict (annotator as key) of annotation_lists from read_annotation().
    """
    #collection_folder is the unzipped content of the CATMA-Corpus_Export-XXX.tar.gz
    #which is named after the annotated text, eg. "Doeblin_Die_Ermordung_der_Butterblume",
    #and which contains:
    # - the file itself (eg. "Doeblin_Die_Ermordung_der_Butterblume.txt")
    # - a folder "annotationcollections" with each annotation of that text
    for file in os.listdir(collection_folder):
        if file.endswith(".txt"):
            text_file = file
            text = read_annotated_file(collection_folder+ "/" +text_file)

    annotation_collection = dict()      
    for folder in os.listdir(collection_folder):
        if folder == "annotationcollections":
            annotationcollections_folder = folder
            for annotation_file in os.listdir(collection_folder + "/" + annotationcollections_folder):
                annotation_list = read_annotation(collection_folder + "/" + annotationcollections_folder + "/" + annotation_file, text)
                annotation_collection[annotation_file.strip(".xml")] = annotation_list

                if write_csv_table:
                    CATMA_timestamp_folder = re.search("(.*)/", collection_folder).group(1)
                    annotation_csv_dir = "%s/annotation_csv" %CATMA_timestamp_folder
                    if not os.path.exists(annotation_csv_dir):
                        os.makedirs(annotation_csv_dir) 
                    path_and_filename = "%s/%s.csv"%(annotation_csv_dir, annotation_file.strip(".xml"))
                    annotation_list2csv(annotation_list, csv_filename=path_and_filename)

    return annotation_collection


def read_annotation_files(collection_folder, write_csv_table=True, only_latest=True):
    """Reads annotation files in CATMA-TEI-format and stores each annotation as annotation_list
        in dict with keys for annotators. Files are expected to be in a folder named after the
        corresponding text, eg. "Fontane__Der_Stechlin", which contains annotation files with a 
        abrevation of the annotator and a timestamp, eg. "FA_2020-11-12". The newest annotation
        is considered for the annotation collection.

    Args:
        collection_folder (str): folder with annotation files.
        write_csv_table (bool): If true a csv file with all annotations is written for each annotator.
        only_latest (bool): Enable special treatment for collections with the naming scheme "AAA-B_YYYY-MM-DD".
            Every annotation file that starts with "AAA" (i.e. "AAA-B", "AAA-C", "AAA-D" ...) is treated to belong to the same group.
            From every group, only the newest annotation file is kept; the timestamp is cut off (e.g. "AAA-B_2020-11-12" -> "AAA-B").
    
    Returns:
        file: dict (annotator as key) of annotation_lists from read_annotation().
    
    """
    for file in os.listdir(collection_folder):
        if file.endswith(".txt"):
            text_file = file
            text = read_annotated_file(collection_folder+ "/" +text_file)
    
    annotation_collection = dict()
    annotators = set()
    for annotation_file in sorted(os.listdir(collection_folder), key=lambda fname: fname.split("_")[-1], reverse=True):
        annotator = annotation_file.strip(".xml")
        annotator_group = annotator
        if only_latest:
            annotator = annotation_file.split("_")[0].strip(".xml")
            annotator_group = annotator.split("-")[0]
        if (not annotation_file.endswith(".xml")) or annotator_group in annotators:
            continue
        annotators.add(annotator_group)
        annotation_list = read_annotation(collection_folder + "/" + annotation_file, text)
        annotation_collection[annotator] = annotation_list

        if write_csv_table:
            CATMA_timestamp_folder = re.search("(.*)/", collection_folder).group(1)
            annotation_csv_dir = "%s/annotation_csv" %CATMA_timestamp_folder
            if not os.path.exists(annotation_csv_dir):
                os.makedirs(annotation_csv_dir) 
            path_and_filename = "%s/%s.csv"%(annotation_csv_dir, annotator)
            annotation_list2csv(annotation_list, csv_filename=path_and_filename)
    
    return annotation_collection


def read_annotation_(collection_folder, write_csv_table=False, only_latest=True):
    """Automatically choose between `read_annotation_collection` and `read_annotation_files`.
    TODO
    """
    if "annotationcollections" in os.listdir(collection_folder):
        return read_annotation_collection(collection_folder, write_csv_table)
    return read_annotation_files(collection_folder, write_csv_table, only_latest)


def property_table(Doc, collection_folder, annotator, prop):
    """Simple csv-output for selected properties (only for manual output)
    """

    with open('%s/property_table/%s_%s.csv'%(collection_folder,annotator,prop), mode="w") as csvfile:
        selection = [anno for anno in doc._.anno[annotator] if prop in anno["property_values"] and anno['tagset_name'].startswith("Reflexion IV")]
        annowriter = csv.writer(csvfile, delimiter='^', quotechar='|')
        annowriter.writerow([prop, "tag", "Passage"])
        for anno_dic in selection:
            annowriter.writerow([anno_dic["property_values"][prop], anno_dic["tag"], anno_dic["string"].replace("\n","")])



if __name__ == "__main__":
    ##test annotation list:
    #tei = '../../../mona_korpus/Goethe__Die_Wahlverwandtschaften/Auswahl/EO_2020-11-04.xml'
    #annotation_list = read_annotation(tei_file=tei, text=read_annotated_file(text_file='../../../mona_korpus/Goethe__Die_Wahlverwandtschaften/Goethe__Die_Wahlverwandtschaften.txt'))
    #print(annotation_list[0:10])

    ##Values for Sicherheit+Kommentar:
    wahlverwandtschaften = read_annotated_file('../../../mona_korpus/Goethe__Die_Wahlverwandtschaften/Goethe__Die_Wahlverwandtschaften.txt')
    pipe = pipeline(pickle_load=True, pickle_save=False, pickle_overwrite=True, corpus_path='../../../mona_korpus')
    doc = pipe(wahlverwandtschaften)

    property_table(Doc=doc, corpus_path='../../../mona_korpus',annotator = 'RvW_2020-11-04',prop = "Sicherheit")