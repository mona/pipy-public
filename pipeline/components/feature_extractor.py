import spacy
from spacy.tokens import Span

from ..global_resources import GERMANET
from ..global_wordlists import COMMENT_MARKERS, COMMENT_MODIFIERS, COMMENT_NOUNS, COMMENT_VERBS, DEICTIC, NEGATIONS, QUANTIFIERS
from ..utils_classes import Debugger
from ..utils_methods import add_extension, is_deictic, is_generic_attr, is_NP, is_vague_attr, is_VP


def dfg_feature_extractor(doc):
    """Spacy pipeline component.
        Adds DFG (delexicalised functional grammar) structures to each clause.
            
            # Clause
            Features are extracted clause-wise.
                clause_
                    dep, head, pos, punct_inner, punct_prec, punct_succ, sent_conditional, sent_start, speech
            
            # NPs
            NPs are distinguished by head relation (NPs with the same head relation are merged into a single structure).
            If coreference resolution is enabled, the last non-pronoun mention of a pronoun is added to its structure.
                NP_[DEPREL](_$coref)_
                    adj_category, adj_degree, adj_emotion, adj_pos, adj_sentiment, adp, art_lemma, art_pos, case, comment, deictic, dep, emotion, gender, numerus, person, pos, quant_pos, quant_type, sentiment

            # (Composite) Verb
            The composite verb is analysed.
                verb_
                    aspect, category, comment, deictic, dep, emotion, form, mode, pos, quant_pos, quant_type, sentiment, tense, voice
            
            # Free Discourse Elements
            Remaining elements on clause-level (complementisers, adverbs etc.)
                disc_[DEPREL]_
                    comment, dep, index, pos

        For example, the clause
            
            "Aber Peter kauft sich jeden Morgen einen schlechten Kaffee."
            
        yields the following features:
        
            / NP     / iobj  / $coref  / case    nom   \ \ \ \   # sich <- Peter
            |        |       |         | dep     nsubj | | | |
            |        |       |         | numerus sing  | | | |
            |        |       |         \ pos     PROPN / | | |
            |        |       |                           | | |
            |        |       | dep     iobj              | | |   # sich
            |        |       | numerus sing              | | |
            |        |       \ pos     PRON              / | |
            |        |                                     | |
            |        | nsubj / case    nom   \             | |   # Peter
            |        |       | dep     nsubj |             | |
            |        |       | numerus sing  |             | |
            |        |       \ pos     PROPN /             | |
            |        |                                     | |
            |        | obj   / adj     / degree    pos \ \ | |   # schlechten
            |        |       |         | pos       ADJ | | | |
            |        |       |         \ sentiment neg / | | |
            |        |       | art     / lemma ein \     | | |   # einen
            |        |       |         \ pos   DET /     | | |            
            |        |       | case    acc               | | |   # Kaffee
            |        |       | dep     obj               | | |
            |        |       | gender  masc              | | |
            |        |       | numerus sing              | | |
            |        |       \ pos     NOUN              / | |
            |        |                                     | |
            |        | obl   / case    acc          \      | |   # Morgen
            |        |       | dep     obl          |      | |
            |        |       | emotion Anticipation |      | |
            |        |       | gender  masc         |      | |
            |        |       | numerus sing         |      | |
            |        |       | pos     NOUN         |      | |
            |        |       | quant   / pos  DET \ |      | |   # jeden
            |        \       \         \ type ALL / /      / |
            |                                                |
            | clause / dep   ROOT          \                 |
            |        | head  0             |                 |
            |        | pos   VERB          |                 |
            |        | punct [ succ . ]    |                 |   # .
            |        \ sent  [ start yes ] /                 |
            |                                                |
            | disc   / cc / comment yes   \ \                |   # Aber
            |        |    | dep     cc    | |                |
            |        |    | index   first | |                |
            |        \    \ pos     CCONJ / /                |
            |                                                |
            | verb   / aspect   imperf           \           |   # kauft
            |        | category WordClass.Besitz |           |
            |        | dep      ROOT             |           |
            |        | form     fin              |           |
            |        | mode     ind              |           |
            |        | pos      VERB             |           |
            |        | tense    pres             |           |
            \        \ voice    active           /           /
        
        Levels are converted to underscores, e.g. "NP_iobj_$coref_case=nom".

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "feats")
    for k, clause in enumerate(doc._.clauses):
        feats = set()
        
        # `free_discourse_elements` are all tokens of the clause at the beginning;
        # tokens that are parts of NPs/VPs will be reduced in the following;
        # what remains are free elements within a clause
        free_discourse_elements = set(clause._.tokens)
        
        # clause features
        prefix = "clause"
        
        # extract dependency relation and POS tag of root token
        feats.add(prefix + "_" + "dep" + "=" + clause.root.dep_.split(":")[0])
        feats.add(prefix + "_" + "pos" + "=" + clause.root.pos_)
        
        # extract directed distance to head clause
        try:
            d = doc._.clauses.index(clause.root.head._.clause)-k
            if d > 0:
                d = "+" + str(d)
            else:
                d = str(d)
            feats.add(prefix + "_" + "head" + "=" + d)
        except ValueError:
            pass # if clause.root.head._.clause is None
        
        # extract speech types
        if hasattr(clause._, "speech"):
            for speech_type in clause._.speech:
                if clause._.speech[speech_type] / len(clause._.tokens) > 0.5:
                    feats.add(prefix + "_" + "speech" + "=" + speech_type)
        
        # extract preceding and succeeding punctuation
        for punct in clause._.prec_punct:
            feats.add(prefix + "_" + "punct_prec" + "=" + punct.text)
        for punct in clause._.succ_punct:
            feats.add(prefix + "_" + "punct_succ" + "=" + punct.text)
        
        # extract whether the clause is the start of a new sentence
        for token in clause._.prec_punct + clause._.tokens + clause._.succ_punct:
            if token.is_sent_start:
                feats.add(prefix + "_" + "sent_start" + "=" + "yes")
                break
        
        # find all NPs in the clause
        NPs = {}
        NP_deps = ["nsubj", "obj", "iobj", "obl", "nmod", "appos"]
        for token in clause._.tokens:
            # extract whether the clause contains a conditional marker
            if token.lower_ in ["wenn", "wer", "wem", "wen"]:
                feats.add(prefix + "_" + "sent_conditional" + "=" + "yes")
                free_discourse_elements.discard(token)
            # extract inner punctuation
            if token.is_punct:
                feats.add(prefix + "_" + "punct_inner" + "=" + token.text)
            # if the token is not a punctuation mark, find its NP head:
            else:
                current = token
                while True:
                    dep = current.dep_.split(":")[0]
                    if is_VP(current) or is_vague_attr(current):
                        # VPs below NPs are cut out of the NP
                        # NPs that are vague quantifiers are ignored
                        break
                    elif is_NP(current) or dep in NP_deps or (dep == "conj" and current.head.dep_.split(":")[0] in NP_deps):
                        if current in clause._.tokens:
                            try:
                                NPs[current.i].append(token)
                            except KeyError:
                                NPs[current.i] = [token]
                        break
                    if current.head == current:
                        break
                    current = current.head
        
        # NP features
        for token_i in NPs:
            token = doc[token_i]
            tokens = NPs[token_i]
            dep = token.dep_.split(":")[0]
            prefix = "NP" + "_" + dep
            feats.update(get_NP_feats(prefix, token, tokens))
            free_discourse_elements.difference_update(set(tokens))

            # extract features for anaphoric pronouns
            # (features of the last non-pronoun mention)
            if token.pos_ == "PRON" and hasattr(token._, "coref_clusters"):
                for cluster in token._.coref_clusters:
                    last_mention = None
                    found = False
                    for mention in reversed(cluster.mentions):
                        if found:
                            if mention.root.pos_ != "PRON":
                                last_mention = mention
                                break
                        elif token in mention:
                            found = True
                    if last_mention is not None:
                        prefix = "NP" + "_" + dep + "_" + "$coref"
                        feats.update(get_NP_feats(prefix, last_mention.root, list(last_mention)))

        # verb features
        prefix = "verb"

        if clause._.form is not None:

            # extract dependency relation, POS tag, sentiment, emotions and comment indicators of main verb
            if clause._.form.main is not None:
                feats.add(prefix + "_" + "dep" + "=" + clause._.form.main.dep_.split(":")[0])
                feats.add(prefix + "_" + "pos" + "=" + clause._.form.main.pos_)
                feats.update(extract_sentiment_feats(prefix, clause._.form.main))
                feats.update(extract_emotion_feats(prefix, clause._.form.main))
                if clause._.form.main.lemma_ in COMMENT_VERBS:
                    feats.add(prefix + "_" + "comment" + "=" + "yes")
            
            # extract grammatical features
            feats.update(extract_morphosyntactic_feats(prefix, clause, "form", ["tense", "aspect", "mode", "voice", "verb_form"]))
            
            # extract modal verbs
            for modal in clause._.form.modals:
                feats.add(prefix + "_" + "modal" + "=" + modal.lemma_)
            
            free_discourse_elements.difference_update(set(clause._.form.verbs))
            free_discourse_elements.difference_update(set([token for token in clause._.tokens if token.head in clause._.form.verbs and (token.pos_ == "PART" or token.dep_.split(":")[0] == "compound")]))
        
        # extract GermaNet verb category
        feats.update(extract_germanet_feats(prefix, clause, "verb_synset_id"))

        # extract quantifiers, negation and deicticness
        VP_mods = set()
        for child in free_discourse_elements:
            if child.lemma_.lower() in QUANTIFIERS:
                feats.add(prefix + "_" + "quant_type" + "=" + QUANTIFIERS[child.lemma_.lower()])
                feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
                VP_mods.add(child)
            elif child.lemma_.lower() in NEGATIONS:
                feats.add(prefix + "_" + "quant_type" + "=" + "NEG")
                feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
                VP_mods.add(child)
            elif is_generic_attr(child):
                feats.add(prefix + "_" + "quant_type" + "=" + "BARE")
                feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
                VP_mods.add(child)
            elif is_vague_attr(child):
                feats.add(prefix + "_" + "quant_type" + "=" + "DIV")
                feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
                VP_mods.add(child)
            elif child.lemma_ in DEICTIC:
                feats.add(prefix + "_" + "deictic" + "=" + "yes")
                VP_mods.add(child)
        free_discourse_elements.difference_update(VP_mods)
        
        # free discourse elements must be children of clause root
        #free_discourse_elements.intersection_update(set([clause.root] + list(clause.root.children)))
        free_discourse_elements.intersection_update(set(clause.root.children))
        
        # non-space non-punctuation tokens
        word_tokens = [token for token in clause._.tokens if not (token.is_punct or token.is_space)]

        # free discourse element features
        for token in free_discourse_elements:
            if not (token.is_punct or token.is_space):
                dep = token.dep_.split(":")[0]
                prefix = "disc" + "_" + dep

                # extract dependency relation and POS tag of free discourse element
                feats.add(prefix + "_" + "dep" + "=" + dep)
                feats.add(prefix + "_" + "pos" + "=" + token.pos_)

                # extract comment indicators
                if token.lemma_ in COMMENT_MARKERS or token.lemma_ in COMMENT_MODIFIERS:
                    feats.add(prefix + "_" + "comment" + "=" + "yes")

                # extract position
                if word_tokens.index(token) == 0:
                    feats.add(prefix + "_" + "index" + "=" + "first")
                elif word_tokens.index(token) == len(word_tokens)-1:
                    feats.add(prefix + "_" + "index" + "=" + "last")
                else:
                    feats.add(prefix + "_" + "index" + "=" + "mid")
    
        clause._.feats = feats
    
    return doc


def get_NP_feats(prefix, token, tokens):
    """Method to extract NP features.

    Args:
        prefix (str): Feature prefix.
        token (`Token`): Head token of the NP.
        tokens (list of `Token`): All tokens of the NP.
    
    Returns:
        set of str: Set of features.
    
    """
    feats = set()

    # extract dependency relation, POS tag, morphological features, sentiment and emotion of NP head
    feats.add(prefix + "_" + "dep" + "=" + token.dep_.split(":")[0])
    feats.add(prefix + "_" + "pos" + "=" + token.pos_)
    feats.update(extract_morphosyntactic_feats(prefix, token, "morph", ["case", "person", "numerus", "gender"]))
    feats.update(extract_sentiment_feats(prefix, token))
    feats.update(extract_emotion_feats(prefix, token))
    
    # extract comment indicators
    if token.lemma_ in COMMENT_NOUNS:
        feats.add(prefix + "_" + "comment" + "=" + "yes")
    
    # extract adpositionality
    for child in token.children:
        if child.dep_.split(":")[0] == "case" and child.pos_ == "ADP":
            feats.add(prefix + "_" + "adp" + "=" + "yes")
            break

    # extract quantifiers, negation, numerals, articles, deicticness and adjectives
    for child in tokens:
        if child.lemma_.lower() in QUANTIFIERS:
            feats.add(prefix + "_" + "quant_type" + "=" + QUANTIFIERS[child.lemma_.lower()])
            feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
        elif child.lemma_.lower() in NEGATIONS:
            feats.add(prefix + "_" + "quant_type" + "=" + "NEG")
            feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
        elif child.pos_ == "NUM" or child.dep_.split(":")[0] == "nummod":
            feats.add(prefix + "_" + "quant_type" + "=" + "ZAHL")
            feats.add(prefix + "_" + "quant_pos" + "=" + child.pos_)
        elif child.pos_ == "DET":
            feats.add(prefix + "_" + "art_lemma" + "=" + child.lemma_.lower())
            feats.add(prefix + "_" + "art_pos" + "=" + child.pos_)
        elif child.lemma_ in DEICTIC:
            feats.add(prefix + "_" + "deictic" + "=" + "yes")
        elif child.pos_ == "ADJ":
            feats.add(prefix + "_" + "adj_pos" + "=" + child.pos_)
            feats.update(extract_morphosyntactic_feats(prefix + "_" + "adj", child, "morph", ["degree"]))
            feats.update(extract_germanet_feats(prefix + "_" + "adj", child, "synset_id"))
            feats.update(extract_sentiment_feats(prefix + "_" + "adj", child))
            feats.update(extract_emotion_feats(prefix + "_" + "adj", child))
    
    # extract existential quantifiers
    if (
        (token.head.lemma_ == "existieren" and token.dep_.split(":")[0] == "nsubj")
        or (token.head.lemma_ == "geben" and "es" in [child.lower_ for child in token.head.children] and token.dep_.split(":")[0] == "obj")
    ):
        feats.add(prefix + "_" + "quant_type" + "=" + "EXIST")
        feats.add(prefix + "_" + "quant_pos" + "=" + token.head.pos_)
    
    return feats


def extract_morphosyntactic_feats(prefix, element, attr, feat_names):
    """Method to extract morphological/grammatical features from a token/clause.

    Args:
        prefix (str): Feature prefix.
        element (obj): Token/Clause to extract features from.
        attr (str): `morph` or `form`.
        feat_names (list of str): The features to extract.

    Returns:
        set of str: Set of features.
    
    """
    feats = set()
    for feat in feat_names:
        val = getattr(getattr(element._, attr), feat)
        if val is not None:
            feats.add(prefix + "_" + feat.split("_")[-1] + "=" + val)
    return feats


def extract_germanet_feats(prefix, element, attr):
    """Method to extract Germanet features from a token/clause.

    Args:
        prefix (str): Feature prefix.
        element (obj): Token/Clause to extract features from.
        attr (str): `synset_id` or `verb_synset_id`.

    Returns:
        set of str: Set of features.
    
    """
    feats = set()
    if hasattr(element._, attr):
        synset_id = getattr(element._, attr)
        if synset_id is not None:
            GERMANET.provide()
            feats.add(prefix + "_" + "category" + "=" + str(GERMANET._.get_synset_by_id(synset_id).word_class))
    return feats


def extract_sentiment_feats(prefix, token):
    """Method to extract sentiment features from a token.

    Args:
        prefix (str): Feature prefix.
        token (`Token`): Token to extract features from.

    Returns:
        set of str: Set of features.
    
    """
    feats = set()
    if hasattr(token._, "sentiws") and token._.sentiws is not None and abs(token._.sentiws) > 0.5:
        feats.add(prefix + "_" + "sentiment" + "=" + ("neg" if token._.sentiws < 0 else "pos"))
    return feats


def extract_emotion_feats(prefix, token):
    """Method to extract emotion features from a token.

    Args:
        prefix (str): Feature prefix.
        token (`Token`): Token to extract features from.

    Returns:
        set of str: Set of features.
    
    """
    feats = set()
    if hasattr(token._, "emotions") and token._.emotions is not None and len(token._.emotions) > 0:
        for emotion in ["Anger", "Anticipation", "Disgust", "Fear", "Joy", "Sadness", "Surprise", "Trust"]:
            try:
                if token._.emotions[emotion] == "1":
                    feats.add(prefix + "_" + "emotion" + "=" + emotion)
            except KeyError:
                pass
    return feats