from flair.data import Sentence
from spacy.tokens import Span, Token

from ..global_resources import SPEECH_TAGGERS
from ..global_wordlists import Q_MARKS_C, Q_MARKS_O
from ..utils_methods import add_extension


def flair_speech_tagger(doc, sentence_level=False):
    """Spacy pipeline component.
        Tags tokens and clauses with speech tags.
        Wrapper for the "Redewiedergabe" taggers from https://github.com/redewiedergabe/tagger.

    Args:
        doc (`Doc`): A spacy document object.
        sentence_level (boolean): If True, the taggers take each sentence separately as input;
            if False, the taggers take chunks of up to 100 tokens as input.
    
    Returns:
        `Doc`: A spacy document object.
    
    """
    global SPEECH_TAGGERS
    SPEECH_TAGGERS.provide()
    
    add_extension(Token, "speech")
    for token in doc:
        token._.speech = {}
    
    if sentence_level:
        for sent in doc.sents:
            text = " ".join([token.text for token in sent if not token.is_space])
            text = Sentence(text, use_tokenizer=False)
            add_speech_tags_to_tokens(sent, text, SPEECH_TAGGERS._, "indirect")
            add_speech_tags_to_tokens(sent, text, SPEECH_TAGGERS._, "freeIndirect")
            add_speech_tags_to_tokens(sent, text, SPEECH_TAGGERS._, "direct")
            add_speech_tags_to_tokens(sent, text, SPEECH_TAGGERS._, "reported")
    else:
        chunks = []
        chunk = []
        for sent in doc.sents:
            tokens = list(sent)
            if len(chunk) + len(tokens) <= 100 or len(chunk) == 0:
                chunk.extend(tokens)
            else:
                chunks.append(chunk)
                chunk = tokens
        if len(chunk) > 0:
            chunks.append(chunk)
        for chunk in chunks:
            text = " ".join([token.text for token in chunk if not token.is_space])
            text = Sentence(text, use_tokenizer=False)
            add_speech_tags_to_tokens(chunk, text, SPEECH_TAGGERS._, "indirect")
            add_speech_tags_to_tokens(chunk, text, SPEECH_TAGGERS._, "freeIndirect")
            add_speech_tags_to_tokens(chunk, text, SPEECH_TAGGERS._, "direct")
            add_speech_tags_to_tokens(chunk, text, SPEECH_TAGGERS._, "reported")
    
    assign_speech_tags_to_clauses(doc)
    
    return doc


def add_speech_tags_to_tokens(sent, text, taggers, speech_type):
    """Add speech tags to tokens in a sentence.

    Args:
        sent (`Span`): The sentence in spacy format.
        text (`Sentence`): The sentence in flair format.
        taggers (dict of str:`SequenceTagger`): The speech taggers; keys are speech types, values are taggers.
        speech_type (str): The speech type to tag.
    
    """
    taggers[speech_type].predict(text)
    s = 0
    for i, token in enumerate(sent):
        if token.is_space:
            s += 1
        else:
            tag = text.tokens[i-s].get_labels()[0]
            if tag.value == speech_type:
                token._.speech[speech_type] = tag.score


def assign_speech_tags_to_clauses(doc):
    """Create clause-level speech tags from token-level speech tags.
        If the document is not split into clauses, nothing happens.

    Args:
        `Doc`: A document whose tokens already have a `_.speech` attribute.
    
    Returns:
        `Doc`: The document whose clauses now also have a `_.speech` attribute.
    
    """
    if hasattr(doc._, "clauses"):
        add_extension(Span, "speech")
        for clause in doc._.clauses:
            clause._.speech = {} # product pobability
            number_of_tokens = {} # number of tokens
            for token in clause._.tokens:
                for val in token._.speech:
                    try:
                        clause._.speech[val] *= token._.speech[val]
                        number_of_tokens[val] += 1
                    except KeyError:
                        clause._.speech[val] = token._.speech[val]
                        number_of_tokens[val] = 1
            for val in clause._.speech:
                clause._.speech[val] += number_of_tokens[val] # number of tokens + product probability


def quotation_marks_speech_tagger(doc):
    """Spacy pipeline component.
        Detects only direct speech within (German) quotation marks.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.
    
    """
    add_extension(Token, "speech")
    o = -1
    for i, token in enumerate(doc):
        token._.speech = {}
        if token.text in Q_MARKS_O and (o < 0 or token.text not in Q_MARKS_C):
            o = i
            mark = token.text
        elif token.text in Q_MARKS_C:
            if o > -1 and Q_MARKS_C.index(token.text) == Q_MARKS_O.index(mark):
                for tok in doc[o:i+1]:
                    tok._.speech["direct"] = 1.0
            o = -1
    
    assign_speech_tags_to_clauses(doc)
    
    return doc