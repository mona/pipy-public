import bz2 as xzip # `bz2` or `gzip`
import copy
import inspect
import os
import pickle
import spacy
import warnings
from hashlib import md5
from neuralcoref.neuralcoref import Cluster
from spacy.tokens import Doc, Span, Token

from ..annotation import Annotation, AnnotationList
from ..passage import Passage
from ..utils_classes import Form, NonEmptyList, NonEmptySet
from ..utils_methods import add_extension


# unpicklable classes
CLASSES_WITH_SPAN_OR_TOKEN_ATTRIBUTES = [Annotation, Cluster, Form, Passage]


# Classes identical to Tokens and Spans but with an implemented `__reduce__` function

class ReducableToken(Token):
    def __new__(cls, vocab, doc, i, *args, **kw):
        obj = super().__new__(cls, vocab, doc, i, *args, **kw)
        return obj
    def __init__(self, *args, **kw):
        pass
    def __reduce__(self):
        return (
            self.__class__,
            (None, None, self.i)
        )

class ReducableSpan(Span):
    def __new__(cls, doc, start, end, label, vector, vector_norm, kb_id, *args, **kw):
        obj = super().__new__(cls, doc, start, end, label, vector, vector_norm, kb_id, *args, **kw)
        return obj
    def __init__(self, *args, **kw):
        pass
    def __reduce__(self):
        return (
            self.__class__,
            (None, self.start, self.end, self.label, self.vector, self.vector_norm, self.kb_id)
        )


# Classes for replacing Tokens and Spans during pickling

class PickleToken():
    def __init__(self, i):
        self.i = i

class PickleSpan():
    def __init__(self, start, end):
        self.start = start
        self.end = end


def pickle_init(doc, model, slicer, max_units):
    """Spacy pipeline component.
        Adds an ID to the document. The ID will be updated with every processing step.

    Args:
        doc (`Doc`): A spacy document object.
        model (str): The name of the spacy model.
        max_units (int): Maximum number of units to process.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Doc, "pickle_id")
    doc._.pickle_id = {"text" : doc.text, "model" : model, "func" : [], "slicer" : slicer, "max_units" : max_units}
    return doc


def handle_complex_object(val, func):
    """Copy an object and apply a function to all of its writable attributes.

    Args:
        val (obj): The object.
        func (func): The function.
    
    Returns:
        obj: A shallow copy of the object with changed attributes.
    
    """
    obj = copy.copy(val)
    for k, v in vars(val).items():
        setattr(obj, k, func(v))
    return obj


def make_picklable(val):
    """Make an object picklable by replacing all referenced `Token`s and `Span`s with `PickleToken`s and `PickleSpan`s.

    Args:
        val (obj): An object.
    
    Returns:
        obj: A picklable object.
    
    """
    type_ = type(val)
    if type_ == Token:
        return PickleToken(val.i)
    elif type_ == Span:
        return PickleSpan(val.start, val.end)
    elif type_ in CLASSES_WITH_SPAN_OR_TOKEN_ATTRIBUTES:
        return handle_complex_object(val, make_picklable)
    elif type_ in [list, NonEmptyList, AnnotationList]:
        return type_([make_picklable(v) for v in val])
    elif type_ == dict:
        return {k : make_picklable(v) for k, v in val.items()}
    return val


def unmake_picklable(val, doc):
    """Restore the original object by replacing all referenced `PickleToken`s and `PickleSpan`s with `Token`s and `Span`s.

    Args:
        val (obj): A pickled object.
    
    Returns:
        obj: The orignal object.
    
    """
    type_ = type(val)
    if type_ == PickleToken:
        return doc[val.i]
    elif type_ == PickleSpan:
        return doc[val.start:val.end]
    elif type_ in CLASSES_WITH_SPAN_OR_TOKEN_ATTRIBUTES:
        return handle_complex_object(val, lambda val: unmake_picklable(val, doc))
    elif type_ in [list, NonEmptyList, AnnotationList]:
        return type_([unmake_picklable(v, doc) for v in val])
    elif type_ == dict:
        return {k : unmake_picklable(v, doc) for k, v in val.items()}
    return val


def get_user_data(obj):
    """Get the custom attributes of a spaCy object in picklable format.

    Args:
        obj (`Doc` or `Span` or `Token`): The spaCy object.
    
    Returns:
        dict of str:obj: Dictionary which maps attribute names to picklable values.
    
    """
    user_data_ = {}
    for attr in dir(obj._):
        if attr not in ["has", "get", "set"]: # those are no custom attributes
            val = getattr(obj._, attr)
            user_data_[attr] = make_picklable(val)
    return user_data_


def exclude_user_data(doc):
    """Convert the custom attributes of a spaCy document into picklable format.

    Args:
        doc (`Doc`): The document.
    
    Returns:
        dict of str:obj: A dictionary with those keys: "doc", "sents", "clauses", "speech_segments", "mentions", "tokens".
            The values are dictionaries which map attribute names to picklable values for the document and every sentence, token etc.
    
    """
    user_data = {}
    
    # document-level attributes
    user_data["doc"] = get_user_data(doc)
    
    # token-level attributes
    user_data["tokens"] = {}
    for token in doc:
        user_data["tokens"][token.i] = get_user_data(token)
    
    # sentence-level attributes
    if doc.is_sentenced:
        user_data["sents"] = {}
        for sent in doc.sents:
            user_data["sents"][(sent.start, sent.end)] = get_user_data(sent)
    
    # clause-level attributes
    if hasattr(doc._, "clauses") and doc._.clauses is not None:
        user_data["clauses"] = {}
        for clause in doc._.clauses:
            user_data["clauses"][(clause.start, clause.end)] = get_user_data(clause)
    
    # speech-segment-level attributes
    if hasattr(doc._, "speech_segments") and doc._.speech_segments is not None:
        user_data["speech_segments"] = {}
        for segment in doc._.speech_segments:
            user_data["speech_segments"][(segment.start, segment.end)] = get_user_data(segment)
    
    # entity-level attributes
    if hasattr(doc._, "coref_clusters") and doc._.coref_clusters is not None:
        user_data["mentions"] = {}
        for cluster in doc._.coref_clusters:
            for mention in cluster.mentions:
                user_data["mentions"][(mention.start, mention.end)] = get_user_data(mention)
    
    # temponym-level attributes
    if hasattr(doc._, "temponyms") and doc._.temponyms is not None:
        user_data["temponyms"] = {}
        for temponym in doc._.temponyms:
            user_data["temponyms"][(temponym.start, temponym.end)] = get_user_data(temponym)
    
    # scene-level attributes
    if hasattr(doc._, "scenes") and doc._.scenes is not None:
        user_data["scenes"] = {}
        for scene in doc._.scenes:
            user_data["scenes"][(scene.start, scene.end)] = get_user_data(scene)
    
    return user_data


def include_user_data(doc, user_data):
    """Convert the custom attributes back to the original format and add them to a spaCy document.

    Args:
        doc (`Doc`): The document.
        user_data (dict of str:obj): Dictionary with user data in picklable format.
            Output of `exclude_user_data(doc)`.
    
    """
    # document-level attributes
    for attr in user_data["doc"]:
        add_extension(Doc, attr)
        setattr(doc._, attr, unmake_picklable(user_data["doc"][attr], doc))
    
    # token-level attributes
    for token in doc:
        for attr in user_data["tokens"][token.i]:
            add_extension(Token, attr)
            setattr(token._, attr, unmake_picklable(user_data["tokens"][token.i][attr], doc))
    
    # sentence-level attributes
    if "sents" in user_data:
        for sent in doc.sents:
            try:
                for attr in user_data["sents"][(sent.start, sent.end)]:
                    add_extension(Span, attr)
                    setattr(sent._, attr, unmake_picklable(user_data["sents"][(sent.start, sent.end)][attr], doc))
            except KeyError:
                warnings.warn(warn_message("sentence", (sent.start, sent.end)))
                # I have absolutely no idea why a sentence could be messing here, 
                # but it happened once. Unless this error goes on to occur more 
                # frequently, this warning solution will have to do.
                # A sentence without any set attributes (or `None` instead of valid 
                # values), however, may cause errors in subsequent processing steps. 
                # Therefore, some attributes have to be set manually:
                setattr(sent._, "clauses", [])
    
    # clause-level attributes
    if "clauses" in user_data:
        for clause in doc._.clauses:
            for attr in user_data["clauses"][(clause.start, clause.end)]:
                add_extension(Span, attr)
                setattr(clause._, attr, unmake_picklable(user_data["clauses"][(clause.start, clause.end)][attr], doc))
    
    # speech-segment-level attributes
    if "speech_segments" in user_data:
        for segment in doc._.speech_segments:
            for attr in user_data["speech_segments"][(segment.start, segment.end)]:
                add_extension(Span, attr)
                setattr(segment._, attr, unmake_picklable(user_data["speech_segments"][(segment.start, segment.end)][attr], doc))
    
    # entity-level attributes
    if "mentions" in user_data:
        for cluster in doc._.coref_clusters:
            for mention in cluster.mentions:
                for attr in user_data["mentions"][(mention.start, mention.end)]:
                    add_extension(Span, attr)
                    setattr(mention._, attr, unmake_picklable(user_data["mentions"][(mention.start, mention.end)][attr], doc))
    
    # temponym-level attributes
    if "temponyms" in user_data:
        for temponym in doc._.temponyms:
            for attr in user_data["temponyms"][(temponym.start, temponym.end)]:
                add_extension(Span, attr)
                setattr(temponym._, attr, unmake_picklable(user_data["temponyms"][(temponym.start, temponym.end)][attr], doc))
    
    # scene-level attributes
    if "scenes" in user_data:
        for scene in doc._.scenes:
            for attr in user_data["scenes"][(scene.start, scene.end)]:
                add_extension(Span, attr)
                setattr(scene._, attr, unmake_picklable(user_data["scenes"][(scene.start, scene.end)][attr], doc))
    

def warn_message(name, index):
    """Return a warn message for a missing element after pickling.

    Args:
        name (str): The name of the element (e.g. "sentence").
        index (obj): The index or boundaries of the element (e.g. (0,6)).
    
    Returns:
        str: A warn message (e.g. "Sentence with boundaries (0,6) is not in user data.").
    """
    message = name[0].upper() + name[1:]
    if type(index) == int:
        message += " at index " + str(index[0])
    else: # tuple
        message += " with boundaries (" + str(index[0]) + "," + str(index[-1]) + ")"
    message += " is not in user data."
    return message


def save_doc(doc, pickle_path):
    """Save a document at a given location.

    Args:
        doc (`Doc`): The document to save.
        pickle_path (str): The path to save the document to.
            The user data is saved to a separate file, to `pickle_path + "_"`.
    
    """
    doc.to_disk(pickle_path, exclude=["tensor", "user_data"])
    pickle.dump(exclude_user_data(doc), xzip.open(pickle_path + "_", 'wb'), protocol=pickle.HIGHEST_PROTOCOL)


def load_doc(doc, pickle_path):
    """Load a document from a given location.

    Args:
        doc (`Doc`): A document providing the vocabulary (`doc.vocab`).
        pickle_path (str): The path to load the document from.
            The user data is loaded from a separate file, from `pickle_path + "_"`.
    
    Returns:
        `Doc`: The loaded document.
    
    """
    doc = Doc(doc.vocab).from_disk(pickle_path, exclude=["tensor", "user_data"])
    user_data = pickle.load(xzip.open(pickle_path + "_", 'rb'))
    include_user_data(doc, user_data)
    return doc


def text_hash(text):
    """Hash a string.
    
    Args:
        text (str): The string to hash.
    
    Returns:
        str: The hash of the string.
    
    """
    return md5(text.encode()).hexdigest()


def get_dir_hash(doc, human_readable=False):
    """Get the pickling directory and file name for a document.

    Args:
        doc (`Doc`): The document.
        human_readable (boolean): If True, human-readable information is added to the directory and file name.
    
    Returns:
        str: The directory name, i.e. a hash (+ human-readable information).
        str: The subdirectory name, i.e. human-readable slice information.
        str: The file name, i.e. a hash (+ human-readable information).

    """
    # hash of the document text
    dir_name = text_hash(doc._.pickle_id["text"])
    
    if human_readable:
        # first 20 characters of document text (non-alphanumeric characters are replaced by "_")
        dir_name += "_" + "".join([x if (x.isalnum() and ord(x) < 128) else "_" for x in doc._.pickle_id["text"][:20]])
    
    # number of units and name of slicer
    subdir_name = str(doc._.pickle_id["max_units"]) + "_" + doc._.pickle_id["slicer"]

    # hash of the model and the computed pipeline components
    id_hash = text_hash(doc._.pickle_id["model"] + "".join(doc._.pickle_id["func"]))
    
    if human_readable:
        # name of the last computed pipeline component
        id_hash += "_" + doc._.pickle_id["func"][-1].split("\n")[0][4:].split("(")[0]
    
    return dir_name, subdir_name, id_hash
    

def pickle_wrapper(doc, component_func, load_output, save_output, overwrite, pickle_path, **kwargs):
    """Wrapper for a spacy pipeline component.
        Calculates or loads the results of the component applied to the document.
        Updates the pickle ID with the current source code of the component.

    Args:
        doc (`Doc`): A spacy document object.
        component_func (func): A spacy pipeline component.
        load_output (boolean): If True, load the output if it exists; if False, calculate the output.
        save_output (boolean): If True, save the output.
        overwrite (boolean): If True, overwrite existing files when saving.
        pickle_path (str): Path to pickle the files.
        **kwargs: Keyword arguments to pass to the pipeline component.
            Warning: This has no effect on pickling! Use this only if you know what you do.
    
    Returns:
        `Doc`: A spacy document object.

    """
    doc._.pickle_id["func"].append(inspect.getsource(component_func))
    dir_name, subdir_name, id_hash = get_dir_hash(doc, human_readable=True)
    pickle_dir = os.path.join(pickle_path, dir_name, subdir_name)
    pickle_path = os.path.join(pickle_dir, id_hash)
    if load_output and os.path.exists(pickle_path):
        doc = load_doc(doc, pickle_path)
        loaded = True
    else:
        doc = component_func(doc, **kwargs)
        loaded = False
    if save_output and (overwrite or not loaded):
        if not os.path.exists(pickle_dir):
            os.makedirs(pickle_dir)
        save_doc(doc, pickle_path)
    return doc
