import numpy as np
import torch
from sklearn.preprocessing import MultiLabelBinarizer
from spacy.tokens import Span
from transformers import BertModel, BertTokenizer

from ..global_constants import TORCH_DEVICE
from ..global_resources import ATTRIBUTION_MODEL
from ..utils_classes import Debugger
from ..utils_methods import add_extension


# BERT tokenizer and model
TOKENIZER = BertTokenizer.from_pretrained("dbmdz/bert-base-german-cased")
MODEL = BertModel.from_pretrained("dbmdz/bert-base-german-cased", output_hidden_states=True)

# attribution categories
LABEL_NAMES = ["Figur", "Erzählinstanz", "Verdacht Autor"]
LABEL_ENCODER = MultiLabelBinarizer()
LABEL_ENCODER.fit([LABEL_NAMES])


# enable/disable debug logs
debugger = Debugger(False)


def neural_attribution_tagger(doc):
    """Spacy pipeline component.
        Add speaker attribution to each clause.
        Uses the model from Dönicke et al. (2022):
        "Modelling Speaker Attribution in Narrative Texts With Biased and Bias-Adjustable Neural Networks".
        (https://gitlab.gwdg.de/mona/neural-attribution)

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    global ATTRIBUTION_MODEL
    ATTRIBUTION_MODEL.provide()

    add_extension(Span, "attribution")

    # create embeddings for each clause
    clause_embeddings = []
    sents = [None] + list(doc.sents) + [None]
    for i in range(1, len(sents)-1):
        context_tokens = []
        try:
            context_tokens.extend(list(sents[i-1]))
        except TypeError:
            pass # first sentence
        context_tokens.extend(list(sents[i]))
        try:
            context_tokens.extend(list(sents[i+1]))
        except TypeError:
            pass # last sentence
        context_tokens = [token for token in context_tokens if not token.is_space]
        tokens = [token.text for token in context_tokens]
        string = " ".join(tokens)
        embeddings, subtokens = create_embeddings(string)
        subtoken_to_token_mappings = map_subtokens_to_tokens(subtokens, tokens)
        subtoken_clauses = [context_tokens[i]._.clause for i in subtoken_to_token_mappings]
        for clause in sents[i]._.clauses:
            clause_embedding = pad_sample([[float(subtoken_clauses[i] == clause)] + embedding for i, embedding in enumerate(embeddings)], 123)
            clause_embeddings.append(clause_embedding)
    
    # predict labels for each clause
    X = prepare(clause_embeddings)
    Y = ATTRIBUTION_MODEL._.predict(X)
    labels = [[round(x) for x in outputs] for outputs in Y]
    labels = LABEL_ENCODER.inverse_transform(np.asarray(labels))
    
    # assign labels to each clause
    for i in range(1, len(sents)-1):
        for clause in sents[i]._.clauses:
            clause._.attribution = set(labels.pop(0))
    
    return doc


# The following methods are copied from:
# https://gitlab.gwdg.de/mona/neural-attribution/-/blob/master/vectorizer.py


def cuda(obj):
    """Add an object to GPU if available, else to CPU.

    Args:
        obj (obj): The object.
    
    Returns:
        obj: The object.
    
    """
    device = TORCH_DEVICE
    obj = obj.to(device)
    return obj


def create_embeddings(string):
    """Create word embeddings for a string.

    Args:
        string (str): The string.
    
    Returns:
        list of (list of float): List of word embeddings, one for each word in the string.
        list of str: List of words in the string (as produced by the tokenizer).
    
    """
    model = cuda(MODEL)
    model = model.eval()
    string_ids = TOKENIZER.encode(string, add_special_tokens=False)
    tokens = TOKENIZER.convert_ids_to_tokens(string_ids)
    string_ids = torch.LongTensor(string_ids)
    string_ids = cuda(string_ids)
    string_ids = string_ids.unsqueeze(0)
    with torch.no_grad():
        out = model(input_ids=string_ids)
    hidden_states = out[2]
    embeddings = hidden_states[-1][0]
    embeddings = [[float(x) for x in embedding] for embedding in embeddings]
    return embeddings, tokens


def map_subtokens_to_tokens(subtokens, tokens):
    """Map subtokens (i.e. tokens produced by the BERT tokenizer) to tokens (as in the data).

    Args:
        subtokens (list of str): List of subtokens.
            Example: ['(', '›abheben‹', 'ist', 'übrigens', 'auch', 'trivial', ';', 'entschuldigen', 'Sie', ',', 'Rex', ')', '.']
        tokens (list of str): List of tokens.
            Example: ['(', '›', 'ab', '##heben', '[UNK]', 'ist', 'übrigens', 'auch', 'tri', '##via', '##l', ';', 'entschuldigen', 'Sie', ',', 'Re', '##x', ')', '.']
    
    Returns:
        list of int: For each subtoken the index of the corresponding token.
            Example: [0, 1, 1, 1, 1, 2, 3, 4, 5, 5, 5, 6, 7, 8, 9, 10, 10, 11, 12]
    
    """
    subtoken_to_token_mappings = []
    current_token_index = 0
    current_token_pos = 0
    for subtoken in subtokens:
        current_token = tokens[current_token_index]
        subtoken = subtoken.strip("#")
        if current_token[current_token_pos:].startswith(subtoken):
            subtoken_to_token_mappings.append(current_token_index)
            current_token_pos += len(subtoken)
        elif subtoken == "[UNK]": # "‹"
            subtoken_to_token_mappings.append(current_token_index)
            current_token_pos += 1
        else:
            raise ValueError("Subtokens cannot be mapped to tokens!")
        if current_token_pos == len(current_token):
            current_token_index += 1
            current_token_pos = 0
    return subtoken_to_token_mappings


def pad_sample(sample, max_len):
    """Pad an embedding to a given length. The embedding is centered.
        If it is too long, an equal number of word vectors is removed from the left and from the right.
        If it is too short, an equal number of zero vectors is inserted at the left and at the right.

    Args:
        sample (list of (list of float)): The embedding as a list of word vectors.
        max_len (int): The wanted size of the embedding.
    
    Returns:
        list of (list of float): The padded version of the embedding.
    
    """
    zero_vector = [0.0] * len(sample[0])
    diff = max_len-len(sample)
    if diff == 0:
        return sample
    d = int(abs(diff)/2.0)
    if diff > 0:
        return ([zero_vector] * (diff%2)) + ([zero_vector] * d) + sample + ([zero_vector] * d)
    if diff < 0:
        debugger.print(len(sample), len(sample[d+(diff%2):(-d if d > 0 else len(sample))]))
        return sample[d+(diff%2):(-d if d > 0 else len(sample))]


def prepare(X):
    """Transform an array-like object to Numpy format.

    Args:
        X (list of obj): Array-like object.
    
    Returns:
        np.array: Numpy array.
    
    """
    return np.asarray(X, dtype=np.float32)
