from spacy.tokens import Span

from ..global_resources import DISRPT_MODEL
from ..utils_methods import add_extension


def disrpt_discourse_segmenter(doc):
    """Spacy pipeline component.
        Segments a document into minimal discourse units.
        Uses the model from Dönicke (2021):
        "Delexicalised Multilingual Discourse Segmentation for DISRPT 2021 and Tense, Mood, Voice and Modality Tagging for 11 Languages".
        (https://gitlab.gwdg.de/tillmann.doenicke/disrpt2021-tmvm)

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Span, "seg_start")
    
    global DISRPT_MODEL
    DISRPT_MODEL.provide()
    
    clf, dv, fs = DISRPT_MODEL._[0], DISRPT_MODEL._[1], DISRPT_MODEL._[2]

    for i, clause in enumerate(doc._.clauses):
        feats = get_feats_with_context(doc._.clauses, i)
        clause._.seg_start = clf.predict(fs.transform(dv.transform([feats])))[0]
    
    return doc


def get_feats_with_context(clauses, index):
    """Return the features of the current clause and its neighbouring clauses.

    Args:
        clauses (list of `Span`): List of clauses in the document.
        index (int): Index of the current clause.
    
    Returns:
        dict of str:bool: Features of the clause (the value is `True` for every feature).
    
    """
    feats = set()
    for i in [-1, 0, 1]:
        try:
            for feat in clauses[index+i]._.feats:
                feats.add(str(i) + "_" + feat)
        except IndexError:
            pass
    return dict.fromkeys(format_feat_names(feats), True)


def format_feat_names(feats):
    """Convert MONAPipe features to DISRPT TMVM features.

    Args:
        feats (set of str): Set of features.
    
    Returns:
        set of str: Set of features.
    
    """
    disrpt_feats = set()
    
    # format sentence features
    if "0_clause_sent_start=yes" not in feats:
        disrpt_feats.add("-1_same_sent")
    if "1_clause_sent_start=yes" not in feats:
        disrpt_feats.add("1_same_sent")    
    
    # format dics for definiteness, modality and punctuation
    lemmas = {u"ein" : "Ind", u"der" : "Def"}
    modals = {u"müssen" : "OBL", u"sollen" : "OBL", u"dürfen" : "POS", u"können" : "POS", u"mögen" : "POS", u"wollen" : "VOL"}
    puncts = {u'„' : '"', u'“' : '"', u'”' : '"', u'»' : '"', u'«' : '"', u'‚' : "'", u'‘' : "'", u'’' : "'", u'›' : "'", u'‹' : "'", u'-' : "-", u'–' : "--", u'—' : "---"}
    
    for feat in feats:
        if "_sent_" in feat:
            continue
        
        name_val = feat.split("=")
        name = name_val[0].split("_")
        val = name_val[1].split(":")[0]

        # format dependency-relation and part-of-speech features
        if name[-1] == "dep":
            if name[-2] == "clause":
                feat = feat.replace("_dep=", "_")
            else:
                feat = feat.split("_dep=")[0]
        elif name[-1] == "pos":
            feat = feat.replace("_pos=", "_")
        
        # format connections between neighbouring clauses
        elif name[-1] == "head":
            feat = feat.replace("-1_clause_head=+1", "-1_subordinate")
            feat = feat.replace("0_clause_head=-1", "-1_superordinate")
            feat = feat.replace("1_clause_head=-1", "1_subordinate")
            feat = feat.replace("0_clause_head=+1", "1_superordinate")
            if not feat.endswith("ordinate"):
                continue
        
        # format punctuation
        elif name[-2] == "punct":
            feat = feat.replace("_punct_", "_")
            for v in puncts:
                feat = feat.replace(v, puncts[v])
        
        else:

            # add pronoun type for personal pronouns
            if name[-1] == "person":
                if "_".join(name[:-1]) + "_pos=PRON" in feats:
                    disrpt_feats.add("_".join(name[:-1]) + "_PronType=Prs")
            
            # move article definiteness to NP level and add pronoun type
            elif name[-2] == "art" and name[-1] == "lemma":
                name = name[:-2] + name[-1:]
                try:
                    val = lemmas[val]
                    disrpt_feats.add("_".join(name[:-1]) + "_PronType=Art")
                except KeyError:
                    disrpt_feats.add("_".join(name[:-1]) + "_PronType=Dem")
                    continue
            
            # move adjective degree to NP level
            elif name[-2] == "adj" and name[-1] == "degree":
                name = name[:-2] + name[-1:]
            
            # format modal verbs
            elif name[-1] == "modal":
                try:
                    val = modals[val]
                except KeyError:
                    continue
            
            # capitalise feature name and value
            name[-1] = name[-1][0].upper() + name[-1][1:]
            val = val[0].upper() + val[1:]
            name = "_".join(name)
            feat = name + "=" + val

            # rename features names and values
            feat = feat.replace("Form", "VerbForm")
            feat = feat.replace("Lemma", "Definite")
            feat = feat.replace("Modal", "Modality")
            feat = feat.replace("Mode", "Mood")
            feat = feat.replace("Numerus", "Number")
            feat = feat.replace("1per", "1")
            feat = feat.replace("2per", "2")
            feat = feat.replace("3per", "3")
            feat = feat.replace("Active", "Act")
            feat = feat.replace("Comp", "Cmp")
            feat = feat.replace("Imperf", "Imp")
            feat = feat.replace("Plu", "Plur")
            feat = feat.replace("Subj", "Sub")
        feat = feat.replace("ROOT", "root")

        # exclude features of adjectives, articles and quantifiers
        exclude = False
        for x in ["_adj_", "_art_", "_quant_"]:
            if x in feat:
                exclude = True
                break
        if exclude:
            continue

        disrpt_feats.add(feat)
    
    return disrpt_feats