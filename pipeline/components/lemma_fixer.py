def lemma_fixer(doc):
    """Spacy pipeline component.
        Fix wrong lemmas from the built-in spacy lemmatizer.
        Spacy basically lemmatises using this list:
            https://raw.githubusercontent.com/explosion/spacy-lookups-data/master/spacy_lookups_data/data/de_lemma_lookup.json

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    # List of words to be re-lemmatised
    # (the idea is to add frequent words here to keep the list short;
    # not every uncommon word spacy has trouble with):
    exceptions = {
        # lowercased word : { POS tag : new lemma }
        # dies
        "dies" : dict.fromkeys(["DET"], "dies"),
        "diese" : dict.fromkeys(["DET"], "dies"),
        "diesem" : dict.fromkeys(["DET"], "dies"),
        "diesen" : dict.fromkeys(["DET"], "dies"),
        "dieser" : dict.fromkeys(["DET"], "dies"),
        "dieses" : dict.fromkeys(["DET"], "dies"),
        # ein
        "ein" : dict.fromkeys(["DET"], "ein"),
        "eine" : dict.fromkeys(["DET"], "ein"),
        "einem" : dict.fromkeys(["DET"], "ein"),
        "einen" : dict.fromkeys(["DET"], "ein"),
        "einer" : dict.fromkeys(["DET"], "ein"),
        "eines" : dict.fromkeys(["DET"], "ein"),
        # müssen
        "muss" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musst" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musste" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mussten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musstest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "musstet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsse" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssen" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsset" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsst" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsste" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müssten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsstest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müsstet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "muß" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußt" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußte" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müße" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßen" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßt" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßte" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßten" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "müßtet" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        # mein
        "dein" : dict.fromkeys(["DET"], "mein"),
        "deine" : dict.fromkeys(["DET"], "mein"),
        "deinem" : dict.fromkeys(["DET"], "mein"),
        "deinen" : dict.fromkeys(["DET"], "mein"),
        "deiner" : dict.fromkeys(["DET"], "mein"),
        "deines" : dict.fromkeys(["DET"], "mein"),
        "ihr" : dict.fromkeys(["DET"], "mein"),
        "ihre" : dict.fromkeys(["DET"], "mein"),
        "ihrem" : dict.fromkeys(["DET"], "mein"),
        "ihren" : dict.fromkeys(["DET"], "mein"),
        "ihrer" : dict.fromkeys(["DET"], "mein"),
        "ihres" : dict.fromkeys(["DET"], "mein"),
        "mein" : dict.fromkeys(["DET"], "mein"),
        "meine" : dict.fromkeys(["DET"], "mein"),
        "meinem" : dict.fromkeys(["DET"], "mein"),
        "meinen" : dict.fromkeys(["DET"], "mein"),
        "meiner" : dict.fromkeys(["DET"], "mein"),
        "meines" : dict.fromkeys(["DET"], "mein"),
        "sein" : dict.fromkeys(["DET"], "mein"),
        "seine" : dict.fromkeys(["DET"], "mein"),
        "seinem" : dict.fromkeys(["DET"], "mein"),
        "seinen" : dict.fromkeys(["DET"], "mein"),
        "seiner" : dict.fromkeys(["DET"], "mein"),
        "seines" : dict.fromkeys(["DET"], "mein"),
        # sein
        "sein" : dict.fromkeys(["AUX", "VERB"], "sein"),
        # sollen
        "soll" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solle" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollen" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollst" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollt" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollte" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "sollten" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solltest" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "solltet" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        # wollen
        "will" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "willst" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolle" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollen" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollt" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollte" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wollten" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolltest" : dict.fromkeys(["AUX", "VERB"], "wollen"),
        "wolltet" : dict.fromkeys(["AUX", "VERB"], "wollen")
    }
    for token in doc:
        if token.lower_ in exceptions and token.pos_ in exceptions[token.lower_]:
            token.lemma_ = exceptions[token.lower_][token.pos_]
    return doc