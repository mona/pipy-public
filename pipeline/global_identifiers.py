# wikidata items that correspond with relations to fictional items
FICTIONAL_ITEMS = {
    "literary character": "Q3658341",
    "fictional human": "Q15632617",
    "fictional entity": "Q14897293",
    #"operatic character": "Q50386450",
    "fictional young girl": "Q21192474"
}

# wikidata items that correspond with relations to real items
REAL_ITEMS = {
    "Greek primordial deity": "Q878099",
    "Greek deity": "Q22989102",
    "God": "Q190",
    "deity": "Q178885",
    "theonym": "Q12160552",
    "God in Christianity": "Q825",
    "God in Abrahamic religions": "Q5576009",
    "God in Judaism": "Q2155501",
    "God in Islam": "Q2095353"
}

REAL_ITEMS_INVERTED = {v: k for k, v in REAL_ITEMS.items()}

# wikidata items that correspond with relations to real items
REAL_CONCEPTS = {
    "belief": "Q34394",
    "biblical concept": "Q30149195",
    "religious concept": "Q23847174"
}