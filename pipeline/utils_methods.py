"""This file contains useful methods for developing the SpaCy pipeline.
"""

import nltk
import spacy
from itertools import chain, combinations
from nltk.corpus import wordnet as wn
from spacy.tokens import Doc, Span, Token

from .global_resources import *
from .global_wordlists import *
from .passage import Passage
from settings import NLTK_PATH
nltk.data.path.append(NLTK_PATH)


def add_extension(obj, name):
    """Add a custom extension to a spacy class.

    Args:
        obj (obj): The spacy class (`Doc`, `Span` or `Token`).
        name (str): The name of the attribute.
    
    """
    try:
        obj.set_extension(name, default=None)
    except ValueError:
        pass


def agreement(set1, set2, strict=False):
    """Checks the agreement between two sets of values.

    Args:
        set1 (set of str): A set of values.
        set2 (set of str): A set of values.
        strict (boolean): If False, the agreement is true if one of the sets is empty.
    
    Returns:
        boolean: True iff either strict=False and one of the sets is empty, or the intersection of both sets is not empty.
            In the strict=False case, "noGender" is removed from the sets.
    
    """
    if strict:
        return len(set1.intersection(set2)) > 0
    set1 = set1.difference({'noGender'})
    set2 = set2.difference({'noGender'})
    return len(set1) == 0 or len(set2) == 0 or len(set1.intersection(set2)) > 0


def check_modifier(token, lexicon):
    """Checks if the token is a discourse marker

    Args:
        token (`Token`): the token.
        lexicon(list of list of list): for each discourse marker a list of three lists: 1. words contained in this discourse marker, 2. list of its possible  part of speech tags, 3. list of possible discouse marker types
    
    Returns:
        boolean: True iff the token is a discourse marker
    
    """     
    for vocabulary in lexicon:
        sences = vocabulary[2]
        if len(vocabulary[0]) == 1 and token.text == vocabulary[0][0] and token.pos_ in vocabulary[1]:
            for sence in sences:
                if check_type(sence, case="beginning"):
                    return True
        elif len(vocabulary[0]) == 2 and token.text == vocabulary[0][0] and token.nbor().text == vocabulary[0][1]:
            for sence in sences:
                if check_type(sence, case="beginning"):
                    return True
        elif len(vocabulary[0]) == 3 and "..." not in vocabulary[0] and token.text == vocabulary[0][0] and token.nbor().text == vocabulary[0][1] and token.nbor().nbor().text == vocabulary[0][2]:
            for sence in sences:
                if check_type(sence, case="beginning"):
                    return True              
        elif len(vocabulary[0]) == 4 and "..." not in vocabulary[0] and token.text == vocabulary[0][0] and token.nbor().text == vocabulary[0][1] and token.nbor().nbor().text == vocabulary[0][2] and token.nbor().nbor().nbor().text == vocabulary[0][3]:
            for sence in sences:
                if check_type(sence, case="beginning"):
                    return True
        elif len(vocabulary[0]) == 3 and "..." in vocabulary[0] and token.text == vocabulary[0][0] and token.nbor().text != vocabulary[0][2]:
            first = False
            for t in token._.clause:
                if t.text == vocabulary[0][0]:
                    first = True
                if t.text == vocabulary[0][2] and first:
                    for sence in sences:
                        if check_type(sence, case="beginning"):
                            return True
        elif len(vocabulary[0]) == 4 and "..." in vocabulary[0] and token.text == vocabulary[0][0]:
            for t in token.sent:
                if t.text == vocabulary[0][2] and t.nbor().text == vocabulary[0][3]:
                    for sence in sences:
                        if check_type(sence, case="beginning"):
                            return True
    return False


def check_type(m_type, case="beginning"):
    """Checks if the possible types that the token and the discourse marker at the same time is among the selected types.

    Args:
        m_type (str): discourse marker type.
        case (str): type of match
            "beginning": match by the beginning and the broad category at the same time
            "identity": exact category match with subcategories
    
    Returns:
        boolean: True iff there is a discourse marker match
    
    """  
    types = [
        "Temporal.Synchronous", 
        #"Temporal.Asynchronous.Precedence", 
        #"Temporal.Asynchronous.Succession", 
        "Contingency.Cause.Reason", 
        "Contingency.Cause.Result", 
        "Contingency.Cause.NegResult", 
        "Contingency.Purpose.Arg1-as-goal", 
        "Contingency.Purpose.Arg2-as-goal", 
        "Contingency.Condition.Arg1-as-cond", 
        "Contingency.Condition.Arg2-as-cond", 
        "Contingency.Negative-condition.Arg1-as-negCond", 
        "Contingency.Negative-condition.Arg2-as-negCond", 
        "Comparison.Contrast", 
        "Comparison.Similarity", 
        "Comparison.Concession.Arg1-as-denier", 
        "Comparison.Concession.Arg2-as-denier", 
        #"Expansion.Conjunction", 
        #"Expansion.Disjunction", 
        #"Expansion.Level-of-detail.Arg1-as-detail", 
        #"Expansion.Level-of-detail.Arg2-as-detail", 
        #"Expansion.Equivalence", 
        #"Expansion.Instantiation.Arg1-as-instance", 
        #"Expansion.Instantiation.Arg2-as-instance", 
        "Expansion.Exception.Arg1-as-except", 
        "Expansion.Exception.Arg2-as-except", 
        "Expansion.Substitution.Arg1-as-subst", 
        "Expansion.Substitution.Arg2-as-subst", 
        "Expansion.Manner.Arg1-as-manner", 
        "Expansion.Manner.Arg2-as-manner"
    ]
    
    type_starts = ["Contingency", "Comparison"] #"Temporal", "Expansion"
    if case == "identity":
        for t in types:
            if m_type == t:
                return True
        return False
    elif case =="beginning":    
        for t in type_starts:
            if m_type.startswith(t):
                return True
        return False


def evaluative_terms(token):
    """Checks if the token is evaluative
    
    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token is evaluative
    
    """

    if ((token.text == "zu" and token.nbor().pos_ in ["ADJ"] and token.nbor().nbor().pos_ not in ["NOUN", "PROPN"]) or token.text in EVALUATIVE_TERMS):
        return True
    return False

def get_attr(span, attr):
    """Get the values of a certain attribute for all tokens of a span.

    Args:
        span (`Span`): A span.
        attr (str): The token attribute.
    
    Returns:
        (*obj): The attribute values for every token as tuple.
    
    """
    return tuple([getattr(token, attr) for token in span])


def get_doc_text(doc, normalized=False):
    """Return the full text of a document.
        (Takes into account the changes of normalizer and slicer.)

    Args:
        doc (`Doc`): The document.
        normalized (boolean): If True, return the normalised text; if False, return the original text.
    
    Returns:
        str: The text of the document.
    
    """
    if (not normalized) and hasattr(doc._, "text"):
        return doc._.text
    if hasattr(doc._, "fulltext"):
        return doc._.fulltext
    return doc.text


def get_ent_subjects(ents, sent):
    """Get all subject entities in a sentence.

    Args:
        ents (list of `Span`): All entities (in a document etc.).
        sent (`Span`): The sentence to extract subject entities from.
    
    Returns:
        list of `Span`: All entities in the sentence which are subjects.
    
    """
    subjs = []
    for ent in ents:
        if ent.root.sent == sent and ent.root.dep_.split(":")[0] in ["nsubj", "sb"]:
            subjs.append(ent)
    return subjs


def get_ents(doc, remove_doublings=False):
    """Get all named entities and noun chunks in a document, possibly overlapping.

    Args:
        doc (`Doc`): A document.
    
    Returns:
        list of `Span`: List of entities, sorted by start index and length.
    
    """
    global SPACY_MODEL_TOKENIZED
    SPACY_MODEL_TOKENIZED.provide()
    
    nchunks = list(SPACY_MODEL_TOKENIZED._([token.text for token in doc]).noun_chunks)
    nchunks = [doc[nchunk.start:nchunk.end] for nchunk in nchunks]
    ents = list(doc.ents) + nchunks
    ents = sorted(ents, key=lambda ent: ent.start-1.0/(ent.end-ent.start))
    if remove_doublings:
        ents = [ent for i, ent in enumerate(ents) if not (i > 0 and (ent.start, ent.end) == (ents[i-1].start, ents[i-1].end))]
    return ents


def get_head_nouns(ent, form="text"):
    """Get the nominal head and the modifiers (adjectives and numerals) of an entity.
        Examples:
            "den Spider-Man hassenden Zeitungsboss J.    Jonah Jameson"
             DET PROPN      ADJ       NOUN         PROPN PROPN PROPN
            -> [hassenden], [zeitungsboss], [j., jonah, jameson]
            
            "Peters betagter Onkel Ben"
             PROPN  ADJ      NOUN  PROPN
            -> [betagter], [onkel], [ben]
            
    Args:
        ent (`Span`): An entity.
        form (str): The token attribute to use as string representation (e.g. "text" or "lemma_").

    Returns:
        list of str: A list of (lowercased) modifiers.
        list of str: A list of (lowercased) common nouns.
        list of str: A list of (lowercased) proper nouns.
    
    """
    pos = get_attr(ent, "pos_")
    propns = []
    nouns = []
    k = len(pos)-1
    if "PROPN" in pos or "NOUN" in pos:
        while pos[k] not in ["PROPN", "NOUN"]:
            k -= 1
        if pos[k] == "PROPN":
            while k >= 0 and pos[k] == "PROPN":
                propns.append(k)
                k -= 1
        if "NOUN" in pos[:k+1]:
            while k >= 0 and pos[k] != "NOUN":
                k -= 1
            while k >= 0 and pos[k] == "NOUN":
                nouns.append(k)
                k -= 1
        nouns = [w.lower() for k, w in enumerate(get_attr(ent, form)) if k in nouns]
        propns = [w.lower() for k, w in enumerate(get_attr(ent, form)) if k in propns]
    adjs = []
    while k >= 0 and pos[k] not in ["NUM", "ADJ", "NOUN", "PROPN"]:
        k -= 1
    while k >= 0 and pos[k] in ["ADJ", "NUM"]:
        adjs.append(k)
        k -= 1
    adjs = [w.lower() for k, w in enumerate(get_attr(ent, form)) if k in adjs]
    return adjs, nouns, propns


def get_highest_verb(clause):
    """Return the syntactically highest verb in a clause (or any other span).

    Args:
        clause (`Span`): A clause.
    
    Returns:
        `Token`: A token.
    
    """
    roots = [clause.root]
    root = None
    while len(roots) > 0:
        root = roots.pop(0)
        if root.pos_ in ["VERB", "AUX"]:
            break
        roots.extend(list(root.children))
        root = None
    return root


def get_morph_attr(span, attr):
    """Get the value of morphological feature for a span.
    
    Args:
        span (`Span`): A span.
        attr (str): The `Morph` attribute (without final underscore).
    
    Returns:
        set of str: Probable values.
    
    """
    # If the span's head has a single value for that feature, return it:
    val = handle_demorphy(getattr(span.root._.morph, attr + "_"), attr)
    if len(val) == 1:
        return val
    
    morphs = [token._.morph for token in span]
    values = [handle_demorphy(getattr(m, attr + "_"), attr) for m in morphs]
    
    # Otherwise, if the intersection of the values of all tokens for that feature is not empty, return it:
    inters = set.intersection(*values)
    if len(inters) > 0:
        return inters
    
    # Otherwise, return the most frequent value(s) for that feature:
    return most_frequent([v for val in values for v in val])


def get_nearby_coreferences(token, before, after):
    """Select the closest coreferencing mentions of a token.

    Args:
        token (`Token`): The token.
        before (int): Number of mentions before the token's mention to return.
        after (int): Number of mentions after the token's mention to return.
    
    Returns:
        list of `Token`: The root tokens of the mentions nearby the mention which contains the token.
            Returns a list with the token itself if the token has no coreferences.

    """
    if hasattr(token._, "in_coref") and token._.in_coref:
        roots = []
        for cluster in token._.coref_clusters:
            for mention in get_nearby_mentions(token, cluster.mentions, before, after):
                roots.append(mention.root)
        return roots
    return [token]


def get_nearby_mentions(token, mentions, before, after):
    """Select the closest coreferencing mentions of a token.

    Args:
        token (`Token`): The token.
        mentions (list of `Span`): All mentions of a cluster.
            The token is supposed to appear in one mention of the cluster.
        before (int): Number of mentions before the token's mention to return.
        after (int): Number of mentions after the token's mention to return.
    
    Returns:
        list of `Span`: The mentions nearby the mention which contains the token.
    
    """
    for i, mention in enumerate(mentions):
        if token.i in range(mention.start, mention.end+1):
            return mentions[i-before:i+1+after]
    return []


def get_nmod_is(token):
    """Return the modifiers of a noun.
        Modifiers are determiners, adjectives and adpositions.
        Works for UD and TIGER dependencies.
    
    Args:
        token (`Token`): The noun.
    
    Returns:
        list of int: The indices (`.i`) of the modifiers.
    
    """
    nmods = []
    for child in token.children:
        dep = child.dep_.split(":")[0]
        if dep in ["det", "amod", "nk"] and child.pos_ in ["DET", "ADJ"]:
            nmods.append(child.i)
        elif dep == "case" and child.pos_ == "ADP": # in UD, the adpositions are dependents of nouns
            nmods.append(child.i)
    if token.head.pos_ == "ADP" and token.dep_ == "nk": # in TIGER, the adpositions are heads of nouns
        nmods.append(token.head.i)
    return nmods


def get_NPs_and_VPs_by_category(clause):
    """Returns all NPs in a clause, grouped by their syntactic function, and all other words, grouped by the type of VP they appear in.
        Only works for UD relations.
    
    Args:
        clause (`Span`): A clause.
    
    Returns:
        list of `Span`: All nominal subjects (nsubj).
        list of `Span`: All direct objects (obj).
        list of `Span`: All indirect objects (iobj).
        list of `Span`: All prepositional objects (obl).
        list of `Span`: All nominal modifiers (nmod).
        list of `Span`: All other NPs.
        list of `Token`: All tokens which are not part of an NP but part of a VERB subtree.
        list of `Token`: All tokens which are not part of an NP but part of an AUX subtree.
        list of `Token`: All other tokens.

    """
    nmods = []
    for token in clause._.tokens:
        dep = token.dep_.split(":")[0]
        if dep == "nmod":
            np = [tok for tok in token.subtree if tok in clause._.tokens]
            if len(np) > 0:
                np = tokens_to_span(np)
                nmods.append(np)
    nmod_tokens = [token for np in nmods for token in np]
    np_rels = ["nsubj", "obj", "iobj", "obl"]
    nps = {np_rel : [] for np_rel in np_rels}
    nps[""] = []
    for token in clause._.tokens:
        if is_NP(token):
            np = [tok for tok in token.subtree if tok in clause._.tokens and tok not in nmod_tokens]
            if len(np) > 0:
                np = tokens_to_span(np)
                dep = token.dep_.split(":")[0]
                if dep in np_rels:
                    nps[dep].append(np)
                else:
                    nps[""].append(np)
    nps["nmod"] = nmods
    np_tokens = [token for np in sum([nps[dep] for dep in nps], []) for token in np]
    others = [token for token in clause._.tokens if token not in np_tokens]
    vps = {"AUX" : [], "VERB" : [], "" : []}
    for token in others:
        current = token
        while current.head != current and not is_VP(current):
            current = current.head
        if current.pos_ in vps:
            vps[current.pos_].append(token)
        else:
            vps[""].append(token)
    return nps["nsubj"], nps["obj"], nps["iobj"], nps["obl"], nps["nmod"], nps[""], vps["VERB"], vps["AUX"], vps[""]


def get_restrictor(token):
    """Returns the head of the governing NP/VP for a quantifier.

    Args:
        token (`Token`): The quantifier.
    
    Returns:
        `Token`: The restrictor of the quantifier.
    
    """
    while token.i != token.head.i and not (is_NP(token) or is_VP(token)):
        token = token.head
    return token


def get_sub_clauses(doc):
    """Extract all sub-clauses from a document.
        A sub-clause is a maximal sequence of subsequent tokens.
    
    Args:
        doc (`Doc`): The document.
    
    Returns:
        list of (list of `Token`): List of all sub-clauses of the document.
        set of int: Set of sub-clause start indices.
        set of int: Set of sub-clause end indices.
    
    """
    all_sub_clauses = []
    sub_clause_starts = set()
    sub_clause_ends = set()
    try: # if doc is clausized
        for clause in doc._.clauses:
            sub_clauses = get_sub_spans(clause._.tokens)
            for sub_clause in sub_clauses:
                sub_clause_starts.add(sub_clause[0].i)
                sub_clause_ends.add(sub_clause[-1].i)
            all_sub_clauses.extend(sub_clauses)
    except AttributeError:
        pass
    return all_sub_clauses, sub_clause_starts, sub_clause_ends


def get_sub_spans(tokens):
    """Extract all sub-spans from a list of tokens.
        A sub-span is a maximal sequence of subsequent tokens.
    
    Args:
        list of `Token`: List of tokens.
    
    Returns:
        list of (list of `Token`): All sub-spans within the given tokens.
    
    """
    sub_spans = []
    sub_span = []
    for token in tokens:
        if len(sub_span) == 0 or token.i == sub_span[-1].i+1:
            sub_span.append(token)
        else:
            sub_spans.append(sub_span)
            sub_span = [token]
    sub_spans.append(sub_span)
    return sub_spans


def get_subj_i(token):
    """Return the nominal subject of a token.
        Works for UD and TIGER dependencies.
    
    Args:
        sent (`Span`): The sentence.
    
    Returns:
        int: The index (`.i`) of the nominal subject.
            Returns -1 if there is no nominal subject.
    
    """
    while True:
        for child in token.children:
            if child.dep_.split(":")[0] in ["nsubj", "sb"]:
                return child.i
        if token.i == token.head.i: # ROOT
            break
        token = token.head
    return -1


def get_synonyms(lemma):
    """Get all synonyms of a lemma from the German WordNet.

    Args:
        lemma (str): A lemma.
    
    Returns:
        set of str: Set of synonyms.
    
    """
    return set([lemma for synset in wn.synsets(lemma, lang="deu") for lemma in synset.lemma_names("deu")])


def handle_demorphy(feats, attr):
    """Convert a set of morphological feature values.
        - Adds "3per" if there is no person.
        - Removes "noGender".

    Args:
        feats (set of str): Set of feature values.
        attr (str): Feature name.
    
    Returns:
        set of str: New set of feature values.
            Also changes the set in-place.
    
    """
    if attr == "person" and len(feats) == 0:
        feats.add("3per")
    elif attr == "gender":
        feats.discard("noGender")
    return feats


def has_argument(token):
    """Checks if token is comment marker based on the wordlists

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token is a comment marker.
    """
    if token.lemma_ in COMMENT_VERBS:
        return True
    if token.lemma_ in COMMENT_NOUNS:
        for word in token.subtree:
            if word.pos_ == "AUX":
                return True
    if token.lemma_ in COMMENT_MARKERS:
        #TODO Disambiguation
        return True
    return False


def has_deictic_coreference(token, before, after):
    """Checks whether a nearby coreference of the token contains a deictic expression.

    Args:
        token (`Token`): The token.
        before (int): Number of coreferences preceding the token to check.
        after (int): Number of coreferences succeeding the token to check.
    
    Returns:
        boolean: True iff a coreference of the token contains a deictic expression.

    """
    corefs = get_nearby_coreferences(token, before, after)
    for coref in corefs:
        if is_deictic(coref):
            return True
    return False


def has_emotion_or_sentiment_score(clause):
    """Checks whether at least one word in the clause has emotion or sentiment

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff at least one word in the clause has emotion or sentiment
    """
    for token in clause:
        if len(token._.emotions) > 0:
            for emotion in token._.emotions:
                if token._.emotions[emotion ]== "1":
                    return True


def has_generic_attr(token):
    """Checks whether the subtree of a token contains an expression of frequency.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the subtree of the token contains an expression of frequency.

    """
    for word in token.subtree:
        if is_generic_attr(word):
            return True
    return False


def has_modifier(clause):
    """Checks if the clause countains at least one token that is a comment modifier

    Args:
        clause (`Span`): The clause.
    
    Returns:
        boolean: True iff the clause countains at least one token that is a modifier
    """
    modifier_token_list = []
    
    for token in clause._.tokens:
        if token.text in COMMENT_MODIFIERS:
            modifier_token_list.append(token)
    if len(modifier_token_list) > 0:
        return True


def has_no_person_coreference(token, before, after):
    """Check if the token has no person corefernce

    Args:
        token (`Token`): The token.
        before (int): Number of coreferencs preceding the token to check if the token has no person corefernce.
        after (int): Number of coreferencs succeeding the token to check if the token has no person corefernce.
    
    Returns:
        boolean: True iff the token has no person corefernce.
    """
    corefs = get_nearby_coreferences(token, before, after)
    for coref in corefs:
        if not is_person(coref):
            return True
    return False


def has_parenthesis(clause):
    """Chechs whether a clause contains two dashes/hypens or two brackets.

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff a clause contains two dashes/hypens or two brackets.
    
    """
    dashes = []
    hypens = []
    brackets = []
    sentence = clause.sent
    for i, token in enumerate(sentence):
        if sentence[i].text == "-":
            dashes.append(sentence[i].text)
        if sentence[i].text == "–":
            hypens.append(sentence[i].text)
        if sentence[i].text in ["(", ")"]:
            brackets.append(sentence[i].text)

    if len(dashes) == 2 or len(brackets) == 2 or len(hypens) == 2:
        return True
    return False


def has_sentiment(clause):
    """Checks whether words in the clause have sentiment above 0.5 in total in the range of [-1:1]

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff words in the have sentiment above 0.5 in total in the range of [-1:1]
    """
    max = 0
    for token in clause:
        try:
            if abs(token._.sentiws) > max:
                max = abs(token._.sentiws)
        except:
            pass
    if max > 0.5:
        return True       
    return False


def has_temporal_change(clause, context_window=0):
    """Checks whether a clause has a change of tense.

    Args:
        clause (`Span`): A clause.
        context_window (int): Number of clauses before and after the current clause that must not express the same tense as in the clause.
    
    Returns:
        boolean: True iff all the adjacent clauses are 1) not in the same tense as the clause, 2) finite, 3) direct speech iff the clause is direct speech.
    
    """
    #taken from gen_tagger with a modification
    #is this function even necessary?
    if context_window > 0:        
        tense = clause._.form.tense
        clauses = clause.doc._.clauses
        index = clauses.index(clause)
        context_clauses = []
        for i in range(context_window):
            if index > i:
                context_clauses.append(clauses[index-1-i])
            if index < len(clauses)-1-i:
                context_clauses.append(clauses[index+1+i])

        for context_clause in context_clauses:
            if context_clause._.form.tense == tense:
                return False
            elif context_clause._.form.verb_form != "fin":
                return False
            elif is_direct_speech(context_clause) != is_direct_speech(clause):
                return False

        return True
    return False


def has_vague_attr(token):
    """Checks whether the subtree of a token contains "in der Regel", "im Allgemeinen" or "im Normalfall".

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the subtree of the token contains one of the expressions.

    """
    for word in token.subtree:
        if is_vague_attr(word):
            return True
    return False


def intersect_clauses(indices1, indices2):
    """Intersect two lists of clause indices.

    Args:
        indices1 (list of int): List of clause indices.
        indices2 (list of int): List of clause indices.
    
    Returns:
        list of int: List of clause indices in the intersection.
        list of int: List of clause indices in the symmetric difference.
    
    """
    indices1 = set(indices1)
    indices2 = set(indices2)
    clauses = indices1.intersection(indices2)
    context = indices1.symmetric_difference(indices2)
    return list(clauses), list(context)


def is_deictic(token):
    """Checks whether the subtree of a token contains a deictic expression.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the subtree of the token contains a deictic expression.

    """
    for word in token.subtree:
        if word.lemma_.lower() in DEICTIC:
            return True
    return False


def is_direct_speech(token):
    """Check whether a span or token is within direct speech.

    Args:
        token (obj): A span or token.
    
    Returns:
        boolean: True iff the span is within direct speech.
            Return False if the document has not been parsed by a speech tagger.
    
    """
    if type(token) != Token:
        token = token.root
    if hasattr(token._, "speech"):
        return ("direct" in token._.speech.keys())
    return False


def is_ellipsis(token):
    """Checks whether a token substitues an elided verb.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff a token substitues an elided verb.

    """
    if token.dep_ == "ROOT" and token.pos_ not in ["VERB", "SPACE", "PUNCT", "AUX"]:
        return True
    return False


def is_generic_attr(token):
    """Checks a token is an expression of frequency.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token is an expression of frequency.

    """
    return token.pos_ != "ADJ" and (token.lemma_.lower() in FREQUENT_ADV or token.lemma_.lower() in TIME_ADV)


def is_generic_NP(token, before, after, check_gnomic):
    """Checks whether a token belongs to a generic noun phrase.

    Args:
        token (`Token`): The token.
        before (int): Number of coreferenced NPs preceding the token to check if the token is a pronoun.
        after (int): Number of coreferenced NPs succeeding the token to check if the token is a pronoun.
        check_gnomic (boolean): If True, plural NPs with determiners are only generic if they are in a present tense clause.
            If False, plural NPs with determiners are never generic.
    
    Returns:
        boolean: True iff the token belongs to a generic noun phrase.

    """
    if is_NP(token):
        if token.pos_ in ["NOUN", "PROPN"] and token.dep_.split(":")[0] in ["nsubj", "obj", "iobj", "nmod"]: # and token.ent_iob_ == "O":
            # plural NPs
            if token._.morph.numerus == "plu":
                for word in [token]+list(token.children):
                    if word.pos_ == "DET":
                        if not (check_gnomic and is_gnomic_present(token._.clause, context_window=0)):
                            return False
                    if word.pos_ == "NUM" or word.dep_.split(":")[0] == "nummod":
                        return False
                    if word.lemma_ in ["beid", "beide", "einzig"]:
                        return False
                return True
            # singular NPs
            elif "sing" in token._.morph.numerus_:
                pass # no rules for singular NPs yet
        elif token.pos_ == "PRON":
            # anaphoric pronouns
            for mention in get_nearby_coreferences(token, before, after):
                if type(mention) == Span:
                    mention = mention.root
                if mention.pos_ != "PRON" and is_generic_NP(mention, before, after, check_gnomic):
                    return True
    return False


def is_generic_VP(token, check_gnomic):
    """Checks whether a token belongs to a generic verb phrase.

    Args:
        token (`Token`): The token.
        check_gnomic (boolean): Iff True, a VP is generic as soon as it potentially expresses gnomic present tense.
    
    Returns:
        boolean: True iff the token belongs to a generic verb phrase.

    """
    if is_VP(token):
        # gnomic present tense, standing alone
        if check_gnomic and is_gnomic_present(token._.clause, context_window=1):
            return True
        for word in token._.clause._.tokens:
            # adv of frequency in VP
            if has_generic_attr(word):
                return True
            # subordinate clause with "wenn" and verb in the past tense
            if word.lower_ == "wenn" and is_past(token._.clause) and token._.clause._.form.mode == "ind":
                return True
        # part of the expression "zu tun pflegen"
        for word in token._.clause._.form.modals:
            if word.lemma_ == "pflegen":
                return True
    return False


def is_gnomic_present(clause, context_window=0):
    """Checks whether a clause potentially expresses gnomic present tense.

    Args:
        clause (`Span`): A clause.
        context_window (int): Number of clauses before and after the current clause that must not express present tense.
    
    Returns:
        boolean: True iff the clause is present and all clauses in the adjacent sentences are 1) not present, 2) finite, 3) direct speech iff the clause is direct speech.
    
    """
    if is_present(clause):
        if context_window > 0:

            clauses = clause.doc._.clauses
            index = clauses.index(clause)
            context_clauses = []
            for i in range(context_window):
                if index > i:
                    context_clauses.append(clauses[index-1-i])
                if index < len(clauses)-1-i:
                    context_clauses.append(clauses[index+1+i])

            for context_clause in context_clauses:
                if is_present(context_clause):
                    return False
                elif context_clause._.form.verb_form != "fin":
                    return False
                elif is_direct_speech(context_clause) != is_direct_speech(clause):
                    return False

        return True
    return False


def is_modal(token):
    """Checks whether a token contains a modal of oblgation outside of a direct speech.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff a token contains a modal of oblgation outside of a direct speech.

    """
    if token.lemma_ in ["sollen", "müssen"]:
        return True
    return False


def is_NE(span):
    """There are three types of entities:
        pronouns (PRON): a single word tagged as PRON
        named entities (NE): a span tagged as named entity or containing a proper noun
        normal noun chunk (NN): any other span, usually a noun chunk
    
    Args:
        span (`Span`): A span.
    
    Returns:
        boolean: True iff the span is a named entity.

    """
    ent_iob = get_attr(span, "ent_iob_")
    return "PROPN" in get_attr(span, "pos") or "B" in ent_iob or "I" in ent_iob


def is_negation(clause):
    """Checks for negations of verb.
        TODO: Check for negation of nomen, eg. "Moritz liest kein Buch, deshalb ist ihm langweilig."

    """
    if clause.root.pos_ == "VERB":
        if "nicht" in clause.root.children:
            return True


def is_NN(span):
    """There are three types of entities:
        pronouns (PRON): a single word tagged as PRON
        named entities (NE): a span tagged as named entity or containing a proper noun
        normal noun chunk (NN): any other span, usually a noun chunk
    
    Args:
        span (`Span`): A span.
    
    Returns:
        boolean: True iff the span is a normal noun chunk.

    """
    return not (is_PRON(span) or is_NE(span))


def is_NP(token):
    """Checks whether a token is a potential NP-head.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token is a potential NP-head.
    
    """
    return token.pos_ in ["NOUN", "PROPN", "PRON"]


def is_past(clause):
    """Chechs whether a clause expresses past tense.

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff the clause has past tense or present perfect tense.
    
    """
    if clause._.form.tense == "past":
        return True
    if clause._.form.tense == "pres":
        return clause._.form.aspect == "perf"
    return False


def is_person(token):
    """Chechs whether a token has an entity type 'PER' (person).

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token has an entity type 'PER' (person).
    
    """
    if hasattr(token._, "in_coref") and token._.in_coref:
        for cluster in token._.coref_clusters:
            for item in cluster:
                if item.root.ent_type_ == "PER":
                    return True
    return False


def is_present(clause):
    """Checks whether a clause expresses present tense.

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff the clause is present, imperfect and finite.
    
    """
    return clause._.form.tense == "pres" and clause._.form.aspect == "imperf" and clause._.form.verb_form == "fin"


def is_PRON(span):
    """There are three types of entities:
        pronouns (PRON): a single word tagged as PRON
        named entities (NE): a span tagged as named entity or containing a proper noun
        normal noun chunk (NN): any other span, usually a noun chunk
    
    Args:
        span (`Span`): A span.
    
    Returns:
        boolean: True iff the span is a pronoun.

    """
    pos = get_attr(span, "pos_")
    return len(pos) == 1 and "PRON" in pos


def is_question(clause):
    """Checks whether a clause is a question outside of a direct speech.

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff a clause is a question outside of a direct speech.
    
    """
    question_marks = list()
    for token in clause.sent:
        if token.text == "?" and "direct" not in token._.speech.keys():
            question_marks.append(token)
    if len(question_marks) > 0:
        return True
    return False


def is_space_ent(mention):
    """Checks whether a mention is a space entity.

    Args:
        mention (`Span`): The mention.
    
    Returns:
        boolean: True iff it is a space.
    
    """
    SPACE_NOUNS.provide()
    adjs, nouns, propns = get_head_nouns(mention, form="lemma_")
    mention_space_nouns = set(nouns).intersection(SPACE_NOUNS._)
    return len(mention_space_nouns) > 0


def is_specific(token, check_deixis, before, after):
    """Checks whether the restrictor is specific (i.e. non-generic).

    Args:
        token (`Token`): The highest token of a restrictor.
        check_deixis (boolean): If False, always returns False.
            Otherwise checks for deictic expressions to determine whether a restrictor is specific/generic
        before (int): Number of coreferences preceding the restrictor to check.
        after (int): Number of coreferences succeeding the restrictor to check.
    
    Returns:
        boolean: True iff the restrictor is specific.

    """
    if not check_deixis:
        return False
    return has_deictic_coreference(token, before, after)


def is_subjunctive(token):
    """Checks whether a token is in the subjunctive mood.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff a token is in the subjunctive mood.

    """
    if token._.morph.mode_ == {"subj"}:
        return True
    return False


def is_vague_attr(word):
    """Checks whether a token is the head of "in der Regel", "im Allgemeinen" or "im Normalfall".

    Args:
        word (`Token`): The token.
    
    Returns:
        boolean: True iff the subtree of the token contains one of the expressions.

    """
    # "in der Regel"
    if word.lemma_ == "Regel":
        for w in word.children:
            if w.lemma_ == "in" and w.nbor().text == "der":
                return True
    # "im Normalfall", "im Allgemeinen"
    if word.lemma_ == "Normalfall" or word.lemma_ == "Allgemeine":
        for w in word.children:
            if w.lemma_ == "im":
                return True
    return False


def is_vocative(clause):
    """Chechs whether a clause contains an exclamation or a question mark outside of a direct speech..

    Args:
        clause (`Span`): A clause.
    
    Returns:
        boolean: True iff a clause contains an exclamation or a question mark outside of a direct speech..
    
    """
    for token in clause.sent:
        if token.text == "!" and "direct" not in token._.speech.keys():
            return True
    return False  


def is_VP(token):
    """Checks whether a token is a potential VP-head.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the token is a potential VP-head.
    
    """
    return token.pos_ in ["VERB", "AUX"]


def lexical_narrative_break(token): 
    """Checks whether a token is a lexical indicator for a narrative break (Erzählpause).

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff a token is a lexical indicator for a narrative break (Erzählpause).

    """
    if GERMANET._ is not None:
        if token.dep_ == "ROOT" and token.pos_ == "VERB" and token._.synset_id is not None:
            if str(GERMANET._.get_synset_by_id(token._.synset_id).word_class) == "WordClass.Kognition":
                return True
    else:
        if token.lemma_ in ["wünschen", "planen", "möglich", "unmöglich"]: # any other? nouns?
            return True
    return False


def longest_common_prefix(str1, str2):
    """Return the longest common prefix of two strings.

    Args:
        str1 (str): A string.
        str2 (str): A string.
    
    Returns:
        str: Longest common prefix.
    
    """
    if str1.startswith(str2):
        return str2
    if str2.startswith(str1):
        return str1
    for i in range(min(len(str1),len(str2))):
        if str1[i] != str2[i]:
            return str1[:i]


def map_tokens_to_closest_clauses(tokens, sub_clauses, sub_clause_starts, sub_clause_ends):
    """Map a list of tokens (of an annotation) to clauses (of a clausized document).

    Args:
        tokens (list of `Token`): List of tokens.
        sub_clauses (list of (list of `Token`)): List of all sub-clauses of a document.
        sub_clause_starts (set of int): Set of sub-clause start indices.
        sub_clause_ends (set of int): Set of sub-clause end indices.
    
    Returns:
        list of `Span`: List of clauses approximately corresponding to the given tokens.
    
    """
    sub_spans = get_sub_spans(tokens)
    clauses = []
    for sub_span in sub_spans:

        if len(sub_span) == 0:
            continue

        # find the closest preceding start index of a sub-clause from the start index of the sub-span
        sub_span_start = sub_span[0].i
        left = min(sub_clause_starts)
        for i in range(sub_span_start, left, -1):
            if i in sub_clause_starts:
                left = i
                break
        
        # find the closest succeeding start index of a sub-clause from the start index of the sub-span
        right = max(sub_clause_starts)
        for i in range(sub_span_start, right):
            if i in sub_clause_starts:
                right = i
                break
        
        # select the closest start index of a sub-clause from the start index of the sub-span
        start = right if abs(sub_span_start-right) < abs(sub_span_start-left) else left
        
        # find the closest preceding end index of a sub-clause from the end index of the sub-span
        sub_span_end = sub_span[-1].i
        left = min(sub_clause_ends)
        for i in range(sub_span_end, left, -1):
            if i in sub_clause_ends:
                left = i
                break
        
        # find the closest succeeding end index of a sub-clause from the end index of the sub-span
        right = max(sub_clause_ends)
        for i in range(sub_span_end, right):
            if i in sub_clause_ends:
                right = i
                break
        
        # select the closest end index of a sub-clause from the end index of the sub-span
        end = left if abs(sub_span_end-left) < abs(sub_span_end-right) else right
        
        # assign all clauses to the sub-span which have a sub-clause within the approximate boundaries of the sub-span
        if start < end:
            for sub_clause in sub_clauses:
                if start <= sub_clause[0].i and sub_clause[-1].i <= end:
                    if sub_clause[0]._.clause not in clauses:
                        clauses.append(sub_clause[0]._.clause)

    return clauses


def most_frequent(l):
    """Return the most frequent elements in a list.

    Args:
        l (list of obj): A list.
    
    Returns:
        set of obj: The most frequent element(s) of the list in a set.
    
    """
    max_freq = -1
    most_freq = []
    for x in l:
        c = l.count(x)
        if c > max_freq:
            max_freq = c
            most_freq = [x]
        elif c == max_freq:
            most_freq.append(x)
    return set(most_freq)


def narrative_break(clause, outside_direct):
    """Checks whether a clause contains an indicator for a narrative break: 1. a token in the subjunctive mood, 2. a modal of oblgation, 3. token that substitutes an elided verb, 4. a lexical indicator

    Args:
        clause (`Span`): The clause.
        outside_direct (boolean): if True tokens in the clause must be outside of a direct speech
    
    Returns:
        boolean: True iff a clause contains an indicator for a narrative break (Erzählpause).

    """
    for token in clause._.tokens:
        if outside_direct:
            if "direct" not in token._.speech.keys() and (is_subjunctive(token) or is_modal(token) or is_ellipsis(token) or lexical_narrative_break(token)):
                return True
        if not outside_direct:
            if is_subjunctive(token) or is_modal(token) or is_ellipsis(token) or lexical_narrative_break(token):
                return True            
    #if has_temporal_change(clause, context_window=1):
    #    return True
    return False


def no_quantifiers_in_subtree(token):
    """Checks whether the subtree of a token contains a quantifier.

    Args:
        token (`Token`): The token.
    
    Returns:
        boolean: True iff the subtree of the token contains a quantifier.

    """
    for word in token.subtree:
        if word.lemma_.lower() in QUANTIFIERS:
            return False
    return True


def powerset(iterable):
    # src: https://docs.python.org/3/library/itertools.html#itertools-recipes
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


def self_reflexion_character(clause):
    """Checks self reflection of characters.
    TAG: Einstellung

    Args:
        clause (`clause`): The clause.
    
    Returns:
        boolean: True if a clause contains self reflexion.

    """
    if clause.root.pos_ != "VERB" and "direct" in clause._.speech.keys():
        for token in clause:
            if token.text in ["Ich", "ich"] and token.dep_ == "nsubj":
                for token in clause:
                    if token.pos_ == "AUX" and token.text == "bin":
                        return True


def self_reflexion_narrator(clause):
    """Checks self reflection of characters.
    TAG: Einstellung

    Args:
        clause (`clause`): The clause.
    
    Returns:
        boolean: True if a clause contains self reflexion.

    """
    if clause.root.pos_ != "VERB" and "direct" not in clause._.speech.keys():
        for token in clause:
            if token.text in ["Ich", "ich"] and token.dep_ == "nsubj":
                for token in clause:
                    if token.pos_ == "AUX" and token.text == "bin":
                        return True


def stringify(span, form="text", pos_=None):
    """Converts a span to a text.

    Args:
        span (`Span`): A span.
        form (str): The token attribute to use as string representation (e.g. "text" or "lemma_").
        pos_ (list of str): List of allowed POS tags. If None, all POS tags are allowed.

    Returns:
        str: The concatenated string representations of the tokens, separated by spaces.
    
    """
    pos = get_attr(span, "pos_")
    return " ".join([w.lower() for k, w in enumerate(get_attr(span, form)) if pos_ is None or pos[k] in pos_])


def subtree_to_span(subtree):
    """Convert a subtree to a span.
        The span covers all tokens from the first to the last in the subtree.

    Args:
        generator of `Token`: A subtree.
    
    Returns:
        `Span`: A span.
    
    """
    return tokens_to_span(list(subtree))


def tokens_to_span(tokens):
    """Convert list of tokens to a span.
        The span covers all tokens from the first to the last in the list.

    Args:
        list of `Token`: A list.
    
    Returns:
        `Span`: A span.
    
    """
    tokens = sorted(tokens, key=lambda token: token.i)
    return tokens[0].doc[tokens[0].i:tokens[-1].i+1]


def transfer_labels_to_passages(doc, attribute, labels):
    """Creates and adds `Passage` objects to the document and its clauses.
        If two or more tags are assigned to the same sequence of clauses, they are merged into one passage.

    Args:
        doc (`Doc`): The document.
        attribute (str): The custom attribute to add the passages.
        labels (list of (set of str)): A set of labels/tags for each clause in the document.
    
    """
    passages = [] # list of complete passages
    current_passages = {} # incomplete passages by tag
    
    # iterate over the labels for every clause;
    # the last iteration does not correspond to a clause
    # but is necessary to add passages that end with the last clause to `passages`
    for i, tags in enumerate(labels + [set()]):
        passages_to_end = {} # passages that end before this clause
        for tag in set([t for t in current_passages]).union(tags):
            if tag in current_passages and tag in tags:
                # add the current clause to a current passage
                current_passages[tag].append(i)
            elif tag in current_passages:
                # move a complete passage from `current_passages` to `passages_to_end`
                try:
                    passages_to_end[tuple(current_passages[tag])].add(tag)
                except KeyError:
                    passages_to_end[tuple(current_passages[tag])] = set([tag])
                del current_passages[tag]
            elif tag in tags:
                # create a new current passage
                current_passages[tag] = [i]
        # add the passages to end to `passages`
        for indices in passages_to_end:
            passages.append(Passage([doc._.clauses[index] for index in indices], passages_to_end[indices]))
    
    # assign passages to doc and clauses
    setattr(doc._, attribute, passages)
    for clause in doc._.clauses:
        setattr(clause._, attribute, [])
    for passage in passages:
        for clause in passage.clauses:
            getattr(clause._, attribute).append(passage)