"""This file contains useful classes for developing the SpaCy pipeline.
"""
import sys
import time

from .global_identifiers import FICTIONAL_ITEMS, REAL_ITEMS
from bs4 import BeautifulSoup
from qwikidata.entity import WikidataItem, WikidataLexeme, WikidataProperty
from qwikidata.json_dump import WikidataJsonDump
from qwikidata.linked_data_interface import get_entity_dict_from_api
from qwikidata.sparql import return_sparql_query_results
from qwikidata.utils import dump_entities_to_json
from urllib.request import urlopen

from settings import ROOT_PATH
sys.path.append(ROOT_PATH)
from corpusreader.corpus import Record
from corpusreader.classes.wikidata import Wikidata


class Debugger():
    """A simple class for debugging.
    
    """
    def __init__(self, debug=False):
        self.DEBUG = debug

    def print(self, *values):
        if self.DEBUG:
            print(*values)

    def input(self):
        if self.DEBUG:
            input()


class Form():
    """A class that can store surface-syntactical analysis information of a clause.
        To access an attribute of a clause, e.g. tense, use:
            clause._.form.tense

    """    
    def __init__(self):
        """__init__ method of the class `Form`.

        """
        self._feats = [
            "tense", # 'pres', 'past', 'fut'
            "aspect", # 'imperf', 'perf'
            "mode", # 'imp', 'ind', 'subj:pres', 'subj:past'
            "voice", # 'active', 'pass', 'pass:dynamic', 'pass:static'
            "verb_form", # 'fin', 'inf', 'part'
            "main", # (main verb `Token`)
            "modals", # (list of modal verbs `Token`)
            "verbs" # (list of verbs `Token`)
        ]
        
        # add features as attributes
        for feat in self._feats:
            setattr(self, feat, None)
        
        # change default value for verb lists from None to empty list
        self.main = None
        self.modals = []
        self.verbs = []

    
    def as_dict(self):
        """Convert the object's attributes to a dictionary.
        
        Returns:
            dict of str:(set of str): Dictionary with syntactical features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in self._feats}
        return as_dict


    def __str__(self):
        """__str__ method of the class `Form`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


class Morph():
    """A class that can store morphological analysis information of a token.
        To access an attribute of a token, e.g. tense, use:
            token._.morph.tense_
            token._.morph.tense
        The first expression returns all possible tenses of the token in a set (the set can be empty).
        The second expression resturns the tense of the token if it is existing and unambiguous; otherwise it returns None.

    """    
    def __init__(self, spacy_features={}, demorphy_features={}):
        """__init__ method of the class `Morph`.

        Args:
            spacy_features (dict of str:(set of str)): Morphological features of a spacy token.
                The features of a token can be accessed through TAG_MAP[token.tag_] (but have be converted to the desired format).
            demorphy_features (dict of str:(set of str)): Morphological features of a demorphy analysis.
                The features of a token can be accessed through Analyzer().analyze(token.text) (but have be converted to the desired format).
        
        """
        # (all) features from spacy and possible values:
        self._spacy_feats = [
            'AdpType', # 'circ', 'prep', 'post'
            'Aspect', # 'perf'
            'ConjType', # 'comp'
            'Foreign', # 'yes'
            'Hyph', # 'yes'
            'Mood', # 'ind', 'imp'
            'NumType', # 'card'
            'PartType', # 'inf', 'res', 'vbp'
            'Polarity', # 'neg'
            'Poss', # 'yes'
            'PronType', # 'dem', 'ind|neg|tot', 'prs', 'rel', 'int', 'art'
            'PunctType', # 'peri', 'comm', 'brck'
            'Reflex', # 'yes'
            'VerbForm', # 'fin', 'inf', 'part'
            'VerbType' # 'mod'
        ]

        # (some) features from demorphy and possible values:
        self._demorphy_feats = [
            "CASE", # "nom", "acc", "dat", "gen"
            "PERSON", # "1per", "2per", "3per"
            "NUMERUS", # "sing", plu
            "GENDER", # "masc", "fem", "neut", "noGender"
            "TENSE", # "pres", "ppres", "past", "ppast"
            "MODE", # "imp", "ind", "subj"
            "INFLECTION", # "inf", "zu"
            "DEGREE", # "pos", "comp", "sup"
            "STARKE", # "strong", "weak"
            "ADDITIONAL_ATTRIBUTES" # "<mod>", "<aux>", "<adv>", "<pred>", "<ans>", "<attr>", "<adj>", "<cmp>", "<coord>", "<def>", "<indef>", "<noinfl>", "<neg>", "personal", "<prfl>", "<rec>", "<pro>", "<refl>", "<subord>", "<subst>"
        ]

        # add both feature sets as attributes
        # (attribute names are lowercased and camelcases are changed to underscores):
        for feat in self._spacy_feats + self._demorphy_feats:
            feat = feat.replace("Type", "_Type").replace("Form", "_Form").lower()
            setattr(self, feat, None)
            setattr(self, feat + "_", set())
        
        # set the attributes' values:
        self._integrate_values(spacy_features)
        self._integrate_values(demorphy_features)

    
    def _integrate_values(self, dd):
        """Method to fill feature-values pairs into the object's attributes.

        Args:
            dd (dict of str:(set of str)): feature-values pairs
        
        """
        for key in dd.keys():
            val = dd[key]
            try:
                key = key.replace("Type", "_Type").replace("Form", "_Form").lower()
                if hasattr(self, key):
                    if len(val) == 1:
                        setattr(self, key, list(val)[0])
                    setattr(self, key + "_", val)
            except:
                pass


    def as_dict(self):
        """Convert the object's attributes to a dictionary.
            Only keep those attributes which have at least one value.
        
        Returns:
            dict of str:(set of str): Dictionary with morphological features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in as_dict if key.endswith("_") and len(as_dict[key]) > 0}
        return as_dict
    
    
    def __str__(self):
        """__str__ method of the class `Morph`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


class NonEmptyList(list):
    """A list that must not be empty.

    """
    def __init__(self, items):
        super(NonEmptyList, self).__init__(items)
        self.empty_check()
    def clear(self):
        super(NonEmptyList, self).clear()
        self.empty_check()
    def pop(self, index):
        res = super(NonEmptyList, self).pop(index)
        self.empty_check()
        return res
    def remove(self, item):
        super(NonEmptyList, self).remove(item)
        self.empty_check()
    def empty_check(self):
        if len(self) == 0:
            raise ValueError("List must not be empty")


class NonEmptySet(set):
    """A set that must not be empty.

    """
    def __init__(self, items):
        super(NonEmptySet, self).__init__(items)
        self.empty_check()
    def clear(self):
        super(NonEmptySet, self).clear()
        self.empty_check()
    def difference(self, set_):
        res = super(NonEmptySet, self).difference(set_)
        self.empty_check()
        return res
    def difference_update(self, set_):
        super(NonEmptySet, self).difference_update(set_)
        self.empty_check()
    def discard(self, item):
        super(NonEmptySet, self).discard(item)
        self.empty_check()
    def intersection(self, set_):
        res = super(NonEmptySet, self).intersection(set_)
        self.empty_check()
        return res
    def intersection_update(self, set_):
        super(NonEmptySet, self).intersection_update(set_)
        self.empty_check()
    def pop(self):
        res =  super(NonEmptySet, self).pop()
        self.empty_check()
        return res
    def remove(self, item):
        super(NonEmptySet, self).remove(item)
        self.empty_check()
    def symmetric_difference(self, set_):
        res = super(NonEmptySet, self).symmetric_difference(set_)
        self.empty_check()
        return res
    def symmetric_difference_update(self, set_):
        super(NonEmptySet, self).symmetric_difference_update(set_)
        self.empty_check()
    def empty_check(self):
        if len(self) == 0:
            raise ValueError("Set must not be empty")


class EntitySet(set):
    """A class that stores a set of `Entity`-objectes.
    """   
    def __init__(self, entity_set=set(), queried_strings=None):
        """__init__ method of the class `EntitySet`.

        Args:
            entity_set (set of Entity-objectes): a set of `Entity`-objectes.
        """
        super(EntitySet, self).__init__(entity_set)

        self.queried_strings = queried_strings

    def find_highest_scoring_entity(self):
        """Method that returns highest scoring Entity-object within a EntitySet-object.
        For the highest scoring Entity-object attributes `fictional` and `real` are set with boolean values.

        Returns:
            `Entity`: highest scoring Entity-object.
        """

        highest_fictional = 0
        highest_fictional_position = 0
        highest_real = 0
        highest_real_position = 0
        entities_list = list(self)
        for i, entity in enumerate(entities_list):
            if entity.fictional_score > highest_fictional:
                highest_fictional = entity.fictional_score
                highest_fictional_position = i
            if entity.real_score > highest_real:
                highest_real = entity.real_score
                highest_real_position = i
                        
        if highest_fictional > highest_real:
            prefered_entity = entities_list[highest_fictional_position]
            prefered_entity.real = False
            prefered_entity.fictional = True
        elif highest_fictional < highest_real:
            prefered_entity = entities_list[highest_real_position]
            prefered_entity.fictional = False
            prefered_entity.real = True    
        elif highest_fictional == highest_real:
            prefered_entity = entities_list[highest_fictional_position]
            prefered_entity.real = False
            prefered_entity.fictional = True

        return prefered_entity
        
class Entity():
    """A class that stores information about an entity, e.g. from wikidata.
    """

    def __init__(self, queried_strings: set, record: Record, ent_type: str, wikidata_id: str = None, wikidata_object = None):
        """__init__ method of the class `Entity`. 
        The objected is intended to initialised with a `wikidata_id`.
        Then, requests are performed that yield metadata from wikidata, which are stored in a 
        Wikidata-object (contains qwikidata_dict and qwikidataitem/qwikidataproperty). If the Wikidata-object
        is already existent in can be passed via `wikidata_object` (than no data from wikidata need to be requested).
        If no parameter for `wikidata_id` or `wikidata_object` is passed only basic metadata will be captured in the
        Entity-object.

        Args:
            queried_strings (str): set of strings for which data has been (to generate Wikidata-object). Also used 
                                    for further requests in this method.
            record (`Record`): Record-object with metadata for the current doc.
            ent_type (str): Type of current ent (PER or LOC).
            wikidata_id (str): wikidata_id for Wikidata-object that corresponds to this Entity-object.
            wikidata_object (`Wikidata`):  Wikidata-object that corresponds to this Entity-object.
        """
        # attributes for fictionality status
        self.fictional = None
        self.real = None
        self.fictional_score = 0
        self.real_score = 0

        # set ent_type
        self.ent_type = ent_type

        # add wikidata object including qwikidata dict and classes
        if wikidata_object:
            self.wikidata_id = wikidata_object.wikidata_id
            self.wikidata = wikidata_object
        elif wikidata_id:
            self.wikidata_id = wikidata_id
            self.wikidata = Wikidata(wikidata_id=wikidata_id)
        else:
            self.wikidata = None

        self.queried_strings = queried_strings

        # wikipedia page in de/en:
        self.wikipedia_work = None
        if record and record.wikidata_id_work:
            self.wikipedia_work = dict()
            for language in ["de", "en"]:
                qwikidata_dict_work = Wikidata.wikidata_request_time_handling(
                    get_entity_dict_from_api,
                    entity_id=record.wikidata_id_work
                )
                self.wikipedia_work[language] = Entity.scrape_wikipedia(url=qwikidata_dict_work["sitelinks"]["%swiki"%language]["url"])

        self.query_in_work = None
        # check if wikipedia_work and queried_strings in not None
        if self.wikipedia_work and queried_strings:
            for query in self.queried_strings:
                if query in self.wikipedia_work["de"] or query in self.wikipedia_work["en"]:
                    self.query_in_work = True

        self.fictional_item_ids = None
        self.real_item_ids = None
        self.instance_of_human = None
        if self.wikidata and self.ent_type == "PER":
            ### attributes with fictional status
            ####################################

            # set of instance_of relations to FICTIONAL_ITEMS 
            self.fictional_item_ids = set()
            for key in FICTIONAL_ITEMS:
                if self.check_p_o(wikidata_id_property="P31", wikidata_id_object=FICTIONAL_ITEMS[key]):
                    self.fictional_item_ids.add(FICTIONAL_ITEMS[key])

            # set of instance_of relations to REAL_ITEMS 
            self.real_item_ids = set()
            for key in REAL_ITEMS:
                if self.check_p_o(wikidata_id_property="P31", wikidata_id_object=REAL_ITEMS[key]):
                    self.real_item_ids.add(REAL_ITEMS[key])

            ### attributes with reality status
            ##################################
            self.instance_of_human = self.check_p_o(wikidata_id_property="P31", wikidata_id_object="Q5")


    def scrape_wikipedia(url: str):
        """Method to scrape wikipedia for a given url.

        Args:
            url (str): url of wikipedia article.
        Returns:
            str: text from wikipedia article including headers and footers.
        """

        # Specify url of the web page
        source = urlopen(url).read()
        # Make a soup 
        soup = BeautifulSoup(source,'lxml')
        text = ''
        for paragraph in soup.find_all('p'):
            text += paragraph.text

        return text

    def check_p_o(self, wikidata_id_property: str, wikidata_id_object: str) -> bool:
        """check if a certain relation s-(self)-p-o exists

        Args: 
            wikidata_id_property (str): wikidata_id for the property (predicate) to search for, e.g. "P31" for "instance_of" relations.
            wikidata_id_object (str): wikidata_id for the object to search.py
        
        Returns:
            bool: True if relation s-(self)-p-o exits.

        """
        if self.wikidata.qwikidata_dict["claims"][wikidata_id_property] and (len(self.wikidata.qwikidata_dict["claims"][wikidata_id_property]) != 0):
            for dic in self.wikidata.qwikidata_dict["claims"][wikidata_id_property]:
                if dic["mainsnak"]["datavalue"]["value"]["id"] == wikidata_id_object:
                    return True
        return False

    def is_instance_of_human(item: WikidataItem, truthy: bool = True) -> bool:
        """Returns True if the Wikidata Item is instance of human.
        
        Args:
            item (`WikidataItem`): Wikidata item to search for.
            truthy (bool): truthy status.

        Returns:
            bool: True if the Wikidata Item is instance of human.
        
        """
        human = "Q5"
        instance_of = "P31"

        if truthy:
            claim_group = item.get_truthy_claim_group(instance_of)
        else:
            claim_group = item.get_claim_group(instance_of)

        occupation_qids = [
            claim.mainsnak.datavalue.value["id"]
            for claim in claim_group
            if claim.mainsnak.snaktype == "value"
        ]
        return human in occupation_qids
