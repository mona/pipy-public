import csv
import nltk
import os
import sys
import numpy as np
import warnings
from nltk.metrics import agreement
from nltk.metrics import masi_distance
from nltk.metrics.agreement import AnnotationTask
from statsmodels.stats.inter_rater import fleiss_kappa

try:
    from pygamma_agreement import (Continuum, CombinedCategoricalDissimilarity)
    from pyannote.core import Segment
except ImportError:
    warnings.warn("ImportError: pygamma_agreement")


def get_property_value_combinations(annotation_elements, searched_tag, tagset, annotators, property):
    """TODO

    Args:
        TODO
    
    Returns:
        list of set: property value combinations
    
    """
    property_value_combinations = list()

    for element in annotation_elements:
        for annotator_key in annotators:
            if annotator_key in element._.annotations.keys():
                annotation_generator_list = list(element._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset))
                if len(annotation_generator_list) > 0:
                    try:
                        property_value_combination = set(annotation_generator_list[0].property_values[property])
                        if property_value_combination not in property_value_combinations:
                            property_value_combinations.append(property_value_combination)
                    except KeyError:
                        pass

    return property_value_combinations


def get_tag_combinations(annotation_elements, searched_tags, tagset, annotators):
    """TODO
    
    Args:
        TODO
    
    Returns:
        list of set: tag combinations
    
    """
    tag_combinations = list()

    for element in annotation_elements:
        for annotator_key in annotators:
            if annotator_key in element._.annotations.keys():
                tag_combination_of_searched_tags = set()
                tags_for_element = element._.annotations[annotator_key].get_tags(tagset=tagset)
                for tag in tags_for_element:
                    if tag in searched_tags:
                        tag_combination_of_searched_tags.add(tag)
                if tag_combination_of_searched_tags not in tag_combinations and len(tag_combination_of_searched_tags) > 0:
                    tag_combinations.append(tag_combination_of_searched_tags)

    return tag_combinations


# `masi_distance` wrapper that also works if both sets are empty.
def masi_distance_with_zero_division(label1, label2):
    try:
        return masi_distance(label1, label2)
    except ZeroDivisionError:
        return 0.0


def return_krippendorff_alpha(annotation_elements, searched_tags, tagset, annotators, property=None, searched_property_value=None, agreement_for_properties="all_tokens"):
    """Calculates Krippendorff's Alpha.

    Args:
        annotation_elements (obj): Can be either (i) list of clauses, or (ii) list of tokens / doc.
        searched_tags (list of str): Tags for which Alpha should be calculated.
        tagset (str): Tagset name.
        annotators (list): List of annotators that should be considered for Alpha.
        property (str): If not None, calculate Alpha on a property.
        searched_property_value (str): Property value for which Alpha should be calculated.
            If None, Alpha is calculated on all property values.
        agreement_for_properties (str): Subselect annotation elements.
            `all_tokens`: Calculate Alpha on all annotation elements. Annotation elements without one of the specified tags are treated as having no property values.
            `tokens_with_tag`: Calculate Alpha only on annotation elements that are annotated with one of the specified tags.
    
    Returns:
        float: Krippendorff's Alpha value.
    
    """
    task_data = []
    number_of_labels = 0
    for i, element in enumerate(annotation_elements):
        all_tags = [tag for annotator_key in annotators if annotator_key in element._.annotations for tag in element._.annotations[annotator_key].get_tags(tags=searched_tags, tagset=tagset)]
        if agreement_for_properties == "all_tokens" or (agreement_for_properties == "tokens_with_tag" and len(all_tags) > 0):
            for annotator_key in annotators:
                if annotator_key in element._.annotations:
                    if property is None:
                        tags = element._.annotations[annotator_key].get_tags(tags=searched_tags, tagset=tagset)
                        task_data.append((annotator_key,str(i),frozenset(tags)))
                        number_of_labels += len(tags)
                    else:
                        annotations = element._.annotations[annotator_key].get_annotations(tags=searched_tags, tagset=tagset)
                        properties = set([property_value for annotation in annotations if property in annotation.property_values for property_value in annotation.property_values[property]])
                        if searched_property_value is not None:
                            properties.intersection_update(set([searched_property_value]))
                        task_data.append((annotator_key,str(i),frozenset(properties)))
                        number_of_labels += len(properties)
    if number_of_labels == 0:
        return np.nan
    task = AnnotationTask(distance=masi_distance_with_zero_division)
    task.load_array(task_data)
    try:
        return task.alpha()
    except ValueError:
        return np.nan


def return_fleiss_kappa(annotation_elements, searched_tags, tagset, annotators, property=None, searched_property_value=None, agreement_for_properties="all_tokens"):
    """Calculates Fleiss Kappa for:
        - a single tag
        - a single property value (property != None, searched_property_value != None)
        - a set of property values from a single tag (property != None, searched_property_value == None)
        - mulitple tags

    Args:
        annotation_elements (obj): Can be either (i) list of clauses, or (ii) list of tokens / doc.
        searched_tags (list): tag (list with one element) or tags (list) for which Kappa should be calculated
        tagset (str): tagset name
        annotators (list): List of annotators that should be considered for Kappa
        property (str): restricts Kappa to a property, 
                        if None: Kappa will be calculated only for the single tag
        searched_property_value (str): specify the property value for which Kappa should be calculated,
                                    if property is selected and property value is None: Kappa will be calculated
                                    as multicatogorial Kappa for all properties
        agreement_for_properties (str): TODO
    
    Returns:
        str: Fleiss Kappa value
    
    """
    anno_matrix = list()

    # collect tag combinations for multi-tag kappa
    if len(searched_tags) > 1:
        tag_combinations = get_tag_combinations(annotation_elements, searched_tags, tagset, annotators)
        # append empty set as class for no annotation in anno matrix
        tag_combinations.append(set())  

    # select first element of searched_tags if single tag is searched:
    if len(searched_tags) == 1:
        searched_tag = searched_tags[0]

    # calculate property value combinations of a single tag for multi-property kappa
    if len(searched_tags) == 1 and property != None and searched_property_value == None:
        property_value_combinations = get_property_value_combinations(annotation_elements=annotation_elements, searched_tag=searched_tag, tagset=tagset, annotators=annotators, property=property)
        # append empty set as class for no annotation in anno matrix
        property_value_combinations.append(set())

    # build anno matrix for kappa:
    for element in annotation_elements:

        # single tag
        if len(searched_tags) == 1 and property == None:
            anno_vector = [0, 0]
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    if searched_tag in element._.annotations[annotator_key].get_tags(tagset=tagset):
                        anno_vector[0] += 1

            anno_vector[-1] = len(annotators) - anno_vector[0]
            anno_matrix.append(anno_vector)
    
        # single tag with single property
        # a) consider not annotated tokens
        if len(searched_tags) == 1 and property != None and searched_property_value != None and agreement_for_properties == "all_tokens":
            anno_vector = [0, 0]
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    annotation_generator_list = list(element._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset))
                    if len(annotation_generator_list) > 0:
                        if property in annotation_generator_list[0].property_values and searched_property_value in annotation_generator_list[0].property_values[property]:
                            anno_vector[0] += 1                                
            anno_vector[-1] = len(annotators) - anno_vector[0]
            anno_matrix.append(anno_vector)

        # b) don't consider not annotated tokens; only tokens of single tag (eg. attribution)
        if len(searched_tags) == 1 and property != None and searched_property_value != None and agreement_for_properties == "tokens_with_tag":
            anno_vector = [0, 0]
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    # only process further if searched tag is annotated on element (token/clause)
                    if searched_tag in element._.annotations[annotator_key].get_tags():
                        annotation_generator_list = list(element._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset))
                        a = False
                        b = False
                        if property in annotation_generator_list[0].property_values and searched_property_value in annotation_generator_list[0].property_values[property]:
                            anno_vector[0] += 1
                        else:
                            anno_vector[-1] += 1
                        
            if anno_vector != [0, 0]:
                ## Check if annotated instances corresponds with number of annotators
                if sum(anno_vector) == len(annotators):
                    anno_matrix.append(anno_vector)
                ## DEBUGGING
                # if sum(anno_vector) != len(annotators):
                #     print(anno_vector)
                #     print(element)
        
        # single tag with multiple properties (properties combined to property-set)
        # a) consider not annotated tokens
        if len(searched_tags) == 1 and property != None and searched_property_value == None and agreement_for_properties == "all_tokens":

            anno_vector = [0] * len(property_value_combinations)
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    Annotation_generator_list = list(element._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset))
                    if len(Annotation_generator_list) == 0:
                        anno_vector[-1] += 1

                    if len(Annotation_generator_list) > 0:
                        for property_value_combination_i, property_value_combination in enumerate(property_value_combinations):
                            if set(Annotation_generator_list[0].property_values[property]) == property_value_combination:
                                anno_vector[property_value_combination_i] += 1
            anno_matrix.append(anno_vector)

        # b) don't consider not annotated tokens; only tokens of single tag (eg. attribution)
        if len(searched_tags) == 1 and property != None and searched_property_value == None and agreement_for_properties == "tokens_with_tag":

            anno_vector = [0] * len(property_value_combinations)
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    # only process further if searched tag is annotated on element (token/clause)
                    if searched_tag in element._.annotations[annotator_key].get_tags():
                        annotation_generator_list = list(element._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset))

                        anno_ok = False
                        for property_value_combination_i, property_value_combination in enumerate(property_value_combinations):
                            if property in annotation_generator_list[0].property_values and set(annotation_generator_list[0].property_values[property]) == property_value_combination:
                                anno_vector[property_value_combination_i] += 1
                                anno_ok = True
                        # if anno_ok is not set to annotator_key, the annotator has made no annotation (== property_value_combination):
                        if not anno_ok:
                            anno_vector[-1] += 1

            if anno_vector != [0] * len(property_value_combinations):
                if sum(anno_vector) == len(annotators):
                    #print(anno_vector)
                    #input()
                    anno_matrix.append(anno_vector)

        # multi-tag
        if len(searched_tags) > 1:
            anno_vector = [0] * len(tag_combinations)
            for annotator_key in annotators:
                if annotator_key in element._.annotations.keys():
                    tags_of_element = element._.annotations[annotator_key].get_tags(tags=searched_tags, tagset=tagset)
                    if len(tags_of_element) == 0:
                        anno_vector[-1] += 1
                    if len(tags_of_element) > 0:
                        for tag_combination_i, tag_combination in enumerate(tag_combinations):
                            if tags_of_element == tag_combination:
                                anno_vector[tag_combination_i] += 1

            anno_matrix.append(anno_vector)

    if len(anno_matrix) == 0:
        return np.nan
    return fleiss_kappa(anno_matrix)


def anno_to_gamma_csv(doc, searched_tags, tagset, annotators, property=None, searched_property_value=None):
    """Writes annotation to csv for Gamma tool

    Args:
        clauses: can be either spacy-token or clauses
        tag (str): Tag to search for
        tagset (str): tagset name
        annotators (list): List of annotators that should be considered for Kappa
        property (str): TODO
        searched_property_value (str): TODO
    
    """
    #filename for single tag with mulit-property Kappa
    if property != None and searched_property_value==None:
        filename = "gamma_csv_files/gamma_%s_%s_%s.csv"%(searched_tags, "property_combinations", annotators)
    #filename for single-tag Kappa and single-tag+single-property Kappa:
    else:
        filename = "gamma_csv_files/gamma_%s_%s_%s.csv"%(searched_tags, searched_property_value, annotators)

    # select first element of searched_tags if single tag is searched:
    if len(searched_tags) == 1:
        searched_tag = searched_tags[0]

    with open(filename, mode="w") as csvfile:
        annowriter = csv.writer(csvfile, delimiter=',', quotechar='"')
        unit_id = 1
        
        # single tag
        if len(searched_tags) == 1 and property==None:
            for annotator_key in annotators:
                if annotator_key in doc._.annotations.keys():
                    for annotation in doc._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset):
                        if annotation.tag == searched_tag:
                            annowriter.writerow(["u%s" %unit_id, annotator_key, "%s"%annotation.tag, "", annotation.tokens[0].i, annotation.tokens[-1].i])
                            unit_id += 1

        # multi tag
        if len(searched_tags) > 1:
            for annotator_key in annotators:
                if annotator_key in doc._.annotations.keys():
                    for annotation in doc._.annotations[annotator_key].get_annotations(tags=searched_tags, tagset=tagset):
                        if annotation.tag in searched_tags:
                            annowriter.writerow(["u%s" %unit_id, annotator_key, "%s"%annotation.tag, "", annotation.tokens[0].i, annotation.tokens[-1].i])
                            unit_id += 1

        # single tag with single property
        if len(searched_tags) == 1 and property != None and searched_property_value != None:
            
            for annotator_key in annotators:
                if annotator_key in doc._.annotations.keys():
                    for annotation in doc._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset):
                        if property in annotation.property_values and searched_property_value in annotation.property_values[property]:
                            annowriter.writerow(["u%s" %unit_id, annotator_key, searched_property_value, "", annotation.tokens[0].i, annotation.tokens[-1].i])
                            unit_id += 1

        # single tag with multi properties (properties not combined)
        if len(searched_tags) == 1 and property != None and searched_property_value == None:
            for annotator_key in annotators:
                if annotator_key in doc._.annotations.keys():
                    for annotation in doc._.annotations[annotator_key].get_annotations(tags={searched_tag}, tagset=tagset):
                        for property in annotation.property_values and property_value in annotation.property_values[property]:
                            annowriter.writerow(["u%s" %unit_id, annotator_key, property_value, "", annotation.tokens[0].i, annotation.tokens[-1].i])
                            unit_id += 1


def return_mathet_gamma(annotation_elements, searched_tags, tagset, annotators, property=None, searched_property_value=None, agreement_for_properties="all_tokens", max_annotators=2, units="tokens", use_clauses_as_segments=False):
    """Calculates Gamma (only on clause level)

    Args:
        annotation_elements (obj): Can be either (i) list of clauses, or (ii) list of tokens / doc.
        searched_tags (list): Tags to search for
        tagset (str): tagset name
        annotators (list): List of annotators that should be considered for Kappa
        units (str): The atomic units on which segments should be computed ("characters", "tokens" or "clauses").
        use_clauses_as_segments (boolean): If True, clause spans are used as segments; if False, complete annotation spans are used as segments.
    
    Returns:
        float: Gamma value
    
    """
    if len(annotators) > max_annotators:
        warnings.warn("Gamma calculation not supported for more than " + str(max_annotators) + " annotators.")
        return np.nan
    if not (property is None and searched_property_value is None and agreement_for_properties == "all_tokens"):
        warnings.warn("Gamma calculation not supported for the specified parameter combination.")
        return np.nan

    sys.stderr = open(os.devnull, "w")

    try:
        annotations = {annotator : set() for annotator in annotators}
        for element in annotation_elements:
            for annotator in annotators:
                if annotator in element._.annotations:
                    annotations[annotator].update(element._.annotations[annotator].get_annotations(tags=searched_tags, tagset=tagset))
        doc_clauses = annotation_elements[0].doc._.clauses
        continuum = Continuum()
        if use_clauses_as_segments:
            clauses = {annotator : set([clause for annotation in annotations[annotator] for clause in annotation.clauses]) for annotator in annotations}
            for annotator in clauses:
                for clause in clauses[annotator]:
                    start, end = None, None
                    if units == "characters":
                        start, end = clause[0].idx, clause[-1].idx+len(clause[-1])
                    elif units == "tokens":
                        start, end = clause.start, clause.end
                    elif units == "clauses":
                        start, end = doc_clauses.index(clause), doc_clauses.index(clause)+1
                    tags = clause._.annotations[annotator].get_tags(tags=searched_tags, tagset=tagset)
                    for tag in tags:
                        continuum.add(annotator, Segment(float(start), float(end)), tag)
        else:
            for annotator in annotators:
                for annotation in annotations[annotator]:
                    start, end = None, None
                    if units == "characters":
                        start, end = annotation.tokens[0].idx, annotation.tokens[-1].idx+len(annotation.tokens[-1])
                    elif units == "tokens":
                        start, end = annotation.tokens[0].i, annotation.tokens[-1].i+1
                    elif units == "clauses":
                        start, end = doc_clauses.index(annotation.clauses[0]), doc_clauses.index(annotation.clauses[-1])+1
                    continuum.add(annotator, Segment(float(start), float(end)), annotation.tag)
        
        if len(continuum.annotators) < 2:
            result = np.nan
        else:
            try:
                dissimilarity = CombinedCategoricalDissimilarity(continuum.categories, alpha=1, beta=1)
            except TypeError:
                dissimilarity = CombinedCategoricalDissimilarity(alpha=1, beta=1)
            gamma_result = continuum.compute_gamma(dissimilarity)
            
            result = gamma_result.gamma

    except:
        # many things can go wrong with gamma computation ...
        result = np.nan
    
    sys.stderr = sys.__stderr__

    return result