import os
import spacy
import sys

# import pipeline components
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.tense_tagger import rb_tense_tagger
from settings import PARSING_PATH

# build the pipeline
nlp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
nlp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
nlp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
nlp.add_pipe(demorphy_analyzer, name="analyzer")
nlp.add_pipe(dependency_clausizer, name="clausizer")
nlp.add_pipe(rb_tense_tagger, name="tense_tagger")
print(nlp.pipe_names)

text = """Johann Wolfgang Goethe
Die Wahlverwandtschaften
Ein Roman
Erster Teil
Erstes Kapitel
Erstes Kapitel
Eduard – so nennen wir einen reichen Baron im besten Mannesalter – Eduard hatte in seiner Baumschule die schönste Stunde eines Aprilnachmittags zugebracht, um frisch erhaltene Pfropfreiser auf junge Stämme zu bringen. Sein Geschäft war eben vollendet; er legte die Gerätschaften in das Futteral zusammen und betrachtete seine Arbeit mit Vergnügen, als der Gärtner hinzutrat und sich an dem teilnehmenden Fleiße des Herrn ergetzte.
"""

# parse the text
doc = nlp(text)

# iterate over sentences, clauses and tokens
for j, sent in enumerate(doc.sents):
    for k, clause in enumerate(sent._.clauses):
        for i, token in enumerate(clause._.tokens):
            print(j, k, i, token.text, token.lemma_, token.pos_, token.dep_, token._.clause._.form.tense)