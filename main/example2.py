import os
import spacy
import sys

# import pipeline components
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.attribution_tagger import neural_attribution_tagger
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.comment_tagger import clf_dfg_comment_tagger
from pipeline.components.coref import rb_coref
from pipeline.components.discourse_segmenter import disrpt_discourse_segmenter
from pipeline.components.emotions_tagger import nrc_emotions
from pipeline.components.event_tagger import event_event_tagger
from pipeline.components.feature_extractor import dfg_feature_extractor
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.reflection_tagger import neural_reflection_tagger
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.speaker_extractor import rb_speaker_extractor
from pipeline.components.speech_tagger import quotation_marks_speech_tagger
from pipeline.components.temponym_tagger import heideltime_temponym_tagger
from pipeline.components.tense_tagger import rb_tense_tagger
from settings import PARSING_PATH

# build the pipeline
nlp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
nlp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
nlp.add_pipe(dictionary_normalizer, name="normalizer", before="tagger")
nlp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
nlp.add_pipe(demorphy_analyzer, name="analyzer")
nlp.add_pipe(dependency_clausizer, name="clausizer")
nlp.add_pipe(rb_tense_tagger, name="tense_tagger")
nlp.add_pipe(event_event_tagger, name="event_tagger")
nlp.add_pipe(quotation_marks_speech_tagger, name="speech_tagger")
nlp.add_pipe(rb_speaker_extractor, name="speaker_extractor")
nlp.add_pipe(rb_coref, name="coref")
nlp.add_pipe(heideltime_temponym_tagger, name="temponym_tagger")
nlp.add_pipe(nrc_emotions, name="emotions_tagger")
nlp.add_pipe(dfg_feature_extractor, name="feature_extractor")
nlp.add_pipe(disrpt_discourse_segmenter, name="discourse_segmenter")
nlp.add_pipe(clf_dfg_comment_tagger, name="comment_tagger")
nlp.add_pipe(neural_reflection_tagger, name="reflection_tagger")
nlp.add_pipe(neural_attribution_tagger, name="attribution_tagger")
print(nlp.pipe_names)

text = """Johann Wolfgang Goethe
Die Wahlverwandtschaften
Ein Roman
Erster Teil
Erstes Kapitel
Erstes Kapitel
Eduard – so nennen wir einen reichen Baron im besten Mannesalter – Eduard hatte in seiner Baumschule die schönste Stunde eines Aprilnachmittags zugebracht, um frisch erhaltene Pfropfreiser auf junge Stämme zu bringen. Sein Geschäft war eben vollendet; er legte die Gerätschaften in das Futteral zusammen und betrachtete seine Arbeit mit Vergnügen, als der Gärtner hinzutrat und sich an dem teilnehmenden Fleiße des Herrn ergetzte.
»Hast du meine Frau nicht gesehen?« fragte Eduard, indem er sich weiterzugehen anschickte.
"""

# parse the text
doc = nlp(text)

# iterate over sentences and tokens
column_format = "{0:<8}{1:<8}{2:<8}{3:<24}{4:<24}{5:<24}{6:<8}{7:<16}{8:<8}{9:<8}{10:<16}{11:<8}{12:<8}{13:<8}{14:<16}{15:<16}{16:<16}{17:<16}{18:<16}{19:<24}"
head_row = ["SENT", "TOKEN", "CLAUSE", "FORM", "NORM", "LEMMA", "POS", "DEP", "CASE", "TENSE", "EVENT", "SPEECH", "SPEAKER", "COREF", "TEMPONYM", "EMOTION", "DISCOURSE", "COMMENT", "REFLECTION", "ATTRIBUTION"]
print(column_format.format(*head_row))
for j, sent in enumerate(doc.sents):
    for i, token in enumerate(sent):
        k = None
        if token._.clause is not None:
            k = sent._.clauses.index(token._.clause)
        
        # "SENT", "TOKEN", "CLAUSE", "FORM", "NORM", "LEMMA", "POS", "DEP", "CASE"
        row = [j, i, k, token._.text, token.text, token.lemma_, token.pos_, token.dep_, token._.morph.case]

        # "TENSE", "EVENT"
        if token._.clause is not None:
            row.extend([token._.clause._.form.tense, token._.clause._.event["event_types"]])
        else:
            row.extend([None, None])
        
        # "SPEECH"
        row.extend(["|".join(sorted(token._.speech.keys()))])

        # "SPEAKER"
        if (token._.speech_segment_ is not None) and (token._.speech_segment_._.speaker_ is not None):
            row.extend([token._.speech_segment_._.speaker_.text])
        else:
            row.extend([None])
        
        # "COREF"
        if token._.in_coref:
            row.extend([token._.coref_clusters[0].i])
        else:
            row.extend([None])
        
        # "TEMPONYM"
        if token._.in_temponym:
            row.extend([token._.temponyms[0]._.temponym_norm["NORM_VALUE"]])
        else:
            row.extend([None])
        
        # "EMOTION"
        row.extend(["|".join(sorted([emotion for emotion in token._.emotions if token._.emotions[emotion] == "1" and emotion not in ["Positive", "Negative"]]))])
        
        # "DISCOURSE", "COMMENT", "REFLECTION", "ATTRIBUTION"
        if token._.clause is not None:
            row.extend([
                token._.clause._.seg_start and token._.clause._.tokens[0] == token, 
                "|".join(sorted(list(set([tag for passage in token._.clause._.comments for tag in passage.tags])))), 
                "|".join(sorted(list(set([tag for passage in token._.clause._.rps for tag in passage.tags])))), 
                "|".join(sorted(list(token._.clause._.attribution)))
            ])
        else:
            row.extend([None, None, None, None])

        row = [("_" if x in [None, ""] else (x.strip() if type(x) == str else x)) for x in row]
        print(column_format.format(*row))