## install requirements
echo "pip install requirements ..."
cat requirements.txt | awk '{print "pip install "$1}' > requirements.sh
chmod u+x requirements.sh
./requirements.sh

## donwload spaCy model data
echo "download spaCy model data ..."
python3 -m spacy download de_core_news_lg

## clone and install DEMorphy
echo "clone and install DEMorphy ..."
git clone https://github.com/DuyguA/DEMorphy
cd DEMorphy || exit
cp ../resources/demorphy/words.dg demorphy/data/
python3 setup.py install
PYSYSVERSION=$(python3 -c "import sys; print(str(sys.version_info.major)+'.'+str(sys.version_info.minor))")
cp demorphy/data/words.dg $(python3 -c "import sys; p=sys.executable; print(p[0:p.rfind('/bin')])")"/lib/python""$PYSYSVERSION""/site-packages/demorphy-1.0-py""$PYSYSVERSION"".egg/demorphy/data/"
cd ..
rm -r -f DEMorphy

## clone and install neuralcoref
echo "clone and install neuralcoref ..."
git clone https://github.com/huggingface/neuralcoref.git --branch v4.0.0
cd neuralcoref || exit
python3 setup.py install
cd ..
rm -r -f neuralcoref

## fetch NLTK data
echo "fetch NLTK data ..."
if [ ! -d nltk_data ]; then mkdir nltk_data; fi
python3 -c "import nltk; nltk.download('wordnet', download_dir='nltk_data'); nltk.download('omw', download_dir='nltk_data')"
if [ ! -d nltk_data/corpora/omw ]; then mkdir nltk_data/corpora/omw; fi
if [ ! -d nltk_data/corpora/omw/deu ]; then mkdir nltk_data/corpora/omw/deu; fi
mkdir tmp_extract_folder
tar -C tmp_extract_folder -xf "resources/open_multilingual_wordnet/wn-wikt.tgz"
cp tmp_extract_folder/data/wikt/wn-wikt-deu.tab nltk_data/corpora/omw/deu/wn-data-deu.tab
rm -r -f tmp_extract_folder

## installing Gitma
echo "pip install Gitma ..."
pip install git+https://github.com/forTEXT/catma_gitlab@1.0.0
